// $Id: IRichTestTool.h,v 1.1 2007-07-05 13:40:59 buszello Exp $
#ifndef IRICHTESTTOOL_H 
#define IRICHTESTTOOL_H 1

// Include files
// from STL
#include <string>
#include <list>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"



#include "GaudiKernel/IHistogramSvc.h"
#include "AIDA/IHistogram1D.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "TH1.h"
#include "TH2.h"


#include "ErrorReport.h"


using namespace Gaudi::Utils ;

static const InterfaceID IID_IRichTestTool ( "IRichTestTool", 1, 0 );

/** @class IRichTestTool IRichTestTool.h IRichTestTool.h
 *   
 *
 *  @author Claus Buszello
 *  @date   2007-05-30
 */
class IRichTestTool : virtual public IAlgTool {
 public: 

  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IRichTestTool; }
  

  virtual int performTest(TH2D *H,ErrorReport &instat,std::list<ErrorReport> * reports)=0;
  virtual int performTest(TH1D *H,ErrorReport &instat,std::list<ErrorReport> * reports)=0;
  
 protected:
  
 private:

};
#endif // IRICHTESTTOOL_H
