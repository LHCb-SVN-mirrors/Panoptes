//------------------------------------------------------------------------
/** @file RichMon.cpp
 *
 *  Implementation file for Rich Monitoring Alg
 *  Author Claus Peter Buszello 2007
 */
//------------------------------------------------------------------------

//The algorithm and associated tools.
#include "RichHitMapsMonitor.h"

namespace Rich {
  namespace Mon {

    //-----------------------------------------------------------------------------
    // Declaration of the Algorithm Factory
    DECLARE_ALGORITHM_FACTORY(HitMapsMonitor)

    //-----------------------------------------------------------------------------
    // Rich::Mon::HitMapsMonitor implementation.
    // ===============
      HitMapsMonitor::HitMapsMonitor( const std::string& name,
                                      ISvcLocator* pSvcLocator )
        : Rich::HistoAlgBase(name, pSvcLocator)
        , mp_RichSys(NULL)
        , mp_SmartIDDecoder(NULL)
        , mp_HPDCount(NULL) 
        , mp_HotPix(NULL)
        , mp_Camera(NULL)
        , m_emptyEvents(0) 
        , m_allEvents(0)
        , m_aliceExt("ALICE") 
        , m_lowResExt("LowRes")
        , m_noiseExt("HitPerHPD")
        , m_Name("RichHitMapsMon")
    {
      m_hpdCntSendHistoMap.clear();
      m_hotPixSendHistoMap.clear();
      // Declare all the necessary properties.
      declareProperty("MonFreq", m_monFreq=1);
      declareProperty("HotPixEventInterval", m_hotpixinterval=10000);
      declareProperty("HotPixelEnabled", m_hotPixEnabled=true);
      declareProperty("HPDCountEnabled", m_hpdCntEnabled=true);
      declareProperty("HitsPerHPDMaps", m_hitsPerHPDMaps=false);
      declareProperty("LowResolutionHitMaps", m_lowResHitMaps=false);
      declareProperty("HighResolutionHitMaps", m_highResHitMaps=false);
      declareProperty("IndividualHpdMaps", m_plotIndividualHpdMaps=false);
      declareProperty("LowResolutionScaleFactor", m_lowResScaleFactor=2);
    }// HitMapsMonitor()

    // ===============
    HitMapsMonitor::~HitMapsMonitor( ){ }

    // ===============
    StatusCode HitMapsMonitor::initialize()
    {
      // Initialise the base class and check for errors.
      const StatusCode sc = Rich::HistoAlgBase::initialize();
      if (sc.isFailure()) { return sc; }

      //
      // get Partition name
      //
      char* partitionName = getenv("PARTITION");
      if (partitionName) 
      {
        info() << "running in partition " << partitionName << endmsg;
        m_Name = name() + boost::lexical_cast<std::string>(partitionName);
      }
      else 
      {
        m_Name = name();
      }

      //
      // print configuration
      //
      info() << "MonFreq                  " << m_monFreq               << endmsg;
      info() << "HotPixEventInterval      " << m_hotpixinterval        << endmsg;
      info() << "HotPixelEnabled          " << m_hotPixEnabled         << endmsg;
      info() << "HPDCountEnabled          " << m_hpdCntEnabled         << endmsg;
      info() << "HitsPerHPDMaps           " << m_hitsPerHPDMaps        << endmsg;
      info() << "LowResolutionHitMaps     " << m_lowResHitMaps         << endmsg;
      info() << "HighResolutionHitMaps    " << m_highResHitMaps        << endmsg;
      info() << "IndividualHpdMaps        " << m_plotIndividualHpdMaps << endmsg;
      info() << "LowResolutionScaleFactor " << m_lowResScaleFactor     << endmsg;

      // get tools, etc
      mp_RichSys = getDet<DeRichSystem>(DeRichLocations::RichSystem);
      acquireTool("RichSmartIDDecoder", mp_SmartIDDecoder, 0, true);

      mp_HPDCount = tool<IHPDCountTool>("Rich::Mon::HPDCountTool", "HPDCountTool");
      mp_HotPix   = tool<IHotPixelReport>("Rich::Mon::HotPixelReport", "HotPixelReport");
      mp_Camera   = tool<ICameraTool>("CameraTool");

      if ( m_lowResHitMaps || m_highResHitMaps || m_hitsPerHPDMaps )
      {
        // Helps if we book the histograms before we start. Just so they are all visible in the presenter.
        // Book hit maps for the 4 RICH panels.
        bookRichPanelHists(Rich::Rich1, Rich::top);
        bookRichPanelHists(Rich::Rich1, Rich::bottom);
        bookRichPanelHists(Rich::Rich2, Rich::left);
        bookRichPanelHists(Rich::Rich2, Rich::right);
      }// if producing some hit maps.

      // We will only ever book histograms for the individual HPDs on the fly.
      // This means we only ever produce and fill histograms for those HPDs with hits.

      // The maps that link a RICH panel level smart ID to the number of errors that occured on that panel for the HPD count
      // or hot pixel tool. Used to determine which histograms are sent to CAMERA when we report errors.
      // Ensure they are empty. stl guarantees that they are initialised to 0 if an element is access that doesn't exist.
      m_hpdCntSendHistoMap.clear();
      m_hotPixSendHistoMap.clear();

      mp_Camera->SendAndClearTS((ICameraTool::MessageLevel)1,m_Name,"Initialized");

      return sc;
    }

    // ===============
    StatusCode HitMapsMonitor::execute() 
    {
      //
      // main loop - get all RICH SmartIDs corresponding to HPDs/hits
      //
      ++m_allEvents;   // Event number used for hot/cold pixel finder

      // Get the smart IDs.
      const Rich::DAQ::L1Map &allSmartIDs = mp_SmartIDDecoder->allRichSmartIDs();

      // Loop over all HPDs/Pixels and fill the histograms
      if (msgLevel(MSG::VERBOSE))
        verbose() << "Found #SmartIDs for HPDs with hits " << allSmartIDs.size() << endmsg;

      if ( allSmartIDs.empty() ) 
      {
        ++m_emptyEvents;
        return StatusCode::SUCCESS;
      }// if empty event.

      // These will contain the list of errors generated by the HPD count and hot pixel tools and any histograms that are associated with them.
      std::list<ErrorReport> hpdCntErrList;
      std::list<TH2D*>       hpdCntHistos;
      std::list<ErrorReport> hotPixErrList;
      std::list<TH2D*>       hotPixHistos;

      // Loop over L1 boards
      for (Rich::DAQ::L1Map::const_iterator iL1 = allSmartIDs.begin();
           iL1 != allSmartIDs.end(); ++iL1) 
      {
        // loop over ingresses for this L1 board
        for (Rich::DAQ::IngressMap::const_iterator iIn = iL1->second.begin();
             iIn != iL1->second.end(); ++iIn) 
        {
          // Loop over HPDs in this ingress
          for (Rich::DAQ::HPDMap::const_iterator iAllSmartIDs= iIn->second.hpdData().begin();
               iAllSmartIDs != iIn->second.hpdData().end(); ++iAllSmartIDs) 
          {
            const Rich::DAQ::HPDInfo &hpdInfo = iAllSmartIDs->second;
            const LHCb::RichSmartID &smartIDHPD = hpdInfo.hpdID();

            // Check for valid HPD
            if (!smartIDHPD.isValid() ) {
              if ( msgLevel(MSG::VERBOSE) )
                verbose() << "HPD SmartID is not valid, continue." << endmsg;
              continue;
            }// SmartID valid
            // Check HPD is active.
            if (hpdInfo.header().inhibit()) {
              if ( msgLevel(MSG::VERBOSE) )
                verbose() << "HPD is inhibited, continue " <<  endmsg;
              continue;
            }// HPD active

            // Get the HPD hits.
            const LHCb::RichSmartID::Vector &smartIDHits  = hpdInfo.smartIDs();

            // Now perform the HPD count test, if necessary.
            if (m_hpdCntEnabled) {
              if (msgLevel(MSG::VERBOSE))
                verbose() << "HPD count test." << endmsg;
              // The tool will only report errors when necessary.
              ErrorReport tmpErr;
              mp_HPDCount->performTest(smartIDHPD, smartIDHits.size(), tmpErr);
              if ("" != tmpErr.message) {
                // Add the error to the list.
                hpdCntErrList.push_back(tmpErr);
                // Determine what the relevant histogram is. As we don't want to send the same histogram more than once
                // just increment a counter and we will add the corresponding histogram if it is greater than 0.
                // There is a histogram per panel. This is guarantied to be zero if it is not present in the map.
                ++m_hpdCntSendHistoMap[smartIDHPD.panelID()];
              }// if(""!=tmpErr.message)
            }// if(m_hpdCntEnabled)

            // Now perform the hot pixel search - fill pixel-occupancy arrays
            if (m_hotPixEnabled) {
              mp_HotPix->FillArray(smartIDHPD, smartIDHits);
            }// if(m_hotPixEnabled)

            // Only update the histograms every nth event.
            if ( 0 == m_allEvents%m_monFreq ) 
            {
              // loop over all hits in this HPD and create the hit maps.
              for ( LHCb::RichSmartID::Vector::const_iterator iHit = smartIDHits.begin();
                    iHit != smartIDHits.end(); ++iHit ) 
              {
                // Get smart ID we are dealing with. Need to use this one as it has the pixel information set.
                const LHCb::RichSmartID &rsm = (*iHit);

                // With the smartID and create a global ordering class in order to determine where to place the HPD pixel.
                Rich::SmartIDGlobalOrdering gOrdering(rsm);

                // Get panel histograms
                HitMapsInfo & histos = m_hitMapsInfo[rsm.panelID()];

                // Now plot the relevant histograms.
                // The RICH level hit maps are booked in initialise and as a result we can rely on them having been booked.
                // Still need to protect against accessing a NULL pointer though.
                if ( m_hitsPerHPDMaps ) 
                {
                  histos.mp_hitsPerHPDMap->fill(gOrdering.globalPdX(), gOrdering.globalPdY());
                }

                if (m_lowResHitMaps) 
                {
                  if (!rsm.pixelSubRowIsSet()) 
                  {
                    histos.mp_lowResHitMapLhcb->fill(gOrdering.globalPixelX(), gOrdering.globalPixelY());
                  }
                  else
                  {
                    histos.mp_lowResHitMapAlice->fill(gOrdering.globalPixelX(), gOrdering.globalPixelY());
                  }
                }

                if (m_highResHitMaps) 
                {
                  if (!rsm.pixelSubRowIsSet()) 
                  {
                    histos.mp_highResHitMapLhcb->fill(gOrdering.globalPixelX(), gOrdering.globalPixelY());
                  }
                  else 
                  {
                    histos.mp_highResHitMapAlice->fill(gOrdering.globalPixelX(), gOrdering.globalPixelY());
                  }
                }

                if (m_plotIndividualHpdMaps) 
                {
                  if (!rsm.pixelSubRowIsSet()) 
                  {
                    AIDA::IHistogram2D * h = m_hitMapsInfo[rsm.pdID()].mp_highResHitMapLhcb;
                    if ( h )
                    {
                      h->fill(gOrdering.localPixelX(), gOrdering.localPixelY());
                    }
                    
                    else
                    {
                      // In this case we want to have a hits per HPD histogram but haven't created one.
                      // Better book it
                      bookIndividualHists(rsm);
                      // and fill it.
                      m_hitMapsInfo[rsm.pdID()].mp_highResHitMapLhcb->fill(gOrdering.localPixelX(), gOrdering.localPixelY());
                    }// else histogram not booked.
                  }// if LHCb
                  else
                  {
                    AIDA::IHistogram2D * h = m_hitMapsInfo[rsm.pdID()].mp_highResHitMapAlice;
                    if ( h ) 
                    {
                      h->fill(gOrdering.localPixelX(), gOrdering.localPixelY());
                    }
                    else 
                    {
                      // In this case we want to have a hits per HPD histogram but haven't created one.
                      // Better book it
                      bookIndividualHists(rsm);
                      // and fill it.
                      m_hitMapsInfo[rsm.pdID()].mp_highResHitMapAlice->fill(gOrdering.localPixelX(), gOrdering.localPixelY());
                    }// else histogram not booked.
                  }// else ALICE
                }// if(m_plotIndividualHpdMaps)

              }// for iHit
            }// if(0==m_allEvents%m_monFreq)
          }// for iAllSmartIds
        }// for iIn
      }// for iL1


      // Check for and report errors for the hot pixel and HPD count tests.
      if ( !hpdCntErrList.empty() ) 
      {
        // Get the histos for the panels that we have seen errors for.
        for (std::map<LHCb::RichSmartID, int>::iterator it(m_hpdCntSendHistoMap.begin());
             it!=m_hpdCntSendHistoMap.end(); ++it )
        {
          // The key (first) is a smart ID with RICH and panel info set. The data (second) is the number of times we saw an
          // error on this RICH panel. Only care if there was more than 1.
          if (0 < it->second) {
            // Identify the relevant histogram from its name and then get a pointer to it, which is converted to ROOT and dropped into the list.
            hpdCntHistos.push_back(Gaudi::Utils::Aida2ROOT::aida2root(m_hitMapsInfo[it->first].mp_hitsPerHPDMap));// histo2D(getRichPanelHistName(it->first.rich(), it->first.panel())+m_noiseExt)));
            // Clear this counter for next time.
            it->second = 0;
          }// if seen errors on this RICH panel
        }// Loop over the map to see if there are any hits on the corresponding RICH panel.

        // Error level and message that will appear in the CAMERA main GUI.
        int lvl(3);
        std::string summary("Noisey HPD summary.");
        submitErrorReports(lvl, summary, hpdCntErrList, hpdCntHistos);
      }// if seen errors

      if ( 0 != hotPixErrList.size() ) {
        // Get the histos for the panels that we have seen errors for.
        for (std::map<LHCb::RichSmartID, int>::iterator it(m_hotPixSendHistoMap.begin());
             it != m_hotPixSendHistoMap.end(); ++it)
        {
          // The key (first) is a smart ID with RICH and panel info set. The data (second) is the number of times we saw an
          // error on this RICH panel. Only care if there was more than 1.
          if ( 0 < it->second ) {
            // Identify the relevant histogram from its name and then get a pointer to it, which is converted to ROOT and dropped into the list.
            hotPixHistos.push_back(Gaudi::Utils::Aida2ROOT::aida2root(m_hitMapsInfo[it->first].mp_hitsPerHPDMap));// Gaudi::Utils::Aida2ROOT::aida2root(histo2D(getRichPanelHistName(it->first.rich(), it->first.panel())+m_noiseExt)));
            // Clear this counter for next time.
            it->second = 0;
          }// if seen errors on this RICH panel
        }// Loop over the map to see if there are any hits on the corresponding RICH panel.

        // Error level and message that will appear in the CAMERA main GUI.
        int lvl(3);
        std::string summary("Hot and cold pixel summary.");
        submitErrorReports(lvl, summary, hotPixErrList, hotPixHistos);
      }// if seen errors on enough events to report.

      if (m_hotPixEnabled) {   // Perform Hot Pixel analysis at specified event interval
        if((m_allEvents % m_hotpixinterval)==0)
        {
          std::list<ErrorReport> tmpErrList;
          mp_HotPix->PerformTest(m_allEvents,&tmpErrList);
          m_allEvents=0;   // Reset event number to zero
          for(std::list<ErrorReport>::const_iterator itmp = tmpErrList.begin(); itmp!= tmpErrList.end(); itmp++){
            if ("" != (*itmp).message)   // If report contains error message
              hotPixErrList.push_back(*itmp);
          }

          if ( 0 != hotPixErrList.size() ) {  // If non-zero error list
            int lvl(3);
            std::string summary("Hot Pixel summary.");
            submitErrorReports(lvl, summary, hotPixErrList, hotPixHistos);
          }

        }  // ...if event interval
      }  // ...if m_hotPixEnabled


      return StatusCode::SUCCESS;
    }

    // ===============
    StatusCode HitMapsMonitor::bookRichPanelHists( const Rich::DetectorType rich, 
                                                   const Rich::Side panel ) 
    {

      // Create a smart ID for this RICH panel and create an entry in the hit maps info map.
      const LHCb::RichSmartID smartId(rich, panel);
      // This either will be given a default constructed case or an existing object.
      // We will later check the pointers to see if the histograms already exist.
      // A default constructed object is initialised to have NULL pointers.
      HitMapsInfo & mapsInfo = m_hitMapsInfo[smartId];

      // Get the ID and name for the histograms.
      const std::string id(getRichPanelHistId(rich, panel));
      const std::string name(getRichPanelHistName(rich, panel));

      // We will only try and create this histogram if we have been told that it should exist
      // and if it doesn't already exist.
      if ( m_hitsPerHPDMaps && (NULL==mapsInfo.mp_hitsPerHPDMap) ) 
      {
        // Create the noise maps which are the same for ALICE and LHCb.
        // Setup x-axis.
        double xmin(-0.5);
        double xmax( Rich::Rich2==rich ? 17.5: 13.5 ); // In the case of RICH2 it is the columns along x and RICH1 the HPDs. Takes into account the -0.5.
        int    xnbins( static_cast<int>(xmax) + 1 );
        if (Rich::Rich2 == rich) {
          // In the case of RICH2 we need to alter the x-axis to shift the axis to cover only the appropriate panel.
          if (Rich::left == panel)
            xmin += xmin + (xmax + 0.5) / 2.0;// A-side start half way along the x-axis at the start of C-side.
          else// As we are one RICH2 this must be C-side (right).
            xmax -= ((xmax + 0.5) / 2.0);// C-side ends at the start of A-side.
          xnbins /= 2;// What ever side we have reduced the x-axis by half.
        }// if(Rich::Rich2)
        // Setup y-axis.
        double ymin(-0.5);
        double ymax( Rich::Rich2==rich ? 15.5: 13.5  ); // In the case of RICH2 it is the HPDs along y and RICH1 the columns. Takes into account the -0.5.
        int    ynbins( static_cast<int>(ymax) + 1);
        if (Rich::Rich1 == rich) {
          // In the case of RICH1 we need to alter the y-axis to shift it to cover only the appropriate panel.
          if(Rich::top == panel)
            ymin  = ymin + (ymax + 0.5) / 2.0; // Top panel starts half way up the y-axis after the bottom panel has ended.
          else // As we are on RICH1 this must be bottom.
            ymax  = ((ymax + 0.5) / 2.0); // Bottom panel ends half way up the y-axis where the top starts.
          ynbins /= 2; // What ever side we have reduced the y-axis by half.
        }// if(Rich::Rich1==rich)
        // Now book the histogram.
        mapsInfo.mp_hitsPerHPDMap = book2D(id+m_noiseExt, name//+m_noiseExt
                                           , xmin, xmax, xnbins
                                           , ymin, ymax, ynbins
                                           );
      }// if(m_hitsPerHPDMaps&&(NULL==mapsInfo.mp_hitsPerHPDMap))

      if (m_lowResHitMaps || m_highResHitMaps) 
      {

        // Setup of the LHCb mode histograms.
        Rich::SmartIDGlobalOrdering gOrdering(smartId, Rich::SmartIDGlobalOrdering::LHCbMode);
        // Setup x-axis.
        double xmin   = -0.5;
        double xmax   = gOrdering.maxGlobalPixelX() - 0.5;
        int    xnbins = gOrdering.maxGlobalPixelX();
        if (Rich::Rich2 == rich) {
          // In the case of RICH2 we need to alter the x-axis to shift the axis to cover only the appropriate panel.
          if (Rich::left == panel)
            xmin += gOrdering.maxGlobalPixelX() / 2.0;// A-side start half way along the x-axis at the start of C-side.
          else// As we are one RICH2 this must be C-side (right).
            xmax -= gOrdering.maxGlobalPixelX() / 2.0;// C-side ends at the start of A-side.
          xnbins /= 2;// What ever side we have reduced the x-axis by half.
        }// if(Rich::Rich2)
        // Setup y-axis.
        double ymin   = -0.5;
        double ymax   = gOrdering.maxGlobalPixelY() - 0.5;
        int    ynbins = gOrdering.maxGlobalPixelY();
        if (Rich::Rich1 == rich) {
          // In the case of RICH1 we need to alter the y-axis to shift it to cover only the appropriate panel.
          if(Rich::top == panel)
            ymin += gOrdering.maxGlobalPixelY() / 2.0;// Top panel starts half way up the y-axis after the bottom panel has ended.
          else // As we are on RICH1 this must be bottom.
            ymax -= gOrdering.maxGlobalPixelY() / 2.0;// Bottom panel ends half way up the y-axis where the top starts.
          ynbins /= 2;// What ever side we have reduced the y-axis by half.
        }// if(Rich::Rich1==rich)
        if (m_lowResHitMaps && (NULL==mapsInfo.mp_lowResHitMapLhcb)) 
        {
          // Book the lower resolution LHCb mode.
          mapsInfo.mp_lowResHitMapLhcb = book2D(id+m_lowResExt, name//+m_lowResExt
                                                , xmin, xmax, xnbins/m_lowResScaleFactor
                                                , ymin, ymax, ynbins/m_lowResScaleFactor
                                                );
        }// if(m_lowResHitMaps&&(NULL==mapsInfo.mp_lowResHitMapLhcb))
        if (m_highResHitMaps && (NULL==mapsInfo.mp_highResHitMapLhcb)) 
        {
          // Book LHCb mode full resolution
          mapsInfo.mp_highResHitMapLhcb = book2D(id, name
                                                 , xmin, xmax, xnbins
                                                 , ymin, ymax, ynbins
                                                 );
        }// if(m_highResHitMaps&&(NULL==mapsInfo.mp_highResHitMapLhcb))

        // Book the ALICE mode histos.
        Rich::SmartIDGlobalOrdering gOrderingA(smartId, Rich::SmartIDGlobalOrdering::ALICEMode);
        // Setup x-axis.
        xmin   = -0.5;
        xmax   = gOrderingA.maxGlobalPixelX() - 0.5;
        xnbins = gOrderingA.maxGlobalPixelX();
        if (Rich::Rich2 == rich) {
          // In the case of RICH2 we need to alter the x-axis to shift the axis to cover only the appropriate panel.
          if (Rich::left == panel)
            xmin += gOrderingA.maxGlobalPixelX() / 2.0;// A-side start half way along the x-axis at the start of C-side.
          else// As we are one RICH2 this must be C-side (right).
            xmax -= gOrderingA.maxGlobalPixelX() / 2.0;// C-side ends at the start of A-side.
          xnbins /= 2;// What ever side we have reduced the x-axis by half.
        }// if(Rich::Rich2)
        // Setup y-axis.
        ymin   = -0.5;
        ymax   = gOrderingA.maxGlobalPixelY() - 0.5;
        ynbins = gOrderingA.maxGlobalPixelY();
        if (Rich::Rich1 == rich) {
          // In the case of RICH1 we need to alter the y-axis to shift it to cover only the appropriate panel.
          if(Rich::top == panel)
            ymin += gOrderingA.maxGlobalPixelY() / 2.0;// Top panel starts half way up the y-axis after the bottom panel has ended.
          else // As we are on RICH1 this must be bottom.
            ymax -= gOrderingA.maxGlobalPixelY() / 2.0;// Bottom panel ends half way up the y-axis where the top starts.
          ynbins /= 2;//What  ever side we have reduced the y-axis by half.
        }// if(Rich::Rich1==rich)
        if (m_lowResHitMaps && (NULL==mapsInfo.mp_lowResHitMapAlice)) {
          // Now book it.
          mapsInfo.mp_lowResHitMapAlice = book2D(id+m_aliceExt+m_lowResExt, name//+m_aliceExt+m_lowResExt
                                                 , xmin, xmax, xnbins/m_lowResScaleFactor
                                                 , ymin, ymax, ynbins/m_lowResScaleFactor
                                                 );
        }// if(m_lowResHitMaps&&(NULL==mapsInfo.mp_lowResHitMapAlice))
        if (m_highResHitMaps && (NULL==mapsInfo.mp_highResHitMapAlice)) {
          mapsInfo.mp_lowResHitMapAlice = book2D(id+m_aliceExt, name//+m_aliceExt
                                                 , xmin, xmax, xnbins
                                                 , ymin, ymax, ynbins
                                                 );
        }// if(m_highResHitMaps&&(NULL==mapsInfo.mp_highResHitMapAlice))
      }// if(m_lowResHitMaps||m_highResHitMaps)
      // Done.
      return StatusCode::SUCCESS;
    }

    // ===============
    StatusCode HitMapsMonitor::bookIndividualHists(const LHCb::RichSmartID &smartID) 
    {
      // Get the entry in the hit maps info map to update the HPD's information.
      // Ensure that we have only HPD level information.
      // When we store the information we will store it under the full resolution
      // histogram pointer as this is the only one that makes sense for an individual HPD.
      HitMapsInfo &mapInfo(m_hitMapsInfo[smartID.pdID()]);
      // Check to see if either of the histograms we want to create exist.
      if ((NULL==mapInfo.mp_highResHitMapLhcb) || (NULL==mapInfo.mp_highResHitMapAlice)) {
        // Create an ordering object so our HPD is orientated the same way as in global hit map.
        // Force LHCb mode as there will be no pixel info.
        Rich::SmartIDGlobalOrdering gOrdering(smartID, Rich::SmartIDGlobalOrdering::LHCbMode);
        // Get the ID and name for the histograms.
        std::string id(getIndividualHistName(smartID));
        std::string name(getIndividualHistName(smartID));
        // Setup x-axis.
        double xmin(-0.5);
        double xmax(gOrdering.maxLocalPixelX() - 0.5); // swap to local.
        int    xnbins(gOrdering.maxLocalPixelX());
        // Setup y-axis.
        double ymin(-0.5);
        double ymax(gOrdering.maxLocalPixelY() - 0.5);
        int    ynbins(gOrdering.maxLocalPixelY());
        // Use the local coordinates but the LHCb HPDs are square.
        if (NULL == mapInfo.mp_highResHitMapLhcb)
          mapInfo.mp_highResHitMapLhcb = book2D(id, name, xmin, xmax, xnbins, ymin, ymax, ynbins);

        // Book the ALICE mode histos.
        Rich::SmartIDGlobalOrdering gOrderingA(smartID, Rich::SmartIDGlobalOrdering::ALICEMode);
        // Setup x-axis.
        xmin   = -0.5;
        xmax   = gOrdering.maxLocalPixelX() - 0.5;
        xnbins = gOrdering.maxLocalPixelX();
        // Setup y-axis.
        ymin   = -0.5;
        ymax   = gOrdering.maxLocalPixelY() - 0.5;
        ynbins = gOrdering.maxLocalPixelY();
        // Need to add the appropriate extention to the name.
        id   += m_aliceExt;
        name += m_aliceExt;
        if (NULL == mapInfo.mp_highResHitMapAlice)
          mapInfo.mp_highResHitMapAlice = book2D(id, name, xmin, xmax, xnbins, ymin, ymax, ynbins);
      }// if((NULL==mapInfo.mp_highResHitMapLhcb)||(NULL==mapInfo.mp_HighResHitMapAlice))
      // Done.
      return StatusCode::SUCCESS;
    }// HitMapsMonitor::bookIndividualHpdHists()


    // ===============
    std::string HitMapsMonitor::getRichPanelHistId(const Rich::DetectorType rich, 
                                                   const Rich::Side panel) 
    {
      // Now create the name.
      std::ostringstream id;
      id << Rich::text(rich)
         << Rich::text(rich, panel)
         << "PixelRC";
      return id.str();
    }// HitMapsMonitor::getRichPanelHistId()


    // ===============
    std::string HitMapsMonitor::getRichPanelHistName(const Rich::DetectorType rich, 
                                                     const Rich::Side panel) 
    {
      std::ostringstream name;
      name <<"HitMap for "
           << Rich::text(rich)
           <<" "
           << Rich::text(rich, panel)
           <<" panel";
      return name.str();
    }// HitMapsMonitor::getRichPanelHistName()


    // ===============
    std::string HitMapsMonitor::getIndividualHistId(const LHCb::RichSmartID &smartId) 
    {
      std::ostringstream id;
      id << Rich::text(smartId.rich())
         << Rich::text(smartId.rich(), smartId.panel())
         << "_HPDCol"
         << boost::lexical_cast<std::string>(smartId.pdCol())
         << "_HPDInCol"
         << boost::lexical_cast<std::string>(smartId.pdNumInCol());
      return id.str();
    }// HitMapsMonitor::getRichPanelHistId()


    // ===============
    std::string HitMapsMonitor::getIndividualHistName(const LHCb::RichSmartID &smartId) 
    {
      // Currently this is the same as the ID.
      return getIndividualHistId(smartId);
    }// HitMapsMonitor::getRichPanelHistName()

    // ===============
    StatusCode HitMapsMonitor::submitErrorReports(int level,
                                                  const std::string &title,
                                                  std::list<ErrorReport> &erList,
                                                  std::list<TH2D*> &histos) 
    {
      // Only submit the errors if there are any errors to submit.
      if (0 < erList.size()) {
        // Deal with the error messages.
        for (std::list<ErrorReport>::const_iterator ierr(erList.begin()); ierr!=erList.end(); ++ierr) {
          if ( msgLevel(MSG::INFO) )
            info() << ierr->message << endmsg;
          mp_Camera->Append(ierr->message);
        }// for ierr;

        // Now append the given histograms.
        for (std::list<TH2D*>::const_iterator iHisto(histos.begin()); iHisto!=histos.end(); ++iHisto)
          mp_Camera->Append(*iHisto);

        // Send to CAMERA.
        mp_Camera->SendAndClearTS((ICameraTool::MessageLevel)level, this->name(), title);

        // Clear messages and histograms for next time.
        erList.clear();
        histos.clear();
      }// if(0<erlist.size())
      return StatusCode::SUCCESS;
    }// HitMapMonitor::submitErrorReports()

  }// namespace Mon
}// namespace Rich
