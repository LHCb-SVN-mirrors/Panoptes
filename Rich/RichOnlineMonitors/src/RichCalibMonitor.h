// $Id: RichCalibMonitor.h,v 1.2 2009-02-03 15:14:35 rogers Exp $
#ifndef RICHCALIBMONITOR_H
#define RICHCALIBMONITOR_H 1

//
// Include files
//

#include <sstream>

// base class
#include "RichKernel/RichHistoAlgBase.h"

// boost
#include "boost/lexical_cast.hpp"

// Interfaces
#include "RichKernel/IRichRawBufferToSmartIDsTool.h"
#include "RichKernel/IRichSmartIDTool.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// RichKernel
#include "RichMonitoringTools/IHpdUkL1DisableTool.h"

// Event
#include "Event/ODIN.h"

// time
#include <time.h>

// boost
#include "boost/assign/list_of.hpp"

// forward declarations
class ICameraTool;
namespace AIDA
{
  class IHistogram;
}

namespace Rich
{
  namespace Mon
  {

    /** @class HPDNHitMonitor RichCalibMonitor.h
     *
     *  This algorithm runs on the calibration farm
     *  @author Ulrich Kerzel
     *  @date   2008-09-19
     */
    class CalibMonitor : public Rich::HistoAlgBase{

      // ---------------------------------------------------------------------------
      //                                PUBLIC
      // ---------------------------------------------------------------------------
    public:

      /// Standard constructor
      CalibMonitor( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~CalibMonitor( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode execute   ();    ///< Algorithm execution
      virtual StatusCode finalize  ();    ///< Algorithm finalization

    private:

      /// Load the HPD disabling tool on demand
      Rich::Mon::IHpdUkL1DisableTool * hpdDisableTool() const
      {
        if ( !m_HpdUKL1DisableTool )
        {
          m_HpdUKL1DisableTool =
            tool<Rich::Mon::IHpdUkL1DisableTool>("Rich::Mon::HpdUkL1DisableTool",
                                                 "RichUKL1Disable");
        }
        return m_HpdUKL1DisableTool;
      }

    private:

      /// Pointer to RICH system detector element
      const DeRichSystem                * m_RichSys;

      /// Raw Buffer Decoding tool
      const Rich::DAQ::IRawBufferToSmartIDsTool * m_SmartIDDecoder;

      mutable Rich::Mon::IHpdUkL1DisableTool    * m_HpdUKL1DisableTool;

      ICameraTool                               * m_CamTool;  ///< CAMERA error reporting tool

      unsigned long     m_nEvts;               ///< Event count
      unsigned long m_nEvtsLastUpdate;         ///< Event count for last message
      bool              m_FillHistos;          ///< determine if histograms should be filled (or just counter)
      bool              m_ExtendedVerbose;     ///< even more output
      bool              m_RemoveFaultyHpdUKL1; ///< use tool to send disable command to UKL1
      time_t            m_TimeStart;
      time_t            m_TimeLastUpdate;
      time_t            m_TimeLastCounterUpdate;
      int               m_UpdateTimerInterval;   ///< Interval at which to send error messages (in seconds)

      int               m_CounterUpdateRate; ///< Rate in secs to update counters
      int               m_CounterUpdateMinEntries; ///< Min number of entries for an update

      /// Count triggers
      typedef std::map<LHCb::ODIN::TriggerType,unsigned long long int> TriggerTypeMap;
      TriggerTypeMap m_TriggerTypeMap;

      /// Count calibration types
      typedef std::map<LHCb::ODIN::CalibrationTypes,unsigned long long int> CalibrationTypeMap;
      CalibrationTypeMap m_CalibrationTypeMap;

      class DimCount
      {
      public:
        DimCount() : valueR1(0), valueR2(0) { }
      public:
        double valueR1, valueR2;
      };
      typedef std::map<std::string,DimCount> DimCountMap;

      DimCountMap m_dimMap;

    }; //class

  } // namespace Mon
} // namespace Rich

#endif // RICHCALIBMONITOR_H
