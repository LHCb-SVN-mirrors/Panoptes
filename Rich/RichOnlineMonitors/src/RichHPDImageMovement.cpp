// $Id: RichHPDImageMovement.cpp,v 1.2 2009-11-25 18:35:55 tblake Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "RichHPDImageMovement.h"
#include "boost/lexical_cast.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : RichHPDImageMovement
//
// 2009-10-13 : Thomas BLAKE
//-----------------------------------------------------------------------------


namespace Rich 
{
  namespace Mon 
  {
    
    // Declaration of the Algorithm Factory
    
    DECLARE_ALGORITHM_FACTORY( RichHPDImageMovement )
    
    
    //=============================================================================
    // Standard constructor, initializes variables
    //=============================================================================
    
    RichHPDImageMovement::RichHPDImageMovement( const std::string& name,
                                                ISvcLocator* pSvcLocator)
      : Rich::HistoAlgBase( name , pSvcLocator ) ,  
        m_nEvt( 0 ), 
        m_arrayCol( NULL ), 
        m_arrayRow( NULL ), 
        m_arrayID( NULL ), 
        m_arraySize( 0 )
    {
      declareProperty( "UpdateFrequency" , m_nUpdate = 1000 );
      declareProperty( "MakeHistos" , m_makeHistos = true );
      declareProperty( "HistoUpdateFrequency", m_nUpdateHistos = 1000 );
      declareProperty( "DisplaySmartIDWarnings" , m_displayWarnings = false );
      declareProperty( "UseCutBasedMethod" , m_useCuts = false );
      declareProperty( "CutThreshold" , m_cutFraction = 0.1 );
    }
    
    //=============================================================================
    // Destructor
    //=============================================================================
    
    RichHPDImageMovement::~RichHPDImageMovement() {
      delete[] m_arrayCol;
      delete[] m_arrayRow;
      delete[] m_arrayID ;
    } 
    
    //=============================================================================
    // Initialization
    //=============================================================================
    
    StatusCode RichHPDImageMovement::initialize() {
      
      StatusCode sc = Rich::HistoAlgBase::initialize(); // must be executed first
      if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
      
      if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
      
      m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
      
      acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, 0, true );
      
      const LHCb::RichSmartID::Vector &activeHPDs  = m_RichSys->allPDRichSmartIDs();
      
      LHCb::RichSmartID::Vector::const_iterator iHPD;
      
      
      for ( iHPD = activeHPDs.begin(); iHPD != activeHPDs.end(); ++iHPD ) {
        const Rich::DAQ::Level0ID hardID = m_RichSys->level0ID( *iHPD );
        
        IRichHPDImageCentre* hpdCentre = NULL ;
        
        if ( m_useCuts ){
          hpdCentre = new RichHPDImageCentreWithCuts( m_nUpdate, m_cutFraction );
        }
        else {
          hpdCentre = new RichHPDImageCentre( m_nUpdate );
        }
        
        m_map.insert( std::make_pair(hardID.data(), hpdCentre) );
      }
      
      m_arraySize = m_map.size() ;
      
      m_arrayCol = new float[m_arraySize] ;
      m_arrayRow = new float[m_arraySize] ;
      m_arrayID = new unsigned int[m_arraySize] ;
      
      int icounter = 0;

      for ( m_iter = m_map.begin() ; m_iter != m_map.end() ; ++m_iter ){
        
        if ( NULL == m_iter->second ) {
          warning() << " Problem initializing HPD objects " << endmsg;
          return StatusCode::FAILURE;
        }
        
        unsigned int intID = m_iter->first ;
        
        Rich::DAQ::Level0ID hardID( intID );
        
        std::string name = "Rich_HPD_" + boost::lexical_cast<std::string>(hardID);
        name += "_ImageCentre";
        
        /*  If you want separate counters for each HPD uncomment these lines */

        // const LHCb::RichSmartID smartID = m_RichSys->richSmartID( hardID );

        //  std::string title = "HPD image centre of Rich=";
        // title += boost::lexical_cast<std::string>( smartID.rich()+1 );
        // title += ", Col= ";
        // title += boost::lexical_cast<std::string>( smartID.hpdCol() );
        // title += ", Pos=";
        // title += boost::lexical_cast<std::string>( smartID.hpdNumInCol() );
        
        // if ( msgLevel(MSG::DEBUG) ) {
        //   debug() << "Declaring Info : Name  = " << name  << endmsg;
        //   debug() << "Declaring Info : Title = " << title << endmsg;
        // }
        
        // declareInfo( name + "_Col", (m_iter->second)->averageCol(), title + "Col" );
        // declareInfo( name + "_Row", (m_iter->second)->averageRow(), title + "Row" ); 
        
        if ( m_makeHistos ) {
          if ( msgLevel(MSG::DEBUG) ) debug() << " Booking histogram " << name << endmsg ;
          m_histo.insert( std::make_pair(intID,book2D(name,name,-0.5,31.5,320,-0.5,31.5,320)));
        }
        
        m_arrayID[icounter] = intID ;
        m_arrayCol[icounter] = 0.0;
        m_arrayRow[icounter] = 0.0;
        
        icounter++;
      }
      
      if ( msgLevel(MSG::DEBUG) ) debug() << " Publishing counters " << endmsg ;
      
      std::string arrayname = "Rich_HPD_Image_Centre" ;
      std::string arraytitle = "HPD Image Centre Array" ;
      
      std::string floatformat = "F:" + boost::lexical_cast<std::string>(m_arraySize);
      std::string intformat = "I:" + boost::lexical_cast<std::string>(m_arraySize);
      
      const int floatsize = m_arraySize*sizeof(float);
      const int intsize = m_arraySize*sizeof(int);
      
      declareInfo( arrayname + "_Col", floatformat, m_arrayCol, floatsize, arraytitle + " (Col)");
      declareInfo( arrayname + "_Row", floatformat, m_arrayRow, floatsize, arraytitle + " (Row)");
      declareInfo( arrayname + "_ID" , intformat,   m_arrayID,  intsize,   arraytitle + " (ID)");
      
      return sc;
    }
    
    //=============================================================================
    // Main execution
    //=============================================================================
    
    StatusCode RichHPDImageMovement::execute() {
      
      if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
      m_nEvt++;
      
      // If make histograms 
      if ( m_makeHistos ) {
        if ( 0 == (m_nEvt % m_nUpdateHistos ) ) { 
          updateHistograms() ;
        }
      }
      
      // Standard loop over Rich Smart IDs 
      const Rich::DAQ::L1Map& mapUKL1 = m_SmartIDDecoder->allRichSmartIDs();
      Rich::DAQ::L1Map::const_iterator iUKL1;
      
      for ( iUKL1 = mapUKL1.begin(); iUKL1 != mapUKL1.end(); ++iUKL1 ){
        const Rich::DAQ::IngressMap& mapIngress = iUKL1->second;
        Rich::DAQ::IngressMap::const_iterator iIngress;
        
        for ( iIngress = mapIngress.begin(); iIngress != mapIngress.end(); ++iIngress ){
          
          const Rich::DAQ::HPDMap& mapHPD = (iIngress->second).hpdData() ;
          Rich::DAQ::HPDMap::const_iterator iHPD;
          
          for ( iHPD = mapHPD.begin(); iHPD != mapHPD.end(); ++iHPD ){
            
            const LHCb::RichSmartID &smartID = (iHPD->second).hpdID();
            
            if ( !smartID.isValid() ) {
              if ( m_displayWarnings ) {
                warning() << " Invalid Rich Smart ID " << endmsg;
              } 
              continue;
            }
        
            const Rich::DAQ::Level0ID hardID = m_RichSys->level0ID( smartID );; 
            const LHCb::RichSmartID::Vector& hitIDs = (iHPD->second).smartIDs() ;
        
            LHCb::RichSmartID::Vector::const_iterator iHit;
            
            IRichHPDImageCentre* hpdCentre = m_map[hardID.data()];

            // Update the HPD image position with each hit
	    if ( hpdCentre )
	    {
	      for ( iHit = hitIDs.begin(); iHit != hitIDs.end(); ++iHit )
	      {
		if ( hpdCentre->update( iHit->pixelCol(), iHit->pixelRow() ) )
		{
		  if ( msgLevel(MSG::DEBUG) )
		    debug() << " Updating array [" << hardID << "]" << endmsg ;

		  updateArray( hardID.data(), 
			       hpdCentre->averageCol(),
			       hpdCentre->averageRow() ) ;
		}
	      }
	    }
          }
        }
      }

      return StatusCode::SUCCESS;
    }

    void RichHPDImageMovement::updateArray( const unsigned int HardID, 
                                            const double COL, 
                                            const double ROW )
    {
      for ( unsigned int iID = 0; iID < m_arraySize; ++iID ){
        if ( HardID == m_arrayID[iID] ) {
          m_arrayCol[iID] = COL;
          m_arrayRow[iID] = ROW;
          return ;
        }
      }
    }
    
    void RichHPDImageMovement::updateHistograms()
    {
      if ( msgLevel(MSG::DEBUG) ) debug() << " Updating histograms" << endmsg;
      
      for ( m_iter = m_map.begin() ; m_iter != m_map.end() ; ++m_iter ){
        unsigned int intID = m_iter->first;
        m_histo[intID]->fill( (m_iter->second)->averageCol(),
                              (m_iter->second)->averageRow() );
      }
      
      return ;
    } 
    
  
    
    
    //=============================================================================
    //  Finalize
    //=============================================================================

    StatusCode RichHPDImageMovement::finalize() {
      
      if ( msgLevel(MSG::DEBUG) ){ 
        debug() << "==> Finalize" << endmsg;
        debug() << "    Algorithm has seen " << m_nEvt << " events" << endmsg;
      }
      
      for ( m_iter = m_map.begin() ; m_iter != m_map.end() ; ++m_iter ){
        delete m_iter->second;
      }
      
      m_histo.clear() ;
      m_map.clear();
      
      if ( msgLevel(MSG::DEBUG) ) debug() << " Freed map of HPD objects " << endmsg;
      
      return Rich::HistoAlgBase::finalize();  // must be called after all other actions
    }
    
    //=============================================================================
    
    
    //-----------------------------------------------------------------------------
    // Implementation file for class : RichHPDImageCentre[WithCuts]
    //
    // 2009-10-13 : Thomas BLAKE
    //-----------------------------------------------------------------------------
    

    
    RichHPDImageCentre::RichHPDImageCentre( const int UpdateFrequency ) : 
      m_centreCol( 0 ), m_centreRow( 0 ), m_lastAverageCol( 0 ), m_lastAverageRow( 0 ) , 
      m_nHit( 0 ), m_nUpdate( UpdateFrequency ) {}
    

    RichHPDImageCentre::RichHPDImageCentre( )  : 
      m_centreCol( 0 ), m_centreRow( 0 ), m_lastAverageCol( 0 ), m_lastAverageRow( 0 ) , 
      m_nHit( 0 ), m_nUpdate( 1000 ) {}
    
    
    
    RichHPDImageCentre::~RichHPDImageCentre() {} 
    
    
    const double& RichHPDImageCentre::averageCol() const 
    {
      return m_lastAverageCol ;
    }
    
    
    const double& RichHPDImageCentre::averageRow() const    
    {
      return m_lastAverageRow ;
    }
    
    
    bool RichHPDImageCentre::update( const int COL , const int ROW ) 
    {
      m_centreCol += COL;
      m_centreRow += ROW;
      m_nHit++;
      
      if ( 0 == ( m_nHit % m_nUpdate ) ){
        m_lastAverageCol = (1.0*m_centreCol)/(1.0*m_nUpdate);
        m_lastAverageRow = (1.0*m_centreRow)/(1.0*m_nUpdate);
        
        m_centreCol = 0 ;
        m_centreRow = 0 ;
        m_nHit = 0;
        
        return true ;
      }
      
      return false ;
    }
    
    
    RichHPDImageCentreWithCuts::RichHPDImageCentreWithCuts( const int UpdateFrequency, const double Fraction ) : 
      m_lastAverageCol( 0 ), m_lastAverageRow( 0 ) ,
      m_fraction( Fraction ) , m_nHit( 0 ), m_nUpdate( UpdateFrequency ) {
      
      initialize();
    }
    
    
    RichHPDImageCentreWithCuts::RichHPDImageCentreWithCuts( )  : 
      m_lastAverageCol( 0 ), m_lastAverageRow( 0 ),
      m_fraction( 0.1 ), m_nHit( 0 ), m_nUpdate( 1000 ) {
      
      initialize();
    }
    
    void RichHPDImageCentreWithCuts::initialize() 
    {
      for ( int icol = 0; icol <  32; ++icol ){
        for ( int irow = 0; irow < 32; ++irow ){
          m_data[icol][irow] = 0;
        }
      } 
      m_nHit = 0;
      return ;
    }
    
    

    RichHPDImageCentreWithCuts::~RichHPDImageCentreWithCuts() {} 
    
    
    const double& RichHPDImageCentreWithCuts::averageCol() const 
    {
      return m_lastAverageCol ;
    }
    
    
    const double& RichHPDImageCentreWithCuts::averageRow() const    
    {
      return m_lastAverageRow ;
    }
    
    
    double  RichHPDImageCentreWithCuts::averagePixelValue() const 
    {
      int sum = 0;
      int occupiedPixels = 0;
      
      for ( int icol = 0; icol < 32; ++icol ){
        for ( int irow = 0; irow < 32 ; ++irow ){
          sum += m_data[icol][irow];
          if ( 0 < m_data[icol][irow] ) occupiedPixels++;
        }
      } 
      
      return ( 1.0*sum )/( 1.0*occupiedPixels );
    }
    
    
    
    bool RichHPDImageCentreWithCuts::update( const int COL , const int ROW ) 
    {
      m_data[COL][ROW]++; 
      m_nHit++;
      
            
      if ( 0 == ( m_nHit % m_nUpdate ) ){
        
        int sumCol = 0;
        int sumRow = 0;
        int nPixels = 0;
        
        double average = averagePixelValue() ;
        double boundary = average*m_fraction ;
        
        for ( int icol = 0; icol < 32 ; ++icol ){
          for ( int irow = 0; irow < 32 ; ++irow ){
            if ( m_data[icol][irow] > boundary ) {
              sumCol += icol ;
              sumRow += irow ;
              nPixels++ ;
            }
          }
        } 
    
        m_lastAverageCol = (1.0*sumCol)/(1.0*nPixels);
        m_lastAverageRow = (1.0*sumRow)/(1.0*nPixels);
        
        initialize() ;
        
        return true ;
      }
      
      return false ;
    }
 
  } // namespace Mon  
} // namespace Rich
