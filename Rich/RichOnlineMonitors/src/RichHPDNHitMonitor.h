// $Id: RichHPDNHitMonitor.h,v 1.18 2010-02-04 12:17:30 ukerzel Exp $
#ifndef RICHHPDNHITMONITOR_H
#define RICHHPDNHITMONITOR_H 1

// Rich Kernel
#include "RichKernel/IRichHPDOccupancyTool.h"
#include "RichKernel/RichMap.h"
#include "RichKernel/RichHistoAlgBase.h"

// Histogramming
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "TH2D.h"
#include "GaudiUtils/Aida2ROOT.h"

// CAMERA
#include "Camera/ICameraTool.h"

// std
#include <deque>
#include <string>
#include <map>
#include <sstream>

// Interfaces
#include "RichKernel/IRichRawBufferToSmartIDsTool.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Monitoring tools
#include "RichMonitoringTools/IHpdUkL1DisableTool.h"

// time
#include <time.h>

namespace Rich
{
  namespace Mon
  {

    /** @class HPDNHitMonitor RichHPDNHitMonitor.h
     *
     *  This algorithm monitors the number of hits seen in each (active) HPD
     *
     *  counters published
     *  ------------------
     *  - per HPD: number of hits in this event
     *  - per HPD: moving average of #hits over the past N events
     *
     *  histograms published
     *  --------------------
     *  In addition, histograms can be published (off by default)
     *  showing the number of hits (inclusively) per HPD, summed over
     *  many events
     *  @author Ulrich Kerzel
     *  @date   2007-04-04
     */
    class HPDNHitMonitor : public Rich::HistoAlgBase
    {

      // ---------------------------------------------------------------------------
      //                                PUBLIC
      // ---------------------------------------------------------------------------
    public:

      /// Standard constructor
      HPDNHitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~HPDNHitMonitor( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode execute   ();    ///< Algorithm execution
      virtual StatusCode finalize  ();    ///< Algorithm finalization

      // ---------------------------------------------------------------------------
      //                                PRIVATE Methods
      // ---------------------------------------------------------------------------
    private:

      std::string GetRichPanelHistId( const Rich::DetectorType rich, 
                                      const Rich::Side panel );
      
      StatusCode  SendErrorReport( const ICameraTool::MessageLevel cameraLevel,
                                   std::multimap<std::string,std::string>& errMap,
                                   const LHCb::ODIN *                      odin = NULL );

      StatusCode  Book2DHisto    ( const Rich::DetectorType rich, 
                                   const Rich::Side panel, 
                                   const std::string& taeEventList );

      /// Load on demand DeRichSystem
      const DeRichSystem * deRichSys() const
      {
        if ( !m_RichSys )
        {
          m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
        }
        return m_RichSys;
      }

      /// Load camera tool on demand
      ICameraTool * cameraTool() const
      {
        if ( !m_CamTool )
        {
          m_CamTool = tool<ICameraTool>("CameraTool");
        }
        return m_CamTool;
      }

      /// Load smartID tool on demand
      const Rich::DAQ::IRawBufferToSmartIDsTool * smartIDDecoder() const
      {
        if ( !m_SmartIDDecoder )
        {
          acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, 0, true );
        }
        return m_SmartIDDecoder;
      }

      /// Load the HPD disabling tool on demand
      Rich::Mon::IHpdUkL1DisableTool * hpdDisableTool() const
      {
        if ( !m_HpdUKL1DisableTool )
        {
          m_HpdUKL1DisableTool =
            tool<Rich::Mon::IHpdUkL1DisableTool>("Rich::Mon::HpdUkL1DisableTool",
                                                 "RichUKL1Disable");
        }
        return m_HpdUKL1DisableTool;
      }

      const Rich::IHPDOccupancyTool * fastHPDOcc() const
      {
        if ( !m_HpdOccupancyToolFast )
        {
          m_HpdOccupancyToolFast = 
            tool<IHPDOccupancyTool>("Rich::HPDOccupancyTool","HPDOccupancyFast"); 
        }
        return m_HpdOccupancyToolFast; 
      }

      const Rich::IHPDOccupancyTool * slowHPDOcc() const
      {
        if ( !m_HpdOccupancyToolSlow )
        {
          m_HpdOccupancyToolSlow = 
            tool<IHPDOccupancyTool>("Rich::HPDOccupancyTool","HPDOccupancySlow"); 
        }
        return m_HpdOccupancyToolSlow; 
      }
      
      /// ODIN info
      std::string odinInfo( const LHCb::ODIN * odin ) const
      {
        std::ostringstream mess;
        if ( odin )
          mess << "ODIN : Run="  << odin->runNumber()
               << " Event="      << odin->eventNumber()
               << " BunchID="    << odin->bunchId()
               << " OrbitNr="    << odin->orbitNumber()
               << " Trigger="    << odin->triggerType()
               << " EvtTime="    << odin->eventTime().format(true,"%X");
        return mess.str();
      }

      void unusualEventSummary();

      // ---------------------------------------------------------------------------
      //                                PRIVATE data
      // ---------------------------------------------------------------------------
    private:

      /// Pointer to RICH system detector element
      mutable const DeRichSystem * m_RichSys;

      /// Raw Buffer Decoding tool
      mutable const Rich::DAQ::IRawBufferToSmartIDsTool * m_SmartIDDecoder;

      mutable Rich::Mon::IHpdUkL1DisableTool            * m_HpdUKL1DisableTool;

      mutable const Rich::IHPDOccupancyTool             * m_HpdOccupancyToolFast;  // fast moving averge, e.g. 50 events
      mutable const Rich::IHPDOccupancyTool             * m_HpdOccupancyToolSlow;  // slow moving average, eg. 500 events

      mutable ICameraTool                       * m_CamTool;         ///< CAMERA error reporting tool

      unsigned long long                      m_nEvts;               ///< Event count

      bool                                    m_FillHistos;          ///< determine if histograms should be filled (or just counter)
      int                                     m_HistoNHitMin;        ///< lower limit of histogram showing inclusive distribution
      int                                     m_HistoNHitMax;        ///< upper

      bool                                    m_ExtendedVerbose;     ///< even more output

      bool                                    m_RemoveFaultyHpdUKL1; ///< use tool to send disable command to UKL1

      unsigned long                           m_HitThreshold;        ///< threshold when an HPD produces too many hits
      Rich::Map<Rich::DAQ::HPDHardwareID, unsigned long> m_NHitMap;  ///< number of hits per hardware ID;

      std::vector<std::string>                m_taeEvents;           ///< The TAE location(s) to monitor

      bool                                    m_AddSnapshotCameraMsg; ///< Send messages to camera for possible snapshots
      bool                                    m_useCamera;           ///< Turn on the sending of any messages to camera

      unsigned long                           m_MovingAverageEvents; ///< for calculating the pull

      //double                                  m_counterAll;          ///< Counter for #hits (to publish)
      double                                  m_counterRich1;
      double                                  m_counterRich2;
      std::string                             m_Name;                ///< Algorithm name, including partition
      double                                  m_PullThreshold;       ///< issue alarm if greater than this
      std::vector<int>                        m_TriggerTypes;        ///< trigger types to monitor (e.g. physics, beam gas, etc)
      int                                     m_UpdateTimerInterval; ///< Send alerts after N seconds
      double                                  m_RichHitRatioMax;     ///< Max RICH2/RICH1 hit ratio to trigger an event snapshot
      double                                  m_RichHitRatioMin;     ///< Min RICH2/RICH1 hit ratio to trigger an event snapshot
      unsigned int                            m_RichMinHitRatio;     ///< Min number of hits for RICH hit ratio to trigger an event snapshot
      unsigned int                            m_minBadHPDPixClusSize; ///< Min size for a 'bad' HPD row/column cluster
      
      int                                     m_CounterUpdateRate; ///< Rate in secs to update counters
      int                                     m_CounterUpdateMinEntries; ///< Min number of entries for an update
      bool                                    m_EnableCounters; ///< Enable counters

      time_t                                  m_TimeStart;
      time_t                                  m_TimeLastUpdate;
      time_t                                  m_TimeLastCounterUpdate;

      std::multimap<std::string,std::string>  m_CameraMessageInfo;
      std::multimap<std::string,std::string>  m_CameraMessageWarning;
      std::multimap<std::string,std::string>  m_CameraMessageError;

      Rich::Map<LHCb::RichSmartID,unsigned long> m_unusualHPDOccCount;
      unsigned long m_unusalOccCount;

      // Cache pointers to histos for speed
      AIDA::IHistogram1D *m_nHits, *m_nHitsR1, *m_nHitsR2;
      AIDA::IHistogram2D *m_2dNhit[Rich::NRiches][Rich::NPDPanelsPerRICH];
      AIDA::IHistogram2D *m_2dPull[Rich::NRiches][Rich::NPDPanelsPerRICH];
      AIDA::IHistogram1D *m_2dHPDHits[Rich::NRiches][Rich::NPDPanelsPerRICH];
      AIDA::IHistogram2D *m_R1R2Corr;

      AIDA::IHistogram1D* _nHitsPhysAndRandom;
      AIDA::IHistogram1D* _nHitsPhysAndRandomRich1;
      AIDA::IHistogram1D* _nHitsPhysAndRandomRich2;

      // Histogram of copy numbers.
      AIDA::IProfile1D* _copyNumHisto;

      AIDA::IProfile1D* _copyNumHistoRich1B; // bottom
      AIDA::IProfile1D* _copyNumHistoRich1T; // top
      AIDA::IProfile1D* _copyNumHistoRich2R; // right
      AIDA::IProfile1D* _copyNumHistoRich2L; // left

    };

  }
}

#endif // RICHHPDNHITMONITOR_H
