#ifndef RICHMON_ERROR_REPORT_H
#define RICHMON_ERROR_REPORT_H

#include "GaudiKernel/Kernel.h" //tmp fix to include ulonglong typedef

#include "Camera/ICameraTool.h"
#include "RichKernel/RichDAQDefinitions.h"


#include <string>

namespace Rich {
  namespace Mon {
    class ErrorReport{
    public:
      ErrorReport() : 
	NrEvents ( 0                 ),
	level    ( ICameraTool::NONE ),
	rich     ( InvalidDetector   ),
	panel    ( InvalidSide       ),
	row      ( 0                 ),
	col      ( 0                 ),
	pixrow   ( 0                 ),
	subpixrow( 0                 ),
	pixcol   ( 0                 ),
	message  ( ""                )
	  { }//ErrorReport()

      unsigned long long NrEvents;
      ICameraTool::MessageLevel level; // ICameraTool::NONE,INFO,WARNING,ERROR
      Rich::DetectorType rich;
      Rich::Side panel;
      int row;
      int col;
      int pixrow;
      int subpixrow;
      int pixcol;
      std::string message;
    };//class ErrorReport
  }//namespace Mon
}//namespace Rich
#endif//RICHMON_ERROR_REPORT_H
