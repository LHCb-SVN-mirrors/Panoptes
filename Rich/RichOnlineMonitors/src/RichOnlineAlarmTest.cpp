// $Id: RichOnlineAlarmTest.cpp,v 1.5 2008-03-27 14:50:53 ukerzel Exp $
// Include files

// local
#include "RichOnlineAlarmTest.h"

using namespace Rich::Mon;


#ifdef WIN32
namespace win {
#include <windows.h>
}
# define mysleep win::Sleep
#else
# define mysleep usleep
#endif

//-----------------------------------------------------------------------------
// Implementation file for class : Rich::Mon::OnlineAlarmTest
//
// 2007-04-05 : Ulrich Kerzel
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( OnlineAlarmTest )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OnlineAlarmTest::OnlineAlarmTest( const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator ),
    m_alarmRate    ( 0                  ),
    m_sleepRate    ( 0                  ),
    m_nEvts        ( 0                  ),
    m_alarmCode    ( 0                  ),
    m_alarmInfo    ( "no alarm"         )
{

  declareProperty("AlarmRate"  , m_alarmRate = 500);
  declareProperty("SleepRate"  , m_sleepRate =   1);
}
//=============================================================================
// Destructor
//=============================================================================
OnlineAlarmTest::~OnlineAlarmTest() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode OnlineAlarmTest::initialize() {
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  debug() << "==> Initialize" << endmsg;

  info() << "alarms will be raised every " << m_alarmRate << " events" << endmsg;

  //
  // publish monitoring info
  //
  // declareInfo("RichTestAlarmCode", m_alarmCode, "test alarm code for RICH online monitoring");
  // declareInfo("RichTestAlarmInfo", m_alarmInfo, "test alarm info for RICH online monitoring");

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode OnlineAlarmTest::execute() {

  ++m_nEvts;

  //
  // reset
  //
  m_alarmCode = 0;
  m_alarmInfo = "no alarm";

  if ( m_nEvts%m_alarmRate == 0) 
  {
    if ( msgLevel(MSG::VERBOSE) )
      verbose() << "raise alarm for event " << m_nEvts << endmsg;
    m_alarmCode = 1;
    std::ostringstream txt;
    txt << "raise alarm for event " << m_nEvts;
    m_alarmInfo = txt.str();
  } // if #Event MOD alarmRate

  mysleep(m_sleepRate);

  return StatusCode::SUCCESS;
}

//=============================================================================
