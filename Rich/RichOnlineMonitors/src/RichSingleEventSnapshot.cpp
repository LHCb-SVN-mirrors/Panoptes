// $Id: RichSingleEventSnapshot.cpp,v 1.20 2010-02-15 18:40:35 hvc Exp $
// Include files

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "RichSingleEventSnapshot.h"

// CAMERA
#include "Camera/ICameraTool.h"

// RICH
#include "RichKernel/RichDAQDefinitions.h"

// histograms

#include "AIDA/IHistogram2D.h"

//-----------------------------------------------------------------------------

using namespace Rich::Mon;
using namespace Rich;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SingleEventSnapshot::SingleEventSnapshot( const std::string& name,
                                          ISvcLocator* pSvcLocator)
  : Rich::Rec::HistoAlgBase ( name , pSvcLocator ),
    m_SmartIDDecoder      ( NULL ),
    m_idTool              ( NULL ),
    m_CamTool             ( NULL ),
    m_nEvt                ( 0 ),
    m_taeEvents           ( 1, "" ),
    m_Name                ( name ),
    m_MinRingPixels       ( 1    ) ,
    m_MinHitRich          ( 1    ) ,
    m_MaxHitRich          ( 300  ) ,
    m_UpdateInterval      (  60  ) ,
    m_nSnapShots          (   0  ) ,
    m_TimeStart           (   0  ) ,
    m_TimeLastUpdate      (   0  ) ,
    m_lastRunNumber       (   0  )
{
  // Job options
  declareProperty( "NBins"            , m_nBins          = 200 );
  declareProperty( "UpdateInterval"   , m_UpdateInterval =  60 );
  declareProperty( "RingLocation"     , m_ringLoc        = LHCb::RichRecRingLocation::MarkovRings+"Isolated" );
  declareProperty( "Message"          , m_message        = "No Message !!!" );
  declareProperty( "RawEventLocations", m_taeEvents             );
  declareProperty( "MinRingPixels"    , m_MinRingPixels  =   1  );
  declareProperty( "MinRings"         , m_MinRings       =   1  );
  declareProperty( "MinHitRich"       , m_MinHitRich     =   1  );
  declareProperty( "MaxHitRich"       , m_MaxHitRich     = 300  );
  declareProperty( "MaxSnapshots"     , m_maxSnapshots   = 100 );

  // Don't send any of these histograms to the presenter
  setProperty( "MonitorHistograms", false );

  // debug
  //setProperty( "OutputLevel", 1 );
}

//=============================================================================
// Destructor
//=============================================================================
SingleEventSnapshot::~SingleEventSnapshot() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode SingleEventSnapshot::initialize()
{
  const StatusCode sc = Rich::Rec::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  //
  // partition name
  //
  const char * partitionName = getenv("PARTITION");
  m_Name = name();
  if ( partitionName ) 
  {
    info() << "running in partition " << partitionName << endmsg;
    m_Name += boost::lexical_cast<std::string>(partitionName);
  } 

  // data decoder
  acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, 0, true );
  // smartid tool
  acquireTool( "RichSmartIDTool",    m_idTool,         0, true );

  // dump configuration
  _ri_debug << "NBins             " << m_nBins          << endmsg;
  _ri_debug << "UpdateInterval    " << m_UpdateInterval << endmsg;
  _ri_debug << "RingLocation      " << m_ringLoc        << endmsg;
  _ri_debug << "Message           " << m_message        << endmsg;
  _ri_debug << "RawEventLocations " << m_taeEvents      << endmsg;
  _ri_debug << "MinRingPixels     " << m_MinRingPixels  << endmsg;
  _ri_debug << "MinRings          " << m_MinRings       << endmsg;
  _ri_debug << "MinHitRich        " << m_MinHitRich     << endmsg;
  _ri_debug << "MaxHitRich        " << m_MaxHitRich     << endmsg;

  //
  // get time of Start-Up
  //
  m_TimeLastUpdate = m_TimeStart = time(NULL);

  // let user know we're here
  _ri_verbo << "Send message to CAMERA" << endmsg;
  cameraTool()->Append("TEXT",m_Name.c_str());
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Initialized");

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode SingleEventSnapshot::execute()
{
  // count events
  ++m_nEvt;

  PD_LOCAL_POSITIONS_X;
  PD_LOCAL_POSITIONS_Y;
  const Rich::HistoID hid;

  //
  // get the ODIN
  //
  const LHCb::ODIN * odin = getIfExists<LHCb::ODIN>( LHCb::ODINLocation::Default );
  if ( !odin )   
  {
    cameraTool()->Clear();
    const std::string m = "No ODIN : Event and Run numbers unknown ...";
    cameraTool()->SendAndClearTS( ICameraTool::WARNING, m_Name.c_str(), m );
    return Warning( m, StatusCode::SUCCESS );
  } // if ODIN
  const unsigned int runNumber = odin->runNumber();
  const ulonglong    evtNumber = odin->eventNumber();
  if ( runNumber != m_lastRunNumber )
  {
    m_lastRunNumber = runNumber;
    m_nSnapShots = 0;
  }

  //
  // get time and check if we should send an update
  //
  const time_t currentTime = time(NULL);
  if ( m_UpdateInterval < 0 ||
       ((currentTime - m_TimeLastUpdate) > m_UpdateInterval) )
  {
    _ri_debug << "Time elapsed - sending snapshots" << endmsg;
  }
  else
  {
    _ri_debug << "Snapshot suppressed -> Too close in time to last" << endmsg;
    cameraTool()->Clear();
    return StatusCode::SUCCESS;
  } 

  //
  // if we got here we should send a snapshot to Camera
  //
 
  unsigned int nRingsRich1(0),nRingsRich2(0),nHitsRich1(0),nHitsRich2(0);

  //
  // rings
  //
  typedef std::vector<float> RingParm;
  typedef std::vector< RingParm > RingParms;
  Rich::Map<Rich::DetectorType,RingParms> ringParams;
  if ( !m_ringLoc.empty() && exist<LHCb::RichRecRings>(m_ringLoc) )
  {
    const LHCb::RichRecRings * rings = get<LHCb::RichRecRings>(m_ringLoc);
    _ri_debug << "Found " << rings->size() << " rings at " << m_ringLoc <<endreq;
    for ( const auto * ring : *rings )
    {
      // which RICH ?
      const Rich::DetectorType ringRich = ring->rich();

      // get the pixels associated to that ring
      const LHCb::RichRecPixelOnRing::Vector& ringPixels = ring->richRecPixels();

      // check if sufficient pixels are on the ring
      if ( ringPixels.size() > m_MinRingPixels )
      {
        // only add ring to list if sufficient hits
        if      ( ringRich == Rich::Rich1 )
        {
          ++nRingsRich1;
        }
        else if ( ringRich == Rich::Rich2 )
        {
          ++nRingsRich2;
        }
      }

      // fit the ring
      Rich::Rec::FastRingFitter fitter;
      // data points (ultimately will be done by fitter itself..)
      for ( const auto& P : ring->richRecPixels() )
      {
        // get pixel from pixelOnRing
        const LHCb::RichRecPixel * pixel = P.pixel();
        // pixel hit
        const Gaudi::XYZPoint & PixelLocal = pixel->localPosition();
        // add (x,y) point to the fitter
        fitter.addPoint( PixelLocal.x(), PixelLocal.y() );
      }
      fitter.fit();
      // add to list of rings to send to camera
      if ( fitter.result().Status == 0 )
      {
        RingParm ringP(4,0);
        ringP[0] = fitter.result().XCenter;
        ringP[1] = fitter.result().YCenter;
        ringP[2] = ringP[3] = fitter.result().Radius;
        ringParams[ring->rich()].push_back(ringP);
      }
    }

    // check if we have enough rings passing the cuts
    if ( nRingsRich1 < m_MinRings || nRingsRich2 < m_MinRings ) 
    {
      std::ostringstream m;
      m << "Too few good rings, Rich1 " << nRingsRich1 << " Rich2 " << nRingsRich2;
      _ri_debug << m.str() << endmsg;
      cameraTool()->Clear();
      return StatusCode::SUCCESS;
    } 

  }

  if ( ++m_nSnapShots > m_maxSnapshots )
  {
    const std::string m = "Max run snapshot limit reached. No more will be sent this run";
    _ri_debug << m << endmsg;
    cameraTool()->Clear();
    return StatusCode::SUCCESS;
  }

  const std::string title =
    ( std::string("Event Snapshot |") +
      std::string(" Run ")   + boost::lexical_cast<std::string>(runNumber) +
      std::string(" Event ") + boost::lexical_cast<std::string>(evtNumber) );

  const std::string ID = std::string("HitMap")+boost::lexical_cast<std::string>(m_nEvt);

  // make the hit maps
  std::map<Rich::DetectorType,AIDA::IHistogram2D *> hitmap;
  for ( const auto& UKL1 : m_SmartIDDecoder->allRichSmartIDs() )
  {
    for ( const auto& Ingress : UKL1.second )
    {
      for ( const auto& HPD : Ingress.second.hpdData() )
      {
        for ( const auto& hit : HPD.second.smartIDs() )
        {
          // pixel hit coord in global coordinates
          Gaudi::XYZPoint gPos;
          const StatusCode scc = m_idTool->globalPosition( hit, gPos );
          if ( scc.isSuccess() ) 
          {
            // pos in local coords
            const Gaudi::XYZPoint lPos = m_idTool->globalToPDPanel(gPos);
            // RICH info
            const Rich::DetectorType rich = hit.rich();
            const std::string & RICH  = Rich::text(rich);
            hitmap[rich] = plot2D( lPos.x(), lPos.y(),
                                   hid(rich,ID),
                                   RICH+" "+title,
                                   xMinPDLoc[rich],xMaxPDLoc[rich],yMinPDLoc[rich],yMaxPDLoc[rich],
                                   m_nBins, m_nBins );
            if ( rich == Rich::Rich1 )
            {
              ++nHitsRich1;
            }
            else if (rich == Rich::Rich2)
            {
              ++nHitsRich2;
            }
          }
          else
          {
            _ri_debug <<  "Problem with SmartID -> global position conversion" << endmsg;
          } // if success
        } // for iHit
      } // for iHPDMap
    } // for iIngressMap
  } // for L1Map

  // now check if we have sufficient (and not too many) hits in each RICH
  if ( ( nHitsRich1 < m_MinHitRich || nHitsRich1 > m_MaxHitRich ) ||
       ( nHitsRich2 < m_MinHitRich || nHitsRich2 > m_MaxHitRich ) ) 
  {
    std::ostringstream m;
    m << "Event failed #hit cuts, min : " << m_MinHitRich
      << " max " << m_MaxHitRich
      << " Rich1 " << nHitsRich1 << " Rich2 " << nHitsRich2;
    _ri_debug << m.str() << endmsg;  
    cameraTool()->Clear();
    return StatusCode::SUCCESS;
  }

  // send to camera
  if ( hitmap[Rich::Rich1] )  
  {
    _ri_verbo << "Send RICH1 hit map to CAMERA" << endmsg;
    cameraTool()->Append( Gaudi::Utils::Aida2ROOT::aida2root(hitmap[Rich::Rich1]) );
    // add RICH1 rings
    for ( auto& R : ringParams[Rich::Rich1] )
    {
      _ri_debug << "Adding RICH1 ring " << R << endreq;
      cameraTool()->Append( "TELLIPSE", &*R.begin(), 4*sizeof(float) );
    }
  }

  if ( hitmap[Rich::Rich2] ) 
  {
    _ri_verbo << "Send RICH2 hit map to CAMERA" << endmsg;
    cameraTool()->Append( Gaudi::Utils::Aida2ROOT::aida2root(hitmap[Rich::Rich2]) );
    // add RICH2 rings
    for ( auto& R : ringParams[Rich::Rich2] )
    {
      _ri_debug << "Adding RICH2 ring " << R << endreq;
      cameraTool()->Append( "TELLIPSE", &*R.begin(), 4*sizeof(float) );
    }
  }

  if ( hitmap[Rich::Rich1] || hitmap[Rich::Rich2] ) 
  {
    _ri_debug << "Send event snapshot" << endmsg;
    cameraTool()->Append("TEXT",title.c_str());
    cameraTool()->Append("TEXT",m_message.c_str());
    std::ostringstream message;
    message << "TAE Events " << m_taeEvents;
    cameraTool()->Append("TEXT",message.str().c_str());
    cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,title.c_str());
    // if we get here, a snapshot was sent so reset timer
    m_TimeLastUpdate = currentTime;
  }
  else
  {
    cameraTool()->Clear();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( SingleEventSnapshot )
