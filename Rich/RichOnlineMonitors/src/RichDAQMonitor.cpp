
// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "RichDAQMonitor.h"

// RICH
#include "RichKernel/RichDAQDefinitions.h"

// CAMERA
#include "Camera/ICameraTool.h"

//#include "GaudiKernel/Kernel.h"
#include "boost/format.hpp"

using namespace Rich::Mon;

//-----------------------------------------------------------------------------
// Implementation file for class : RichDAQMonitor
//
// 2007-04-04 : Ulrich Kerzel
//
// N.B. OK to check against bunchID from ODIN but not eventID
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DAQMonitor::DAQMonitor( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : Rich::HistoAlgBase                 ( name , pSvcLocator ),
    m_RichSys                          ( NULL               ),
    m_SmartIDDecoder                   ( NULL               ),
    m_HpdUKL1DisableTool               ( NULL               ),
    m_CamTool                          ( NULL               ),
    m_nEvts                            (     0              ),
    m_nEvtsLastUpdate                  (     0              ),
    m_TimeLastUpdate                   (     0              ),
    m_Name                             (     name           ),
    m_AlertEmptyEvent                  (     0              ),
    m_AlertActiveHPDs                  (     0              ),
    m_AlertEmptyHPD                    (     0              ),
    m_AlertDuplicateHPDL0              (     0              ),
    m_AlertEventID                     (     0              ),
    m_AlertBxID                        (     0              ),
    m_AlertL0ID                        (     0              ),
    m_AlertInvalidOdinTime             (     0              ),
    m_AlertMissingOdin                 (     0              ),
    m_AlertParityFooter                (     0              ),
    m_AlertHPDInhibit                  (     0              ),
    m_AlertHPDNotInhibitInvalidSmartRef(     0              ),
    m_AlertExtendedHeader              (     0              ),
    m_AlertHPDSuppressed               (     0              ),
    m_SendAlertMessages                (     false          ),
    m_SendAlertDirect                  (     false          ),
    m_AlertLevelBxID                   (     3              )
{
  // Add partition to camera name
  const char* partitionName = getenv("PARTITION");
  if ( partitionName ) { m_Name += std::string(partitionName); }
  
  declareProperty( "ExtendedVerbose"     , m_ExtendedVerbose     = false  );
  declareProperty( "OdinMinGpsTime"      , m_OdinMinGpsTime      =     0  );
  declareProperty( "NL1Boards"           , m_NL1Boards           =    30  );
  declareProperty( "CheckOdin"           , m_CheckOdin           = false  );
  declareProperty( "SendAlertMessages"   , m_SendAlertMessages   = false  );
  declareProperty( "SendAlertDirect"     , m_SendAlertDirect     = false  );
  declareProperty( "AlertLevelBxID"      , m_AlertLevelBxID      =     3  );
  declareProperty( "RemoveFaultyHpdUKL1" , m_RemoveFaultyHpdUKL1 = false  );
  declareProperty( "UpdateTimerInterval" , m_UpdateTimerInterval =   600  );
  declareProperty( "CheckEmptyHPD"       , m_CheckEmptyHPD       =  true  );
  declareProperty( "MonitorBxID"         , m_MonitorBxID         =  true  );
  declareProperty( "MaxErrorMessages"    , m_MaxErrorMessages    =   500  );
  declareProperty( "Plot2DHisto"         , m_Plot2DHisto         = false  );
  declareProperty( "PrintMessages"       , m_PrintMessages       = true   );
  declareProperty( "TAELocation"         , m_TaeLocation         =   ""   );
  declareProperty( "AddSnapshotCameraMsg", m_AddSnapshotCameraMsg = false );
  declareProperty( "MinErrorRate"        , m_MinErrorRate        = 2      );
  declareProperty( "VetoNoBias"          , m_vetoNoBias          = true   );
  // setProperty("OutputLevel",1);
}

//=============================================================================
// Destructor
//=============================================================================
DAQMonitor::~DAQMonitor() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode DAQMonitor::initialize()
{
  const StatusCode sc = Rich::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  // partition name
  const char* partitionName = getenv("PARTITION");

  // add TAE location to algorithm name
  // CRJ _ Already in the name from the python options
  //m_Name += ( m_TaeLocation.empty() ? "Central" : m_TaeLocation );

  // Alert levels
  if      ( m_AlertLevelBxID < 1 ) { m_AlertLevelBxID = 1; }
  else if ( m_AlertLevelBxID > 3 ) { m_AlertLevelBxID = 3; }

  // Get SmartIDs of all active HPDs which should be monitored
  // The list is obtained from DeRichSystem which gets it from the
  // ConditionDB for a given run period
  const LHCb::RichSmartID::Vector & activeHPDs = deRichSys()->activePDRichSmartIDs();

  // vector<int> of hardwareIDs of all monitored HPDs for histogram showing which HPD has hits
  std::vector<int> hardIDVec;
  hardIDVec.reserve(activeHPDs.size());
  for ( LHCb::RichSmartID::Vector::const_iterator iHPD = activeHPDs.begin();
        iHPD != activeHPDs.end(); ++iHPD )
  {
    hardIDVec.push_back( (deRichSys()->hardwareID(*iHPD)).data() );
  }// for iHPD

  // sort vector
  std::stable_sort( hardIDVec.begin(), hardIDVec.end() );

  if (m_ExtendedVerbose)
  {
    for ( std::vector<int>::const_iterator iHardID = hardIDVec.begin();
          iHardID != hardIDVec.end(); ++iHardID )
    {
      verbose() << "monitor HPD with hardware ID " << *iHardID << endmsg;
    }//for iHardID
  }//if

  if ( msgLevel(MSG::VERBOSE) )
  {
    verbose() << "smallest hardwareID " << hardIDVec.front()
              << " largest " << hardIDVec.back()
              << endmsg;
  }

  //
  // pre-book histograms
  //

  m_odinH = book1D( "TriggerType", "TriggerType-" + m_Name, -0.5, 7.5, 8 );

  m_ErrorPerHardIDH = book1D("ErrorPerHardID", "ErrPerHardwareID-" + m_Name,
                             *(hardIDVec.begin())-0.5, *(hardIDVec.end()-1)+0.5, hardIDVec.size()+1);

  m_HitPerHardIDH = book1D("HitPerHardID", "HitPerHardwareID-" + m_Name,
                           *(hardIDVec.begin())-0.5, *(hardIDVec.end()-1)+0.5, hardIDVec.size()+1);

  m_ErrorPerUKL1BoardH = book1D("ErrorPerUKL1Board", "ErrPerUKL1Board-" + m_Name,
                                -0.5, m_NL1Boards+0.5, m_NL1Boards+1);

  m_TruncatedEventUKL1 = book1D("TruncatedEventUKL1", "TruncatedEventUKL1-" + m_Name,
                                -0.5, m_NL1Boards+0.5, m_NL1Boards+1);

  if (m_Plot2DHisto)
  {
    book2D( "DAQErrorRich1Side0" , "DAQErrorRich1Side0" , 0, 9, 9, 0, 16, 16);
    book2D( "DAQErrorRich1Side1" , "DAQErrorRich1Side1" , 0, 9, 9, 0, 16, 16);

    book2D( "DAQErrorRich2Side0" , "DAQErrorRich2Side0" , 0, 9, 9, 0, 16, 16);
    book2D( "DAQErrorRich2Side1" , "DAQErrorRich2Side1" , 0, 9, 9, 0, 16, 16);
  } //if

  //
  // pre-book counters
  //

  // alert PVSS if empty event found
  StatEntity &alertA = counter("AlertEmptyEvent");
  StatEntity &alertB = counter("AlertActiveHPDs");
  StatEntity &alertC = counter("AlertEventID");
  StatEntity &alertD = counter("AlertBxID");
  StatEntity &alertE = counter("AlertL0ID");
  StatEntity &alertF = counter("AlertInvalidOdinTime");
  StatEntity &alertG = counter("AlertParityFooter");
  StatEntity &alertH = counter("AlertHPDInhibit");
  StatEntity &alertI = counter("AlertHPDNotInhibitInvalidSmartRef");
  StatEntity &alertJ = counter("AlertExtendedHeader");
  StatEntity &alertK = counter("AlertHPDSuppressed");
  StatEntity &alertL = counter("AlertEmptyHPD");

  //
  // print information about steering options
  //
  if ( msgLevel(MSG::INFO) )
  {
    if (partitionName)
      info() << "Running in partition " << std::string(partitionName) << endmsg;

    info() << "sent alerts at " << m_UpdateTimerInterval << " seconds interval "
           << "or when " << m_MaxErrorMessages << " are reached" << endmsg;

    info() << "alert-level for BxID warnings " << m_AlertLevelBxID << endmsg;

    info() << "check with ODIN bank          " << m_CheckOdin      << endmsg;

    info() << "monitor BxIDs                 " << m_MonitorBxID    << endmsg;

    info() << "Plot 2D histograms            " << m_Plot2DHisto    << endmsg;

    info() << "TAE location ("" = central)   " << m_TaeLocation    << endmsg;

    info() << "Print Messages                " << m_PrintMessages  << endmsg;

    info() << "number of currently active HPDs (from ConditionDB) "
           << activeHPDs.size() << endmsg;
  }


  if (m_ExtendedVerbose)
  {
    verbose() << "alert counters" << endmsg;
    verbose() << alertA.print() << endmsg;
    verbose() << alertB.print() << endmsg;
    verbose() << alertC.print() << endmsg;
    verbose() << alertD.print() << endmsg;
    verbose() << alertE.print() << endmsg;
    verbose() << alertF.print() << endmsg;
    verbose() << alertG.print() << endmsg;
    verbose() << alertH.print() << endmsg;
    verbose() << alertI.print() << endmsg;
    verbose() << alertJ.print() << endmsg;
    verbose() << alertK.print() << endmsg;
    verbose() << alertL.print() << endmsg;
  }

  //
  // get Time of startup
  //
  m_TimeLastUpdate = time(NULL);

  // create disabling tool
  // must do this to make sure histograms it creates are ready for the presenter
  hpdDisableTool();

  //
  // let user know we're here
  //
  cameraTool()->Append("TEXT",m_Name.c_str());
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Initialized");

  return sc;
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode DAQMonitor::finalize()
{
  const StatusCode sc = SendErrorSummary();
  if ( sc.isFailure() ) 
  {
    return Warning("Error sending StatusReport",sc);
  }

  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Finalized");

  return Rich::HistoAlgBase::finalize();
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DAQMonitor::execute()
{

  // by default no events are 'selected'
  // only bad events should be selected
  setFilterPassed(false);

  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "==> Execute for event " << m_nEvts << endmsg;

  //
  // check if ODIN information is available
  //
  const LHCb::ODIN * odin = getIfExists<LHCb::ODIN>( LHCb::ODINLocation::Default );
  if ( odin )
  {
    if ( msgLevel(MSG::VERBOSE) )
    {
      verbose() << " ODIN event time " << odin->eventTime()
                << " GPS time "        << odin->gpsTime ()
                << " runNr "           << odin->runNumber ()
                << " orbit Nr "        << odin->orbitNumber()
                << " event type "      << odin->eventType()
                << " event number "    << odin->eventNumber()
                << " bunch ID "        << odin->bunchId()
                << " detector status " << odin->detectorStatus ()
                << endmsg;
      verbose() << "EventTime "
                << odin->eventTime().year(true)    << "-"
                << odin->eventTime().month(true)+1 << "-"
                << odin->eventTime().day(true)     << " "
                << odin->eventTime().hour(true)    << "h"
                << odin->eventTime().minute(true)  << "m"
                << odin->eventTime().second(true)  << "s"
                << odin->eventTime().nsecond()     << endmsg;
      verbose() << "ODIN info string " << odinInfo(odin) << endmsg;
    }
  }
  else
  {
    ++counter("MissingOdin");
    ++m_AlertMissingOdin;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,"ODIN bank missing");
    verbose() << "ODIN bank missing" << endmsg;
    return StatusCode::SUCCESS;
  }
  
  // count events
  ++m_nEvts;

  // First event printout
  if ( UNLIKELY( m_nEvts == 1 ) )
  {
    const std::string m = "First event seen";
    info() << m << endmsg;
    cameraTool()->Append("TEXT",odinInfo(odin).c_str());
    cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,m);
    // set last time to now.
    m_TimeLastUpdate = time(NULL);
  }

  // Monitor for new runs
  static unsigned long int lastRunNumber = 0;
  const unsigned long int RunNumber = odin->runNumber();
  if ( UNLIKELY( RunNumber != lastRunNumber ) )
  {
    lastRunNumber = RunNumber;
    std::ostringstream m;
    m << "=============> New Run " << RunNumber << " <=============";
    info() << m.str() << endmsg;
    cameraTool()->Append("TEXT",odinInfo(odin).c_str());
    cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,m.str());
  }

  // Veto NoBias
  if ( m_vetoNoBias )
  {
    const Gaudi::Math::Bit< unsigned int, 2 > bit{};
    if ( bit(odin->eventType()) ) { return StatusCode::SUCCESS; }
  }

  // cached messages to add to camera, for inclusion in a possible event snapshot
  std::vector<std::string> snapshotMessages;

  // map all L0IDs encountered, check for occasions with >1 entry
  std::map<Rich::DAQ::Level0ID,int> l0MapHPD;  

  // Event type histogram
  m_odinH->fill( odin->triggerType() );
  
  // count which triggers have been sent
  ++m_TriggerTypeMap[odin->triggerType()];
  
  // count event types
  ++m_EventTypeMap[odin->eventType()];
  
  // check ODIN time
  if ( odin->gpsTime() < m_OdinMinGpsTime )
  {
    info() << "invalid ODIN GPS time " << odin->gpsTime() << " min. time accepted " << m_OdinMinGpsTime
           << " -> skip event " << endmsg;
    ++counter("AlertInvalidOdinTime");
    ++m_AlertInvalidOdinTime;
    if (m_SendAlertDirect)
    {
      cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,"Invalid ODIN time");
    }// sendAlert
    // return StatusCode::SUCCESS;
  }//if gpsTime

  const time_t currentTime = time(NULL);

  //
  // check if an error report should be sent
  // -> elapsed time
  //
  bool sendErrorReport = false;
  const time_t deltaT = (currentTime - m_TimeLastUpdate);
  if ( m_UpdateTimerInterval > 0 && deltaT >= m_UpdateTimerInterval )
  {
    struct tm * timeinfo = localtime ( &deltaT );
    std::ostringstream messageString;
    const unsigned long long nNewEvts = m_nEvts-m_nEvtsLastUpdate;
    messageString << "Seen " << nNewEvts << " events in past ";
    if ( timeinfo->tm_hour-1 > 0 ) { messageString << timeinfo->tm_hour-1 << " hours "; }
    if ( timeinfo->tm_min    > 0 ) { messageString << timeinfo->tm_min << " mins "; }
    if ( timeinfo->tm_sec    > 0 ) { messageString << timeinfo->tm_sec << " secs "; }
    messageString << "( " << (double)(nNewEvts) / (double)(deltaT) << " Evt/s )";

    // add number of each trigger type
    if ( !m_TriggerTypeMap.empty() )
    {
      std::ostringstream tString;
      tString << "Triggers -";
      for ( const auto& trig : m_TriggerTypeMap )
      {
        tString << " [ " << trig.second << " " << trig.first << " ]";
      }
      m_CamTool->Append("TEXT",tString.str().c_str());
      // clear for next set of messages
      m_TriggerTypeMap.clear();
    }

    // add number of each trigger type
    if ( !m_EventTypeMap.empty() )
    {
      std::ostringstream tString;
      tString << "EventTypes -";
      for ( const auto& evt : m_EventTypeMap )
      {
        tString << " [ " << evt.second << " " << evt.first << " ]";
      }
      m_CamTool->Append("TEXT",tString.str().c_str());
      // clear for next set of messages
      m_EventTypeMap.clear();
    }

    if ( msgLevel(MSG::DEBUG) )
      debug() << messageString.str() << endmsg;
    cameraTool()->SendAndClearTS(ICameraTool::INFO, m_Name, messageString.str());

    m_TimeLastUpdate  = currentTime;
    sendErrorReport   = true;
    m_nEvtsLastUpdate = m_nEvts;
  }

  // -> too many messages, send error report now
  if ( !sendErrorReport && 
       ( m_AlertMessagesActiveHPD.size()                    >= m_MaxErrorMessages ||
         m_AlertMessagesEventID.size()                      >= m_MaxErrorMessages ||
         m_AlertMessagesBxID.size()                         >= m_MaxErrorMessages ||
         m_AlertMessagesL0ID.size()                         >= m_MaxErrorMessages ||
         m_AlertMessagesHPDInhibit.size()                   >= m_MaxErrorMessages ||
         m_AlertMessagesParityFooter.size()                 >= m_MaxErrorMessages ||
         m_AlertMessagesHPDNotInhibitInvalidSmartRef.size() >= m_MaxErrorMessages ||
         m_AlertMessagesExtendedHeader.size()               >= m_MaxErrorMessages ||
         m_AlertMessagesHPDSuppressed.size()                >= m_MaxErrorMessages ||
         m_AlertMessagesEmptyHPD.size()                     >= m_MaxErrorMessages ) )
  {

    if  ( msgLevel(MSG::VERBOSE) )
    {
      verbose() << "Number of messages so far " << endmsg;
      verbose() << "#Messages ActiveHPD       " <<   m_AlertMessagesActiveHPD.size()                     << endmsg;
      verbose() << "#Messages EventID         " <<   m_AlertMessagesEventID.size()                       << endmsg;
      verbose() << "#Messages BxID            " <<   m_AlertMessagesBxID.size()                          << endmsg;
      verbose() << "#Messages L0ID            " <<   m_AlertMessagesL0ID.size()                          << endmsg;
      verbose() << "#Messages HPDInhibit      " <<   m_AlertMessagesHPDInhibit.size()                    << endmsg;
      verbose() << "#Messages Parity Footer   " <<   m_AlertMessagesParityFooter.size()                  << endmsg;
      verbose() << "#Messages InvalidSmartRef " <<   m_AlertMessagesHPDNotInhibitInvalidSmartRef.size()  << endmsg;
      verbose() << "#Messages Extended Header " <<   m_AlertMessagesExtendedHeader.size()                << endmsg;
      verbose() << "#Messages HPDSuppressed   " <<   m_AlertMessagesHPDSuppressed.size()                 << endmsg;
      verbose() << "#Messages Empty HPD       " <<   m_AlertMessagesEmptyHPD.size()                      << endmsg;
    } // if msglevel

    const time_t deltaT = (currentTime - m_TimeLastUpdate);

    if ( deltaT >= m_MinErrorRate )
    {
      struct tm * timeinfo = localtime ( &deltaT );
      std::ostringstream messageString;
      messageString << "Too many errors in past ";
      if ( timeinfo->tm_hour-1 > 0 ) { messageString << timeinfo->tm_hour-1 << " hours "; }
      if ( timeinfo->tm_min    > 0 ) { messageString << timeinfo->tm_min << " mins "; }
      if ( timeinfo->tm_sec    > 0 ) { messageString << timeinfo->tm_sec << " secs "; }
      info() << messageString.str() << endmsg;
      cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name, messageString.str());
      m_TimeLastUpdate  = currentTime;
      sendErrorReport   = true;
      m_nEvtsLastUpdate = m_nEvts;
    }
    else
    {
      // error rate too high to send to camera, so just reset
      //ResetCounters();
    }

  }

  // create Rich eventID from ODIN eventID
  const Rich::DAQ::EventID odinEventNr(odin->eventNumber());
  const Rich::DAQ::BXID    odinBxID   (odin->bunchId());
  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Created ODIN eventID " << odinEventNr
              << " BxID "           << odinBxID
              << endmsg;

  //
  // main loop - get all RICH SmartIDs corresponding to HPDs/hits
  //

  int nAllHits = 0;  // number of hits seen in total so far in this event
  const Rich::DAQ::L1Map & l1Map = smartIDDecoder()->allRichSmartIDs(m_TaeLocation);
  if  ( msgLevel(MSG::VERBOSE) )
    verbose() << "L1 map has size " << l1Map.size() << endmsg;

  // Use this ostringstream to create error messages that are sent to CAMERA and the message streams if the level is appropriate.
  std::ostringstream errStream;
  // In order to clear the error stream between use we need to write an empty string to its buffer via the .str() method.
  // This is the only way. .str().erase() and .str().clear() only operate on copies of the buffer. Create an empty string
  // to use each time. Saves repeatedly constructing one.
  const std::string emptyString("");

  for ( Rich::DAQ::L1Map::const_iterator iL1Map = l1Map.begin(); iL1Map != l1Map.end(); ++iL1Map )
  {

    const Rich::DAQ::Level1HardwareID     l1HardID        = iL1Map->first;
    const Rich::DAQ::Level1LogicalID      l1LogicalID     = deRichSys()->level1LogicalID(l1HardID);
    const Rich::DAQ::IngressMap&          ingressMap      = iL1Map->second;
    const Rich::DetectorType              richDetector    = deRichSys()->richDetector(l1HardID);

    bool UKL1HasSuppressedIngress = false;

    if  ( msgLevel(MSG::VERBOSE) )
      verbose() << " L1 board (hardwareID)" << l1HardID << " with value " << l1HardID.data()
                << " detector " << Rich::text(richDetector)
                << " L1 logical ID " << l1LogicalID
                << " IngressMap has size " << ingressMap.size() << endmsg;

    for ( Rich::DAQ::IngressMap::const_iterator iIngressMap = ingressMap.begin();
          iIngressMap != ingressMap.end(); ++iIngressMap)
    {

      const Rich::DAQ::L1IngressID & l1IngressID = iIngressMap->first;
      const Rich::DAQ::IngressInfo & ingressInfo = iIngressMap->second;

      //
      // check Ingress Header information
      //
      const Rich::DAQ::L1IngressHeader &ingressHeader = ingressInfo.ingressHeader();
      if  ( msgLevel(MSG::VERBOSE) )
      {
        verbose() << "IngressHeader Event ID " << ingressHeader.eventID()
                  << " BX ID "                 << ingressHeader.bxID()
                  << " IngressID "             << l1IngressID
                  << " (data "                 << ingressHeader.data() << " ) "
                  << " L1 board hardware ID "  << l1HardID
                  << " L1 board logical ID "   << l1LogicalID
                  << endmsg;
        verbose() << "   activeHPDbits "     << ingressHeader.activeHPDbits()
                  << " #activeHPDs "         << ingressHeader.numActiveHPDs()
                  << " all hpdsSuppressed "  << ingressHeader.hpdsSuppressed()
                  << endmsg;
      } // if msgLevel

      if ( ingressHeader.hpdsSuppressed() )
      {
        // HPD data (i.e. hits) are suppressed in the UKL1 for this ingress
        // in this (multi-) event packet. The header information is still there,
        // contining the HPD information which would have sent data had the UKL1
        // not removed the hits
        UKL1HasSuppressedIngress = true;
        errStream.str(emptyString);
        errStream << " L1[" << l1LogicalID << " , " << l1IngressID << "] " << richDetector;

        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "HPDs suppressed " << errStream.str() << endmsg;

        //if (m_PrintMessages)
        //  info() << "HPD suppressed (truncated flag set) "  << errStream.str() << endmsg;
        ++counter("AlertHPDSuppressed");
        ++m_AlertHPDSuppressed;
        ++m_AlertMessagesHPDSuppressed[errStream.str()];
        m_ODINLastError[errStream.str()] = odinInfo(odin);
        if (m_SendAlertDirect)
        {
          cameraTool()->Append("TEXT",errStream.str());
          cameraTool()->Append("TEXT",odinInfo(odin));
          cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,"HPD Suppressed");
          setFilterPassed(true);
          // Save messages for snapshots
          snapshotMessages.push_back( errStream.str() );
        }// sendAlert

        // no futher HPD hits for this ingress if HPDs are suppresed in the UKL1
        continue;
      } // if HPD suppressed

      // check ODIN information - EventID
      if ( m_CheckOdin )
      {

        if  ( msgLevel(MSG::VERBOSE) )
          verbose()  <<
            " BX info: Ingress ID " << l1IngressID           <<
            " Bx ID "               << ingressHeader.bxID()  <<
            " ODIN: "               << odinBxID              <<
            endmsg;

        // check ODIN information - bunch crossing ID
        if ( m_MonitorBxID )
        {

          if ( msgLevel(MSG::VERBOSE) )
            verbose()  << "check BX ID" << endmsg;

          if ( ingressHeader.bxID() != odinBxID )
          {

            errStream.str(emptyString);
            errStream << " L1HardwareID " << l1HardID
                      << " L1LogicalID[" << l1LogicalID << " , " << l1IngressID << "] " 
                      << " Bx ID "      << ingressHeader.bxID()  
                      << " ODIN: "      << odinBxID;

            if  ( msgLevel(MSG::VERBOSE) )
              verbose() << "BunchID mis-match " << errStream.str() << endmsg;

            //if (m_PrintMessages)
            //  info() << "BunchID mis-match " << errStream.str() << endmsg;
            ++counter("AlertBxID");
            m_ErrorPerUKL1BoardH->fill(l1HardID.data());

            ++m_AlertBxID;
            ++counter("AlertBxID");
            ++m_AlertMessagesBxID[errStream.str()];
            m_ODINLastError[errStream.str()] = odinInfo(odin);

            if (m_SendAlertDirect)
            {
              cameraTool()->Append("TEXT",errStream.str().c_str());
              cameraTool()->Append("TEXT",odinInfo(odin).c_str());
              cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,"BxID mismatch");
            }// sendAlert
          }
          else
          {
            if ( msgLevel(MSG::VERBOSE) )
              verbose() << "BunchID OK " << endmsg;
          }// if BxID
        } // if MonitorBxID
      } // if checkOdin


      const Rich::DAQ::HPDMap & hpdMap = ingressInfo.hpdData();
      if ( msgLevel(MSG::VERBOSE) )
        verbose() << "HPD map has size " << hpdMap.size() << endmsg;

      int nInhibitedHPDs = 0;

      for ( Rich::DAQ::HPDMap::const_iterator iHPDMap = hpdMap.begin();
            iHPDMap != hpdMap.end(); ++iHPDMap )
      {

        const Rich::DAQ::Level1Input          l1Input        = iHPDMap->first;
        const Rich::DAQ::HPDInfo&             hpdInfo        = iHPDMap->second;

        const Rich::DAQ::L1InputWithinIngress l1InputIngress = l1Input.l1InputWithinIngress();
        const LHCb::RichSmartID&              smartIDHPD     = hpdInfo.hpdID();

        // // TEMPORARY - Skip 4 inhibited HPDs. To be removed at Sept TS
        // //============================================================
        // if ( richDetector   == Rich::Rich1 &&
        //      l1LogicalID    == Rich::DAQ::Level1LogicalID(8) &&
        //      l1InputIngress == Rich::DAQ::L1InputWithinIngress(0) )
        // {
        //   continue;
        // }
        // //============================================================

        // make HPD data object for disabling tool
        const IHpdUkL1DisableTool::HPDData hpdData(richDetector,l1LogicalID,l1IngressID,l1InputIngress);

        if ( hpdInfo.header().inhibit() )
        {
          const std::string reason = "HPD Inhibited - Possible front-end (L0) data link malfunction or front-end not powered";
          errStream.str(emptyString);
          errStream << reason << " " << hpdData;
          if ( msgLevel(MSG::VERBOSE) ) 
          {
            verbose() << errStream.str() << endmsg;
          }
          ++m_AlertMessagesHPDInhibit[errStream.str()];
          m_ODINLastError[errStream.str()] = odinInfo(odin);
          ++counter("AlertHPDInhibit");
          hpdDisableTool() -> ReportHPD( hpdData, reason );
          m_ErrorPerUKL1BoardH->fill(l1HardID.data());
        } //inhibited HPD

        // normal: invalid SmartID but inhibited HPD:
        if ( !smartIDHPD.isValid() && hpdInfo.header().inhibit() )
        {
          if  ( msgLevel(MSG::VERBOSE) )
          {
            verbose() << "HPD SmartID invalid and HPD inhibited"
                      << " logical L1 ID  "   << l1LogicalID
                      << " L1 Input Ingress " << l1InputIngress
                      << " L1 Ingress ID "    << l1IngressID
                      << " detector "         << Rich::text(richDetector)
                      << endmsg;
          } //verbose
        } //if invalid SmartID

        // not OK: invalid SmartID but not inhibited HPD
        if ( !smartIDHPD.isValid() && !hpdInfo.header().inhibit() )
        {
          const std::string reason = "Invalid front-end (L0) hardware ID in header - probable front-end upset";

          // Create a stream to use to build up the message which is sent to the error stream and CAMERA.
          errStream.str(emptyString);
          errStream << reason << " " << hpdData;
          if (m_PrintMessages)
            Error(errStream.str());
          if (msgLevel(MSG::DEBUG) )
            debug() << "ODIN info string " << odinInfo(odin) << endmsg;

          hpdDisableTool() -> ReportHPD(hpdData,reason);

          if ( msgLevel(MSG::VERBOSE) ) verbose() << errStream.str() << endmsg;

          ++m_AlertHPDNotInhibitInvalidSmartRef;
          ++counter("AlertHPDNotInhibitInvalidSmartRef");
          ++m_AlertMessagesHPDNotInhibitInvalidSmartRef[errStream.str()];
          m_ODINLastError[errStream.str()] = odinInfo(odin);
          if (m_SendAlertDirect)
          {
            cameraTool()->Append("TEXT",errStream.str().c_str());
            cameraTool()->Append("TEXT",odinInfo(odin).c_str());
            cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,reason);
          }// sendAlert
          setFilterPassed(true);
          // Save messages for snapshots
          snapshotMessages.push_back( errStream.str() );

          // disable this HPD in the UKL1 stream
          if (m_RemoveFaultyHpdUKL1)
          {
            errStream.str(emptyString);
            errStream << reason << " " << hpdData;
            //if (m_PrintMessages)
            //  info() << errStream.str() << endmsg;// << endmsg;
            if (m_SendAlertDirect)
            {
              cameraTool()->Append("TEXT",errStream.str().c_str());
              cameraTool()->Append("TEXT",odinInfo(odin).c_str());
              cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,"Request HPD disable");
            } //sendAlert
            setFilterPassed(true);
            hpdDisableTool() -> DisableHPD( hpdData, reason );
            // Save messages for snapshots
            snapshotMessages.push_back( errStream.str() );
          } //if m_RemoveFaultyHpdUKL1

        } //if invalid SmartID

        // skip if issue detected
        if ( !smartIDHPD.isValid() || hpdInfo.header().inhibit() )
        {
          verbose() << "skip this HPD" << endmsg;
          continue;
        } // valid or inhibited

        const Rich::DAQ::HPDHardwareID   hardID      = deRichSys()->hardwareID(smartIDHPD); // corresponding hardware ID
        const Rich::DAQ::Level0ID        l0ID        = deRichSys()->level0ID(smartIDHPD);   //               L0
        const LHCb::RichSmartID::Vector &hitSmartIDs = hpdInfo.smartIDs();                // vector of SmartIDs with hits
        const int                        nHits       = hpdInfo.smartIDs().size();

        ++l0MapHPD[l0ID];

        m_HitPerHardIDH->fill(hardID.data());

        //
        // check HPD header and footer
        //

        if  ( msgLevel(MSG::VERBOSE) )
        {
          verbose() << "HPD with SmartID " << smartIDHPD  << " L0 ID "      << hpdInfo.header().l0ID()
                    << " eventID " << hpdInfo.header().eventID() << " ALICE mode " << hpdInfo.header().aliceMode()
                    << " inhibit " << hpdInfo.header().inhibit() << " zero-supp. " << hpdInfo.header().zeroSuppressed()
                    << " #hits "   << nHits
                    << endmsg;
        }//if verbose

        //
        // check header
        //

        // check if extended information is available - if yes,
        // likely to be an error as this is added by the UKL1
        // if it detects inconsistencies
        if ( hpdInfo.header().extendedFormat() )
        {
          ++m_AlertExtendedHeader;
          ++counter("AlertExtendedHeader");
          const Rich::DAQ::L0Header l0Header       = hpdInfo.header().l0Header();
          const Rich::DAQ::L0Header::Word0 l0Word0 = l0Header.word0();
          const Rich::DAQ::L0Header::Word1 l0Word1 = l0Header.word1();

          if (m_PrintMessages)
            Error("HPD with extended information, L0ID " + boost::lexical_cast<std::string>( hpdInfo.header().l0ID()));
          if (msgLevel(MSG::DEBUG) )
            debug() << "ODIN info string " << odinInfo(odin) << endmsg;

          const std::string reason = "Extended HPD Header";

          errStream.str(emptyString);
          errStream << reason << " "
                    << boost::format(" parity %x") % hpdInfo.footer().parityWord()
                    << " ALICE mode "       << hpdInfo.header().aliceMode()
                    << " inhibit "          << hpdInfo.header().inhibit()
                    << boost::format(" L0 header word 0 %x ")  % l0Word0.data()
                    << boost::format(" L0 header word 1 %x ")  % l0Word1.data()
                    << " L0 header " << l0Header
                    << hpdData << ":" << " L0ID[" << l0ID << "]";

          if ( msgLevel(MSG::VERBOSE) ) verbose() << errStream.str() << endmsg;

          ++m_AlertMessagesExtendedHeader[errStream.str()];
          m_ODINLastError[errStream.str()] = odinInfo(odin);
          hpdDisableTool() -> ReportHPD(hpdData,reason );
          m_ErrorPerHardIDH->fill(hardID.data());
          m_ErrorPerUKL1BoardH->fill(l1HardID.data());
          if (m_SendAlertDirect)
          {
            cameraTool()->Append("TEXT",errStream.str());
            cameraTool()->Append("TEXT",odinInfo(odin));
            cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,reason);
            setFilterPassed(true);
            // Save messages for snapshots
            snapshotMessages.push_back( errStream.str() );
          }// sendAlert
          if (m_PrintMessages)
            Error(errStream.str());

          // disable this HPD in the UKL1 stream
          if (m_RemoveFaultyHpdUKL1)
          {
            errStream.str(emptyString);
            errStream << reason << " "
                      << " SmartID "          << smartIDHPD << " "
                      << hpdData << ":" << " L0ID[" << l0ID << "]";
            //if (m_PrintMessages)
            //  info()    << errStream.str() << endmsg;
            if (m_SendAlertDirect)
            {
              cameraTool()->Append("TEXT",errStream.str().c_str());
              cameraTool()->Append("TEXT",odinInfo(odin).c_str());
              cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,"Request HPD disable");
            } //sendAlert
            setFilterPassed(true);
            // Save messages for snapshots
            snapshotMessages.push_back( errStream.str() );
            hpdDisableTool() -> DisableHPD( hpdData, reason );
          } //if m_RemoveFaultyHpdUKL1

          if (m_Plot2DHisto)
            DAQMonitor::PlotErrorHPD(smartIDHPD);
        }//if extendedHeader

        // check for empty HPD
        if ( nHits == 0                         &&
             !hpdInfo.header().inhibit()        &&
             !hpdInfo.header().extendedFormat() &&
             !hpdInfo.header().aliceMode()      )
        {
          const std::string reason = "Empty HPD without extended header";
          ++m_AlertEmptyHPD;
          ++counter("AlertEmptyHPD");
          errStream.str(emptyString);
          errStream << reason << " "
                    << " SmartID "          << smartIDHPD << " "
                    << hpdData << ":" << " L0ID[" << l0ID << "]";
          //if (m_PrintMessages)
          //  info() << errStream.str() << endmsg;

          if ( msgLevel(MSG::VERBOSE) ) verbose() << errStream.str() << endmsg;

          ++m_AlertMessagesEmptyHPD[errStream.str()];
          m_ODINLastError[errStream.str()] = odinInfo(odin);
          hpdDisableTool() -> ReportHPD(hpdData, reason );
          m_HitPerHardIDH->fill(hardID.data());
          m_ErrorPerUKL1BoardH->fill(l1HardID.data());
          if (m_SendAlertDirect)
          {
            cameraTool()->Append("TEXT",errStream.str().c_str());
            cameraTool()->Append("TEXT",odinInfo(odin).c_str());
            cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,"Empty HPD");
          }// sendAlert
          setFilterPassed(true);
          // Save messages for snapshots
          snapshotMessages.push_back( errStream.str() );
        } //if nHits

        if  ( msgLevel(MSG::VERBOSE) )
          verbose() << "Check EventID ODIN " << odinEventNr
                    << " HPD header "        << hpdInfo.header().eventID()
                    << " Ingress header "    << ingressHeader.eventID()
                    << endmsg;

        if ( hpdInfo.header().eventID() != ingressHeader.eventID() )
          //odinEventNr                != ingressHeader.eventID() )
        {
          const std::string reason = "EventID mis-match between ODIN, HPD and ingress headers";

          errStream.str(emptyString);
          errStream
            << "ODIN EventID: " << odinEventNr
            << " , HPD EventID: "     << hpdInfo.header().eventID()
            << " , Ingress EventID: " << ingressHeader.eventID() 
            << hpdData << ":" << " L0ID[" << l0ID << "]";

          if ( msgLevel(MSG::VERBOSE) ) 
          {
            verbose() << reason << endmsg;
            verbose() << errStream.str() << endmsg;
          }

          ++m_AlertEventID;
          ++counter("AlertEventID");
          ++m_AlertMessagesEventID[errStream.str()];
          m_ODINLastError[errStream.str()] = odinInfo(odin);
          hpdDisableTool() -> ReportHPD(hpdData,reason);
          m_HitPerHardIDH->fill(hardID.data());
          m_ErrorPerUKL1BoardH->fill(l1HardID.data());
          if (m_SendAlertDirect)
          {
            cameraTool()->Append("TEXT",errStream.str().c_str());
            cameraTool()->Append("TEXT",odinInfo(odin).c_str());
            cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,reason);
          }// sendAlert

          if (m_Plot2DHisto) PlotErrorHPD(smartIDHPD);

          // disable this HPD in the UKL1 stream
          if (m_RemoveFaultyHpdUKL1)
          {
            errStream.str(emptyString);
            errStream << "Request HPD to be disabled, EventID mis-match "
                      << " SmartID "          << smartIDHPD
                      << hpdData << ":" << " L0ID[" << l0ID << "]";
            //if (m_PrintMessages)
            //  info()    << errStream.str() << endmsg;
            if (m_SendAlertDirect)
            {
              cameraTool()->Append("TEXT",errStream.str().c_str());
              cameraTool()->Append("TEXT",odinInfo(odin).c_str());
              cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,"Request HPD disable");
            } //sendAlert
            hpdDisableTool() -> DisableHPD( hpdData, reason );
          } //if m_RemoveFaultyHpdUKL1
        }//if eventID

        if ( hpdInfo.header().l0ID() != l0ID )
        {
          const std::string reason = "L0 ID mis-match between HPD and DB";

          errStream.str(emptyString);
          errStream <<
            "L0 ID (SmartID): " << l0ID             <<
            " HPD header: "     << hpdInfo.header().l0ID();
          //if (m_PrintMessages)
          //  info() << "L0 ID mis-match " << errStream.str()  << endmsg;

          if ( msgLevel(MSG::VERBOSE) ) 
          {
            verbose() << reason << endmsg;
            verbose() << errStream.str() << endmsg;
          }

          ++m_AlertL0ID;
          ++m_AlertMessagesL0ID[errStream.str()];
          m_ODINLastError[errStream.str()] = odinInfo(odin);
          hpdDisableTool() -> ReportHPD(hpdData,reason);
          m_HitPerHardIDH->fill(hardID.data());
          m_ErrorPerUKL1BoardH->fill(l1HardID.data());
          if (m_SendAlertDirect)
          {
            cameraTool()->Append("TEXT",errStream.str().c_str());
            cameraTool()->Append("TEXT",odinInfo(odin).c_str());
            cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,reason);
          }// sendAlert
          setFilterPassed(true);
          // Save messages for snapshots
          snapshotMessages.push_back( errStream.str() );

          // disable this HPD in the UKL1 stream
          if (m_RemoveFaultyHpdUKL1)
          {
            errStream.str(emptyString);
            errStream << reason << " "
                      << " SmartID "          << smartIDHPD << " "
                      << hpdData << ":" << " L0ID[" << l0ID << "]";
            //if (m_PrintMessages)
            //  info() << errStream.str() << endmsg;
            if (m_SendAlertDirect)
            {
              cameraTool()->Append("TEXT",errStream.str().c_str());
              cameraTool()->Append("TEXT",odinInfo(odin).c_str());
              cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name.c_str(),"Request HPD disable");
            } //sendAlert
            hpdDisableTool() -> DisableHPD( hpdData, reason );
            setFilterPassed(true);
            // Save messages for snapshots
            snapshotMessages.push_back( errStream.str() );
          } //if m_RemoveFaultyHpdUKL1

          ++counter("AlertL0ID");
          m_HitPerHardIDH->fill(hardID.data());
          m_ErrorPerUKL1BoardH->fill(l1HardID.data());

          if (m_Plot2DHisto) PlotErrorHPD(smartIDHPD);
        }// if L0ID

        // check footer
        if  ( msgLevel(MSG::VERBOSE) )
          verbose() << "HPD footer has parity word " << hpdInfo.footer().hasParityWord () << endmsg;
        if (hpdInfo.footer().hasParityWord())
        {
          const Rich::DAQ::LongType parityWord = hpdInfo.footer().parityWord();
          bool  parityOK = hpdInfo.footer().testParityWord(parityWord);
          if  ( msgLevel(MSG::VERBOSE) )
            verbose() << "   parity word " << parityWord << " is OK " << parityOK << endmsg;
          if (!parityOK)
          {
            const std::string reason = "Parity Word not OK";

            errStream.str(emptyString);
            errStream <<
              " HPD L0ID: "        << l0ID                       <<
              " IngressID "        << l1IngressID                <<
              " HPD EventID "      << hpdInfo.header().eventID() << hpdData << ":" << " L0ID[" << l0ID << "]";
            //if (m_PrintMessages)
            //  info() << "Footer parity word not OK " << errStream.str()  << endmsg;

            if ( msgLevel(MSG::VERBOSE) ) 
            {
              verbose() << reason << endmsg;
              verbose() << errStream.str() << endmsg;
            }

            ++counter("AlertParityFooter");
            m_HitPerHardIDH->fill(hardID.data());
            m_ErrorPerUKL1BoardH->fill(l1HardID.data());
            hpdDisableTool() -> ReportHPD(hpdData,reason);

            ++m_AlertParityFooter;
            ++m_AlertMessagesParityFooter[errStream.str()];
            m_ODINLastError[errStream.str()] = odinInfo(odin);

            if (m_SendAlertDirect)
            {
              cameraTool()->Append("TEXT",errStream.str().c_str());
              cameraTool()->Append("TEXT",odinInfo(odin).c_str());
              cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,reason);
            }// sendAlert
            setFilterPassed(true);
            // Save messages for snapshots
            snapshotMessages.push_back( errStream.str() );

            // disable this HPD in the UKL1 stream
            if (m_RemoveFaultyHpdUKL1)
            {
              errStream.str(emptyString);
              errStream << reason << " "
                        << " SmartID "          << smartIDHPD << " "
                        << hpdData << ": L0ID[" << l0ID << "]";
              //if (m_PrintMessages)
              //  info()    << errStream.str() << endmsg;
              if (m_SendAlertDirect)
              {
                cameraTool()->Append("TEXT",errStream.str().c_str());
                cameraTool()->Append("TEXT",odinInfo(odin).c_str());
                cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,"Request HPD disable");
              } //sendAlert
              setFilterPassed(true);
              // Save messages for snapshots
              snapshotMessages.push_back( errStream.str() );
              hpdDisableTool() -> DisableHPD( hpdData, reason );
            } //if m_RemoveFaultyHpdUKL1

            if (m_Plot2DHisto) PlotErrorHPD(smartIDHPD);
          }// if !parityOK
        }// if parityWord

        nAllHits += hitSmartIDs.size();

      }//for iHPDMap

      //
      // check number of active HPDs
      // N.B. dynamically inhibited HPDs should not be reported
      //
      if (hpdMap.size() != ingressHeader.numActiveHPDs() - nInhibitedHPDs)
      {
        if  ( msgLevel(MSG::VERBOSE) )
          verbose() << "HPD map size "     << hpdMap.size()
                    << " from ingress "    << ingressHeader.numActiveHPDs()
                    << " #inhibited HPDs " << nInhibitedHPDs
                    << endmsg;
        ++counter("AlertActiveHPDs");
        m_ErrorPerUKL1BoardH->fill(l1HardID.data());
        errStream.str(emptyString);
        errStream << "size of HPDMap "    << hpdMap.size()
                  << " from ingress  "    << ingressHeader.numActiveHPDs()
                  << " #inhibited HPDs "  << nInhibitedHPDs
                  << " L1 Ingress ID "    << l1IngressID
                  << " L1 ID (hardware) " << l1HardID
                  << " L1 ID (logical) "  << l1LogicalID
                  << " RICH "             << richDetector;

        if ( msgLevel(MSG::VERBOSE) ) 
        {
          verbose() << errStream.str() << endmsg;
        }

        ++m_AlertActiveHPDs;
        ++counter("AlertActiveHPDs");
        ++m_AlertMessagesActiveHPD[errStream.str()];
        m_ODINLastError[errStream.str()] = odinInfo(odin);

        if (m_SendAlertDirect)
        {
          cameraTool()->Append("TEXT",errStream.str().c_str());
          cameraTool()->Append("TEXT",odinInfo(odin).c_str());
          cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,"#active HPDs mismatch");
          setFilterPassed(true);
          // Save messages for snapshots
          snapshotMessages.push_back( errStream.str() );
        }//if sendDirect
      } // if hpdMap.size()
    } //for iIngressMap

    if (UKL1HasSuppressedIngress)
    {
      m_TruncatedEventUKL1->fill(l1HardID.data());
    } // if hasSuppressed

  }//for iL1Map


  //
  // check if there are any LO IDs reported multiple times
  //
  for ( std::map<Rich::DAQ::Level0ID,int>::const_iterator iL0Map = l0MapHPD.begin();
        iL0Map != l0MapHPD.end(); ++iL0Map )
  {
    const int nL0 = iL0Map->second;
    if (nL0 >1)
    {
      errStream.str(emptyString);
      errStream << "!! Duplicate L0IDs in RICH!! L0ID " << iL0Map->first << " occurred " << nL0 << " times ";
      if (m_PrintMessages)
        Error( errStream.str() ).ignore();

      ++m_AlertDuplicateHPDL0;
      ++counter("AlertDuplicateHPDL0");
      ++m_AlertMessagesDuplicateHPDL0[errStream.str()];
      m_ODINLastError[errStream.str()] = odinInfo(odin);
      if (m_SendAlertDirect)
      {
        cameraTool()->Append("TEXT",errStream.str().c_str());
        cameraTool()->Append("TEXT",odinInfo(odin).c_str());
        cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,"Duplicate L0 IDs");
        setFilterPassed(true);
        // Save messages for snapshots
        snapshotMessages.push_back( errStream.str() );
      }//if sendDirect
    } //if
  } //for

  if (nAllHits <= 0)
  {
    ++counter("AlertEmptyEvent"); // send alarm to PVSS
    if  ( msgLevel(MSG::VERBOSE) )
      verbose() << "No hits found in event " << m_nEvts << ", number of empty events now "
                << counter("AlertEmptyEvent").nEntries() << endmsg;
    errStream.str(emptyString);
    errStream << "#empty events now:" << counter("AlertEmptyEvent").nEntries();
    ++m_AlertEmptyEvent;
    if (m_SendAlertDirect && !m_TaeLocation.empty())
    {
      // don't report this as error for other than central bin
      cameraTool()->Append("TEXT",errStream.str().c_str());
      cameraTool()->Append("TEXT",odinInfo(odin).c_str());
      cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Empty event");
    }
  }

  //
  // send alerts to CAMERA every n'th event
  //
  if ( sendErrorReport )
  {
    const std::string odinindo = odinInfo(odin);
    if ( msgLevel(MSG::DEBUG) )
      debug() << "Now send error report " << odinindo << endmsg;
    const StatusCode sc = SendErrorSummary(odinindo);
    if ( sc.isFailure() )
    {
      return Warning("Error sending StatusReport",sc);
    }// if sc
    if ( msgLevel(MSG::DEBUG) )
      debug() << " ======= End of error report" << endmsg;
  } // if sendErrorReport

  // Send cached messages to camera for inclusion in snapshots
  if ( m_AddSnapshotCameraMsg && !snapshotMessages.empty() )
  {
    cameraTool()->Append("TEXT",odinInfo(odin).c_str());
    for ( std::vector<std::string>::const_iterator iM = snapshotMessages.begin();
          iM != snapshotMessages.end(); ++iM )
    {
      cameraTool()->Append("TEXT",(*iM).c_str());
    }
  }

  return StatusCode::SUCCESS;
} // execute

//=============================================================================

StatusCode DAQMonitor::SendErrorSummary(const std::string& odinInfo)
{

  // Use this ostringstream to create error messages that are sent to CAMERA and
  // the message streams if the level is appropriate.
  std::ostringstream errStream;
  // In order to clear the error stream between use we need to write an empty string
  // to its buffer via the .str() method.
  // This is the only way. .str().erase() and .str().clear() only operate on copies
  // of the buffer. Create an empty string
  // to use each time. Saves repeatedly constructing one.
  const std::string emptyString("");

  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "send alerts to CAMERA" << endmsg;
  // cameraTool()->SendAndClearTS(ICameraTool::IFNO,m_Name,"now send error summary");

  if ( m_AlertEmptyEvent > 0 && !m_TaeLocation.empty() )
  {
    // don't report this as error for other than central bins in TAE mode
    errStream.str(emptyString);
    errStream <<  "total #empty events " << counter("AlertEmptyEvent").nEntries();
    cameraTool()->Append("TEXT",errStream.str().c_str());
    cameraTool()->Append("TEXT",odinInfo.c_str());
    errStream.str(emptyString);
    errStream << "#empty events in past " << m_UpdateTimerInterval
              << " seconds: "             << m_AlertEmptyEvent;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  } // if

  std::map<std::string,int>::const_iterator iAlert;
  std::map<std::string,int>::const_iterator iAlertBegin;
  std::map<std::string,int>::const_iterator iAlertEnd;

  if (m_AlertActiveHPDs > 0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesActiveHPD.begin();
      iAlertEnd   = m_AlertMessagesActiveHPD.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
        if ( m_PrintMessages )
          warning() << "Active HPD error " << errStream.str() 
                    << " " << m_ODINLastError[(*iAlert).first] 
                    << endmsg;

      } //for
    } //if
    errStream.str(emptyString);
    errStream << "#active HPD mis-matches in past " << m_UpdateTimerInterval
              << " seconds: "                       << m_AlertActiveHPDs;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  }//if

  if (m_AlertEventID > 0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesEventID.begin();
      iAlertEnd   = m_AlertMessagesEventID.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents " << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT", errStream.str());
        if ( m_PrintMessages )
          error() << " EventID mis-match " << errStream.str() 
                  << " " << m_ODINLastError[(*iAlert).first] 
                  << endmsg;
      } //for
    } //if
    errStream.str(emptyString);
    errStream << "#EventID mis-matches in past " << m_UpdateTimerInterval
              << " seconds: "                    << m_AlertEventID;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  }//if

  if (m_AlertBxID >0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesBxID.begin();
      iAlertEnd   = m_AlertMessagesBxID.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
        if ( m_PrintMessages )
          error() << "BxID mis-match " << errStream.str() 
                  << " " << m_ODINLastError[(*iAlert).first] 
                  << endmsg;
      } //for
    }//if
    errStream.str(emptyString);
    errStream << "#BxID mis-matches in past " << m_UpdateTimerInterval
              << " seconds: "                  << m_AlertBxID;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  }//if

  if (m_AlertL0ID > 0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesL0ID.begin();
      iAlertEnd   = m_AlertMessagesL0ID.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
        if ( m_PrintMessages )
          error() << "L0ID mis-match " << errStream.str() 
                  << " " << m_ODINLastError[(*iAlert).first] 
                  << endmsg;
      } //for
    }//if
    errStream.str(emptyString);
    errStream << "#L0 mis-matches in past " << m_UpdateTimerInterval
              << " seconds: "               << m_AlertL0ID;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  }//if

  if (m_AlertInvalidOdinTime > 0)
  {
    errStream.str(emptyString);
    errStream << "#invalid ODIN times in past " << m_UpdateTimerInterval
              << " seconds: "                   << m_AlertInvalidOdinTime;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name, errStream.str());
    if ( m_PrintMessages )
      warning() << errStream.str() << endmsg;
  }//if

  if ( m_AlertMissingOdin > 0)
  {
    errStream.str(emptyString);
    errStream << "#missing ODIN bank in past " << m_UpdateTimerInterval
              << " seconds: "                  << m_AlertMissingOdin;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,  errStream.str());
    if ( m_PrintMessages )
      warning() << errStream.str() << endmsg;
  }//if

  if (m_AlertParityFooter > 0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesParityFooter.begin();
      iAlertEnd   = m_AlertMessagesParityFooter.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
        if ( m_PrintMessages )
          warning() << "Parity footer " << errStream.str() 
                    << " " << m_ODINLastError[(*iAlert).first] 
                    << endmsg;
      } //for
    } //if sendAlert
    errStream.str(emptyString);
    errStream << "#parity footer mis-match in past " << m_UpdateTimerInterval
              << " seconds: "                        << m_AlertParityFooter;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  }//if parityFooter

  if (m_AlertHPDNotInhibitInvalidSmartRef > 0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesHPDNotInhibitInvalidSmartRef.begin();
      iAlertEnd   = m_AlertMessagesHPDNotInhibitInvalidSmartRef.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
        if ( m_PrintMessages )
          warning() << "Invalid SmartRef " << errStream.str() 
                    << " " << m_ODINLastError[(*iAlert).first] 
                    << endmsg;
      } //for
    } //if sendAlert
    errStream.str(emptyString);
    errStream << "#invalid SmartRefs for not inhibited HPDs in past " << m_UpdateTimerInterval
              << " seconds: "                                         << m_AlertHPDNotInhibitInvalidSmartRef;
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  }//if parityFooter

  if (m_AlertExtendedHeader>0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesExtendedHeader.begin();
      iAlertEnd   = m_AlertMessagesExtendedHeader.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
      } //for
    } //if sendAlert
    errStream.str(emptyString);
    errStream << "#extended headers in past " << m_UpdateTimerInterval
              << " seconds: "                 << m_AlertExtendedHeader;

    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  } //if extended Header

  if (m_AlertHPDSuppressed>0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesHPDSuppressed.begin();
      iAlertEnd   = m_AlertMessagesHPDSuppressed.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
      } //for
    } //if sendAlert
    errStream.str(emptyString);
    errStream << "#HPD suppressed in UKL1 in past " << m_UpdateTimerInterval
              << " seconds: "                       << m_AlertHPDSuppressed;

    cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,errStream.str());
  }

  if (m_AlertEmptyHPD>0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesEmptyHPD.begin();
      iAlertEnd   = m_AlertMessagesEmptyHPD.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
      } //for
    } //if sendAlert
    errStream.str(emptyString);
    errStream << "#Empty HPDs in past " << m_UpdateTimerInterval
              << " seconds: "           << m_AlertEmptyHPD;

    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  } //if EmptyHPD

  //
  if (m_AlertDuplicateHPDL0>0)
  {
    if (m_SendAlertMessages)
    {
      iAlertBegin = m_AlertMessagesDuplicateHPDL0.begin();
      iAlertEnd   = m_AlertMessagesDuplicateHPDL0.end();
      for (iAlert = iAlertBegin; iAlert != iAlertEnd; ++iAlert)
      {
        errStream.str(emptyString);
        errStream << (*iAlert).first
                  << " #incidents "  << (*iAlert).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "send to CAMERA " << errStream.str() << endmsg;
        cameraTool()->Append("TEXT",m_ODINLastError[(*iAlert).first]);
        cameraTool()->Append("TEXT",errStream.str());
        if ( m_PrintMessages )
          warning() << errStream.str() 
                    << " " << m_ODINLastError[(*iAlert).first] 
                    << endmsg;
      } //for
    } //if sendAlert
    errStream.str(emptyString);
    errStream << "#duplicate L0 IDs   " << m_UpdateTimerInterval
              << " seconds: "           << m_AlertDuplicateHPDL0;

    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,errStream.str());
  } //if EmptyHPD

  // reset all error counts after reporting them
  ResetCounters();

  return StatusCode::SUCCESS;
}

//=============================================================================

void DAQMonitor::ResetCounters()
{
  m_AlertEmptyEvent                    = 0;
  m_AlertActiveHPDs                    = 0;
  m_AlertEventID                       = 0;
  m_AlertBxID                          = 0;
  m_AlertL0ID                          = 0;
  m_AlertInvalidOdinTime               = 0;
  m_AlertMissingOdin                   = 0;
  m_AlertParityFooter                  = 0;
  m_AlertHPDInhibit                    = 0;
  m_AlertHPDNotInhibitInvalidSmartRef  = 0;
  m_AlertExtendedHeader                = 0;
  m_AlertHPDSuppressed                 = 0;
  m_AlertEmptyHPD                      = 0;
  m_AlertDuplicateHPDL0                = 0;

  m_AlertMessagesActiveHPD.clear();
  m_AlertMessagesEventID.clear();
  m_AlertMessagesBxID.clear();
  m_AlertMessagesL0ID.clear();
  m_AlertMessagesHPDInhibit.clear();
  m_AlertMessagesParityFooter.clear();
  m_AlertMessagesHPDNotInhibitInvalidSmartRef.clear();
  m_AlertMessagesExtendedHeader.clear();
  m_AlertMessagesHPDSuppressed.clear();
  m_AlertMessagesEmptyHPD.clear();
  m_AlertMessagesDuplicateHPDL0.clear();

  m_ODINLastError.clear();
}

//=============================================================================

StatusCode DAQMonitor::PlotErrorHPD(const LHCb::RichSmartID &smartID)
{

  if (!m_Plot2DHisto)
  {
    verbose() << " DAQMonitor::PlotErrorHPD should not have been called" << endmsg;
    return StatusCode::SUCCESS;
  } //if

  int iRich     = 0;
  int iPanel    = 0;
  int iCol      = 0;
  int iHpdInCol = 0;

  StatusCode sc = StatusCode::SUCCESS;

  if (smartID.rich()  != Rich::InvalidDetector &&
      smartID.panel() != Rich::InvalidSide)
  {
    iRich     = (int) smartID.rich();
    iPanel    = (int) smartID.panel();
    iCol      = (int) smartID.pdCol();
    iHpdInCol = (int) smartID.pdNumInCol();

    std::ostringstream histoName;
    histoName << "DAQErrorRich" << iRich+1 << "Side" << iPanel;

    if ( msgLevel(MSG::VERBOSE) )
      verbose() << "Fill histogram with title " << histoName.str() << endmsg;
    plot2D( iCol ,iHpdInCol, histoName.str(), histoName.str(), 0,0,0, 0,0,0);

  }
  else
  {
    sc = StatusCode::FAILURE;
  }// if not invalid Rich, Panel

  return sc;
} 

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( DAQMonitor )

//=============================================================================
