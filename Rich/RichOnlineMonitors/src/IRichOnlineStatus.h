// $Id: IRichOnlineStatus.h,v 1.2 2008-02-28 16:50:45 jonrob Exp $
#ifndef IRICHONLINESTATUS_H
#define IRICHONLINESTATUS_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

#include "ErrorReport.h"
#include "iostream"

static const InterfaceID IID_IRichOnlineStatus ( "IRichOnlineStatus", 1, 0 );

namespace Rich
{
  namespace Mon
  {

    /** @class IOnlineStatus
     *
     *
     *  @author Claus Buszello
     *  @date   2007-05-30
     */
    class IOnlineStatus : virtual public IAlgTool 
    {
    
    public:

      // Return the interface ID
      static const InterfaceID& interfaceID() { return IID_IRichOnlineStatus; }

      virtual float getHV(int rich, int pan, int col)=0;
      virtual float getSiBias(int rich, int pan, int col)=0;
      virtual float getLV(int rich, int pan, int col)=0;
      virtual float UpdatePVSSNow(){ std::cerr << "PVSS not supported yet"<<std::endl;return 0;}

    };

  }
}

#endif // IRICHTESTTOOL_H
