// Gaudi
#include "GaudiKernel/ToolFactory.h"

// Local
#include "HalfDiagonalPixelTestPatternTool.h"

// Std
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : HalfDiagonalPixelTestPattern
//
// 2009-02-12 : Gareth Rogers
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
namespace Rich {
  namespace Mon {
    DECLARE_TOOL_FACTORY(HalfDiagonalPixelTestPattern)
  }// namespace Mon
}// namespace Rich

//-----------------------------------------------------------------------------
Rich::Mon::HalfDiagonalPixelTestPattern::HalfDiagonalPixelTestPattern(const std::string &type
                                                          , const std::string &name
                                                          , const IInterface *parent)
  : GaudiHistoTool(type, name, parent)
  , m_totalPatternPixels(16)
  , m_colMin(0)
  , m_colMax(16)
  , m_nBinsCol(16)
  , m_rowMin(0)
  , m_rowMax(16)
  , m_nBinsRow(16)
{
  declareInterface<ITestPattern>(this);
}// Rich::Mon::HalfDiagonalPixelTestPattern::HalfDiagonalPixelTestPattern(const std::string &type, const std::string &name, const IInterface *parent)

//-----------------------------------------------------------------------------
Rich::Mon::HalfDiagonalPixelTestPattern::~HalfDiagonalPixelTestPattern() {
}// Rich::Mon::HalfDiagonalPixelTestPattern::~HalfDiagonalPixelTestPattern()


//-----------------------------------------------------------------------------
StatusCode Rich::Mon::HalfDiagonalPixelTestPattern::initialize() {
  // Done.
  return GaudiHistoTool::initialize();
}// Rich::Mon::HalfDiagonalPixelTestPattern::initialize()

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::HalfDiagonalPixelTestPattern::finalize() {
  // Done.
  return GaudiHistoTool::finalize();
}// Rich::Mon::HalfDiagonalPixelTestPattern::finalize()


//-----------------------------------------------------------------------------
LHCb::RichSmartID::Vector Rich::Mon::HalfDiagonalPixelTestPattern::testPatternHits(const LHCb::RichSmartID &hpdId, bool invertRows) const {
  // As this is done for a different smart ID, we can't cache the test pattern hits
  // and it must be done each time. Ensure that there is no pixel information in
  // the ID we are given. We want to set only those pixels in the test pattern.
  LHCb::RichSmartID::Vector testPattern(m_totalPatternPixels, hpdId.pdID());
  // Decide if we are in ALICE mode or not.
  // We are given a smart ID. This ID may be a pixel hit, in which case it may
  // or may not be in ALICE mode so have to check for ALICE mode pixel.
  // It may be a HPD in which case we don't know the mode and assume LHCb.
  bool alice(hpdId.pixelSubRowDataIsValid());
  // A diagonal line across half the pixel chip, starting from (0,0) to (16,16).
  // Always set the largest ALICE mode pixel.
  for (int pixel(0); pixel<16; ++pixel) {
    // Set the column first as we don't want to invert that.
    testPattern[pixel].setPixelCol(pixel);
    // We may need to change the row pixel.
    int rowPix(pixel);
    if (invertRows)
      rowPix = 31 - rowPix;
    testPattern[pixel].setPixelRow(rowPix);
    if (alice)
      testPattern[pixel].setPixelSubRow( invertRows ? 7:0 );
  }//for pixel

  // Done.
  return testPattern;
}// Rich::Mon::HalfDiagonalPixelTestPattern::testPattern(const LHCb::RichSmartID &)

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::HalfDiagonalPixelTestPattern::histogramDimensions(Rich::DetectorType rich, int &nBinsX, double &xMin, double &xMax, int &nBinsY, double &yMin, double &yMax) const {
  // We will need to know the size of the HPDs to request an appropriate sized RICH.
  // As our HPD is asymmetric in pattern we need to check which RICH we are using.
  if (Rich::Rich1 == rich) {
    nBinsX = m_nBinsRow;
    xMin   = m_rowMin;
    xMax   = m_rowMax;
    nBinsY = m_nBinsCol;
    yMin   = m_colMin;
    yMax   = m_colMax;
  }// if RICH1
  else if (Rich::Rich2 == rich) {
    nBinsX = m_nBinsCol;
    xMin   = m_colMin;
    xMax   = m_colMax;
    nBinsY = m_nBinsRow;
    yMin   = m_rowMin;
    yMax   = m_rowMax;
  }// else if RICH2
  else {
    nBinsX = -1;
    xMin   = -1;
    xMax   = -1;
    nBinsY = -1;
    yMin   = -1;
    yMax   = -1;
  }// else InvalidDetector

  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::HalfDiagonalPixelTestPattern::histogramDimensions()

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::HalfDiagonalPixelTestPattern::plotCoordinates(Rich::DetectorType rich, bool alice, int hpdX, int hpdY, int &patternX, int &patternY, bool invertRows) const {
  // Whether the rows are on X or Y depends on the RICH.
  // We must adjust the row number depending on whether it is ALICE or not.
  if (Rich::Rich1 == rich) {
    // In the case of RICH1 the columns don't change and are on Y.
    patternY = hpdY;
    // We must convert the ALICE pixel pattern to 0 to 15 range. It nothing needs to be done in LHCb mode.
    patternX = hpdX;
    if (alice)
      patternX = (patternX/8)%16;
  }// if RICH1
  else if (Rich::Rich2 == rich) {
    // In the case of RICH2 the columns don't change and are on X.
    patternX = hpdX;
    // We must convert the ALICE pixel pattern to 0 to 15 range. It nothing needs to be done in LHCb mode.
    patternY = hpdY;
    if (alice)
      patternY = (patternY/8)%16;
    // In the case that we are inverting the rows we need to subtract the y-axis length
    // from the y-coordinate, this is because we have moved from (0,0) to (0,31) as the
    // start and this is out of the histogram dimensions range.
    // This is only done for RICH2 whose HPD chip has the rows along the y-axis.
    if (invertRows)
      patternY -= m_rowMax;
  }// elseif RICH2
  else {
    // We don't know which we are dealing with so test pattern must not have been set.
    patternX = -1;
    patternY = -1;
  }// don't know which RICH

  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::HalfDiagonalPixelTestPattern::plotCoordinates()

//-----------------------------------------------------------------------------
int Rich::Mon::HalfDiagonalPixelTestPattern::totalTestPatternPixels() const {
  return m_totalPatternPixels;
}// Rich::Mon::HalfDiagonalPixelTestPattern::totalTestPatternPixels()

//=============================================================================
