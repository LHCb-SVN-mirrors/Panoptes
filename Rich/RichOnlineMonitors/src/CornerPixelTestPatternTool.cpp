// Gaudi
#include "GaudiKernel/ToolFactory.h"

// Local
#include "CornerPixelTestPatternTool.h"

// Std
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : CornerPixelTestPattern
//
// 2009-02-12 : Gareth Rogers
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
namespace Rich {
  namespace Mon {
    DECLARE_TOOL_FACTORY(CornerPixelTestPattern)
  }// namespace Mon
}// namespace Rich

//-----------------------------------------------------------------------------
Rich::Mon::CornerPixelTestPattern::CornerPixelTestPattern(const std::string &type
                                                          , const std::string &name
                                                          , const IInterface *parent)
  : GaudiHistoTool(type, name, parent)
  , m_totalPatternPixels(4)
  , m_colMin(0)
  , m_colMax(2)
  , m_nBinsCol(2)
  , m_rowMin(0)
  , m_rowMax(2)
  , m_nBinsRow(2)
{
  declareInterface<ITestPattern>(this);
}// Rich::Mon::CornerPixelTestPattern::CornerPixelTestPattern(const std::string &type, const std::string &name, const IInterface *parent)

//-----------------------------------------------------------------------------
Rich::Mon::CornerPixelTestPattern::~CornerPixelTestPattern() {
}// Rich::Mon::CornerPixelTestPattern::~CornerPixelTestPattern()


//-----------------------------------------------------------------------------
StatusCode Rich::Mon::CornerPixelTestPattern::initialize() {
  // Done.
  return GaudiHistoTool::initialize();
}// Rich::Mon::CornerPixelTestPattern::initialize()

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::CornerPixelTestPattern::finalize() {
  // Done.
  return GaudiHistoTool::finalize();
}// Rich::Mon::CornerPixelTestPattern::finalize()


//-----------------------------------------------------------------------------
LHCb::RichSmartID::Vector Rich::Mon::CornerPixelTestPattern::testPatternHits(const LHCb::RichSmartID &hpdId, bool) const {
  // As this is done for a different smart ID, we can't cache the test pattern hits
  // and it must be done each time. Ensure that there is no pixel information in
  // the ID we are given. We want to set only those pixels in the test pattern.
  LHCb::RichSmartID::Vector testPattern(m_totalPatternPixels, hpdId.pdID());
  // Decide if we are in ALICE mode or not.
  // We are given a smart ID. This ID may be a pixel hit, in which case it may
  // or may not be in ALICE mode so have to check for ALICE mode pixel.
  // It may be a HPD in which case we don't know the mode and assume LHCb.
  bool alice(hpdId.pixelSubRowDataIsValid());
  // There is no explicit row inversion done here. There is little point as it is completely symmetric.
  int pixel(0);
  for (int row(m_rowMin); row<m_rowMax; ++row) {
    for (int col(m_colMin); col<m_colMax; ++col) {
      pixel = 2*row + col;
      testPattern[pixel].setPixelRow(31*row);
      if (alice)
        testPattern[pixel].setPixelSubRow(7*row);
      testPattern[pixel].setPixelCol(31*col);
    }// for col
  }// for row
  // Done.
  return testPattern;
}// Rich::Mon::CornerPixelTestPattern::testPattern(const LHCb::RichSmartID &hpdID)

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::CornerPixelTestPattern::histogramDimensions(Rich::DetectorType, int &nBinsX, double &xMin, double &xMax, int &nBinsY, double &yMin, double &yMax) const {
  // We will need to know the size of the HPDs to request an appropriate sized RICH.
  // As our HPD is symmetric in pattern we don't need to worry about which RICH we are using.
  // Just comes from the interface.
  nBinsX = m_nBinsRow;
  xMin   = m_rowMin;
  xMax   = m_rowMax;
  nBinsY = m_nBinsCol;
  yMin   = m_colMin;
  yMax   = m_colMax;
  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::CornerPixelTestPattern::histogramDimensions()

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::CornerPixelTestPattern::plotCoordinates(Rich::DetectorType, bool, int hpdX, int hpdY, int &patternX, int &patternY, bool) const {
  // This pattern is symmetric so it doesn't matter who is X or Y.
  // Map (0,0),(0,31),(31,0),(31,31) to (0,0),(0,1),(1,0),(1,1)
  // Position coordinates on the HPD are independent of ALICE or LHCb mode,
  // hence we don't look for an exact match.
  if (1 < hpdX)
    patternX = 1;
  else
    patternX = 0;
  if (1 < hpdY)
    patternY = 1;
  else
    patternY = 0;
  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::CornerPixelTestPattern::plotCoordinates()

//-----------------------------------------------------------------------------
int Rich::Mon::CornerPixelTestPattern::totalTestPatternPixels() const {
  return m_totalPatternPixels;
}// Rich::Mon::CornerPixelTestPattern::totalTestPatternPixels()

//=============================================================================
