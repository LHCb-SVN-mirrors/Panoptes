// $Id: RichMissingHPDMonitor.h,v 1.18 2010-02-04 12:17:30 ukerzel Exp $
#ifndef RichMissingHPDMonitor_H
#define RichMissingHPDMonitor_H 1

// base class
#include "RichKernel/RichHistoAlgBase.h"

// CAMERA
#include "Camera/ICameraTool.h"

// std
#include <string>
#include <map>
#include <sstream>

// Interfaces
#include "RichKernel/IRichRawBufferToSmartIDsTool.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Monitoring tools
#include "RichMonitoringTools/IHpdUkL1DisableTool.h"

// LHCbMath
#include "LHCbMath/Bit.h"

namespace Rich
{
  namespace Mon
  {

    /** @class MissingHPDMonitor RichMissingHPDMonitor.h
     *
     *  This algorithm monitors the number of hits seen in each (active) HPD
     *
     *  Find HPDs not sending any data
     *   
     *  @author Chris Jones
     *  @date   22/08/2011
     */
    class MissingHPDMonitor : public Rich::HistoAlgBase{

      // ---------------------------------------------------------------------------
      //                                PUBLIC
      // ---------------------------------------------------------------------------
    public:

      /// Standard constructor
      MissingHPDMonitor( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~MissingHPDMonitor( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode execute   ();    ///< Algorithm execution
      virtual StatusCode finalize  ();    ///< Algorithm finalization

      // ---------------------------------------------------------------------------
      //                                PRIVATE Methods
      // ---------------------------------------------------------------------------
    private:
      
      /// Load on demand DeRichSystem
      const DeRichSystem * deRichSys() const
      {
        if ( !m_RichSys )
        {
          m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
        }
        return m_RichSys;
      }

      /// Load camera tool on demand
      ICameraTool * cameraTool() const
      {
        if ( !m_CamTool )
        {
          m_CamTool = tool<ICameraTool>("CameraTool");
        }
        return m_CamTool;
      }

      /// Load smartID tool on demand
      const Rich::DAQ::IRawBufferToSmartIDsTool * smartIDDecoder() const
      {
        if ( !m_SmartIDDecoder )
        {
          acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, 0, true );
        }
        return m_SmartIDDecoder;
      }

      /// Load the HPD disabling tool on demand
      Rich::Mon::IHpdUkL1DisableTool * hpdDisableTool() const
      {
        if ( !m_HpdUKL1DisableTool )
        {
          m_HpdUKL1DisableTool =
            tool<Rich::Mon::IHpdUkL1DisableTool>("Rich::Mon::HpdUkL1DisableTool",
                                                 "RichUKL1Disable");
        }
        return m_HpdUKL1DisableTool;
      }

      // ---------------------------------------------------------------------------
      //                                PRIVATE data
      // ---------------------------------------------------------------------------
    private:

      /// Pointer to RICH system detector element
      mutable const DeRichSystem * m_RichSys;

      /// Raw Buffer Decoding tool
      mutable const Rich::DAQ::IRawBufferToSmartIDsTool * m_SmartIDDecoder;

      mutable Rich::Mon::IHpdUkL1DisableTool * m_HpdUKL1DisableTool; ///< HPD reporting/disable tool

      mutable ICameraTool                       * m_CamTool; ///< The camera reporting tool

      std::vector<std::string>                m_taeEvents; ///< The TAE location(s) to monitor

      unsigned long int m_checkFreq;        ///< The rate, in number of events, to check for missing HPDs
      unsigned long long m_lastEventCheck;  ///< The last event checked
      unsigned long int m_maxMissingEvents; ///< Maximum allowed number of events an HPD can be missing for 
      bool  m_vetoNoBias;  ///< Veto no bias events

      /// Count events
      unsigned long long m_nEvts;

      std::string m_Name; ///< Name for camera messages  

      /// keep tabs on the seen HPDs and the event number last seen in
      std::map<LHCb::RichSmartID,unsigned long int > m_hpdCount;

      /// Which RICHes are active (based on partition)
      bool m_activeRICH[Rich::NRiches];

    };

  }
}

#endif // RichMissingHPDMonitor_H
