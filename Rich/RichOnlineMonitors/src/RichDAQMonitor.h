
#ifndef RICHDAQMONITOR_H
#define RICHDAQMONITOR_H 1

//
// Include files
//

#include <sstream>

// base class
#include "RichKernel/RichHistoAlgBase.h"

// boost
#include "boost/lexical_cast.hpp"

// Interfaces
#include "RichKernel/IRichRawBufferToSmartIDsTool.h"
#include "RichKernel/IRichSmartIDTool.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Monitoring tools
#include "RichMonitoringTools/IHpdUkL1DisableTool.h"

// time
#include <time.h>

// Histogramming
#include "AIDA/IHistogram1D.h"

// LHCbMath
#include "LHCbMath/Bit.h"

// Forward declarations
class ICameraTool;

namespace Rich
{
  namespace Mon
  {

    /** @class DAQMonitor RichDAQMonitor.h
     *
     *  @author Ulrich Kerzel
     *  @date   2007-04-04
     */
    class DAQMonitor : public Rich::HistoAlgBase
    {

      // ---------------------------------------------------------------------------
      //                                PUBLIC
      // ---------------------------------------------------------------------------
    public:
      /// Standard constructor
      DAQMonitor( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~DAQMonitor( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode finalize  ();    ///< Algorithm finalization
      virtual StatusCode execute   ();    ///< Algorithm execution

    private:
      
      /// Load on demand DeRichSystem
      const DeRichSystem * deRichSys() const
      {
        if ( !m_RichSys )
        {
          m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
        }
        return m_RichSys;
      }

      /// Load camera tool on demand
      ICameraTool * cameraTool() const
      {
        if ( !m_CamTool )
        {
          m_CamTool = tool<ICameraTool>("CameraTool");
        }
        return m_CamTool;
      }

      /// Load smartID tool on demand
      const Rich::DAQ::IRawBufferToSmartIDsTool * smartIDDecoder() const
      {
        if ( !m_SmartIDDecoder )
        {
          acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, NULL, true );
        }
        return m_SmartIDDecoder;
      }

      /// Load the HPD disabling tool on demand
      Rich::Mon::IHpdUkL1DisableTool * hpdDisableTool() const
      {
        if ( !m_HpdUKL1DisableTool )
        {
          m_HpdUKL1DisableTool = 
            tool<Rich::Mon::IHpdUkL1DisableTool>("Rich::Mon::HpdUkL1DisableTool",
                                                 "RichUKL1Disable");
        }
        return m_HpdUKL1DisableTool;
      }

      /// ODIN info
      std::string odinInfo( const LHCb::ODIN * odin ) const
      {
        std::ostringstream mess;
        if ( odin )
          mess << "ODIN : Run="  << odin->runNumber()
               << " Event="      << odin->eventNumber()
               << " BunchID="    << odin->bunchId()
               << " OrbitNr="    << odin->orbitNumber()
               << " Trigger="    << odin->triggerType()
               << " EvtTime="    << odin->eventTime().format(true,"%X");
        return mess.str();
      }

    private:

      StatusCode SendErrorSummary(const std::string& odinInfo="");
      StatusCode PlotErrorHPD(const LHCb::RichSmartID &smartID);
      void ResetCounters();

    private:

      /// Pointer to RICH system detector element
      mutable const DeRichSystem * m_RichSys;

      /// Raw Buffer Decoding tool
      mutable const Rich::DAQ::IRawBufferToSmartIDsTool * m_SmartIDDecoder;

      /// HPD disabling tool
      mutable Rich::Mon::IHpdUkL1DisableTool * m_HpdUKL1DisableTool;

      mutable ICameraTool * m_CamTool;       ///< CAMERA error reporting tool

      unsigned long long m_nEvts;            ///< Event count
      unsigned long long m_nEvtsLastUpdate;  ///< Event count for last message
      bool          m_ExtendedVerbose;       ///< even more output
      ulonglong     m_OdinMinGpsTime;        ///< Time at which data is considered to be valid
      int           m_NL1Boards;             ///< Number of UKL1 boards used
      bool          m_CheckOdin;             ///< enable/disable checks comparing to the ODIN bank
      bool          m_RemoveFaultyHpdUKL1;   ///< remove faulty HPDs from data - stream in UKL1

      time_t        m_TimeLastUpdate;
      int           m_UpdateTimerInterval;   ///< Interval at which to send error messages (in seconds)
      bool          m_CheckEmptyHPD;         ///< check for empty HPDs (w/o extended headers)
      bool          m_MonitorBxID;           ///< Monitor BX ID errors (not simulated in FEST)
      unsigned int  m_MaxErrorMessages;      ///< Max. number of error messages before error report is sent
      std::string   m_Name;                  ///< Algorithm name + partition
      bool          m_PrintMessages;         ///< Print error messages, etc
      std::string   m_TaeLocation;           ///< TAE location to look at, "" = centra/default bin
      bool          m_AddSnapshotCameraMsg;  ///< Send messages to camera for possible snapshots

      bool  m_vetoNoBias;  ///< Veto no bias events

      int   m_AlertEmptyEvent;
      int   m_AlertActiveHPDs;
      int   m_AlertEmptyHPD;
      int   m_AlertDuplicateHPDL0;
      int   m_AlertEventID;
      int   m_AlertBxID;
      int   m_AlertL0ID;
      int   m_AlertInvalidOdinTime;
      int   m_AlertMissingOdin;
      int   m_AlertParityFooter;
      int   m_AlertHPDInhibit;
      int   m_AlertHPDNotInhibitInvalidSmartRef;
      int   m_AlertExtendedHeader;
      int   m_AlertHPDSuppressed;
      bool  m_SendAlertMessages;           ///< send text in addition to number of occourrance
      bool  m_SendAlertDirect;             ///< send alerts as they arise to CAMERA (or just every nth event as summary)
      int   m_AlertLevelBxID;              ///< level with which BsID errors are sent to CAMERA
      bool  m_Plot2DHisto;      
      int   m_MinErrorRate;

      /// ODIN info for the last event that gave a given error
      std::map< std::string, std::string > m_ODINLastError;

      std::map<std::string,int>  m_AlertMessagesEventID; ///< messages sent to CAMERA
      std::map<std::string,int>  m_AlertMessagesBxID;
      std::map<std::string,int>  m_AlertMessagesActiveHPD;
      std::map<std::string,int>  m_AlertMessagesL0ID;
      std::map<std::string,int>  m_AlertMessagesHPDInhibit;
      std::map<std::string,int>  m_AlertMessagesParityFooter;
      std::map<std::string,int>  m_AlertMessagesHPDNotInhibitInvalidSmartRef;
      std::map<std::string,int>  m_AlertMessagesExtendedHeader;
      std::map<std::string,int>  m_AlertMessagesHPDSuppressed;
      std::map<std::string,int>  m_AlertMessagesEmptyHPD;
      std::map<std::string,int>  m_AlertMessagesDuplicateHPDL0;

      /// Count triggers
      typedef std::map<LHCb::ODIN::TriggerType,unsigned long long> TriggerTypeMap;
      TriggerTypeMap m_TriggerTypeMap;

      /// Count event types (use the enum in future)
      typedef std::map<unsigned int,unsigned long long> EventTypeMap;
      EventTypeMap m_EventTypeMap;

      // cache histogram pointers for speed
      AIDA::IHistogram1D *m_odinH, *m_ErrorPerHardIDH, *m_HitPerHardIDH, *m_ErrorPerUKL1BoardH, *m_TruncatedEventUKL1;

    };

  }
}

#endif // RICHDAQMONITOR_H
