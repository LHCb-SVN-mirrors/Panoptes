// $Id: HPDCountTool.h,v 1.8 2009-05-12 08:18:38 rogers Exp $
#ifndef RICHONLINEMONITORS_HPDCOUNTTOOL_H
#define RICHONLINEMONITORS_HPDCOUNTTOOL_H 1

// Include files
// Interface
#include "IHPDCountTool.h"

// from RICH kernel
#include "RichKernel/RichDAQDefinitions.h"

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from std
#include <deque>
#include <map>

namespace Rich {
  namespace Mon {

    /** @class HPDCountTool HPDCountTool.h
     *
     * Monitors the HPDs following the moving average of their hit count per event.
     *
     * If the percentage difference between the number of hits on a HPD and the current moving
     * average of hits per event for this HPD is too large then an error is reported.
     *
     *  @author Claus Buszello
     *  @date   2007-05-30
     */
    class HPDCountTool : public GaudiTool,
                         virtual public IHPDCountTool
    {

    private:
      /*!
       * This private class is used to store the information for a HPD that
       * allows a moving average of the number of hits per event to be recorded.
       */
      class HPDCountInfo {
      public:
        /*!
         * Default constructor.
         *
         * Only a default constructor is provided. The member variable must be initialised
         * once the object is constructed.
         */
        HPDCountInfo()
          : m_nHits(0)
            , m_nHitsMA(0)
            , m_nHitsMADifference(20)
            , m_errorCount(0)
        {
        }// HPDCountInfo()

        int m_nHits; //!< Number of hits that were seen in the last event.
        int m_nHitsMA; //!< Moving average for the previous n hits.
        std::deque<int> m_nHitsMAData; //!< Holds the data for the last n events that are used to calculate the moving average.
        /*!
         * Allowed difference between the current number of hits and
         * current moving average before it is flagged as an error.
         */
        unsigned m_nHitsMADifference;
        int m_errorCount; //!< Number of errors that have occured.
      };// class HPDCountInfo

    public:

      /// Standard constructor
      HPDCountTool( const std::string& type,
                    const std::string& name,
                    const IInterface* parent);

      virtual ~HPDCountTool( ); ///< Destructor

      // Initialization of the tool after creation
      virtual StatusCode initialize();

      /*!
       * Takes a HPD smart ID and compares the number of hits to the upper limit specified in the
       * job options. If it exceeds this upper limit then an error report is produced.
       *
       * @param  hitHPD Smart ID of the HPD that the hits were seen on. Should contain no pixel information.
       * @param  nHits Number of hits that the HPD saw.
       * @param  errReport This is the error report generated for the HPD. If the message data member
       *          of ErrorReport is an empty string then no error was reported.
       * @return StatusCode.
       *
       * The test keeps track of the number of times that it has seen a noisey event on the given
       * HPD and will only report an error if it is seen more than the NoiseyTolerance option.
       */
      virtual StatusCode performTest(const LHCb::RichSmartID &hitHPD, int nHits, ErrorReport &errReport);

    private:

      std::map<LHCb::RichSmartID, HPDCountInfo> m_hpdInfoMap; //!< Links the statistics to a HPD. Contains an entry only for seen HPDs.

      int m_noiseyTolerence; //!< Number of events that can be noisey on a single HPD before an error is reported.
      unsigned  m_movingAverageDataEvents; //!< Number of events to consider for the the moving average.
    };//class HPDCountTool

  }//namespace Mon
}//namespace Rich

#endif//RICHONLINEMONITORS_HPDCOUNTTOOL_H
