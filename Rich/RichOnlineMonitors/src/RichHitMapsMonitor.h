#ifndef RICHMON_RichHitMapsMonitor_H
#define RICHMON_RichHitMapsMonitor_H 1

// std
#include <list>
#include <map>
#include <string>

// Base
#include "RichKernel/RichHistoAlgBase.h"

// Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiUtils/Aida2ROOT.h"

// Histograms
#include "AIDA/IHistogram2D.h"

// local
#include "ErrorReport.h"
#include "IHPDCountTool.h"
#include "IHotPixelReport.h"

// RICH kernel
#include "RichKernel/IRichRawBufferToSmartIDsTool.h"
#include "RichKernel/RichDAQDefinitions.h"
#include "RichKernel/RichSmartIDCnv.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// Boost
#include "boost/lexical_cast.hpp"

// CAMERA
#include "Camera/ICameraTool.h"

namespace Rich {
  namespace Mon {

    class HitMapsMonitor : public Rich::HistoAlgBase {

    private:
      /*!
       * This private class is used to store the histograms that are associated with a smart ID.
       *
       * It allows for direct access to the histograms through a selection of public data members.
       */
      class HitMapsInfo {
      public:
        /*!
         * Default constructor.
         *
         * Only a default constructor is provided. The member variable must be initialised
         * once the object is constructed.
         */
        inline HitMapsInfo()
          : mp_hitsPerHPDMap(NULL)
          , mp_lowResHitMapLhcb(NULL)
          , mp_highResHitMapLhcb(NULL)
          , mp_lowResHitMapAlice(NULL)
          , mp_highResHitMapAlice(NULL)
        {
        }// HitMapsInfo()

        // The class has only a default constructor as we never own the pointer to the
        // histograms and it is up to Gaudi to deal with their deletion.

        ///< Stores a pointer to the hit per HPD hit map.
        AIDA::IHistogram2D *mp_hitsPerHPDMap;
        ///< Stores a pointer to the full resolution hit map for LHCb mode.
        AIDA::IHistogram2D *mp_lowResHitMapLhcb;
        ///< Stores a pointer to the low resolution hit map for LHCb mode.
        AIDA::IHistogram2D *mp_highResHitMapLhcb;
        ///< Stores a pointer to the full resolution hit map for ALICE mode.
        AIDA::IHistogram2D *mp_lowResHitMapAlice;
        ///< Stores a pointer to the low resolution hit map for ALICE mode.
        AIDA::IHistogram2D *mp_highResHitMapAlice;
      };// class HitMapsInfo

    public:
      HitMapsMonitor(const std::string& name, ISvcLocator* pSvcLocator ); ///< Constructor
      virtual ~HitMapsMonitor( );      ///< Destructor

      virtual StatusCode initialize(); ///< Algorithm initialization
      virtual StatusCode execute   (); ///< Algorithm execution

    private:
      /*!
       * Books the hit map histograms for the given RICH panel.
       *
       * @param  rich RICH that the histogram should be booked for.
       * @param  panel Panel that the histogram should be booked for.
       * @return Status code.
       */
      StatusCode bookRichPanelHists(const Rich::DetectorType rich, 
                                    const Rich::Side panel);

      /*!
       * Books the hit maps for the given HPD.
       *
       * @param  smartID Smart ID of the HPD that the histograms should be booked for.
       * @return Status code.
       */
      StatusCode bookIndividualHists(const LHCb::RichSmartID &smartID);

      /*!
       * Constructs the ID of the RICH panel histogram that the HPD belongs in.
       *
       * @param  rich RICH that the histogram ID should be generated for.
       * @param  panel Panel that the histogram name should be generated for.
       * @return std::string ID of the histogram.
       *
       * This function is to be used with the book methods to create an appropriately named histogram.
       */
      std::string getRichPanelHistId(const Rich::DetectorType rich, 
                                     const Rich::Side panel);

      /*!
       * Constructs the name of the RICH panel histogram that the HPD belongs in.
       *
       * @param  rich RICH that the histogram ID should be generated for.
       * @param  panel Panel that the histogram name should be generated for.
       * @return std::string Name of the histogram.
       *
       * This function is to be used with the book methods to create an appropriately named histogram.
       */
      std::string getRichPanelHistName(const Rich::DetectorType rich, 
                                       const Rich::Side panel);

      /*!
       * Constructs the ID of the individual HPD hit map's histogram.
       *
       * @param  smartId Smart ID of the HPD whose histogram ID is to be constructed.
       * @return std::string ID of the histogram.
       *
       * This function is to be used with the book methods to create an appropriately named histogram.
       */
      std::string getIndividualHistId(const LHCb::RichSmartID &smartId);

      /*!
       * Constructs the name of the individual HPD hit map's histogram.
       *
       * @param  smartId Smart ID of the HPD whose histogram name is to be constructed.
       * @return std::string Name of the histogram.
       *
       * This function is to be used with the book methods to create an appropriately named histogram.
       */
      std::string getIndividualHistName(const LHCb::RichSmartID &smartId);

      /*!
       *
       */
      StatusCode submitErrorReports(int level, 
                                    const std::string &title, 
                                    std::list<ErrorReport> &erList,
                                    std::list<TH2D*> &histos);

    private:

      const DeRichSystem *mp_RichSys; ///< RICH detector.
      const Rich::DAQ::IRawBufferToSmartIDsTool *mp_SmartIDDecoder; ///< Decodes the raw banks into Smart IDs for use in monitoring alg.
      IHPDCountTool *mp_HPDCount;
      IHotPixelReport *mp_HotPix;
      //Camera tool for reporting messages.
      ICameraTool *mp_Camera;

      long int m_monFreq; ///< Can to limit the number of events that the algorithm see. Will only run if the number of events seen is a multiple of this number.
      int m_emptyEvents;  ///< Number of empty events seen.
      unsigned long long m_allEvents; ///< Number of events seen.
      long m_hotpixinterval;  /// Number of events between hot/cold pixel analysis
      bool m_hotPixEnabled; ///< If true then the HPD pixel find will run.
      bool m_hpdCntEnabled; ///< If true then the hot pixel enabled will run.
      bool m_hitsPerHPDMaps; ///< If true then the 2D histograms showing the number of hits per HPD will be filled.
      bool m_lowResHitMaps; ///< If true then the reduced resolution RICH panel hit maps will be filled.
      bool m_highResHitMaps; ///< This should only really be enabled for offline analysis. It creates hit maps at the full resolution of the RICHes, which are too big (in MBs) for use online.
      bool m_plotIndividualHpdMaps; ///< This should only really be enabled for offline analysis. It creates hit maps for each individual HPD.

      unsigned m_lowResScaleFactor; ///< Factor by which the number of bins on the x and y axis are reduced by for LHCb plots.

      const std::string m_aliceExt;  ///< This is the extension to be added to the histogram IDs and names for ALICE mode.
      const std::string m_lowResExt; ///< This is the extension to be added to the histogram IDs and names for low res plots.
      const std::string m_noiseExt;  ///< This is the extension to be added to the histogram IDs and names for the hits per HPD plots.
      std::string       m_Name;      ///< alg name including partition

      std::map<LHCb::RichSmartID, int> m_hpdCntSendHistoMap; ///< Each key in the map is a RICH panel level smart ID which maps to a counter to how many errors we saw on the corresponding RICH panel for the HPD count tool.
      std::map<LHCb::RichSmartID, int> m_hotPixSendHistoMap; ///< Each key in the map is a RICH panel level smart ID which maps to a counter to how many errors we saw on the corresponding RICH panel for the hot pixel tool.

      std::map<LHCb::RichSmartID, HitMapsInfo> m_hitMapsInfo; ///< Map to link smart IDs to the histograms and counters that are published for them.

    };// class HitMapsMonitor
  }// namespace Mon
}// namespace Rich

#endif// RICHMON_RichHitMapsMonitor_H
