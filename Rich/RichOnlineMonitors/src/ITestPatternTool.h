#ifndef ITestPatternTool_H
#define ITestPatternTool_H 1

#include "GaudiKernel/IAlgTool.h"

// Need this for LHCb::RichSmartID::Vector as forward declaring typedefs is difficult.
#include "Kernel/RichSmartID.h"

static const InterfaceID IID_ITestPatternTool("ITestPatternTool", 1, 0);

namespace Rich {
  namespace Mon {

    /**
     * @class ITestPattern
     *
     * @author Gareth Rogers
     * @date   2009-02-12
     */
    class ITestPattern : virtual public IAlgTool {

    public:

      /* Return the interface ID. */
      static const InterfaceID& interfaceID() { return IID_ITestPatternTool; }

      /* Inherited virtual functions. */
      virtual StatusCode initialize() = 0;
      virtual StatusCode finalize() = 0;

      /*!
       * Implementation should return a vector of smart IDs that specify the hits
       * for the given HPD smart ID that represent the test pattern. In addition
       * it should take flag to indicate whether the rows should be inverted.
       *
       * If the smart ID contains subpixel information then an ALICE mode pattern
       * is returned, if not then an LHCb mode pattern.
       */
      virtual LHCb::RichSmartID::Vector testPatternHits(const LHCb::RichSmartID &, bool) const = 0;

      /*!
       * Implementation should return the dimensions of a single HPD test pattern.
       *
       * RICH information is required to get the orientation correct only for asymmetric patterns.
       */
      virtual StatusCode histogramDimensions(Rich::DetectorType, int &, double &, double &, int &, double &, double &) const  = 0;

      /*!
       * Implementation should take the pixel coordinates on a HPD and convert them to
       * bins on an individual hit map for the test pattern. The hit map should be
       * compressed to have no spacing between hits where appropriate.
       *
       * The plot coordinates take a bool which can be used to invert the pixel rows
       * if this is required.
       */
      virtual StatusCode plotCoordinates(Rich::DetectorType, bool, int, int, int &, int &, bool) const = 0;

      /*!
       * Implementaiton should return the total number of hits on the test pattern.
       */
      virtual int totalTestPatternPixels() const = 0;

    };//class ITestPatternTool
  }// namespace Mon
}// namespace Rich

#endif//ITestPatternTool_H
