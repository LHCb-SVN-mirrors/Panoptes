//-----------------------------------------------------------------------------
/** @file RichTestPatternMon.cpp
 *
 *  Implementation file for algorithm class : RichTestPatternMon
 *
 *  @author Gareth Rogers rogers@hep.phy.cam.ac.uk
 *  @date   20/08/2007
 */

//-----------------------------------------------------------------------------
// Local
#include "RichTestPatternMon.h"

// Rich detector
#include "RichDet/DeRichSystem.h"

// Interfaces
#include "Camera/ICameraTool.h"
#include "RichKernel/IRichRawBufferToSmartIDsTool.h"
#include "RichKernel/RichSmartIDCnv.h"
#include "ITestPatternTool.h"

// Gaudi
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiUtils/Aida2ROOT.h"

//root
#include <TH1.h>
#include <TH2.h>
#include <TH2D.h>

// Boost
#include "boost/lexical_cast.hpp"

// Std
#include <cstdlib>
#include <cmath>
#include <sstream>

//-----------------------------------------------------------------------------
// Declaration of the Algorithm Factory
namespace Rich
{
  namespace Mon 
  {
    DECLARE_ALGORITHM_FACTORY(TestPatternMonitor)
  }// namespace Mon
}// namespace Rich

//-----------------------------------------------------------------------------
// Standard constructor, initializes variables
Rich::Mon::TestPatternMonitor::TestPatternMonitor(const std::string &name,
                                                  ISvcLocator *pSvcLocator)
  : Rich::HistoAlgBase(name, pSvcLocator)
  , mp_decoder(NULL)
  , mp_richSys(NULL)
  , mp_camera(NULL)
  , mp_testPattern(NULL)
  , m_hitsPerHPDMap("HitsPerHPD")
  , m_hitMap("HitMap")
  , m_deadHPDMap("NoHitsHPDMap")
  , m_effHPDMap("TestPatternEfficiencyHPDMap")
  , m_xEfficiencyProjection("XEfficiencyProfile")
  , m_yEfficiencyProjection("YEfficiencyProfile")
  , m_xDeadEfficiencyProjection("XNoHitsEfficiencyProfile")
  , m_yDeadEfficiencyProjection("YNoHitsEfficiencyProfile")
  , m_efficiencyCounter("EfficiencyCounter")
  , m_efficiencyHistogram("EfficiencyHist")
  , m_efficiencyMACounter("EfficiencyMACounter")
  , m_efficiencyMAHistogram("EfficiencyMAHist")
  , m_deadHPDCounter("DeadHPDCounter")
  , m_deadHPDHistogram("DeadHPDHistogram")
  , m_nEventsSeen(0)
  , m_nEvtsLastUpdate(0)
  , m_DeadPanelAverage(0)
  , m_deadRICHesFlag(false)
  , m_wrongTriggerFlag(false)
  , m_name(name)
  , m_TimeStart     ( 0 )
  , m_TimeLastUpdate( 0 )
  , m_MessagesCounter(0)
  , m_finalMsg("")
{
  declareProperty("ProduceDeadHPDMap", m_deadHPDMapFlag=true);
  declareProperty("ProduceEffHPDMap", m_effHPDMapFlag=true);
  declareProperty("ProduceHitsPerHPDMap", m_hitsPerHPDMapFlag=false);
  declareProperty("ProduceHitMap", m_hitMapFlag=false);
  declareProperty("ProduceIndividualHPDHitMaps", m_individualHitMapsFlag=false);
  declareProperty("EventMonRate", m_monitorRate=1);
  declareProperty("MovingAverageDataEvents", m_movingAverageDataEvents=70);
  declareProperty("MovingAverageHistogramEvents", m_movingAverageHistEvents=70);
  declareProperty("ErrorHPDEfficiency", m_errorEfficiency=70);
  declareProperty("DeadHPDEfficiency", m_deadEfficiency=70);
  declareProperty("ErrorReportingRate", m_errorReportRate=50000);
  declareProperty("DeadPanelReportingRate", m_DeadPanelReportRate=1000);
  declareProperty("FakeTestPattern", m_fakeTestPattern=false);
  declareProperty("InvertRows", m_invertRows=false);//Need to decide what the sensible default for this is.
  declareProperty("ExtendedVerbose", m_extendedVerbose=false);
  declareProperty("TAELocation", m_taeLocation="");
  declareProperty("CheckTriggerType", m_checkTriggerTypeFlag=true);
  declareProperty("DeadPanelThreshold", m_DeadPanelThreshold=75.0);
  declareProperty("UpdateTimerInterval" ,m_UpdateTimerInterval=600);
  // Done.
}// Rich::Mon::TestPatternMonitor::TestPatternMonitor(const std::string &name, ISvcLocator *pSvcLocator)

//-----------------------------------------------------------------------------
// Destructor
Rich::Mon::TestPatternMonitor::~TestPatternMonitor() {
  // Done.
}// Rich::Mon::TestPatternMonitor::~TestPatternMonitor()


// ----------------------------------------------------------------------
//  Initialize
StatusCode Rich::Mon::TestPatternMonitor::initialize()
{
  // Init base class.
  StatusCode sc(Rich::HistoAlgBase::initialize());
  if (sc.isFailure())
    return sc;

  // Get the partition name and add it to the algorithm name in the m_name variable.
  // This will then unqiuely identify messages from this algorithm in CAMERA.
  // We've already initialised it with the algorithm name so only append anything
  // if that PARTITION is defined. In Offline running it won't be.
  char *partitionName = getenv("PARTITION");
  if (NULL!=partitionName)
  {
    if (msgLevel(MSG::INFO)) 
    {
      info() << "Running in partition " << partitionName << endmsg;
    }// if(msgLevel(MSG::INFO))
    m_name += boost::lexical_cast<std::string>(partitionName);
  }// if(NULL!=partitionName)

  // Add TAE location to algorithm name.
  if (m_taeLocation == "")
    m_name += "Central";
  else
    m_name += m_taeLocation;

  // RichSmartIDDecoder is use to retrieve all the smart IDs present in an event.
  acquireTool("RichSmartIDDecoder", mp_decoder, 0, true);
  // The TestPatternTool points to a derived class of the ITestPattern and will
  // provide use with the structure of the test pattern to search for.
  // The exact tool given depends on that specified in the configurables and
  // ultimately the algorithm does not need to know what pattern it is searching
  // for just how.
  acquireTool("TestPattern", mp_testPattern);
  // CAMERA is used to submit our event by event error messages.
  mp_camera = tool<ICameraTool>("CameraTool");

  // This is the RICH detector and it is used to when we need to know something
  // about the RICH layout.
  mp_richSys = getDet<DeRichSystem>(DeRichLocations::RichSystem);

  // Decide what histograms to book.
  if (msgLevel(MSG::VERBOSE))
    verbose() << "Booking all the declared histograms." << endmsg;
  bookRichPanelHistograms(Rich::Rich1, Rich::top);
  bookRichPanelHistograms(Rich::Rich1, Rich::bottom);
  bookRichPanelHistograms(Rich::Rich2, Rich::left);
  bookRichPanelHistograms(Rich::Rich2, Rich::right);

  // get Time of startup
  m_TimeStart = m_TimeLastUpdate = time(NULL);

  // Done.
  if (msgLevel(MSG::DEBUG))
    debug() << "End of initialize() method." << endmsg;

  const std::string msg("Initialized");
  mp_camera->SendAndClearTS(ICameraTool::INFO, m_name, msg);

  // Done.
  return sc;
}// Rich::Mon::TestPatternMonitor::initialize()

// ----------------------------------------------------------------------
// Main execution
StatusCode Rich::Mon::TestPatternMonitor::execute() 
{
  if (msgLevel(MSG::DEBUG))
    debug() << "Start of execute() method." << endmsg;
  StatusCode status(StatusCode::SUCCESS);

  // Check all the 4 panels are dead (on average) and always drop the execute status.
  if(m_deadRICHesFlag)
  {
    if (msgLevel(MSG::DEBUG))
    {
      debug() << "End of execute() method." << endmsg;
      debug() << endmsg;
      // Send error message only once.
      // Since m_deadRICHesFlag = true a SendAndClearTS is automatically sent by appendErrorMessages
      if ( 0 == m_nEventsSeen%m_DeadPanelReportRate ) appendErrorMessages(false,true,false);
    }// if(msgLevel(MSG::DEBUG))
    return status;
  }

  // We will check to see if an ODIN bank exists and if it does
  // then we should only be running if calibration A trigger
  // has been sent. If we can't find an ODIN then we will carry
  // on anyway. Probably it is some kind of fake event where
  // the test pattern has been forced. Should always have an ODIN.
  LHCb::ODIN *odin(NULL);
  if (exist<LHCb::ODIN>(LHCb::ODINLocation::Default))
  {
    odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    if ( m_checkTriggerTypeFlag &&
         !((LHCb::ODIN::CalibrationTrigger==odin->triggerType()) && 
           (LHCb::ODIN::A==odin->calibrationType())))
    {
      // Set the flag m_wrongTriggerFlag, useful for the final report in the finalize method
      m_wrongTriggerFlag = true;

      debug() << "Odin Calibration Type: " <<odin->calibrationType() << endmsg;
      debug() << "Odin Trigger Type: " <<odin->triggerType() << endmsg;
      debug() << "Calibration Trigger Type: " <<LHCb::ODIN::CalibrationTrigger << endmsg;
      std::ostringstream msgTrig;
      msgTrig << "Wrong Trigger. Current Trigger Type: "<<odin->triggerType()
              << " (must be 7 = CalibrationTrigger). Current Calibration Type: "
              << odin->calibrationType() << " (must be A). No test pattern will be checked.";
      // Check that it's the first event
      if ( m_nEventsSeen == 0 )
      {
        // Send the message to the log file
        Info(msgTrig.str());
        // Send message to camera file:
//         mp_camera->Append("TEXT", msgTrig.str().c_str());
//         mp_camera->SendAndClearTS(ICameraTool::INFO,
//                                   m_name,
//                                   std::string("Wrong Trigger Type. No test pattern will be checked."));
      }
      ++m_nEventsSeen;
      return StatusCode::SUCCESS;
    }
  } // if ODIN exists.

  // Seen another event. Must be incremented at the start in order
  // to ensure we can normalise properly to the number of events seen.
  ++m_nEventsSeen;

  if (1==m_nEventsSeen) 
  {
    const std::string m = "First event seen";
    info() << m << endmsg;
    mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,m);
    m_TimeStart = m_TimeLastUpdate = time(NULL);
  }// if first event.

  //These are the maps containing the coordinates of the checked HPDs
  // (i.e. all the HPDs that have at least one hit)
  std::map<int , std::vector<int> > m_checkedHPD_Rich1_top;
  std::map<int , std::vector<int> > m_checkedHPD_Rich1_bottom;
  std::map<int , std::vector<int> > m_checkedHPD_Rich2_left;
  std::map<int , std::vector<int> > m_checkedHPD_Rich2_right;

  // Only monitor limited number of events to minimise CPU impact, etc..
  if (0 == m_nEventsSeen%m_monitorRate)
  {
    if (msgLevel(MSG::DEBUG))
      debug() << "Monitoring event " << m_nEventsSeen << endmsg;

    // Run over the events and count number of hits in the test pattern locations.

    // This is used to ensure that we only send an error message to CAMERA
    // when there is actually one present.
    bool errMsgPresent(false);

    //
    //START LOOP OVER ACTIVE HPDs
    //
    // Obtain smart IDs of HPDs with hits from the raw decoding.
    const Rich::DAQ::L1Map &l1Map(mp_decoder->allRichSmartIDs());

    // Iterate over all the HPDs getting to find the hits.
    Rich::DAQ::L1Map::const_iterator iL1Map;
    const Rich::DAQ::L1Map::const_iterator iL1MapBegin(l1Map.begin());
    const Rich::DAQ::L1Map::const_iterator iL1MapEnd(l1Map.end());
    // Holds the L1 ID of the current L1 board in the loop.

    for (iL1Map=iL1MapBegin; iL1Map!=iL1MapEnd; ++iL1Map)
    {
      // We must search through the HPDs on a per Ingress FPGA basis.
      // const Rich::DAQ::Level1ID &l1ID(iL1Map->first); // could be useful one day, but commented out to suppress warning about unused variables.
      const Rich::DAQ::IngressMap &ingressMap(iL1Map->second);
      // These are useful for error print out.
      const Rich::DAQ::Level1HardwareID l1HardID(iL1Map->first);
      const Rich::DetectorType richDetector(mp_richSys->richDetector(l1HardID));
      const Rich::DAQ::Level1LogicalID l1LogicalID(mp_richSys->level1LogicalID(l1HardID));

      // Iterate over the Ingress FPGAs to get the each HPD found on them.
      Rich::DAQ::IngressMap::const_iterator iIngressMap;
      const Rich::DAQ::IngressMap::const_iterator iIngressMapBegin(ingressMap.begin());
      const Rich::DAQ::IngressMap::const_iterator iIngressMapEnd(ingressMap.end());

      for (iIngressMap=iIngressMapBegin; iIngressMap!=iIngressMapEnd; ++iIngressMap)
      {
        // Get a list of all the HPDs on this Ingress FPGA.
        const Rich::DAQ::L1IngressID &l1IngressID = iIngressMap->first;
        const Rich::DAQ::IngressInfo &ingressInfo(iIngressMap->second);
        const Rich::DAQ::HPDMap &hpdMap(ingressInfo.hpdData());

        // Now we can loop over these HPDs for this Ingress FPGA and retrieve the hit map for this HPD.
        Rich::DAQ::HPDMap::const_iterator iHPDMap;
        const Rich::DAQ::HPDMap::const_iterator iHPDMapBegin(hpdMap.begin());
        const Rich::DAQ::HPDMap::const_iterator iHPDMapEnd(hpdMap.end());

        for (iHPDMap=iHPDMapBegin; iHPDMap!=iHPDMapEnd; iHPDMap++)
        {
          // Smart and hardware ID of the current HPD under observation.
          const Rich::DAQ::Level1Input l1Input(iHPDMap->first);
          const Rich::DAQ::HPDInfo &hpdInfo(iHPDMap->second);
          const Rich::DAQ::L1InputWithinIngress l1InputIngress(l1Input.l1InputWithinIngress());
          const LHCb::RichSmartID &hpdSmartID(hpdInfo.hpdID());

          // Check for error conditions on the smart ID that would prevent us analysing the data.
          if (!hpdSmartID.isValid()) 
          {
            if (msgLevel(MSG::DEBUG))
              debug() << "HPD SmartID is not valid, ignoring HPD on RICH " << richDetector
                      << ", UKL1 " << l1LogicalID
                      << ", Ingress FPGA " << l1IngressID
                      << ", channel " << l1InputIngress
                      << "." << endmsg;
            continue;
          }// SmartID valid
          if (hpdInfo.header().inhibit())
          {
            if (msgLevel(MSG::DEBUG))
              debug() << "HPD is inhibited, ignoring HPD on RICH " << richDetector
                      << ", UKL1 " << l1LogicalID
                      << ", Ingress FPGA " << l1IngressID
                      << ", channel " << l1InputIngress
                      << "." << endmsg;
            continue;
          }// HPD inhitbited.

          // This will hold the number of hits of the test pattern that the find function
          // found in the HPD hits.
          int nHits(0);
          // We must decide here whether to use the HPD hits or a fake test pattern.
          // Due to the necessity to use const variables we cannot just create the
          // variable and then change it later. As the variables are declared inside
          // the if they can only be used in this scope. This results in some duplicate
          // code, but it is hopefully kept to a minimum.
          if (!m_fakeTestPattern)
          {
            // Get the real hits from the smart ID.
            const LHCb::RichSmartID::Vector &hitSmartIDs(hpdInfo.smartIDs());
            if (msgLevel(MSG::VERBOSE)) 
            {
              verbose() << "Loop over " << hitSmartIDs.size() << " smart ID hits." << endmsg;
              if (m_extendedVerbose)
                verbose() << hitSmartIDs << endmsg;
            }// if(msgLevel(MSG::VERBOSE)
            // Now search for the test pattern hits in the data if there are any hits.
            // If there are no hits just leave the nHits as 0, it initialised value.
            if (0 != hitSmartIDs.size())
              nHits = find(hitSmartIDs);
            //Use the pattern finder tool to determine if this HPD contains the necessary hits.
            StatusCode sc = updateHPDStats(hpdSmartID, nHits);
            if ((StatusCode::FAILURE==sc) && msgLevel(MSG::WARNING))
            {
              std::ostringstream msg;
              msg << "Failed to create an entry for the HPD " << hpdSmartID.pdID() 
                  << ". Error checking cannot be performed for this panel.";
              mp_camera->Append(msg.str());
              mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,"Algorithm debugging info");
              debug() << msg.str() <<endmsg;
            }// if failed to update this HPDs stats.
            //Now fill the map with the checked HPDs
            else
            {
              // Create a global ordering object to ensure we plot things correctly.
              Rich::SmartIDGlobalOrdering globOrdering(hpdSmartID);
              if(hpdSmartID.rich() == Rich::Rich1 && hpdSmartID.panel() == Rich::top)
              {
                m_checkedHPD_Rich1_top[globOrdering.globalPdX()].push_back(globOrdering.globalPdY());}
              else if(hpdSmartID.rich() == Rich::Rich1 && hpdSmartID.panel() == Rich::bottom)
              {
                m_checkedHPD_Rich1_bottom[globOrdering.globalPdX()].push_back(globOrdering.globalPdY());
              }
              else if(hpdSmartID.rich() == Rich::Rich2 && hpdSmartID.panel() == Rich::left)
              {
                m_checkedHPD_Rich2_left[globOrdering.globalPdX()].push_back(globOrdering.globalPdY());
              }
              else if(hpdSmartID.rich() == Rich::Rich2 && hpdSmartID.panel() == Rich::right)
              {
                m_checkedHPD_Rich2_right[globOrdering.globalPdX()].push_back(globOrdering.globalPdY());
              }
              else  
              {
                debug()<<"Invalid RICH type or RICH panel (must be: either RICH1(top or bottom) or RICH2(left or right))"
                       << endmsg;
              }
            }//Checked HPDs map filled.
            //HPD stats updated
          }//if(!m_fakeTestPattern)
          else 
          {
            // We only need to be warned a few times about this. It won't change later.
            if (msgLevel(MSG::INFO))
            {
              info() <<"Using fake test pattern."<<endmsg;
            }
            if(m_nEventsSeen == 1)mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,std::string("Using fake test pattern."));
            // Ask the test pattern for a list of hits on the given smart ID in the pattern coordinates.
            const LHCb::RichSmartID::Vector &hitSmartIDs(mp_testPattern->testPatternHits(hpdSmartID, m_invertRows));
            if (msgLevel(MSG::VERBOSE)) {
              verbose() << "Loop over " << hitSmartIDs.size() << " fake smart ID hits." << endmsg;
              if (m_extendedVerbose)
                verbose() << hitSmartIDs << endmsg;
            }//if(msgLevel(MSG::VERBOSE)

            //Now search for the hits in the data.
            nHits = find(hitSmartIDs);
            //Use the pattern finder tool to determine if this HPD contains the necessary hits.
            StatusCode sc = updateHPDStats(hpdSmartID, nHits);
            if ((StatusCode::FAILURE==sc) && msgLevel(MSG::WARNING)) 
            {
              std::ostringstream msg;
              msg << "Failed to create an entry for the HPD " << hpdSmartID.pdID() << ". Error checking cannot be performed for this panel.";
              mp_camera->Append(msg.str());
              mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,"Algorithm debugging info");
            }// if failed to update this HPDs stats.
          }//else(!m_fakeTestPattern)
          // If we have verbose print out it is useful to mark the change to a new HPD.
          if (msgLevel(MSG::VERBOSE))
            verbose() << endmsg;
        }//for HPDMap
      }//for ingressMap
    }//for iL1Map
    //
    //END LOOP OVER ACTIVE HPDs
    //

    // Need to update the statistics for each of the 4 HPD panels.
    // Counter of Dead Panels:
    int deadPanelsCounter(0);
    // RICH1 top panel.
    LHCb::RichSmartID panelID(Rich::Rich1, Rich::top);
    if ((StatusCode::FAILURE==updateRichPanelStats(panelID, m_checkedHPD_Rich1_top))) {
      if(msgLevel(MSG::WARNING)){
        std::ostringstream msg;
        msg << "Failed to create an entry for the panel " << panelID << ". Error checking cannot be performed for this panel.";
        mp_camera->Append(msg.str());
        mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,"Algorithm debugging info");
      }
    }// if failed to update RICH panel stats.
    else {
      std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelInfoIt(m_panelInfoMap.find(panelID.panelID()));
      if(panelInfoIt->second.m_panelDead == true) deadPanelsCounter++;
    }// check to see if the panel is dead

    // RICH1 Bottom panel
    panelID.setPanel(Rich::bottom);
    if ((StatusCode::FAILURE==updateRichPanelStats(panelID, m_checkedHPD_Rich1_bottom))) {
      if(msgLevel(MSG::WARNING)){
        std::ostringstream msg;
        msg << "Failed to create an entry for the panel " << panelID << ". Error checking cannot be performed for this panel.";
        mp_camera->Append(msg.str());
        mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,"Algorithm debugging info");
      }
    }// if failed to update RICH panel stats.
    else {
      std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelInfoIt(m_panelInfoMap.find(panelID.panelID()));
      if(panelInfoIt->second.m_panelDead == true) deadPanelsCounter++;
    }// check to see if the panel is dead

    //  RICH2 Left panel
    panelID.setRich(Rich::Rich2);
    panelID.setPanel(Rich::left);
    if ((StatusCode::FAILURE==updateRichPanelStats(panelID,  m_checkedHPD_Rich2_left))) {
      if(msgLevel(MSG::WARNING)){
        std::ostringstream msg;
        msg << "Failed to create an entry for the panel " << panelID << ". Error checking cannot be performed for this panel.";
        mp_camera->Append(msg.str());
        mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,"Algorithm debugging info");
      }
    }// if failed to update RICH panel stats.
    else 
    {
      std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelInfoIt(m_panelInfoMap.find(panelID.panelID()));
      if(panelInfoIt->second.m_panelDead == true) deadPanelsCounter++;
    }// check to see if the panel is dead

    // RICH2 Right panel
    panelID.setPanel(Rich::right);
    if ((StatusCode::FAILURE==updateRichPanelStats(panelID, m_checkedHPD_Rich2_right))) {
      if( msgLevel(MSG::WARNING) ){
        std::ostringstream msg;
        msg << "Failed to create an entry for the panel " << panelID << ". Error checking cannot be performed for this panel.";
        mp_camera->Append(msg.str());
        mp_camera->SendAndClearTS(ICameraTool::INFO,m_name,"Algorithm debugging info");
      }
    }// if failed to update RICH panel stats.
    else {
      std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelInfoIt(m_panelInfoMap.find(panelID.panelID()));
      if(panelInfoIt->second.m_panelDead == true) deadPanelsCounter++;
    }// check to see if the panel is dead

    // Check every m_DeadPanelReportRate that the mean number of deadPanel is < 4
    if(0==m_nEventsSeen%m_DeadPanelReportRate){
      m_DeadPanelAverage += deadPanelsCounter;
      m_DeadPanelAverage = (m_DeadPanelAverage*100)/(m_DeadPanelReportRate * 4);
      if(m_DeadPanelAverage > m_DeadPanelThreshold){
        // Enable flag to skip the execute() method when the RICHes are not active
        // and skip the execute method immediately.
        m_deadRICHesFlag = true;
        return StatusCode::SUCCESS;
      }
      m_DeadPanelAverage = 0;
    }
    else 
    {
      m_DeadPanelAverage += deadPanelsCounter;
    }

    // Now we build the CAMERA error messages based on the runs over the infoMaps.
    // A message is sent to CAMERA every m_UpdateTimerInterval seconds.
    // Normally the message is of INFO type.
    // The option " If the dead efficiency or the fluctuating efficiency is larger than 0,
    // and if the number of messages sent to Camera is 0 or a multiple of 10 then
    // the message is sent with WARNING type. " won't be used anymore. All the messages are INFO.
    const time_t currentTime = time(NULL);
    const bool timePassed = ( (m_UpdateTimerInterval>0) && 
                              ((currentTime - m_TimeLastUpdate) > m_UpdateTimerInterval)) ;

    // Send the error message even if m_errorReportRate haven't been processed.
    // errMsgPresent == true is any message have been appended before.
    // I recently eliminated all the instances that do that.
    if ( timePassed || errMsgPresent )
    {
      const time_t deltaT = (currentTime - m_TimeLastUpdate);
      struct tm * timeinfo = localtime ( &deltaT );

      if (msgLevel(MSG::DEBUG))
        debug() << "CAMERA -> SendAndClearTS." << endmsg;

      const unsigned int nNewEvts = m_nEventsSeen-m_nEvtsLastUpdate;
      std::ostringstream errMsg;
      errMsg << "Summary : Seen " << nNewEvts << " events in past ";
      if ( timeinfo->tm_hour-1 > 0 ) { errMsg << timeinfo->tm_hour-1 << " h "; }
      if ( timeinfo->tm_min    > 0 ) { errMsg << timeinfo->tm_min << " min "; }
      if ( timeinfo->tm_sec    > 0 ) { errMsg << timeinfo->tm_sec << " s "; }
      errMsg << "( " << (double)(nNewEvts) / (double)(deltaT)
             << " Evt/s )";

      const bool HPDmisbehaving = appendErrorMessages(true,true);
      verbose() <<"HPDmisbehaving flag: "<<HPDmisbehaving << endmsg;

      // Add Odin Info:
      std::ostringstream odinMsg;
      odinMsg <<"ODIN Information: Run Number: "<< odin->runNumber()
              << ", Event Number: " <<odin->eventNumber()
              << ", Bunch Id(): " <<odin->bunchId()
              << ", Orbit Number: " <<odin->orbitNumber()
              << ", Event Type: " <<odin->eventType()
              << ", Odin Calibration Type: " <<odin->calibrationType()
              << ", Odin Trigger Type: " <<odin->triggerType();
      mp_camera->Append(odinMsg.str());

      // Option " If the dead efficiency or the fluctuating efficiency is larger than 0,
      // and if the number of messages sent to Camera is 0 or a multiple of 10 then
      // the message is sent with WARNING type. " This is not used anymore. All the messages are INFO.
      //       if(((0==m_MessagesCounter%10) || (m_MessagesCounter == 0)) && HPDmisbehaving)
      //         mp_camera->SendAndClearTS(ICameraTool::WARNING, m_name, errMsg.str().c_str());
      //       else
      mp_camera->SendAndClearTS(ICameraTool::INFO, m_name, errMsg.str().c_str());

      ++m_MessagesCounter;
      m_TimeLastUpdate = currentTime;
      m_nEvtsLastUpdate = m_nEventsSeen;

    }//if (timePassed || errMsgPresent)
  }//if(0==m_nEventsSeen%m_monitorRate) N.B. m_monitorRate == 1

  // Done.
  if (msgLevel(MSG::DEBUG)) {
    debug() << "End of execute() method." << endmsg;
    debug() << endmsg;
  }// if(msgLevel(MSG::DEBUG))
  return status;
}// Rich::Mon::TestPatternMonitor::execute()

// ----------------------------------------------------------------------
//  Finalize
StatusCode Rich::Mon::TestPatternMonitor::finalize() {
  // Execute base class method.
  if (msgLevel(MSG::DEBUG))
    debug() << "Start of finalize() method." << endmsg;

  // Send the last report both to camera and to the log file.
  std::ostringstream msg1;
  msg1 <<"";
  mp_camera->Append("TEXT", msg1.str().c_str());
  std::ostringstream msg2;
  msg2 <<"LAST EVENT SUMMARY:";
  mp_camera->Append("TEXT", msg2.str().c_str());
  appendErrorMessages(true,true,true);
  mp_camera->Append("TEXT", m_finalMsg.str().c_str());
  // Add some line (antitled HISTORY) to remember if for any reason the
  // execute() method have been skipped for any reason.
  if(m_deadRICHesFlag || m_wrongTriggerFlag){
    mp_camera->Append("TEXT", " ");
    mp_camera->Append("TEXT", "HISTORY:");
    std::ostringstream msgHistory;
    if(m_deadRICHesFlag){
      msgHistory << "The algorithm has been stopped after "
                 <<m_DeadPanelReportRate<<" events "
                 <<"since all the RICH panels show a percentage of HPDs with no hits > "
                 <<m_DeadPanelThreshold<< "p.c. ";
      mp_camera->Append("TEXT",msgHistory.str().c_str());
    }
    else if(m_wrongTriggerFlag){
      msgHistory << "The algorithm has been stopped since"
                 <<" either the wrong  Trigger Type or the wrong Calibration Type"
                 <<" has been set for this run.";
      mp_camera->Append("TEXT",msgHistory.str().c_str());
    }

  }
  std::ostringstream msg;
  msg <<"Test Pattern Monitor Final Summary";
  mp_camera->SendAndClearTS(ICameraTool::INFO, m_name, msg.str());

  // Done.
  if (msgLevel(MSG::DEBUG))
    debug() << "End of finalize() method." << endmsg;
  return Rich::HistoAlgBase::finalize();
}// Rich::Mon::TestPatternMonitor::finalize()

// ----------------------------------------------------------------------
int Rich::Mon::TestPatternMonitor::find(const LHCb::RichSmartID::Vector &pixelHitIDs) {
  if (msgLevel(MSG::VERBOSE))
    verbose() << "Searching " << pixelHitIDs.size() << " smartID hits. First smartID: " << *(pixelHitIDs.begin()) << endmsg;
  //   if (m_extendedVerbose && msgLevel(MSG::VERBOSE))
  //     verbose() << "All hits! " << pixelHitIDs << endmsg;

  // We will return the number of hits that we found on this HPD.
  int nHits(0);

  // Get points to the first and last hit.
  LHCb::RichSmartID::Vector::const_iterator hitIter;
  LHCb::RichSmartID::Vector::const_iterator hitIterBegin(pixelHitIDs.begin());
  LHCb::RichSmartID::Vector::const_iterator hitIterEnd(pixelHitIDs.end());
  // This will hold the smart ID of the HPD that we are searching for the test pattern.
  // It will then be updated with the particular hit in the test pattern we are searching for.
  // Take the first one as only the pixel information should change in the smart IDs in the vector
  // and we will modify those in this variable anyway.
  LHCb::RichSmartID testPatternHitSmartID(hitIterBegin->pdID());
  // #bins and bin borders for 2D histograms displaying the test pattern.
  // All these will be updated in the loop for the specific HPD.
  int nBinsX(0), localX(0), plotX(0);
  double xMin(1.0), xMax(1.0);
  int nBinsY(0), localY(0), plotY(0);
  double yMin(1.0), yMax(1.0);

  // Get some information about this HPD, this is common to all the smart ID hits.
  // Need to know our RICH and panel. These will only be required if we are plotting
  // hit maps, but it saves getting them once for each hit.
  Rich::DetectorType rich(hitIterBegin->rich());
  Rich::Side panel(hitIterBegin->panel());
  // Get the test pattern information object that contains the histograms we wish to fill for the panel.
  RichPanelTestPatternInfo &panelInfo(m_panelInfoMap[hitIterBegin->panelID()]);
  // Get the test pattern information object that contains the histograms we wish to fill for the panel.
  HPDTestPatternInfo &hpdInfo(m_hpdInfoMap[hitIterBegin->pdID()]);

  // Get the test pattern, ensuring that we give a pixel hit so it knows whether to return
  // a ALICE mode pattern or not.
  LHCb::RichSmartID::Vector theTestPatternHits(mp_testPattern->testPatternHits(*hitIterBegin, m_invertRows));
  // Now loop over the hits in the test pattern and search the hit pixels for the test pattern hits.
  LHCb::RichSmartID::Vector::iterator patternIt;
  const LHCb::RichSmartID::Vector::iterator patternBeginIt(theTestPatternHits.begin());
  const LHCb::RichSmartID::Vector::iterator patternEndIt(theTestPatternHits.end());
  for (patternIt=patternBeginIt; patternIt!=patternEndIt; ++patternIt) {
    // Now see if this is present in the hits for this HPD.
    if (hitIterEnd != std::find(hitIterBegin, hitIterEnd, *patternIt)) {
      // We have found this pixel has a hit, lets add it to the histograms and counters.
      if (msgLevel(MSG::VERBOSE))
        verbose() << "Consider new hit " << *patternIt << endmsg;

      // Note one of the test pixels was hit.
      ++nHits;
      if (m_extendedVerbose && msgLevel(MSG::VERBOSE))
        verbose() << "#hits for HPD " << testPatternHitSmartID << " currently is " << nHits << endmsg;

      // Now we will deal with the 2D histograms, but only do the necessary set up if we
      // are going to plot at least one of the hit maps. Ignore the dead HPD map as we do that
      // only once we know the HPD is dead. Plot the hits per HPD once this HPD outside the loop.
      if (m_hitMapFlag || m_individualHitMapsFlag) {
        // Create a global ordering object to ensure we plot things correctly.
        Rich::SmartIDGlobalOrdering gOrdering(*patternIt);
        // Need to know whether we are in ALICE mode or not to ensure we plot things correctly.
        const bool alice(patternIt->pixelSubRowDataIsValid());

        // This will return the x and y coordinate for the individual HPD.
        localX = gOrdering.localPixelX();
        localY = gOrdering.localPixelY();
        // Now ask the test pattern object to convert these to a test pattern hit plot point.
        // It converts from the 32x32/256 HPD pixel array to the test pattern array e.g. 2x2.
        mp_testPattern->plotCoordinates(rich, alice, localX, localY, plotX, plotY, m_invertRows);
        if (m_extendedVerbose && msgLevel(MSG::VERBOSE)) {
          verbose() << "Local plotX: " << plotX << endmsg;
          verbose() << "Local plotY: " << plotY << endmsg;
        }// if extended verbose

        // We need to know the dimensions of the histograms that we are plotting.
        // These are for the individual HPDs. Will need to scale up for full hit maps.
        mp_testPattern->histogramDimensions(rich, nBinsX, xMin, xMax, nBinsY, yMin, yMax);

        if (m_hitMapFlag) {
          // Fill the histogram and then if it failed and we are printing error messages
          // inform people what failed.
          StatusCode sc(fillRichPanel2D(rich, panel, &panelInfo.mp_hitMap, gOrdering.globalPixelX(plotX, static_cast<int>(xMax)), gOrdering.globalPixelY(plotY, static_cast<int>(yMax))));
          if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
            debug() << "Attempted to plot hits on the hit map histogram for " << rich << ", " << Rich::text(rich,panel) << " and it failed." << endmsg;
        }// if(m_hitMapFlag)

        // If we are plotting the individual hit maps then update it.
        // These are book on demand as we don't know at start up which HPDs we are expecting.
        if (m_individualHitMapsFlag) {
          StatusCode sc(fillHPD2D(*patternIt, &hpdInfo.mp_hitMap, plotX, plotY));
          if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
            debug() << "Attempted to plot hits on the hit map histogram for " << *patternIt << " and it failed." << endmsg;
        }// if(m_individualHitMapsFlag)

      }// if plotting a 2D hit map.
    }//if(hitIterEnd!=hitIter)
    else {
      if (msgLevel(MSG::VERBOSE))
        verbose() << "Didn't find a hit corresponding to test pattern hit " << *patternIt << endmsg;
    }//else(hitIterEnd!=hitIter)
  }//for patternIt

  // Only attempt to update the histogram if any hits were found.
  // If we are passed an empty vector then this will really mess up this call as invalid smartIDs start appearing.
  if ((0!=nHits) && m_hitsPerHPDMapFlag) {
    // Fill the histogram and then if it failed and we are printing error messages
    // inform people what failed.
    // Create a global ordering object to ensure we plot things correctly.
    Rich::SmartIDGlobalOrdering gOrdering(hitIterBegin->pdID());
    // Weight the entry by the number of hits that was found in order to reduce the number of calls.
    StatusCode sc(fillRichPanel2D(rich, panel, &panelInfo.mp_hitsPerHPDMap, gOrdering.globalPdX(), gOrdering.globalPdY(), nHits));
    if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
      debug() << "Attempted to plot hits for the hits per HPD histogram for " << rich << ", " << Rich::text(rich,panel) << " and it failed." << endmsg;
  }// if(m_hitsPerHPDMapFlag)

  if (msgLevel(MSG::VERBOSE))
    verbose() << nHits << " found for " << testPatternHitSmartID << endmsg;

  //Done.
  return nHits;
}// Rich::Mon::TestPatternMonitor::find()


// ----------------------------------------------------------------------
StatusCode Rich::Mon::TestPatternMonitor::updateHPDStats(const LHCb::RichSmartID &hpdID,
                                                         const int nHits) {
  // Return value.
  StatusCode sc(StatusCode::SUCCESS);
  // We need to find the HPD in the map and if it is not there then
  // create an entry and book everything.
  // Must becareful to ensure we don't have any hit information as this
  // could create multiple entries for the same HPD.
  std::map<LHCb::RichSmartID, HPDTestPatternInfo>::iterator infoIt(m_hpdInfoMap.find(hpdID.pdID()));
  // Book on demand if this has not been done. The book will ensure we use on the HPD
  // information and not pixel in the smart ID.
  if (m_hpdInfoMap.end() == infoIt) {
    bookIndividualHistograms(hpdID);
    infoIt = m_hpdInfoMap.find(hpdID.pdID());
    if (m_hpdInfoMap.end() == infoIt) {
      if (msgLevel(MSG::INFO)) {
        // We want to limit the number of error messages that we display in this case.
        std::ostringstream msg;
        msg << "Failed to create an entry for the HPD " << hpdID.pdID() << ". Error checking cannot be performed for this panel.";
        // This will return with the status code failure by default.
        info() <<msg.str() <<endmsg;
        return StatusCode::FAILURE;
      } // if warning message level.
      else
        return StatusCode::FAILURE;
    }// if not found HPD in info map after booking.
  }// if not found HPD in info map.

  // Note that we can only get this far if the entry is in the map.

  // Save the number of hits. Don't do anything on the individual HPD level with this number.
  // Use for the panel level stats. Also useful to know.
  infoIt->second.m_nHits = nHits;

  // Calculate the efficiency moving average for this HPD.
  // Need to convert the nHits into an efficiency. In order to keep
  // everything as ints and to a sensible precision store the efficiency
  // as a whole number of percent. This means multiplying by 100 first
  // and then dividing by the number of hits to ensure that we don't drop
  // below zero unless it is actually some fraction of a %.
  // This always rounds down.
  infoIt->second.m_efficiency = (nHits*100) / mp_testPattern->totalTestPatternPixels();
  // Save this efficiency for the moving average calculation.
  infoIt->second.m_efficiencyMAData.push_back(infoIt->second.m_efficiency);
  // We are maintaining the moving average over a constant number of elements.
  if (m_movingAverageDataEvents < infoIt->second.m_efficiencyMAData.size())
    infoIt->second.m_efficiencyMAData.pop_front();

  // Loop over the deque and sum up all the element values.
  int sum(0);
  const std::deque<int>::const_iterator maDataBeginIt(infoIt->second.m_efficiencyMAData.begin());
  const std::deque<int>::const_iterator maDataEndIt(infoIt->second.m_efficiencyMAData.end());
  for (std::deque<int>::const_iterator maDataIt(maDataBeginIt); maDataIt!=maDataEndIt; ++maDataIt)
    sum += *maDataIt;

  // Now determine the average and store it in our class variable.
  // This will enter the HPD into the map if it didn't exist or
  // just over write it if it does.
  infoIt->second.m_efficiencyMA = sum / infoIt->second.m_efficiencyMAData.size();

  // Check if HPD is alive by checking its moving average is greater than a user defined efficiency.
  if (m_deadEfficiency < infoIt->second.m_efficiencyMA) {
    // We want to ensure that our HPD is behaving `normally', where we
    // define normally to be as it has been behaving.
    // Do this by checking the current moving average is not too different from before.
    // Check to see if the percentage difference between the current efficiency
    // and the moving average of the efficiency has changed by too large an amount.
    // We will adapt the amount of change we consider an error in order to keep
    // the number of messages we send fairly low (in the error checking function).
    // Remain as an integer calculation for the moment.
    infoIt->second.m_isDead = false;
    if (abs((infoIt->second.m_efficiencyMA-infoIt->second.m_efficiency)/infoIt->second.m_efficiencyMA) > infoIt->second.m_effMADifference)
      ++(infoIt->second.m_errorCount);
  }// if HPD is alive.
  // The HPD is considered to be dead so increment the dead count
  // There is only a counter to represent this no histogram for the individual HPD.
  // The counter has not necessarily been declared but we still use
  // the variable for error checking.
  // We will plot the HPD on the panel dead HPD count in the updateRichPanelStats function.
  else {
    if (msgLevel(MSG::VERBOSE)) {
      verbose() << "m_nEventsSeen    -- " << m_nEventsSeen << endmsg;
      verbose() << "hpd->m_deadCount -- " << infoIt->second.m_deadCount << endmsg;
      verbose() << "Now incrementing the dead count for this event." << endmsg;
    }// if(msgLevel(MSG::VERBOSE))
    ++(infoIt->second.m_deadCount);
    infoIt->second.m_isDead = true;
  }// else HPD is dead

  if (msgLevel(MSG::DEBUG)) {
    debug() << hpdID << endmsg;
    debug() << "nHits         -- " << nHits << endmsg;
    debug() << "nExpectedHits -- " << mp_testPattern->totalTestPatternPixels() << endmsg;
    debug() << "Efficiency    -- " << infoIt->second.m_efficiency << endmsg;
    debug() << "Efficiency MA -- " << infoIt->second.m_efficiencyMA << endmsg;
    debug() << "Error count   -- " << infoIt->second.m_errorCount << endmsg;
    debug() << "Dead count    -- " << infoIt->second.m_deadCount << endmsg;
  }// if(msgLevel(MSG::DEBUG))
  //Done.
  return sc;
}//StatusCode Rich::Mon::TestPatternMonitor::updateHPDStats()

// ----------------------------------------------------------------------
StatusCode
Rich::Mon::TestPatternMonitor::updateRichPanelStats(const LHCb::RichSmartID &panelID,
                                                    const std::map<int,std::vector<int> > &checkedHPD_map) {
  // Return value.
  StatusCode sc(StatusCode::SUCCESS);
  // We need to find the HPD in the map and if it is not there then
  // create an entry and book everything.
  // Becareful as we don't want to create an entry for multiple panel if more info is set in the panel ID.
  // This is very important when booking the HPD.
  std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelInfoIt(m_panelInfoMap.find(panelID.panelID()));
  // Book on demand if this has not been done. The book will ensure we use on the HPD
  // information and not pixel in the smart ID.
  // By this stage the histograms should have been filled, hence if it doesn't exist then there
  // must have been something serious. Still we need to get the object anyway and the check
  // doesn't hurt.
  if (m_panelInfoMap.end() == panelInfoIt) {
    bookRichPanelHistograms(panelID.rich(), panelID.panel());
    panelInfoIt = m_panelInfoMap.find(panelID.panelID());
    if (m_panelInfoMap.end() == panelInfoIt) {
      if (msgLevel(MSG::INFO)) {
        // We want to limit the number of error messages that we display in this case.
        std::ostringstream msg;
        msg << "Failed to create an entry for the RICH panel " << panelID.pdID() << ". Error checking cannot be performed for this panel.";
        // This will return with the status code failure by default.
        info() <<msg.str() <<endmsg;
        return StatusCode::FAILURE;
      } // if warning message level.
      else
        return StatusCode::FAILURE;
    }// if not found HPD in info map after booking.
  }// if not found HPD in info map.

  // Note that we can only get this far if the entry is in the map.

  // This counts the total number hits that were seen.
  int nHits(0);
  // This counts the number of HPDs that are in error.
  int errorCount(0);
  // This counts the number of HPDs that we saw as dead.
  int deadCount(0);
  // This counts the number of HPDs checked in the event.
  int checkedHPDsCount(0);


  // Add information on the unchecked HPDs. These could have been disabled,
  // or they didn't show any active pixel because the test pattern haven't been sent to the HPDs.
  // Dimensions of the histograms in terms of the HPDs.
  int xmin (0);
  int xmax(Rich::Rich2==panelID.rich() ? 8:13);
  //   int xmin(gOrdering.minHPDX());
  //   int xmax(gOrdering.maxHPDX());
  if ((Rich::Rich2==panelID.rich()) && (Rich::left==panelID.panel())) {
    // In the case of RICH2 we need to alter the x-axis to shift the axis to cover only the appropriate panel.
    // A-side start half way along the x-axis at the start of C-side.
    xmin += (xmax + 1);
    xmax += (xmax + 1);
  }// if((Rich::Rich2)&&(Rich::left==panel))

  int ymin(0);
  int ymax(Rich::Rich2==panelID.rich() ? 15:6);
  //   int ymin(gOrdering.minHPDY());
  //   int ymax(gOrdering.maxHPDY());
  if ((Rich::Rich1==panelID.rich()) && (Rich::top==panelID.panel())) {
    // In the case of RICH1 we need to alter the y-axis to shift it to cover only the appropriate panel.
    // Top panel starts half way up the y-axis after the bottom panel has ended.
    ymin += (ymax + 1);
    ymax += (ymax + 1);
  }// if((Rich::Rich1==rich)&&(Rich::top==panel))

  //
  // START OF THE LOOP ON CHECKED HPDs (in this event or in previuos events)
  //
  const std::map<LHCb::RichSmartID, HPDTestPatternInfo>::iterator mapBeginIt(m_hpdInfoMap.begin());
  const std::map<LHCb::RichSmartID, HPDTestPatternInfo>::iterator mapEndIt(m_hpdInfoMap.end());
  for (std::map<LHCb::RichSmartID, HPDTestPatternInfo>::iterator mapIt(mapBeginIt); mapIt!=mapEndIt; ++mapIt) {
    // The info map contains not only the stats for the HPDs but also the stats for the panel that we are
    // currently accumulating stats for.
    // We loop through this map and identify the HPDs that are on this RICH panel, however we must check
    // that we don't have the panel ID that we are currently setting the stats for!
    if ((mapIt->first!=panelID) && (mapIt->first.panelID()==panelID)) {
      // Create a global ordering object to ensure we plot things correctly.
      Rich::SmartIDGlobalOrdering gOrdering(mapIt->first);
      // Check that the HPD have been effectively checked in the CURRENT event (checkedHPD_Flag == true).
      int hpd_x(gOrdering.globalPdX());
      int hpd_y(gOrdering.globalPdY());
      bool checkedHPD_xFlag = false;
      if(checkedHPD_map.find(hpd_x) != checkedHPD_map.end())checkedHPD_xFlag = true;
      bool checkedHPD_Flag = false;
      if(checkedHPD_xFlag){
        std::vector<int>::const_iterator hpd_y_ItBegin((checkedHPD_map.find(hpd_x)->second).begin());
        std::vector<int>::const_iterator hpd_y_ItEnd((checkedHPD_map.find(hpd_x)->second).end());
        if(std::find(hpd_y_ItBegin, hpd_y_ItEnd , hpd_y) != hpd_y_ItEnd){
          checkedHPD_Flag = true; // true if the HPD have been checked in this event.
          checkedHPDsCount++;
        }
      }

      //Perform operations on HPDs cheched IN THIS EVENT
      if(checkedHPD_Flag){

        // Add on the number of hits seen. This might not be 0 for a dead HPD as there dead HPD threshold is configurable.
        nHits += mapIt->second.m_nHits;

      }//if the HPD have been checked in this event

      // Perform operations on the HPDs CHECKED but NOT IN THIS EVENT
      // The statistic of these HPDs havent't been updated before, it must be done now:
      else {
        mapIt->second.m_nHits = 0;
        // Calculate the efficiency moving average for this HPD.
        // Need to convert the nHits into an efficiency. In order to keep
        // everything as ints and to a sensible precision store the efficiency
        // as a whole number of percent. This means multiplying by 100 first
        // and then dividing by the number of hits to ensure that we don't drop
        // below zero unless it is actually some fraction of a %.
        // This always rounds down.
        mapIt->second.m_efficiency = (mapIt->second.m_nHits*100) / mp_testPattern->totalTestPatternPixels();
        // Save this efficiency for the moving average calculation.
        mapIt->second.m_efficiencyMAData.push_back(mapIt->second.m_efficiency);
        // We are maintaining the moving average over a constant number of elements.
        if (m_movingAverageDataEvents < mapIt->second.m_efficiencyMAData.size())mapIt->second.m_efficiencyMAData.pop_front();

        // Loop over the deque and sum up all the element values.
        int sum(0);
        const std::deque<int>::const_iterator maDataBeginIt(mapIt->second.m_efficiencyMAData.begin());
        const std::deque<int>::const_iterator maDataEndIt(mapIt->second.m_efficiencyMAData.end());
        for (std::deque<int>::const_iterator maDataIt(maDataBeginIt); maDataIt!=maDataEndIt; ++maDataIt)
          sum += *maDataIt;

        // Now determine the average and store it in our class variable.
        // This will enter the HPD into the map if it didn't exist or
        // just over write it if it does.
        mapIt->second.m_efficiencyMA = sum / mapIt->second.m_efficiencyMAData.size();

        // Check if HPD is alive by checking its moving average is greater than a user defined efficiency.
        if (m_deadEfficiency < mapIt->second.m_efficiencyMA) {
          // We want to ensure that our HPD is behaving `normally', where we
          // define normally to be as it has been behaving.
          // Do this by checking the current moving average is not too different from before.
          // Check to see if the percentage difference between the current efficiency
          // and the moving average of the efficiency has changed by too large an amount.
          // We will adapt the amount of change we consider an error in order to keep
          // the number of messages we send fairly low (in the error checking function).
          // Remain as an integer calculation for the moment.
          mapIt->second.m_isDead = false;
          if (abs((mapIt->second.m_efficiencyMA-mapIt->second.m_efficiency)/mapIt->second.m_efficiencyMA) > mapIt->second.m_effMADifference)
            ++(mapIt->second.m_errorCount);
        }// if HPD is alive.
        // The HPD is considered to be dead so increment the dead count
        // There is only a counter to represent this no histogram for the individual HPD.
        // The counter has not necessarily been declared but we still use
        // the variable for error checking.
        // We will plot the HPD on the panel dead HPD count in the updateRichPanelStats function.
        else {
          if (msgLevel(MSG::VERBOSE)) {
            verbose() << "m_nEventsSeen    -- " << m_nEventsSeen << endmsg;
            verbose() << "hpd->m_deadCount -- " << mapIt->second.m_deadCount << endmsg;
            verbose() << "Now incrementing the dead count for this event." << endmsg;
          }// if(msgLevel(MSG::VERBOSE))
          ++(mapIt->second.m_deadCount);
          mapIt->second.m_isDead = true;
        }// else HPD is dead
      }//end update stats for HPD checked but not in this event

      // The X and Y efficiencies must be filled for EVERY HPD. We now do it for the Checked HPDs
      // (regardless if they have been checked in this event or in the previous)
      // Fill the profile histograms with the efficiency for this HPD.
      // First use the column for the x-axis and plot the efficiency.
      sc = fillRichPanelProfile1D(mapIt->first.rich(), mapIt->first.panel()
                                  , &panelInfoIt->second.mp_xAverageEfficiencyHist
                                  , gOrdering.globalPdX(), mapIt->second.m_efficiency);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel x efficiency profile histogram for " << mapIt->first.rich() << ", " << Rich::text(mapIt->first.rich(),mapIt->first.panel()) << " and it failed." << endmsg;
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << mapIt->first.pdID() << endmsg;
        verbose() << "Plotting x " << gOrdering.globalPdX() << " on profile." << endmsg;
        verbose() << "Efficiency -- " << mapIt->second.m_efficiency << endmsg;

      }// if(msgLevel(MSG::VERBOSE))

      // Now use the HPD number in the column i.e. row for the y-axis and plot the efficiency.
      sc = fillRichPanelProfile1D(mapIt->first.rich(), mapIt->first.panel()
                                  , &panelInfoIt->second.mp_yAverageEfficiencyHist
                                  , gOrdering.globalPdY(), mapIt->second.m_efficiency);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel y efficiency profile histogram for " << mapIt->first.rich() << ", " << Rich::text(mapIt->first.rich(),mapIt->first.panel()) << " and it failed." << endmsg;
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << mapIt->first.pdID() << endmsg;
        verbose() << "Plotting y " << gOrdering.globalPdY() << " on profile." << endmsg;
        verbose() << "Efficiency -- " << mapIt->second.m_efficiency << endmsg;
      }// if(msgLevel(MSG::VERBOSE))

      // The efficiencies must be filled for EVERY HPD. We now do it for the Checked HPDs
      // (regardless if they have been checked in this event or in the previous)
      // Fill the 2D histograms with the efficiency for this HPD.
      if(m_effHPDMapFlag){
        sc = fillBinEntryRichPanel2D(mapIt->first.rich(), mapIt->first.panel()
                                     , &panelInfoIt->second.mp_effHPDMap
                                     , gOrdering.globalPdX(), gOrdering.globalPdY(), (double)mapIt->second.m_efficiency);
        if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
          debug() << "Attempted to book panel 2D efficiency profile histogram for " << mapIt->first.rich() << ", " << Rich::text(mapIt->first.rich(),mapIt->first.panel()) << " and it failed." << endmsg;
        if (msgLevel(MSG::VERBOSE)) {
          verbose() << mapIt->first.pdID() << endmsg;
          verbose() << "Plotting x " << gOrdering.globalPdX()
                    << "Plotting y " << gOrdering.globalPdY()
                    << " on 2D histogram." << endmsg;
          verbose() << "Efficiency -- " << mapIt->second.m_efficiency << endmsg;
        }// if(msgLevel(MSG::VERBOSE))
      }// if(m_effHPDMapFlag)

      // Fill the AverageDeadEfficiencyHist for the CHECKED HPDs.
      int deadEfficiency((mapIt->second.m_deadCount*100)/m_nEventsSeen);
      // First use the column for the x-axis and plot the Dead efficiency.
      sc = fillRichPanelProfile1D(mapIt->first.rich(), mapIt->first.panel()
                                  , &panelInfoIt->second.mp_xAverageDeadEfficiencyHist
                                  , gOrdering.globalPdX(), deadEfficiency);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel x dead efficiency profile histogram for " << mapIt->first.rich() << ", " << Rich::text(mapIt->first.rich(),mapIt->first.panel()) << " and it failed." << endmsg;
      if (msgLevel(MSG::VERBOSE)  && (0==m_nEventsSeen%m_errorReportRate)) {
        verbose() << mapIt->first.pdID() << endmsg;
        verbose() << "Plotting x " << gOrdering.globalPdX() << " on profile." << endmsg;
        verbose() << "Dead efficiency -- " << mapIt->second.m_efficiency << endmsg;
      }// if(msgLevel(MSG::VERBOSE))
      // Now use the HPD number in the column i.e. row for the y-axis and plot the
      // Dead efficiency.
      sc = fillRichPanelProfile1D(mapIt->first.rich(), mapIt->first.panel()
                                  , &panelInfoIt->second.mp_yAverageDeadEfficiencyHist
                                  , gOrdering.globalPdY(), deadEfficiency);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel y dead efficiency profile histogram for " << mapIt->first.rich() << ", " << Rich::text(mapIt->first.rich(),mapIt->first.panel()) << " and it failed." << endmsg;
      if (msgLevel(MSG::VERBOSE)) {
        verbose() << mapIt->first.pdID() << endmsg;
        verbose() << "Plotting y " << gOrdering.globalPdY() << " on profile." << endmsg;
        verbose() << "Dead efficiency -- " << mapIt->second.m_efficiency << endmsg;
      }// if(msgLevel(MSG::VERBOSE))

      // Overall stat:
      // Check to see if the HPD is in error.
      if (checkedHPD_Flag && (0!=mapIt->second.m_errorCount)) ++errorCount;
      // Check to see if the HPD is dead.
      if ((checkedHPD_Flag && (0!=mapIt->second.m_isDead))|| (!checkedHPD_Flag)){
        // Increment the dead count.
        if(checkedHPD_Flag)++deadCount;
        if (msgLevel(MSG::VERBOSE)  && (0==m_nEventsSeen%m_errorReportRate)) {
          verbose() << "Overall Event Statistic:" << endmsg;
          verbose() << mapIt->first.pdID() << endmsg;
          verbose() << "deadCount      -- " << mapIt->second.m_deadCount << endmsg;
          verbose() << "m_nEventsSeen  -- " << m_nEventsSeen  << endmsg;
          verbose() << "deadEfficiency -- " << deadEfficiency << endmsg;
        }// if(msgLevel(MSG::VERBOSE))
        // Plot this HPD on the panel hit map, if requested.
        if (m_deadHPDMapFlag && checkedHPD_Flag) {
          // Fill the histogram and then if it failed and we are printing error messages
          // inform people what failed.
          sc = fillRichPanel2D(mapIt->first.rich(), mapIt->first.panel(), &panelInfoIt->second.mp_deadHPDMap, gOrdering.globalPdX(), gOrdering.globalPdY());
          if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
            debug() << "Attempted to book dead HPD histogram for " << mapIt->first.rich() << ", " << Rich::text(mapIt->first.rich(),mapIt->first.panel()) << " and it failed." << endmsg;
        }// if(m_deadHPDMapFlag)
      }// if HPD dead (checked at some point, maybe in previous events).
    }// If a HPD ID and on this RICH panel (checked at some point, maybe in previous events).
  }// for mapIt
  //
  // END OF THE LOOP ON CHECKED HPDs
  //

  //
  // START OF THE LOOP ON UNCHECKED HPDs
  //

  for(int hpd_x = xmin; hpd_x <= xmax; hpd_x++){
    bool checkedHPD_xFlag = false;
    if(checkedHPD_map.find(hpd_x) != checkedHPD_map.end())checkedHPD_xFlag = true;
    for(int hpd_y = ymin; hpd_y <= ymax; hpd_y++){
      bool checkedHPD_Flag = false;
      if(checkedHPD_xFlag){
        const std::vector<int>::const_iterator hpd_y_ItBegin((checkedHPD_map.find(hpd_x)->second).begin());
        const std::vector<int>::const_iterator hpd_y_ItEnd((checkedHPD_map.find(hpd_x)->second).end());
        if(std::find(hpd_y_ItBegin, hpd_y_ItEnd , hpd_y) != hpd_y_ItEnd)checkedHPD_Flag = true;
      }
      // Here we want only HPDs that haven't been checked in the current event
      if(checkedHPD_Flag)continue;
      // Check to see if the HPD is in error.
      // So far we won't send errors on single unchecked HPDs, since it can generate a huge
      // amount of error. We will send an overhall error if the overhall amount of dead HPDs
      // (checked and unchecked) is too large.
      ++errorCount;
      // Check to see if the HPD is dead.
      // Increment the dead count.
      ++deadCount;
      // Fill the Average efficiency for unchecked HPDs with 0%
      sc = fillRichPanelProfile1D(panelID.rich(), panelID.panel()
                                  , &panelInfoIt->second.mp_xAverageEfficiencyHist
                                  , hpd_x, 0);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel x efficiency profile histogram for " << panelID.rich() << ", " << panelID.panel() << " and it failed." << endmsg;
      // Now use the HPD number in the column i.e. row for the y-axis and plot the efficiency.
      sc = fillRichPanelProfile1D(panelID.rich(), panelID.panel()
                                  , &panelInfoIt->second.mp_yAverageEfficiencyHist
                                  , hpd_y, 0);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel y efficiency profile histogram for " << panelID.rich() << ", " << panelID.panel() << " and it failed." << endmsg;

      // Fill the Average efficiency for unchecked HPDs with 0%. This the 2D histogram.
      if(m_effHPDMapFlag){
        sc = fillBinEntryRichPanel2D(panelID.rich(), panelID.panel()
                                     , &panelInfoIt->second.mp_effHPDMap
                                     , hpd_x, hpd_y, (double)0);

        if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
          debug() << "Attempted to book panel x efficiency profile histogram for " << panelID.rich() << ", " << panelID.panel() << " and it failed." << endmsg;
      }// if(m_effHPDMapFlag)


      // We have to plot the Dead efficiency for an unchecked HPD because it combines with the checked one.
      // For an unchecked HPD the Dead efficiency is put to 100%.
      // X axis:
      sc = fillRichPanelProfile1D(panelID.rich(), panelID.panel()
                                  , &panelInfoIt->second.mp_xAverageDeadEfficiencyHist
                                  ,  hpd_x, 100);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel x dead efficiency profile histogram for " << panelID.rich() << ", " <<  panelID.panel() << " and it failed." << endmsg;
      // Y Axis:
      sc = fillRichPanelProfile1D(panelID.rich(), panelID.panel()
                                  , &panelInfoIt->second.mp_yAverageDeadEfficiencyHist
                                  , hpd_y, 100);
      if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
        debug() << "Attempted to book panel y dead efficiency profile histogram for " << panelID.rich() << ", " << panelID.panel() << " and it failed." << endmsg;

      // Plot this HPD on the panel hit map, if requested.
      if (m_deadHPDMapFlag) {
        // Fill the histogram and then if it failed and we are printing error messages
        // inform people what failed.
        sc = fillRichPanel2D(panelID.rich(), panelID.panel(), &panelInfoIt->second.mp_deadHPDMap, hpd_x, hpd_y);
        if ((StatusCode::FAILURE == sc) && msgLevel(MSG::DEBUG))
          debug() << "Attempted to book dead HPD histogram for " << panelID.rich() << ", " << panelID.panel() <<", x: "<<hpd_x<<", y: "<<hpd_y<< " and it failed." << endmsg;
      }// if(m_deadHPDMapFlag)
    }// loop on y
  }// loop on x

  //
  //END LOOP OVER CHECKED HPDs
  //

  // These will be useful for plotting the histograms.
  Rich::DetectorType rich(panelID.rich());
  // Need to know the number of HPDs on the given panel. This is half the number on RICH panel.
  // RICH1 14x14 HPDs, RICH2 16x18 HPDs.
  int nHPDs(Rich::Rich1==rich ? 98:144);
  // Now we have accumulated the relevant stats for this panel we can perform the analysis.
  // If the test pattern is not sent to the panel, or the panel is switched off,
  // send a warning to Camera.
  double noHitHPDs_percentage((((float)nHPDs - (float)checkedHPDsCount)/((float)nHPDs)) * 100.0);
  if (noHitHPDs_percentage > m_DeadPanelThreshold){
    //flag the panel as Dead
    panelInfoIt->second.m_panelDead = true;
    if(1==m_nEventsSeen){
      std::ostringstream msg1;
      msg1 <<noHitHPDs_percentage << "p.c. of HPDs in " <<panelID << " have no hits";
      std::ostringstream msg2;
      msg2 << "It's likely that either "<<panelID<<" isn't running, or no test pattern have been sent.";
      mp_camera->Append(msg2.str());
      mp_camera->SendAndClearTS(ICameraTool::INFO, m_name, msg1.str());
      //errMsgPresent = true;
    }
  }

  panelInfoIt->second.m_nHits = nHits;
  if (msgLevel(MSG::VERBOSE))
    verbose() << "Total hits " << panelInfoIt->second.m_nHits << ", total test pattern hits " << (mp_testPattern->totalTestPatternPixels()*nHPDs) << endmsg;
  // Give the RICH panel and the number of hits on a test pattern we can determine the efficiency.
  // Work in percent from the start so we can use integers. This means multiplying by 100 first.
  // This is a published counter.
  panelInfoIt->second.m_efficiency = (nHits*100) / (mp_testPattern->totalTestPatternPixels()*nHPDs);

  // Save this efficiency.
  // m_efficiency changes at each event, m_efficiencyMAData stores the efficiencies of the previous events.
  // m_efficiencyMA is m_efficiency averaged over m_movingAverageDataEvents events.
  panelInfoIt->second.m_efficiencyMAData.push_back(panelInfoIt->second.m_efficiency);
  // We are maintaining the moving average over a constant number of elements (m_movingAverageDataEvents).
  if (m_movingAverageDataEvents < panelInfoIt->second.m_efficiencyMAData.size())
    panelInfoIt->second.m_efficiencyMAData.pop_front();
  // Loop over the deque and sum up all the element values.
  int sum(0);
  const std::deque<int>::const_iterator maDataBeginIt(panelInfoIt->second.m_efficiencyMAData.begin());
  const std::deque<int>::const_iterator maDataEndIt(panelInfoIt->second.m_efficiencyMAData.end());
  for (std::deque<int>::const_iterator maDataIt(maDataBeginIt); maDataIt!=maDataEndIt; ++maDataIt)
    sum += *maDataIt;

  // Now determine the average and store it in our class variable.
  // This will enter the HPD into the map if it didn't exist or
  // just over write it if it does.
  // This is a published counter.
  panelInfoIt->second.m_efficiencyMA = sum / panelInfoIt->second.m_efficiencyMAData.size();

  // Save the number of HPDs in error.
  m_panelInfoMap[panelID].m_errorCount = (errorCount*100) / nHPDs;

  // Save the number of dead HPDs.
  m_panelInfoMap[panelID].m_deadCount = (deadCount*100) / nHPDs;

  if (msgLevel(MSG::DEBUG)) {
    debug() << panelID << endmsg;
    debug() << " Efficiency          -- " << m_panelInfoMap[panelID].m_efficiency << endmsg;
    debug() << " EfficiencyMA        -- " << m_panelInfoMap[panelID].m_efficiencyMA << endmsg;
    debug() << " Error efficiency    -- " << m_panelInfoMap[panelID].m_errorCount << endmsg;
    debug() << " Dead HPD efficiency -- " << m_panelInfoMap[panelID].m_deadCount << endmsg;
  }// if(msgLevel(MSG::DEBUG))

  //Done.
  return sc;
}// Rich::Mon::TestPatternMonitor::updateRichPanelStats()

// ----------------------------------------------------------------------
bool Rich::Mon::TestPatternMonitor::appendErrorMessages(bool ForceSummary, bool LogMsg, bool noAppend) {

  // If the RICHes are not active, send the message to the log file and to camera.
  if(m_deadRICHesFlag){
    std::ostringstream msg3;
    msg3 << "All RICH panel with no hits => RichTestPatternMon execute() will be skipped.";
    std::ostringstream msg4;
    msg4<< "After " <<m_nEventsSeen << " events, all the RICH panels show "
        << "a percentage of HPDs with no hits > "<<m_DeadPanelThreshold<< "p.c. "
        << "It's likely that either the RICHes aren't running, or no test pattern have been sent. "
        << "RichTestPatternMon algorithm have been interrupted after "<<m_DeadPanelReportRate<<" events";
    if(!noAppend)mp_camera->Append(msg4.str());
    mp_camera->SendAndClearTS(ICameraTool::INFO, m_name, msg3.str());
    if(LogMsg)Info(msg3.str());
    return true;
  }

  // This is the whole message we wish to append.
  std::ostringstream errMsg;
  m_finalMsg.str( "" );   //empty the string
  bool PanelInError = false;
  bool messageAppended = false;
  // Loop through the RICH panel map and check for errors and append them to CAMERA if a sufficient amount have accumulated.
  const std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelMapBeginIt(m_panelInfoMap.begin());
  const std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelMapEndIt(m_panelInfoMap.end());
  for (std::map<LHCb::RichSmartID, RichPanelTestPatternInfo>::iterator panelInfoIt(panelMapBeginIt); panelInfoIt!=panelMapEndIt; ++panelInfoIt) 
  {
    // Do we need to add a summary message for HPD error counts (if there is at least an error).
    if (0 < panelInfoIt->second.m_errorCount || ForceSummary)
    {
      errMsg.str( "" );   //empty the string
      // Send message to Camera.
      errMsg << panelInfoIt->first
             << " -- Event " << m_nEventsSeen
             << " -- Have seen "
             << panelInfoIt->second.m_errorCount
             << "p.c. of HPDs with a fluctuating test pattern efficiency.";
      m_finalMsg << errMsg.str();
      if (NULL != mp_camera)
        if(!noAppend)mp_camera->Append("TEXT", errMsg.str().c_str());
      // Send massage to the log file (N.B. avoid "\n" and avoid "%" with std::ostringstream)
      if(LogMsg){
        if (msgLevel(MSG::VERBOSE)){
          verbose() << panelInfoIt->first
                    << " -- Event " << m_nEventsSeen
                    << " -- Have seen "
                    << panelInfoIt->second.m_errorCount
                    << " percent " <<" of HPDs with a fluctuating test pattern efficiency." <<endmsg;
        }
      }
      errMsg.str( "" );   //empty the string
      messageAppended = true;
      // We clear the number of errors seen here and start again next time.
      panelInfoIt->second.m_errorCount = 0;
    }// if error seen.
    // Do we need to add a summary message for HPD dead counts (if there is at least a dead HPD).
    if (0 < panelInfoIt->second.m_deadCount || ForceSummary) {
      // Send message to Camera.
      errMsg.str( "" );   //empty the string
      errMsg << panelInfoIt->first
             << " -- Have seen "
             << panelInfoIt->second.m_deadCount
             << "p.c. of HPDs have no hits.";
      m_finalMsg  << panelInfoIt->first
                  << " -- Have seen "
                  << panelInfoIt->second.m_deadCount
                  << "p.c. of HPDs have no hits.";
      if (NULL != mp_camera)
        if(!noAppend)mp_camera->Append("TEXT", errMsg.str().c_str());
      // Send massage to the log file (N.B. avoid "\n" and avoid "%" with std::ostringstream)
      if(LogMsg){
        if (msgLevel(MSG::VERBOSE)){
          verbose() << panelInfoIt->first
                    << " -- Have seen "
                    << panelInfoIt->second.m_deadCount
                    << " percent " << " of HPDs have no hits." <<endmsg;
        }
      }
      errMsg.str( "" );   //empty the string
      messageAppended = true;
      // We clear the number of errors seen here and start again next time.
      panelInfoIt->second.m_errorCount = 0;
    }// if we have an error message on this panel.
    if (0 < panelInfoIt->second.m_errorCount || 0 < panelInfoIt->second.m_deadCount)PanelInError = true;
  }// for panelInfoIt


  // Done.
  if(!ForceSummary)return messageAppended;
  else if(PanelInError)return messageAppended;
  else return !messageAppended;
}// Rich::Mon::TestPatternMonitor::appendErrorMessages()

// ----------------------------------------------------------------------
StatusCode Rich::Mon::TestPatternMonitor::bookRichPanelHistograms(const Rich::DetectorType rich,
                                                                  const Rich::Side panel) {
  // Create the ID and name for the counters and histograms.
  const std::string id(richPanel1DHistId(rich, panel));
  const std::string name(richPanel1DHistName(rich, panel));

  // Create a smart ID for this RICH panel in order allow the global ordering class to know which RICH.
  LHCb::RichSmartID smartID(rich, panel);

  // Get the info object from the map for this smart ID.
  // This will give us a default constructed object if one does not exist.
  RichPanelTestPatternInfo &info(m_panelInfoMap[smartID]);
  // Ensure the moving average efficiency difference limit is initialised to that given to the algorithm.
  info.m_effMADifference = m_errorEfficiency;

  // We should only declare the counters once and the flag is checked to see this.
  if (!info.m_countersDeclared) {
    declareInfo(id+m_efficiencyCounter, info.m_efficiency, "Efficiency of the test pattern for this panel.");
    declareInfo(id+m_efficiencyMACounter, info.m_efficiencyMA, "Moving average for the efficiency of the test pattern on this panel.");
    declareInfo(id+m_deadHPDCounter, info.m_deadCount, "Percentage of HPD that are currently considered dead for the panel.");
    // Now we have declared the counters we must mark this, as the counters should only ever
    // be declared once, the behaviour is undefined if declared more than once.
    info.m_countersDeclared = true;
  }// if(!info.m_countersDeclared)

  // Using our created smart ID we can determine the dimensions of our histograms
  // in terms HPDs and HPD pixels, which will give use our 1D profile histograms,
  // profile taken over HPDs in columns and number of columns; and our 2D per HPD
  // and 2D hit maps dimensions.
  // The SmartIDGlobalOrdering will give us the orientation of the RICH so we know
  // where the HPDs in columns or the columns are on the x- and y-axis respectively.
  // We can always use LHCb mode pixels as the test pattern tool just maps the corresponding ALICE
  // pixel to the appropriate bin. Its just for the orientation.
  Rich::SmartIDGlobalOrdering gOrdering(smartID, Rich::SmartIDGlobalOrdering::LHCbMode);

  // Dimensions of the histograms in terms of the HPDs.
  double hxmin(-0.5);
  double hxmax(Rich::Rich2==rich ? 8.5:13.5);
  int hxnbins(static_cast<int>(hxmax-hxmin));
  //   double hxmin(gOrdering.minHPDX()-0.5);
  //   double hxmax(gOrdering.maxHPDX()-0.5);
  //   int hxnbins(gOrdering.totalHPDX());
  if ((Rich::Rich2==rich) && (Rich::left==panel)) {
    // In the case of RICH2 we need to alter the x-axis to shift the axis to cover only the appropriate panel.
    // A-side start half way along the x-axis at the start of C-side.
    hxmin += (hxmax + 0.5);
    hxmax += (hxmax + 0.5);
  }// if((Rich::Rich2)&&(Rich::left==panel))

  double hymin(-0.5);
  double hymax(Rich::Rich2==rich ? 15.5:6.5);
  int hynbins(static_cast<int>(hymax-hymin));
  //   double hymin(gOrdering.minHPDY()-0.5);
  //   double hymax(gOrdering.maxHPDY()-0.5);
  //   int hynbins(gOrdering.totalHPDY());
  if ((Rich::Rich1==rich) && (Rich::top==panel)) {
    // In the case of RICH1 we need to alter the y-axis to shift it to cover only the appropriate panel.
    // Top panel starts half way up the y-axis after the bottom panel has ended.
    hymin += (hymax + 0.5);
    hymax += (hymax + 0.5);
  }// if((Rich::Rich1==rich)&&(Rich::top==panel))

  // Now check the 1D histograms to see if they exist in the info object and
  // if not book them. They should only ever be booked once and they are owned
  // by Gaudi and shouldn't be delete'd here.
  // These are always booked.
  // We are projecting the row and column efficiencies into a single bin
  // on separate graphs. Need to get the dimensions for the row or column,
  // which is either on the x- or y-axis of our determined dimensions depending
  // on the RICH.
  if (NULL == info.mp_xAverageEfficiencyHist) {
    info.mp_xAverageEfficiencyHist = bookProfile1D(id+m_xEfficiencyProjection, name+m_xEfficiencyProjection
                                                   , hxmin, hxmax
                                                   , hxnbins
                                                   );
  }// if(NULL==info.mp_xAverageEfficiencyHist)

  if (NULL == info.mp_yAverageEfficiencyHist) {
    info.mp_yAverageEfficiencyHist = bookProfile1D(id+m_yEfficiencyProjection, name+m_yEfficiencyProjection
                                                   , hymin, hymax
                                                   , hynbins);
  }// if(NULL==info.mp_yAverageEfficiencyHist)

  if (NULL == info.mp_xAverageDeadEfficiencyHist) {
    info.mp_xAverageDeadEfficiencyHist = bookProfile1D(id+m_xDeadEfficiencyProjection, name+m_xDeadEfficiencyProjection
                                                       , hxmin, hxmax
                                                       , hxnbins);
  }// if(NULL==info.mp_xAverageDeadEfficiencyHist)

  if (NULL == info.mp_yAverageDeadEfficiencyHist) {
    info.mp_yAverageDeadEfficiencyHist = bookProfile1D(id+m_yDeadEfficiencyProjection, name+m_yDeadEfficiencyProjection
                                                       , hymin, hymax
                                                       , hynbins);
  }// if(NULL==info.mp_yAverageDeadEfficiencyHist)

  // The decision whether to plot the histogram or not depends on both the boolean
  // flag, which is a class property and whether the histogram exists. Convert this
  // to a single boolean for simplicity.
  bool plotHitMap(m_hitMapFlag && (NULL==info.mp_hitMap));
  bool plotHitsPerHPD(m_hitsPerHPDMapFlag && (NULL==info.mp_hitsPerHPDMap));
  bool plotDeadHPDs(m_deadHPDMapFlag && (NULL==info.mp_deadHPDMap));
  bool plotEffHPDs(m_effHPDMapFlag && (NULL==info.mp_effHPDMap));
  if (plotHitMap || plotHitsPerHPD || plotDeadHPDs) {
    // All our histograms have the same base name, simply add the appropriate extension

    // These two histograms have the same dimensions.
    if (plotHitsPerHPD || plotDeadHPDs || plotEffHPDs) {
      // Now book the histograms.
      if (plotHitsPerHPD) {
        info.mp_hitsPerHPDMap = book2D(id+m_hitsPerHPDMap, name+m_hitsPerHPDMap
                                       , hxmin, hxmax, hxnbins
                                       , hymin, hymax, hynbins
                                       );
        if ((NULL==info.mp_hitsPerHPDMap) && msgLevel(MSG::DEBUG))
          debug() << "Failed to book hits per HPD map for " << rich << " " << Rich::text(rich,panel) << endmsg;
      }// if(plotHitsPerHPD)
      if (plotDeadHPDs) {
        info.mp_deadHPDMap = book2D(id+m_deadHPDMap, name+m_deadHPDMap
                                    , hxmin, hxmax, hxnbins
                                    , hymin, hymax, hynbins
                                    );
        if ((NULL==info.mp_deadHPDMap) && msgLevel(MSG::DEBUG))
          debug() << "Failed to book dead HPD map for " << rich << " " << Rich::text(rich,panel) << endmsg;
      }// if(plotDeadHPDs)
      if (plotEffHPDs) {
        info.mp_effHPDMap = book2D(id+m_effHPDMap, name+m_effHPDMap
                                   , hxmin, hxmax, hxnbins
                                   , hymin, hymax, hynbins
                                   );
        if ((NULL==info.mp_effHPDMap) && msgLevel(MSG::DEBUG))
          debug() << "Failed to book efficiency HPD map for " << rich << " " << Rich::text(rich,panel) << endmsg;
      }// if(plotEffHPDs)
    }// if(plotHitMap||plotDeadHPDs||plotEffHPDs)

    if (plotHitMap) {
      // Dimensions of the histograms in terms of pixels.
      // Ask the test pattern finder for the dimensions of a single HPD hit map,
      // which we use to scale the RICH panel histograms appropriately.
      double lxmin, lxmax, lymin, lymax;
      int lxnbins, lynbins;
      mp_testPattern->histogramDimensions(rich, lxnbins, lxmin, lxmax, lynbins, lymin, lymax);
      // Our histograms should be have whole numbers for the max bin as they represent a pixel.
      // We should be able to safely cast from a double to an int with the xmax and ymax variables.
      double gxmin(-0.5);
      double gxmax(gOrdering.maxGlobalPixelX(static_cast<int>(lxmax)) - 0.5);
      int gxnbins = gOrdering.maxGlobalPixelX(static_cast<int>(lxmax));
      if (Rich::Rich2 == rich) {
        //In the case of RICH2 we need to alter the x-axis to shift the axis to cover only the appropriate panel.
        if (Rich::left == panel)
          gxmin += gOrdering.maxGlobalPixelX(static_cast<int>(lxmax)) / 2.0;// A-side start half way along the x-axis at the start of C-side.
        else//As we are one RICH2 this must be C-side (right).
          gxmax -= gOrdering.maxGlobalPixelX(static_cast<int>(lxmax)) / 2.0;// C-side ends at the start of A-side.
        gxnbins /= 2;// What ever side we have reduced the x-axis by half.
      }// if(Rich::Rich2)
      double gymin(-0.5);
      double gymax(gOrdering.maxGlobalPixelY(static_cast<int>(lymax)) - 0.5);
      int gynbins(gOrdering.maxGlobalPixelY(static_cast<int>(lymax)));
      if (Rich::Rich1 == rich) {
        // In the case of RICH1 we need to alter the y-axis to shift it to cover only the appropriate panel.
        if(Rich::top == panel)
          gymin += gOrdering.maxGlobalPixelY(static_cast<int>(lymax)) / 2.0;// Top panel starts half way up the y-axis after the bottom panel has ended.
        else // As we are on RICH1 this must be bottom.
          gymax -= gOrdering.maxGlobalPixelY(static_cast<int>(lymax)) / 2.0;// Bottom panel ends half way up the y-axis where the top starts.
        gynbins /= 2;// What ever side we have reduced the y-axis by half.
      }// if(Rich::Rich1==rich)

      // Now book the histogram.
      info.mp_hitMap = book2D(id+m_hitMap, name+m_hitMap
                              , gxmin, gxmax, gxnbins
                              , gymin, gymax, gynbins
                              );
      if ((NULL==info.mp_hitMap) && msgLevel(MSG::DEBUG))
        debug() << "Failed to book hit map for " << rich << " " << Rich::text(rich,panel) << endmsg;
    }// if(plotHitMap)
  }// if plotting hit maps.

  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::TestPatternMonitor::bookRichPanelHistograms()

// ----------------------------------------------------------------------
StatusCode Rich::Mon::TestPatternMonitor::bookIndividualHistograms(const LHCb::RichSmartID &smartID) {
  // Create the ID and name for the declared histograms and counters.
  const std::string id(individualHistId(smartID));
  const std::string name(individualHistId(smartID));
  // Get the entry in the test pattern info map to update the HPD's information.
  // Ensure that we have only HPD level information.
  // Only the hit map for a single HPD makes sense as the hits per HPD and dead count are
  // presented best on the panel displays.
  HPDTestPatternInfo &info(m_hpdInfoMap[smartID.pdID()]);
  // Ensure the moving average efficiency difference limit is initialised to that given to the algorithm.
  info.m_effMADifference = m_errorEfficiency;
  // bookIndividualHistograms must at least create an entry in the info map, but otherwise it
  // should only produce counter etc. if we want to publish the individual stats.
  if (m_individualHitMapsFlag) {
    // Check to see if the counters have been booked.
    if (!info.m_countersDeclared) {
      declareInfo(id+m_efficiencyCounter, info.m_efficiency, "Efficiency of the test pattern for this HPD.");
      declareInfo(id+m_efficiencyMACounter, info.m_efficiencyMA, "Moving average of the efficiency for the test pattern for this HPD.");
      declareInfo(id+m_deadHPDCounter, info.m_deadCount, "Percentage of the events that the HPD has been considered dead.");
      // Now we have declared the counters we must mark this, as the counters should only ever
      // be declared once, the behaviour is undefined if declared more than once.
      info.m_countersDeclared = true;
    }// if(!info.m_countersDeclared)

    // Now check the 1D histograms to see if they exist in the info object and
    // if not book them. They should only ever be booked once and they are owned
    // by Gaudi somehow and shouldn't be delete'd here.
    // The dead count does not make sense for the individual HPD.
    if (NULL == info.mp_efficiencyHist) {
      // The efficiency can only vary from 0 to 100 as it is in percent.
      info.mp_efficiencyHist = book1D(id+m_efficiencyHistogram, name+m_efficiencyHistogram
                                      , -0.5, 100.5 // min, max
                                      , 101 // number bins
                                      );
    }// if(NULL==info.mp_efficiencyHist)

    if (NULL == info.mp_efficiencyMAHist) {
      // The efficiency is plotted in percent.
      info.mp_efficiencyMAHist = book1D(id+m_efficiencyMAHistogram, name+m_efficiencyMAHistogram
                                        , -0.5, 100.5 // min, max
                                        , 101 // number bins
                                        );
    }// if(NULL==info.mp_efficiencyMAHist)

    // It only makes sense to book the hit map histogram as the dead count
    // and hits per HPD are represented directly by the counters.
    if (NULL == info.mp_hitMap) {
      // Ask the test pattern finder for the dimensions of a single HPD hit map,
      // which we use to scale the RICH panel histograms appropriately.
      double xmin, xmax, ymin, ymax;
      int xnbins, ynbins;
      mp_testPattern->histogramDimensions(smartID.rich(), xnbins, xmin, xmax, ynbins, ymin, ymax);
      // This gives us the dimension of an individual pixel and determines the orientation.
      // We can use these values directly in the booking.
      info.mp_hitMap = book2D(id+m_hitMap, name+m_hitMap
                              , xmin, xmax, xnbins
                              , ymin, ymax, ynbins
                              );
    }// if(NULL==info.mp_hitMap)
  }// if(m_individualHitsMapsFlag)

  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::TestPatternMonitor::bookIndividualHistograms()


// ----------------------------------------------------------------------
std::string Rich::Mon::TestPatternMonitor::richPanel1DHistId(const Rich::DetectorType rich,
                                                             const Rich::Side panel) {
  //Now create the name.
  std::ostringstream id;
  id << Rich::text(rich)
     << Rich::text(rich, panel);
  return id.str();
}// Rich::Mon::TestPatternMonitor::richPanelHistId()

// ----------------------------------------------------------------------
std::string
Rich::Mon::TestPatternMonitor::richPanel1DHistName(const Rich::DetectorType rich,
                                                   const Rich::Side panel) {
  return richPanel1DHistId(rich, panel);
}// Rich::Mon::TestPatternMonitor::richPanelHistName()

// ----------------------------------------------------------------------
std::string
Rich::Mon::TestPatternMonitor::richPanel2DHistId(const Rich::DetectorType rich,
                                                 const Rich::Side panel) {
  //Now create the name.
  std::ostringstream id;
  id << Rich::text(rich)
     << Rich::text(rich, panel);
  return id.str();
}// Rich::Mon::TestPatternMonitor::richPanel2DHistId()

// ----------------------------------------------------------------------
std::string
Rich::Mon::TestPatternMonitor::richPanel2DHistName(const Rich::DetectorType rich,
                                                   const Rich::Side panel) {
  return richPanel2DHistId(rich, panel);
}// Rich::Mon::TestPatternMonitor::richPanel2DHistName()

// ----------------------------------------------------------------------
std::string 
Rich::Mon::TestPatternMonitor::individualHistId(const LHCb::RichSmartID &smartId) 
{
  std::ostringstream id;
  id << Rich::text(smartId.rich())
     << Rich::text(smartId.rich(), smartId.panel())
     << "_HPDCol"
     << smartId.pdCol()
     << "_HPDInCol"
     << smartId.pdNumInCol();
  return id.str();
}// Rich::Mon::TestPatternMonitor::individualHistId()

// ----------------------------------------------------------------------
std::string 
Rich::Mon::TestPatternMonitor::individualHistName(const LHCb::RichSmartID &smartId)
{
  //Currently this is the same as the ID.
  return individualHistId(smartId);
}// Rich::Mon::TestPatternMonitor::individualHistName()

// ----------------------------------------------------------------------
StatusCode Rich::Mon::TestPatternMonitor::fillRichPanel1D(const Rich::DetectorType rich, 
                                                          const Rich::Side panel, 
                                                          AIDA::IHistogram1D **hist, 
                                                          const int value) {
  StatusCode sc(StatusCode::SUCCESS);
  // Check that that histogram we have been given exists.
  if (NULL != *hist) {
    if (msgLevel(MSG::VERBOSE))
      verbose() << "value: " << value << endmsg;
    (*hist)->fill(value);
  }// if histogram booked.
  else {
    // In this case we want to fill the histogram but it hasn't been booked.
    // Better book it. This protects against multiple booking of the other histograms.
    // It will also book what ever is asked for.
    bookRichPanelHistograms(rich, panel);
    // and fill it.
    if (NULL != *hist) {
      if (msgLevel(MSG::VERBOSE))
        verbose() << "value: " << value << endmsg;
      (*hist)->fill(value);
    }// if histogram booked.
    else
      sc = StatusCode::FAILURE;
  }// else histogram not booked.
  // Done.
  return sc;
}// Rich::Mon::TestPatternMonitor::fillRichPanel1D()

// ----------------------------------------------------------------------
StatusCode 
Rich::Mon::TestPatternMonitor::fillRichPanelProfile1D(const Rich::DetectorType rich, 
                                                      const Rich::Side panel, 
                                                      AIDA::IProfile1D **hist, 
                                                      const int x, const int y) {
  StatusCode sc(StatusCode::SUCCESS);
  // Check that that histogram we have been given exists.
  if (NULL != *hist) {
    if (msgLevel(MSG::VERBOSE))
      verbose() << "x: " << x << ", y: " << y << endmsg;
    (*hist)->fill(x,y);
  }// if histogram booked.
  else {
    // In this case we want to fill the histogram but it hasn't been booked.
    // Better book it. This protects against multiple booking of the other histograms.
    // It will also book what ever is asked for.
    bookRichPanelHistograms(rich, panel);
    // and fill it.
    if (NULL != *hist) {
      if (msgLevel(MSG::VERBOSE))
        verbose() << "x: " << x << ", y: " << y << endmsg;
      (*hist)->fill(x,y);
    }// if histogram booked.
    else
      sc = StatusCode::FAILURE;
  }// else histogram not booked.
  // Done.
  return sc;
}// Rich::Mon::TestPatternMonitor::fillRichPanel1D()

// ----------------------------------------------------------------------
StatusCode Rich::Mon::TestPatternMonitor::fillRichPanel2D(const Rich::DetectorType rich,
                                                          const Rich::Side panel,
                                                          AIDA::IHistogram2D **hist,
                                                          const int x,
                                                          const int y,
                                                          const int wt )
{
  StatusCode sc(StatusCode::SUCCESS);
  // Check that that histogram we have been given exists.
  if (NULL != *hist) {
    if (msgLevel(MSG::VERBOSE))
      verbose() << "x: " << x << ", y: " << y << ", wt: " << wt << endmsg;
    (*hist)->fill(x, y, wt);
  }// if histogram booked.
  else {
    // In this case we want to fill the histogram but it hasn't been booked.
    // Better book it. This protects against multiple booking of the other histograms.
    // It will also book what ever is asked for.
    bookRichPanelHistograms(rich, panel);
    // and fill it.
    if (NULL != *hist) {
      if (msgLevel(MSG::VERBOSE))
        verbose() << "x: " << x << ", y: " << y << endmsg;
      (*hist)->fill(x, y);
    }// if histogram booked.
    else
      sc = StatusCode::FAILURE;
  }// else histogram not booked.
  // Done.
  return sc;
}// Rich::Mon::TestPatternMonitor::fillRichPanel2D()

// ----------------------------------------------------------------------
StatusCode 
Rich::Mon::TestPatternMonitor::fillBinEntryRichPanel2D(const Rich::DetectorType rich, 
                                                       const Rich::Side panel, 
                                                       AIDA::IHistogram2D **hist, 
                                                       const int x, const int y, 
                                                       const double value, 
                                                       const int wt) {
  StatusCode sc(StatusCode::SUCCESS);

  if (msgLevel(MSG::VERBOSE))
    verbose() << "x: " << x << ", y: " << y << ", value: " << value<< ", wt: " << wt << endmsg;

  if (NULL == *hist) { bookRichPanelHistograms(rich, panel); }
  if (NULL != *hist)
  {
    TH2D* root_hist(Gaudi::Utils::Aida2ROOT::aida2root(*hist));
    // Trasform the coordinate in the bin number:
    const int nBinX(root_hist->GetNbinsX());
    const double lowX((root_hist->GetXaxis())->GetXmin()); 
    const double upX((root_hist->GetXaxis())->GetXmax());
    const int nBinY(root_hist->GetNbinsY());
    const double lowY((root_hist->GetYaxis())->GetXmin()); 
    const double upY((root_hist->GetYaxis())->GetXmax());
    const double deltaX((upX-lowX)/nBinX),deltaY((upY-lowY)/nBinY);
    const int _x = (int)(x - lowX);
    const int binX = ((int)_x/(int)deltaX) + 1;
    const int _y = (int)(y - lowY);
    const int binY = ((int)_y/(int)deltaY) + 1;
    // Set the bin content of the histogram
    root_hist->SetBinContent(binX, binY, value);
  }
  else
  {
    sc = StatusCode::FAILURE;
  }
  // Done.
  return sc;
}// Rich::Mon::TestPatternMonitor::fillRichPanel2D()

// ----------------------------------------------------------------------
StatusCode Rich::Mon::TestPatternMonitor::fillHPD2D(const LHCb::RichSmartID &smartID, 
                                                    AIDA::IHistogram2D **hist, 
                                                    const int x, const int y) {
  StatusCode sc(StatusCode::SUCCESS);
  // Check that that histogram we have been given exists.
  if (NULL != *hist) {
    if (msgLevel(MSG::VERBOSE))
      verbose() << "x: " << x << ", y: " << y << endmsg;
    (*hist)->fill(x, y);
  }// if histogram booked.
  else {
    // In this case we want to fill the histogram but it hasn't been booked.
    // Better book it. This protects against multiple booking of the other histograms.
    // It will also book what ever is asked for.
    bookIndividualHistograms(smartID);
    // and fill it.
    if (NULL != *hist) {
      if (msgLevel(MSG::VERBOSE))
        verbose() << "x: " << x << ", y: " << y << endmsg;
      (*hist)->fill(x, y);
    }// if histogram booked.
    else
      sc = StatusCode::FAILURE;
  }// else histogram not booked.
  // Done.
  return sc;
}// Rich::Mon::TestPatternMonitor::fillHPD2D()
