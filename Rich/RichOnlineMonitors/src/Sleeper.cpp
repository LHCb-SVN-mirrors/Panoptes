//------------------------------------------------------------------------
/** @file RichMon.cpp
 *
 *  Implementation file for Rich Monitoring Alg
 *  Author Claus Peter Buszello 2005
 */
//------------------------------------------------------------------------

#include "Sleeper.h"

#include <stdio.h>

#ifdef WIN32
namespace win {
#include <windows.h>
}
#define mysleep win::Sleep
#define factor 1000
#else
#define mysleep usleep
#define factor 1e6
#endif

//
// Sleep(ms) , usleep(us)
//

using namespace std;
using namespace Rich::Mon;

//------------------------------------------------------------------------
//------------------------------------------------------------------------
DECLARE_ALGORITHM_FACTORY( Sleeper )


Sleeper::Sleeper( const std::string& name,
                  ISvcLocator* pSvcLocator )
  : GaudiAlgorithm  ( name, pSvcLocator ) 
{
  declareProperty("Name"          , m_myname = "a");
  declareProperty("SleepSeconds"  , m_val1   =  2 );
  declareProperty("SleepUSeconds" , m_val2   =  0 );
}



//------------------------------------------------------------------------
//------------------------------------------------------------------------
Sleeper::~Sleeper( ){

}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
StatusCode Sleeper::initialize() {  
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  info() << m_myname << ": Initializing sleeper" << endmsg;
  info() << "modify second setting by " << factor << endmsg;
 
  return StatusCode::SUCCESS;
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
StatusCode Sleeper::finalize() 
{

  info() << "Finalizing sleeper"<<endreq;
  info() <<m_myname<<endreq;
  info() << endreq;
  
  return GaudiAlgorithm::finalize();
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
StatusCode Sleeper::execute()
{   

  info() << "Sleeper "<<m_myname<<" sleeping " << m_val1*factor  <<" + " << m_val2 << " useconds"<<endreq; 

  mysleep((unsigned int)(m_val1*factor + m_val2));

  return StatusCode::SUCCESS;

}
