// $Id: CameraRunFolder.h,v 1.1 2010-01-14 11:17:00 nmangiaf Exp $
#ifndef CAMERARUNFOLDER_H
#define CAMERARUNFOLDER_H 1

//
// Include files
//

// from Gaudi
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "DetDesc/RunChangeIncident.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/AlgFactory.h"

// from RichKernel
#include "RichKernel/RichAlgBase.h"

// Event
#include "Event/ODIN.h"

// Forward declarations
class ICameraTool;

namespace Rich
{
  namespace Mon
  {

    /** @class  CameraRunFolder CameraRunFolder.h
     *  An algorithm that changes the Camera folder according to the 
     *  run number.
     *  @author Nicola Mangiafave
     *  @date   2010-01-09
     */
    class CameraRunFolder : public Rich::AlgBase,
                            virtual public IIncidentListener
    {

      // ---------------------------------------------------------------------------
      //                                PUBLIC
      // ---------------------------------------------------------------------------
    public:

      /// Standard constructor
      CameraRunFolder( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~CameraRunFolder( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode execute   ();    ///< Algorithm execution

    private:

      virtual void   handle (const Incident &incident);

      // ---------------------------------------------------------------------------
      //                                PRIVATE
      // ---------------------------------------------------------------------------
    private:

      ICameraTool *m_CameTool;             ///< CAMERA error reporting tool
      bool m_newRun;

    };

  }
}

#endif // CAMERARUNFOLDER_H
