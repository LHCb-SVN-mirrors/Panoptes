// $Id: HotPixelReport.cpp,v 1.12 2009-10-09 14:59:28 jonrob Exp $
// Include files

// local
#include "HotPixelReport.h"

// RichKernel
#include "RichKernel/RichDAQDefinitions.h"

// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include <iomanip>
#include "Event/ODIN.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram1D.h"
#include "GaudiUtils/Aida2ROOT.h"

using namespace Rich::Mon;

//-----------------------------------------------------------------------------
// Implementation file for class : HotPixelReport
//
// 2009-15-09 : Ross Young
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( HotPixelReport )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HotPixelReport::HotPixelReport( const std::string& type,
                                const std::string& name,
                                const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  // interface
  declareInterface<IHotPixelReport>(this);
  // options
  declareProperty("CutOff",m_cutoff=.01);
  declareProperty("ColumnCutOff",m_colcutoff=5);
}

//=============================================================================
// Destructor
//=============================================================================
HotPixelReport::~HotPixelReport() {}

//=============================================================================
// Fill array of hit pixels
//=============================================================================
int HotPixelReport::FillArray(const LHCb::RichSmartID &hitHPD,
                              const LHCb::RichSmartID::Vector &pixelHits)
{
  const int dummy(0); // CRJ : Why ....

  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Peforming Hot/Cold pixel test for: " << hitHPD << endmsg;

  for ( LHCb::RichSmartID::Vector::const_iterator iHit = pixelHits.begin();
        iHit != pixelHits.end(); ++iHit )
  {
    // count hits per channel
    ++m_hitCount[*iHit];
  }
  
  return dummy;
}

//=============================================================================
// Analyse hit occupancy of pixels
//=============================================================================
StatusCode HotPixelReport::PerformTest( int evts, std::list<ErrorReport> * reports)
{
  if ( msgLevel(MSG::VERBOSE) )
  {
    int OdinEvt = 0;
    LHCb::ODIN* odin = NULL;
    if ( exist<LHCb::ODIN>(LHCb::ODINLocation::Default) )
    {
      odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);
      OdinEvt = odin->eventNumber();
    }
    verbose() << "Analysing hitrates at Odin Evt Num: " << OdinEvt << endmsg;
  }

  for ( HitCountMap::const_iterator iHPDC = m_hitCount.begin();
        iHPDC != m_hitCount.end(); ++iHPDC )
  {
    const double hitrate = (double)(iHPDC->second) / (double)evts;
    if ( hitrate > m_cutoff ) 
    {
      Rich::Mon::ErrorReport er;        // Create error report of hot pixel
      er.rich   = iHPDC->first.rich();
      er.panel  = iHPDC->first.panel();
      er.row    = iHPDC->first.pdNumInCol();
      er.col    = iHPDC->first.pdCol();
      er.pixcol = iHPDC->first.pixelCol();
      er.pixrow = iHPDC->first.pixelRow();
      std::stringstream buf;
      er.level  = (ICameraTool::MessageLevel)3;
      buf <<"Hot pixel with hits " <<iHPDC->second << " and occupancy " << hitrate*100 <<"% > "
          << m_cutoff*100 <<"% in "<< iHPDC->first;
      er.message = buf.str();
      reports->push_back(er);   // Add error report to list
    }  // If pixel occupancy...
  }

  // Clear Map for next analysis
  m_hitCount.clear();

  return StatusCode::SUCCESS;
}
