#ifndef TestPatternMonitor_H
#define TestPatternMonitor_H 1

// Base class
#include "RichKernel/RichHistoAlgBase.h"

// Gaudi
#include "GaudiKernel/AlgFactory.h"

// LHCb
#include "Kernel/RichSmartID.h"

// Std
#include <deque>
#include <map>
#include <vector>

// Forward declarations.
class DeRichSystem;
class ICameraTool;

namespace AIDA {
  class IHistogram1D;
  class IHistogram2D;
  class IProfile1D;
}// namespace AIDA

namespace Rich {
  namespace DAQ {
    class IRawBufferToSmartIDsTool;
  }// namespace DAQ

  namespace Mon {
    class ITestPattern;

    //-----------------------------------------------------------------------------
    /** @class TestPatternMonitor TestPatternMonitor.h
     *
     *  Algorithm to check the HPD for the test pattern hits.
     *  Used to check the test pattern is being seen.
     *
     *  @author Gareth Rogers rogers@hep.phy.cam.ac.uk
     *  @date   20/08/2007
     */
    //-----------------------------------------------------------------------------
    class TestPatternMonitor : public Rich::HistoAlgBase {

    private:
      /*!
       * This private class is used to store the histograms and counters that are
       * associated with an HPD smart ID.
       */
      class HPDTestPatternInfo {
      public:
        /*!
         * Default constructor.
         *
         * Only a default constructor is provided. The member variable must be initialised
         * once the object is constructed.
         */
        HPDTestPatternInfo()
          : m_nHits(0)
          , m_efficiency(0)
          , m_efficiencyMA(0)
          , m_effMADifference(20)
          , m_deadCount(0)
          , m_errorCount(0)
          , m_isDead(false)
          , mp_efficiencyHist(NULL)
          , mp_efficiencyMAHist(NULL)
          , mp_deadHPDHist(NULL)
          , mp_hitsPerHPDMap(NULL)
          , mp_deadHPDMap(NULL)
          , mp_effHPDMap(NULL)
          , mp_hitMap(NULL)
	  , m_countersDeclared( false )
        {        }// HPDTestPatternInfo()

        int m_nHits; //!< Number of hits that were seen in the last event.

        int m_efficiency; //!< Test pattern efficiency per event.
        int m_efficiencyMA; //!< Test pattern moving average efficiency.
        std::deque<int> m_efficiencyMAData; //!< Holds the data for the last n events that are used to calculate the moving average.
        int m_effMADifference; //!< Allowed difference between the efficiency and the current moving average before it is flagged as an error.
        int m_deadCount; //!< Gives the percentage of events it has been dead for.
        int m_errorCount; //!< Number of errors that have occured.
        bool m_isDead; //!< True if the HPD have been declared dead in the event under consideration.

        // The class has only a default constructor as we never own the pointer to the
        // histograms and it is up to Gaudi to deal with their deletion.
	
        AIDA::IHistogram1D *mp_efficiencyHist;
        //!< A pointer to a 1D histogram of the efficiency.
        AIDA::IHistogram1D *mp_efficiencyMAHist;
        //!< A pointer to the 1D histogram of moving average efficiency.
        AIDA::IHistogram1D *mp_deadHPDHist;
        //!< A pointer to the 1D histogram for the number of dead HPDs per event.
        AIDA::IHistogram2D *mp_hitsPerHPDMap;
        //!< A pointer to the hit per HPD hit map.
        AIDA::IHistogram2D *mp_deadHPDMap;
        //!< A pointer to the dead HPD map.
        AIDA::IHistogram2D *mp_effHPDMap;
        //!< A pointer to the efficiency HPD map.
        AIDA::IHistogram2D *mp_hitMap;
        //!< A pointer to a hit map for this smart ID.

        //!< If true then the counters have been declared and do not need to be declared again. All counters must declared at the same time.
        bool m_countersDeclared;

      };// class HPDTestPatternInfo

      /*!
       * This private class is used to store the histograms and counters that are
       * associated with a smart ID.
       */
      class RichPanelTestPatternInfo {
      public:
        /*!
         * Default constructor.
         *
         * Only a default constructor is provided. The member variable must be initialised
         * once the object is constructed.
         */
        RichPanelTestPatternInfo()
          : m_nHits(0)
          , m_efficiency(0)
          , m_efficiencyMA(0)
          , m_effMADifference(20)
          , m_deadCount(0)
          , m_errorCount(0)
          , mp_xAverageEfficiencyHist(NULL)
          , mp_yAverageEfficiencyHist(NULL)
          , mp_xAverageDeadEfficiencyHist(NULL)
          , mp_yAverageDeadEfficiencyHist(NULL)
          , mp_hitsPerHPDMap(NULL)
          , mp_deadHPDMap(NULL)
          , mp_effHPDMap(NULL)
          , mp_hitMap(NULL)
	  , m_countersDeclared( false )
          , m_panelDead(false)
        {
        }// RichPanelTestPatternInfo()

        int m_nHits; //!< Number of hits that were seen in the last event.

        int m_efficiency; //!< Test pattern efficiency per event.
        int m_efficiencyMA; //!< Test pattern moving average efficiency.
        std::deque<int> m_efficiencyMAData; //!< Holds the data for the last n events that are used to calculate the moving average.
        int m_effMADifference; //!< Allowed difference between the efficiency and the current moving average before it is flagged as an error.
        int m_deadCount; //!< Gives the percentage of HPDs that are dead.
        int m_errorCount; //!< Number of errors that have occured.

        // The class has only a default constructor as we never own the pointer to the
        // histograms and it is up to Gaudi to deal with their deletion.
	
        AIDA::IProfile1D *mp_xAverageEfficiencyHist;
        //!< A pointer to a 1D histogram that gives the average efficiency on the x-axis. Either row or column depending on RICH.
        AIDA::IProfile1D *mp_yAverageEfficiencyHist;
        //!< A pointer to a 1D histogram that gives the average efficiency on the y-axis. Either row or column depending on RICH.
        AIDA::IProfile1D *mp_xAverageDeadEfficiencyHist;
        //!< A pointer to a 1D histogram that gives the dead HPD efficiency on the x-axis. Either row or column depending on RICH.
        AIDA::IProfile1D *mp_yAverageDeadEfficiencyHist;
        //!< A pointer to a 1D histogram that gives the dead HPD efficiency on the y-axis. Either row or column depending on RICH.

        AIDA::IHistogram2D *mp_hitsPerHPDMap;
        //!< A pointer to the hit per HPD hit map.
        AIDA::IHistogram2D *mp_deadHPDMap;
        //!< A pointer to the dead HPD map.
        AIDA::IHistogram2D *mp_effHPDMap;
        //!< A pointer to the efficiency HPD map.
        AIDA::IHistogram2D *mp_hitMap;
        //!< A pointer to a hit map for this smart ID.

        bool m_countersDeclared;
        //!< If true then the counters have been declared and do not need to be declared again. All counters must declared at the same time.

        bool m_panelDead;
        //!< If true the Dead efficiency for the Panel is larger then m_DeadPanelThreshold.

      };// class RichPanelTestPatternInfo

    public:
      /* Standard constructor. */
      TestPatternMonitor(const std::string &name, ISvcLocator *pSvcLocator);

      /* Destructor. */
      virtual ~TestPatternMonitor();

      /* Algorithm initialization. */
      virtual StatusCode initialize();

      /* Algorithm execution. */
      virtual StatusCode execute   ();

      /* Algorithm finalization. */
      virtual StatusCode finalize  ();

    private:
      /*!
       * Returns the number of test pattern hits found in the given vector of HPD hits.
       *
       * @param  pixelHitIDs Vector of the pixel hits for a given HPD in which to search
       *           for the test pattern.
       * @return int The number of hits found in the vector of hits that corresponded to
       *           a test pattern hit.
       */
      int find(const LHCb::RichSmartID::Vector &pixelHitIDs);

      /*!
       * Updates the statistics for the given HPD from the number test pattern hits found.
       *
       * @param  hpdId ID of the HPD who the number of test pattern hits found corresponds to.
       * @param  nHits Number of hits found on this HPD that correspond to the test pattern hits.
       * @return Status code.
       */
      StatusCode updateHPDStats(const LHCb::RichSmartID &hpdID, const int nHits);

      /*!
       * Updates the statistics for the given HPD from the number test pattern hits found.
       *
       * @param  panelID ID of the RICH panel whose statistics are to be updated.
       * @return Status code.
       *
       * This function should only be called once updateHPDStats has been called for each of the
       * HPDs in the event.
       */
      StatusCode updateRichPanelStats(const LHCb::RichSmartID &panelID, 
                                      const std::map<int,std::vector<int> > &checkedHPD_map);

      /*!
       * Checks through the RichSmartID to TestPatternMonitor map and appends an error message to CAMERA.
       * Normally a message is appended only if there is at least one HPD with no hits or with fluctuating 
       * test pattern efficiency.
       * If ForceSummary == true the message is appended to CAMERA even if all the HPDs behave correctly.
       * If LogMsg = true it sends the message to the log file.
       *
       * @param ForceSummary If true a message is sent even if all the HPDs behave correctly.
       * @param LogMsg If true a message is sent to the log file.
       * @param noAppend If true no message is appended to CAMERA
       * @return bool If ForceSummary == False returns true if a message was added to CAMERA. If ForceSummary == True returns true if at least one HPD is misbehaving.
       *
       * This function should be called after updateHPDStats has been called for each HPD in the event
       * and updateRichPanelStats has been called for each RICH panel. Using the information that the
       * accumulate it decides whether to append an error message to the CAMERA message buffer.
       */
      bool appendErrorMessages(bool ForceSummary = false, bool LogMsg = false, bool noAppend = false);

      /*!
       * Books the histograms and counters that accumulate statics per RICH panel.
       *
       * @param  rich RICH that the histogram should be booked for.
       * @param  panel Panel that the histogram should be booked for.
       * @return Status code.
       *
       * The panel is checked to ensure it is on the on the given RICH. If it is not the mapping
       * used by the function checkPanelIsOnRich will be performed. This ensures valid histogram names.
       */
      StatusCode bookRichPanelHistograms(const Rich::DetectorType rich, const Rich::Side panel);

      /*!
       * Books the histograms and counters that accumulate statistics per HPD.
       *
       * @param  smartID HPD that the histogram and counters should be booked for.
       * @return Status code.
       */
      StatusCode bookIndividualHistograms(const LHCb::RichSmartID &smartID);

      /*!
       * Constructs the ID of the 1D RICH histogram that the HPD belongs in.
       *
       * @param  rich RICH that the histogram ID should be generated for.
       * @param  panel Panel that the histogram name should be generated for.
       * @return std::string ID of the histogram.
       *
       * This function is to be used with the plot methods to ensure that a given HPD is plotted on the correct RICH and panel histogram.
       */
      std::string richPanel1DHistId(const Rich::DetectorType rich, const Rich::Side panel);

      /*!
       * Constructs the name of the 1D RICH histogram that the HPD belongs in.
       *
       * @param  rich RICH that the histogram ID should be generated for.
       * @param  panel Panel that the histogram name should be generated for.
       * @return std::string Name of the histogram.
       *
       * This function is to be used with the plot methods to ensure that a given HPD is plotted on the correct RICH and panel histogram.
       */
      std::string richPanel1DHistName(const Rich::DetectorType rich, const Rich::Side panel);

      /*!
       * Constructs the ID of the 2D RICH panel histogram that the HPD belongs in.
       *
       * @param  rich RICH that the histogram ID should be generated for.
       * @param  panel Panel that the histogram name should be generated for.
       * @return std::string ID of the histogram.
       *
       * This function is to be used with the plot methods to ensure that a given HPD is plotted on the correct RICH and panel histogram.
       */
      std::string richPanel2DHistId(const Rich::DetectorType rich, const Rich::Side panel);

      /*!
       * Constructs the name of the 2D RICH panel histogram that the HPD belongs in.
       *
       * @param  rich RICH that the histogram ID should be generated for.
       * @param  panel Panel that the histogram name should be generated for.
       * @return std::string Name of the histogram.
       *
       * This function is to be used with the plot methods to ensure that a given HPD is plotted on the correct RICH and panel histogram.
       */
      std::string richPanel2DHistName(const Rich::DetectorType rich, const Rich::Side panel);

      /*!
       * Constructs the ID of the individual HPD hit map's histogram.
       *
       * @param  smartId Smart ID of the HPD whose histogram ID is to be constructed.
       * @return std::string ID of the histogram.
       *
       * This function is to be used with the plot methods to ensure that a given HPD is plotted on the correct individual HPD histogram.
       */
      std::string individualHistId(const LHCb::RichSmartID &smartId);

      /*!
       * Constructs the name of the individual HPD hit map's histogram.
       *
       * @param  smartId Smart ID of the HPD whose histogram name is to be constructed.
       * @return std::string Name of the histogram.
       *
       * This function is to be used with the plot methods to ensure that a given HPD is plotted on the correct individual HPD histogram.
       */
      std::string individualHistName(const LHCb::RichSmartID &smartId);

      /*!
       * Fills the given histogram with the given data. Checks the histogram exists and books it if not.
       *
       * @param  rich RICH that the panel should be on.
       * @param  panel Panel to be checked. Change by reference if necessary. Not change if it is on the given RICH.
       * @param  hist The histogram to be filled.
       * @param  value Value to be added to the histogram.
       * @return Status code.
       *
       * The attempt to book is simply a call to bookRichPanelHistograms hence the RICH and panel information is required.
       */
      StatusCode fillRichPanel1D(const Rich::DetectorType rich, 
                                 const Rich::Side panel, 
                                 AIDA::IHistogram1D **hist, 
                                 const int value);

      /*!
       * Fills the given profile with the given data. Checks the profile exists and books it if not.
       *
       * @param  rich RICH that the panel should be on.
       * @param  panel Panel to be checked. Change by reference if necessary. Not change if it is on the given RICH.
       * @param  hist The histogram to be filled.
       * @param  x x bin to fill.
       * @param  y y bin to fill.
       * @return Status code.
       *
       * The attempt to book is simply a call to bookRichPanelHistograms hence the RICH and panel information is required.
       */
      StatusCode fillRichPanelProfile1D(const Rich::DetectorType rich, 
                                        const Rich::Side panel, 
                                        AIDA::IProfile1D **hist, 
                                        const int x, const int y);

      /*!
       * Fills the given histogram with the given data. Checks the histogram exists and books it if not.
       *
       * @param  rich RICH that the panel should be on.
       * @param  panel Panel to be checked. Change by reference if necessary. Not change if it is on the given RICH.
       * @param  hist The histogram to be filled.
       * @param  x x bin to fill.
       * @param  y y bin to fill.
       * @param  wt Weight to use for the filled entry.
       * @return Status code.
       *
       * The attempt to book is simply a call to bookRichPanelHistograms hence the RICH and panel information is required.
       */
      StatusCode fillRichPanel2D(const Rich::DetectorType rich, 
                                 const Rich::Side panel, 
                                 AIDA::IHistogram2D **hist,
                                 const int x, const int y, const int wt=1);

      /*!
       * Fills the given histogram with the given data. For a given bin the bin value is set to value. Checks the histogram exists and books it if not.
       *
       * @param  rich RICH that the panel should be on.
       * @param  panel Panel to be checked. Change by reference if necessary. Not change if it is on the given RICH.
       * @param  hist The histogram to be filled.
       * @param  x x bin to fill.
       * @param  y y bin to fill.
       * @param  value value value to put in the bin.
       * @param  wt Weight to use for the filled entry.
       * @return Status code.
       *
       * The attempt to book is simply a call to bookRichPanelHistograms hence the RICH and panel information is required.
       */
      StatusCode fillBinEntryRichPanel2D(const Rich::DetectorType rich, 
                                         const Rich::Side panel, 
                                         AIDA::IHistogram2D **hist, 
                                         const int x, const int y, 
                                         const double value, const int wt=1);

      /*!
       * Fills the given histogram with the given data. Checks the histogram exists and books it if not.
       *
       * @param  smartID SmartID of the HPD that the histogram is associated with.
       * @param  hist The histogram to be filled.
       * @param  x x bin to fill.
       * @param  y y bin to fill.
       * @return Status code.
       *
       * The attempt to book is simply a call to bookIndividualHistograms hence the smartID is required.
       */
      StatusCode fillHPD2D(const LHCb::RichSmartID &smartID, AIDA::IHistogram2D **hist, int x, int y);

      /* Private data. */
    private:

      const Rich::DAQ::IRawBufferToSmartIDsTool *mp_decoder; //!< Raw Buffer Decoding tool.
      const DeRichSystem *mp_richSys; //!< Pointer to RICH system detector element.
      ICameraTool *mp_camera; //!< Pointer to the CAMERA tool.
      const ITestPattern *mp_testPattern; //!< Pointer to the HPD test pattern tool.

      // These are the extension for the ID and name of the histograms that are used
      // to identify those we wish to book.
      const std::string m_hitsPerHPDMap;
      const std::string m_hitMap;
      const std::string m_deadHPDMap;
      const std::string m_effHPDMap;
      const std::string m_xEfficiencyProjection;
      const std::string m_yEfficiencyProjection;
      const std::string m_xDeadEfficiencyProjection;
      const std::string m_yDeadEfficiencyProjection;
      const std::string m_efficiencyCounter;
      const std::string m_efficiencyHistogram;
      const std::string m_efficiencyMACounter;
      const std::string m_efficiencyMAHistogram;
      const std::string m_deadHPDCounter;
      const std::string m_deadHPDHistogram;

      bool m_deadHPDMapFlag;
      //!< These flag to indicate which 2D plots to produce.
      bool m_effHPDMapFlag;
      //!< These flag to indicate which 2D plots to produce.
      bool m_hitsPerHPDMapFlag;
      //!< These flag to indicate which 2D plots to produce.
      bool m_hitMapFlag;
      //!< These flag to indicate which 2D plots to produce.
      bool m_individualHitMapsFlag;
      //!< These flag to indicate which 2D plots to produce.
      bool m_checkTriggerTypeFlag;

      unsigned long m_nEventsSeen;
      //!< Keep track of the number of events we have been given.
      unsigned long m_nEvtsLastUpdate;       
      //!< Event count for last message
      int m_monitorRate;
      //!< Check only every nth event. A property.
      double m_DeadPanelThreshold;
      //!< Percentage of HPDs with no hit above which the panel is declared dead and a message is sent to Camera.
      int m_DeadPanelAverage;
      //!< Average number of panels with deadEfficiency > m_DeadPanelThreshold (averaged over m_DeadPanelReportRate events)
      bool m_deadRICHesFlag;
      //!< True if deadEfficiency > m_DeadPanelThreshold (averaged over m_DeadPanelReportRate events)
      bool m_wrongTriggerFlag;
      //!< True if the run has either trigger type or calibration type not suited for TestPatternMonitor


      unsigned m_movingAverageDataEvents;
      //!< Number of events to use to calculate the efficiency moving average. A property.
      int m_movingAverageHistEvents;
      //!< Number of moving average values to display on the histogram. A property.
      int m_errorEfficiency;
      //!< When the efficiency of the test pattern differs from the moving average efficiency by this amount the HPD is considered to be in error. A property.
      int m_deadEfficiency;
      //!< When the efficiency of the test pattern is below this value the HPD is considered dead. A property.
      int m_errorReportRate;
      //!< Some errors for the HPDs on a given L1 will be report every x number of errors. A property.
      int m_DeadPanelReportRate;
      //!< The dead efficiency for a panel is averaged over m_DeadPanleReportRate avent.  

      bool m_fakeTestPattern;
      //!< Use to indicate that we wish to use HPD hits generated by the algorithm rather than those read from the RICH. A property.

      bool m_invertRows;
      //!< Passed to the test pattern tool to determine whether to invert the row numbers in the test patterns.

      bool m_extendedVerbose;
      //!< Switch on even more verbose output. A property.
      
      std::string m_taeLocation;
      //!< Holds the TAE location that we have been set up to look at. A property.

      /*!
       * This map links a smart ID for a HPD to a TestPatternInfo class
       * which contains the various counters and histograms for that smart ID.
       */
      std::map<LHCb::RichSmartID, HPDTestPatternInfo> m_hpdInfoMap;

      /*!
       * This map links a smart ID for a RICH panel to a TestPatternInfo class
       * which contains the various counters and histograms for that smart ID.
       */
      std::map<LHCb::RichSmartID, RichPanelTestPatternInfo> m_panelInfoMap;

      std::string m_name;
      //!< This is the name of that we will submit to CAMERA. It contains the algorithm name and partition, which should uniquely identify the sender.

      time_t            m_TimeStart;
      //!< Time counter to send messages to camera every m_UpdateTimerInterval seconds
      time_t            m_TimeLastUpdate;
      //!< Time counter to send messages to camera every m_UpdateTimerInterval seconds

      int               m_UpdateTimerInterval;
      //!< Interval at which to send error messages (in seconds)

      int               m_MessagesCounter;
      //!< Messages counter. It counts the number of Summary messages sent to Camera.

      std::ostringstream m_finalMsg;
      //!< Final Message. It's the last message sent to Camera.

    };// TestPatternMonitor

  }// namespace Mon
}// namesspace Rich

#endif// TestPatternMonitor_H
