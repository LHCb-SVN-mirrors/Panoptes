// $Id: HPDCountTool.cpp,v 1.15 2009-09-16 04:23:29 ryoung Exp $
// Include files

// from Gaudi
#include "GaudiKernel/ToolFactory.h"

// local
#include "HPDCountTool.h"
#include "ErrorReport.h"

#include <cmath>

//-----------------------------------------------------------------------------
// Implementation file for class : HPDCountTool
//
// 2007-05-30 : Claus Buszello
//-----------------------------------------------------------------------------

namespace Rich {
  namespace Mon {
    // Declaration of the Tool Factory
    DECLARE_TOOL_FACTORY( HPDCountTool )
  }//namespace Mon
}//namespace Rich

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Rich::Mon::HPDCountTool::HPDCountTool( const std::string& type,
                                       const std::string& name,
                                       const IInterface* parent )
  : GaudiTool ( type, name , parent )
{
  declareInterface<IHPDCountTool>(this);

  declareProperty("NoiseyTolerance", m_noiseyTolerence);
  declareProperty("MovingAverageDataEvents", m_movingAverageDataEvents=500);
}// HPDCountTool::HPDCountTool()

//=============================================================================
// Initialize
//=============================================================================
StatusCode Rich::Mon::HPDCountTool::initialize()
{
  const StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return sc;
  
  if (msgLevel(MSG::VERBOSE))
    verbose() << this->name() << " initialised." << endmsg;
  return sc;
}// HPDCountTool::initialize()

//=============================================================================
// Destructor
//=============================================================================
Rich::Mon::HPDCountTool::~HPDCountTool() {}

//=============================================================================
StatusCode Rich::Mon::HPDCountTool::performTest(const LHCb::RichSmartID &hitHPD, int nHits, ErrorReport &errReport) {
  // Get the info for his HPD. This will just create a default constructed object if necessary.
  HPDCountInfo &hpdInfo = m_hpdInfoMap[hitHPD.pdID()];

  // Save the number of hits we saw on this event.
  hpdInfo.m_nHits = nHits;

  // Calculate the moving average for this HPD.
  // Save the number of hits for the MA calculation.
  hpdInfo.m_nHitsMAData.push_back(nHits);
  // We are maintaining the moving average over a constant number of elements.
  if (m_movingAverageDataEvents < hpdInfo.m_nHitsMAData.size())
    hpdInfo.m_nHitsMAData.pop_front();
  // Loop over the deque and sum up all the element values.
  int sum(0);
  const std::deque<int>::const_iterator maDataBeginIt(hpdInfo.m_nHitsMAData.begin());
  const std::deque<int>::const_iterator maDataEndIt(hpdInfo.m_nHitsMAData.end());
  for (std::deque<int>::const_iterator maDataIt(maDataBeginIt); maDataIt!=maDataEndIt; ++maDataIt)
    sum += *maDataIt;

  // Now determine the average and store it in our class variable.
  // This will enter the HPD into the map if it didn't exist or
  // just over write it if it does.
  if (0 != hpdInfo.m_nHitsMAData.size())
    hpdInfo.m_nHitsMA = sum / hpdInfo.m_nHitsMAData.size();
  else {
    Warning("No data to compute moving average with. Has the number of entries to calculate moving average been set to zero?");
    return StatusCode::SUCCESS;
  }// if(0!=hpdInfo.m_nHitsMAData.size())

  // We need to compare the moving average to the number of hits to see if they differ significantly.
  // If they do then we increment the number of errors seen. Later in the function we decide whether
  // to mark it an error.
  if (0 != hpdInfo.m_nHitsMA) {
    unsigned nHitsMinusMA = abs((nHits - hpdInfo.m_nHitsMA) / hpdInfo.m_nHitsMA);
    if (nHitsMinusMA > hpdInfo.m_nHitsMADifference)
      ++hpdInfo.m_errorCount;
  }// if(0!=hpdInfo.m_nHitsMA)

  if (m_noiseyTolerence < hpdInfo.m_errorCount) {
    //Create the error message.
    std::stringstream buf;
    buf << hitHPD
        << " has been noisey for "
        << hpdInfo.m_errorCount
        << " monitored events (not consequtive).\n";
    errReport.message = buf.str();
    errReport.level   = (ICameraTool::MessageLevel)3;
    //Clear the counter.
    hpdInfo.m_errorCount = 0;
  }//if(m_noiseyTolerence<hpdInfo.m_errorCount)

  // Done.
  return StatusCode::SUCCESS;
}// HPDCountTool::performTest()
