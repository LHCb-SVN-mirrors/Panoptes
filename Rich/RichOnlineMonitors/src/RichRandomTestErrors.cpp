
// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "RichRandomTestErrors.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichRandomTestErrors
//
// 2011-08-10 : Chris Jones
//-----------------------------------------------------------------------------

using namespace Rich::Mon;

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( RandomTestErrors )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  RandomTestErrors::RandomTestErrors( const std::string& name,
                                      ISvcLocator* pSvcLocator)
    : HistoAlgBase         ( name , pSvcLocator ),
      m_HpdUKL1DisableTool ( NULL               ),
      m_RichSys            ( NULL               ),
      m_enabled            ( false              ),
      m_nEvts              ( 0                  )
{
  declareProperty( "DisableRate", m_disableRate = 0.001 );
  declareProperty( "ErrorRate",   m_errorRate   = 0.001 );
  declareProperty( "ErrorInterval",   m_evtFreq     = 5000  );
  declareProperty( "ErrorBurstSize",  m_burstSize   = 1500  );
  declareProperty( "ResetInterval", m_resetFreq     = 10000 );
}

//=============================================================================
// Destructor
//=============================================================================
RandomTestErrors::~RandomTestErrors() { }

//=============================================================================
// Initialization
//=============================================================================
StatusCode RandomTestErrors::initialize()
{
  const StatusCode sc = HistoAlgBase::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  // disabling tool
  m_HpdUKL1DisableTool =
    tool<Rich::Mon::IHpdUkL1DisableTool>("Rich::Mon::HpdUkL1DisableTool",
                                         "RichUKL1Disable");

  // RichDet
  m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  // random number generator
  IRndmGenSvc * randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  if ( !m_rndm.initialize( randSvc, Rndm::Flat(0.,1.) ) )
  {
    return Error( "Unable to create Random generator" );
  }

  if ( m_disableRate > 0 )
  {
    Warning( "Random HPD Disabling enabled", StatusCode::SUCCESS ).ignore();
    m_enabled = true;
  }
  if ( m_errorRate   > 0 )
  {
    Warning( "Random HPD Errors enabled", StatusCode::SUCCESS ).ignore();
    m_enabled = true;
  }

  if ( m_enabled )
  {
    info() << "ErrorInterval  " << m_evtFreq  << endmsg;
    info() << "ErrorBurstSize " << m_burstSize << endmsg;
  }

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode RandomTestErrors::execute()
{
  if ( !m_enabled ) return StatusCode::SUCCESS;

  if      ( !m_inErrorBurst && (m_nEvts % m_evtFreq == 0) )
  {
    m_inErrorBurst = true;
    info() << "Starting sending error reports or disables" << endmsg;
  }
  else if ( m_inErrorBurst  && ((m_nEvts-m_burstSize) % m_evtFreq == 0) )
  {
    m_inErrorBurst = false;
    info() << "Stopping sending error reports or disables" << endmsg;
  }

  // count events
  ++m_nEvts;

  if ( m_inErrorBurst )
  {

    // get list of all active HPDs
    const LHCb::RichSmartID::Vector & hpds = m_RichSys->activePDRichSmartIDs();

    // Loop over HPDs and issue a disable or error report at the given rates
    for ( LHCb::RichSmartID::Vector::const_iterator iHPD = hpds.begin();
          iHPD != hpds.end(); ++iHPD )
    {
      if ( m_rndm() < m_disableRate )
      {
        m_HpdUKL1DisableTool->DisableHPD( *iHPD, "Random Test Disable" );
      }
      if ( m_rndm() < m_errorRate   )
      {
        m_HpdUKL1DisableTool->ReportHPD( *iHPD, "Random Test Report" );
      }
    }

  }

  // time to reset the disabled HPDs
  if ( ( m_nEvts % m_resetFreq ) == 0 )
  {
    //m_HpdUKL1DisableTool->ResetAll();
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode RandomTestErrors::finalize()
{
  // finalize random number generator
  m_rndm.finalize();
  // return
  return Rich::HistoAlgBase::finalize();
}

//=============================================================================
