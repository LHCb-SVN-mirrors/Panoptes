// $Id: RichHPDImageMovement.h,v 1.2 2009-11-25 18:35:55 tblake Exp $
#ifndef RICHHPDIMAGEMOVEMENT_H 
#define RICHHPDIMAGEMOVEMENT_H 1

#include "RichKernel/RichHistoAlgBase.h"

#include "RichKernel/IRichRawBufferToSmartIDsTool.h"
#include "RichDet/DeRichSystem.h"

#include "AIDA/IHistogram2D.h"

#include <map>

/** @class RichHPDImageMovement RichHPDImageMovement.h
 *  
 *
 *  @author Thomas BLAKE
 *  @date   2009-10-13
 */


namespace Rich 
{
  namespace Mon 
  {
    
    class IRichHPDImageCentre 
    {
    public:
      virtual ~IRichHPDImageCentre() {};
      virtual const double& averageCol() const = 0;
      virtual const double& averageRow() const = 0;
      virtual bool update( const int COL, const int ROW ) = 0;
    };
    
    
    class RichHPDImageCentreWithCuts :  virtual public IRichHPDImageCentre 
    {
    public:
      
      RichHPDImageCentreWithCuts( const int UpdateFrequency, const double Fraction ) ;
      RichHPDImageCentreWithCuts() ;
      
      virtual ~RichHPDImageCentreWithCuts() ;
      
      bool update( const int COL , const int ROW ) ;
      
      const double& averageCol() const ;
      const double& averageRow() const ;
      
      
    protected:
      
      void initialize() ;
      double averagePixelValue() const ;
      
    private:
      
      /// Last average column position
      double m_lastAverageCol;

      /// Last average row position 
      double m_lastAverageRow;

      /// Fraction for boundary
      double m_fraction;
      
      /// Hit Counter 
      int m_nHit;
      
      /// Update Frequency For HPDs
      int m_nUpdate;
      
      /// Data
      int m_data[32][32] ;
      
    };
    
    class RichHPDImageCentre : virtual public IRichHPDImageCentre{
      
    public :
      
      RichHPDImageCentre( const int UpdateFrequency ) ;
      RichHPDImageCentre() ;
      
      virtual ~RichHPDImageCentre() ;

      bool update( const int COL , const int ROW ) ;
      
      //  void reset() ;
      
      const double& averageCol() const ;
      const double& averageRow() const ;
      
      
    private:
      
      /// Running tally of Col. 
      int m_centreCol;
      
      /// Running tally of Row.
      int m_centreRow;
      
      /// Last average column position
      double m_lastAverageCol;
      
      /// Last average row position 
      double m_lastAverageRow;
      
      /// Hit Counter 
      int m_nHit;
      
      /// Update Frequency For HPDs
      int m_nUpdate;
    };
    

    class RichHPDImageMovement : public Rich::HistoAlgBase  {

    public: 
      /// Standard constructor
      RichHPDImageMovement( const std::string& name, ISvcLocator* pSvcLocator );
      
      virtual ~RichHPDImageMovement( ); ///< Destructor
      
      virtual StatusCode initialize();    ///< Algorithm initialization
      
      virtual StatusCode execute   ();    ///< Algorithm execution
      
      virtual StatusCode finalize  ();    ///< Algorithm finalization
      
    protected:

      void updateArray( const unsigned int HardID, 
                        const double COL, 
                        const double ROW );
      
      void updateHistograms() ;
      
    private:
      
      /// Update Frequency for HPDs 
      int m_nUpdate ;
      
      /// Event Counter 
      int m_nEvt ;

      /// Flag for cut based analysis 
      bool m_useCuts ;
      
      /// Fraction of average pixel for cut based analysis 
      double m_cutFraction ;
      
      /// Flag for histogram production  
      bool m_makeHistos;
      
      /// Histogram update frequency 
      int m_nUpdateHistos;
      
      /// Display Smart ID warnings
      bool m_displayWarnings ;
      
      
      /// Pointer to Rich Sys Detector Element
      const DeRichSystem * m_RichSys;
  
      /// Raw Buffer Decoding tool
      const Rich::DAQ::IRawBufferToSmartIDsTool * m_SmartIDDecoder;
      
      /// Map between RichHardID and object describing image centre  
      std::map< unsigned int , IRichHPDImageCentre* > m_map ;
      
      /// Iterator for map 
      std::map< unsigned int, IRichHPDImageCentre* >::iterator m_iter ;
      
      /// Histograms 
      std::map< unsigned int, AIDA::IHistogram2D*> m_histo;
      std::map< unsigned int, AIDA::IHistogram2D*>::iterator  m_histoiter;

      /// Arrays for DIM publishing 
      float* m_arrayCol;
      float* m_arrayRow;
      unsigned int* m_arrayID;
      unsigned int m_arraySize;
    
    };
    
  } // namespae Mon
} // namespace Rich 

#endif // RICHHPDIMAGEMOVEMENT_H
