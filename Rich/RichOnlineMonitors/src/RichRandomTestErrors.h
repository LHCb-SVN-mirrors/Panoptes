// $Id: $
#ifndef RICHRANDOMTESTERRORS_H
#define RICHRANDOMTESTERRORS_H 1

// Gaudi
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IRndmGenSvc.h"

// base class
#include "RichKernel/RichHistoAlgBase.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// RichKernel
#include "RichMonitoringTools/IHpdUkL1DisableTool.h"

namespace Rich
{
  namespace Mon
  {

    /** @class RichRandomTestErrors RichRandomTestErrors.h
     *
     *
     *  @author Chris Jones
     *  @date   2011-08-10
     */
    class RandomTestErrors : public Rich::HistoAlgBase
    {
    
    public:
    
      /// Standard constructor
      RandomTestErrors( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~RandomTestErrors( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode execute   ();    ///< Algorithm execution
      virtual StatusCode finalize  ();    ///< Algorithm finalization

    private:

      /// HPD disabling tool
      mutable Rich::Mon::IHpdUkL1DisableTool * m_HpdUKL1DisableTool;

      /// Pointer to RICH system detector element
      mutable const DeRichSystem * m_RichSys;

      /// Disable rate
      double m_disableRate;

      /// Error rate
      double m_errorRate;

      /// random number generator
      mutable Rndm::Numbers m_rndm;

      /// Frequency in events to send the errors/disable requests
      unsigned long int m_evtFreq;

      /// When a burst of events is started, the number of events to continue for
      unsigned long int m_burstSize;

      /// The frequency in events to issue a reset, to clear all current reports
      unsigned long int m_resetFreq;

      /// Flag to indicated if we are enabled or not
      bool m_enabled;

      /// Count events
      unsigned long int m_nEvts;

      /// Flag to say if we are currently sending errors/disables
      bool m_inErrorBurst;

    };

  }
}

#endif // RICHRANDOMTESTERRORS_H
