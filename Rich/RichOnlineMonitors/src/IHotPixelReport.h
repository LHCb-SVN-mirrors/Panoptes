// $Id: IHotPixelReport.h,v 1.5 2009-09-16 04:23:29 ryoung Exp $
#ifndef IHOTPIXELREPORT_H
#define IHOTPIXELREPORT_H 1

// Include files
// from STL
#include <string>
#include <list>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/IHistogramSvc.h"

#include "AIDA/IHistogram1D.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "TH1.h"
#include "TH2.h"

#include "ErrorReport.h"

using namespace Gaudi::Utils ;

static const InterfaceID IID_IHotPixelReport ( "IHotPixelReport", 1, 0 );

namespace Rich
{
  namespace Mon
  {

    /** @class IHotPixelReport IHotPixelReport.h IHotPixelReport.h
     *
     *
     *  @author Claus Buszello
     *  @date   2007-05-30
     */
    class IHotPixelReport : virtual public IAlgTool 
    {
    
    public:

      // Return the interface ID
      static const InterfaceID& interfaceID() { return IID_IHotPixelReport; }

      //      virtual StatusCode performTest(TH2D *H, Rich::Mon::ErrorReport &instat,std::list<Rich::Mon::ErrorReport> * reports)=0;
      virtual int FillArray(const LHCb::RichSmartID &hitHPD, const LHCb::RichSmartID::Vector &pixelHits)=0;                                                                                    
      virtual StatusCode PerformTest(int evts, std::list<ErrorReport> * reports)=0;
                         
    };

  }
}

#endif // IRICHTESTTOOL_H
