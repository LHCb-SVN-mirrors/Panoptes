
// $Id: RichSingleEventSnapshot.h,v 1.11 2009-12-08 13:20:14 ukerzel Exp $

#ifndef RICHONLINEMONITORS_RichSingleEventSnapshot_H
#define RICHONLINEMONITORS_RichSingleEventSnapshot_H 1

// Include files
#include <map>
#include <sstream>

// Gaudi
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichRecBase/RichRecHistoAlgBase.h"

// boost
#include "boost/lexical_cast.hpp"

// RichKernel
#include "RichKernel/RichMap.h"
#include "RichRecBase/FastRingFitter.h"
#include "RichKernel/IRichRawBufferToSmartIDsTool.h"
#include "RichKernel/IRichSmartIDTool.h"

// temporary histogramming numbers
#include "RichRecBase/RichDetParams.h"

// Event
#include "Event/ODIN.h"
#include "Event/RichRecRing.h"

// ROOT
#include "TH2D.h"

// time
#include <time.h>

// Forward declarations
class ICameraTool;

namespace Rich
{
  namespace Mon
  {

    /** @class SingleEventSnapshot RichSingleEventSnapshot.h
     *
     *  This algorithm produces hit maps for events with identified trackless rings
     *
     *  @author Susan Haines
     *  @author Chris Jones
     *  @date   2008-08-22
     */
    class SingleEventSnapshot : public Rich::Rec::HistoAlgBase
    {

      // ---------------------------------------------------------------------------
      //                                PUBLIC
      // ---------------------------------------------------------------------------
    public:

      /// Standard constructor
      SingleEventSnapshot( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~SingleEventSnapshot( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode execute   ();    ///< Algorithm execution

      // ---------------------------------------------------------------------------
      //                                PRIVATE
      // ---------------------------------------------------------------------------
    private:

      /// Load camera tool on demand
      ICameraTool * cameraTool() const
      {
        if ( !m_CamTool )
        {
          m_CamTool = tool<ICameraTool>("CameraTool");
        }
        return m_CamTool;
      }

    private:

      /// Raw Buffer Decoding tool
      const Rich::DAQ::IRawBufferToSmartIDsTool * m_SmartIDDecoder;

      /// SmartID tool
      const Rich::ISmartIDTool * m_idTool;

      /// CAMERA error reporting tool
      mutable ICameraTool * m_CamTool;

      unsigned long long m_nEvt; ///< Event number count

      /// Number of bins for event snapshots
      unsigned int m_nBins;

      /// Ring objects TES location to add to snapshots
      std::string m_ringLoc;

      /// Message to send with events
      std::string m_message;

      /// The TAE location(s) to monitor
      std::vector<std::string> m_taeEvents;

      /// Algorithm name and partition
      std::string m_Name;

      /// min. number of pixels associated to that ring
      unsigned int m_MinRingPixels;

      /// min. / max. number of hits in RICH1 or RICH2
      unsigned int m_MinHitRich;
      unsigned int m_MaxHitRich;

      /// Min number of rings
      unsigned int m_MinRings;

      /// send a snapshot each N seconds
      int m_UpdateInterval;

      /// maximum number of snapshots
      unsigned int m_maxSnapshots;

      /// Number of snapshots sent
      unsigned int m_nSnapShots;

      time_t        m_TimeStart;
      time_t        m_TimeLastUpdate;

      /// Last run number
      unsigned int m_lastRunNumber;

    };

  }
}

#endif // RICHONLINEMONITORS_RichSingleEventSnapshot_H
