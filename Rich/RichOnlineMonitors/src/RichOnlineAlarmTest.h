// $Id: RichOnlineAlarmTest.h,v 1.3 2008-02-28 16:50:45 jonrob Exp $
#ifndef RICHONLINEALARMTEST_H
#define RICHONLINEALARMTEST_H 1

// STL
#include <sstream>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/AlgFactory.h"

// boost
#include "boost/lexical_cast.hpp"

namespace Rich
{
  namespace Mon
  {

    /** @class OnlineAlarmTest RichOnlineAlarmTest.h
     *
     *  simple algorithm to test the ability to raise alarms in PVSS
     *  from the monitoring processes
     *
     *  The alarms are raised by monitoring a counter, the algorithm
     *  publishes
     *  - an integer counter to indicate the alarm:
     *    0  = normal
     *    >0 = error code
     *
     *  - a string with some further information
     *
     * This test algorithm raises an alarm every N events
     *
     *  @author Ulrich Kerzel
     *  @date   2007-04-05
     */
    class OnlineAlarmTest : public GaudiAlgorithm {

    public:

      /// Standard constructor
      OnlineAlarmTest( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~OnlineAlarmTest( ); ///< Destructor

      virtual StatusCode initialize();    ///< Algorithm initialization
      virtual StatusCode execute   ();    ///< Algorithm execution

    private:

      int           m_alarmRate;  ///< raise an alarm every N events
      int           m_sleepRate;  ///< pause execute() method
      unsigned long m_nEvts;      ///< Event count
      int           m_alarmCode;  ///< published to indicate alarm
      std::string   m_alarmInfo;  ///< additional info

    };

  }
}

#endif // RICHONLINEALARMTEST_H
