// $Id: CameraRunFolder.cpp,v 1.1 2010-01-14 11:17:00 nmangiaf Exp $
// Include files

// from Gaudi

// local
#include "CameraRunFolder.h"

// CAMERA
#include "Camera/ICameraTool.h"

using namespace Rich::Mon;

//-----------------------------------------------------------------------------
// Implementation file for class : CameraRunFolder
//
// 2010-01-09 : Nicola Mangiafave
//
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( CameraRunFolder )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CameraRunFolder::CameraRunFolder( const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : Rich::AlgBase  ( name, pSvcLocator )
  , m_CameTool(NULL)
  , m_newRun(true)
{ }

//=============================================================================
// Destructor
//=============================================================================
CameraRunFolder::~CameraRunFolder() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode CameraRunFolder::initialize() 
{
  StatusCode sc = Rich::AlgBase::initialize(); 
  if ( sc.isFailure() ) return sc;

  // Tool to send messages to camera
  m_CameTool = tool<ICameraTool>("CameraTool");

  // subscribe to incidents at new runs
  IIncidentSvc* incSvc = 0;
  sc = service("IncidentSvc", incSvc, true);
  if (sc.isFailure()) return sc;
  incSvc->addListener( this, IncidentType::RunChange );

  return sc;
}

//=============================================================================
// Main execution
//============================================================================= 
StatusCode CameraRunFolder::execute() 
{
 
  //
  //Change directory if the run changes 
  //
  if ( m_newRun ) 
  {
    // Retrieve the ODIN and check if it's present
    if (exist<LHCb::ODIN>(LHCb::ODINLocation::Default)) 
    {
      LHCb::ODIN * odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default); 
      
      const unsigned int runNumber = odin->runNumber();

      std::ostringstream msg;
      msg << "switch " << runNumber;

      m_CameTool->SendAndClear(ICameraTool::CAM_COMMAND,"COMMAND",msg.str());

      m_newRun = false;

      if (msgLevel(MSG::DEBUG))
      {
        debug()<<"Change directory message: "<<msg.str()<<endmsg;
        debug() <<"New run: Change Camera Directory in "<<runNumber << endmsg;
      }

    }
    else 
    {
      if (msgLevel(MSG::DEBUG))
        debug() << "Failed in changing Camera directory after run changed."
                <<" ODIN bank doesn't exists."
                <<" It will retry when the next message arrives."<<endmsg;
    }    
  }

  return StatusCode::SUCCESS;
} 

//=============================================================================

void CameraRunFolder::handle(const Incident &incident) 
{
  if (IncidentType::RunChange == incident.type() ) { m_newRun = true; }
}
