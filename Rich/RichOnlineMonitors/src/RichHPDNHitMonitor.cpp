
// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "RichHPDNHitMonitor.h"

// RICH
#include "RichKernel/RichDAQDefinitions.h"
#include "RichKernel/RichSmartIDCnv.h"

#include "AIDA/IAxis.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichHPDNHitMonitor
//
// 2007-04-04 : Ulrich Kerzel
//-----------------------------------------------------------------------------

using namespace Rich::Mon;

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( HPDNHitMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  HPDNHitMonitor::HPDNHitMonitor( const std::string& name,
                                  ISvcLocator* pSvcLocator )
    : Rich::HistoAlgBase        ( name , pSvcLocator ),
      m_RichSys                 ( nullptr            ),
      m_SmartIDDecoder          ( nullptr            ),
      m_HpdUKL1DisableTool      ( nullptr            ),
      m_HpdOccupancyToolFast    ( nullptr            ),
      m_HpdOccupancyToolSlow    ( nullptr            ),
      m_CamTool                 ( nullptr            ),
      m_nEvts                   (     0              ),
      m_taeEvents               (   1, ""            ),
      m_counterRich1            (     0              ),
      m_counterRich2            (     0              ),
      m_Name                    (     name           ),
      m_PullThreshold           (     1              ),
      m_TriggerTypes            (   1, 0             ),
      m_TimeStart               (     0              ),
      m_TimeLastUpdate          (     0              ),
      m_unusalOccCount          (     0              )
{
  declareProperty( "FillHistos"              , m_FillHistos          =  true );
  declareProperty( "ExtendedVerbose"         , m_ExtendedVerbose     = false );

  declareProperty( "HistoNHitMin"            , m_HistoNHitMin        =     0 );
  declareProperty( "HistoNHitMax"            , m_HistoNHitMax        =   100 );

  declareProperty( "RemoveFaultyHpdUKL1"     , m_RemoveFaultyHpdUKL1 = false );
  declareProperty( "HitThreshold"            , m_HitThreshold        =    50 );

  declareProperty( "RawEventLocations"       , m_taeEvents                   );

  declareProperty( "MovingAverageEvents"     , m_MovingAverageEvents =   500 );

  declareProperty( "PullThreshold"           , m_PullThreshold       =     5 );

  declareProperty( "UpdateTimerInterval"     , m_UpdateTimerInterval =   600 );

  declareProperty( "TriggerTypes"            , m_TriggerTypes                );

  declareProperty( "RICHHitRatioMax"         , m_RichHitRatioMax  = 15.0/1.0 ); // RICH2/RICH1
  declareProperty( "RICHHitRatioMin"         , m_RichHitRatioMin  = 1.0/20.0 ); // RICH2/RICH1
  declareProperty( "RICHHitRatioMinHits"     , m_RichMinHitRatio  = 1000  );

  declareProperty( "AddSnapshotCameraMsg"    , m_AddSnapshotCameraMsg = false );
  declareProperty( "UseCamera"               , m_useCamera            = true );

  declareProperty( "MinBadHPDClusSize"  ,  m_minBadHPDPixClusSize     = 27   );

  declareProperty( "CounterUpdateRate",        m_CounterUpdateRate = 30 );
  declareProperty( "CounterUpdateMinEntries",  m_CounterUpdateMinEntries = 1000 );
  declareProperty( "EnableDIMCounters",        m_EnableCounters = true );

}

//=============================================================================
// Destructor
//=============================================================================
HPDNHitMonitor::~HPDNHitMonitor() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode HPDNHitMonitor::initialize()
{
  const StatusCode sc = Rich::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  //
  // Partition
  //
  const char* partitionName = getenv("PARTITION");
  m_Name = name();
  if (partitionName) { m_Name += std::string(partitionName); }

  //
  // print information about steering options
  //
  if ( msgLevel(MSG::INFO) )
  {
    if (partitionName)
      info() << "running in partition " << std::string(partitionName) << endmsg;
    info() << "Fill histograms              " << m_FillHistos                       << endmsg;
    info() << "Threshold for too many hits  " << m_HitThreshold                     << endmsg;
    info() << "Send diable command to UKL1  " << m_RemoveFaultyHpdUKL1              << endmsg;
    info() << "Moving average over          " << m_MovingAverageEvents << " events" << endmsg;
    info() << "Using TAE events             " << m_taeEvents                        << endmsg;
    info() << "Send alerts every            " << m_UpdateTimerInterval << " s"      << endmsg;
    info() << "Add camera snapshot messages " << m_AddSnapshotCameraMsg << endmsg;
    info() << "Monitor event with Trigger types :";
    for ( std::vector<int>::const_iterator iTrig = m_TriggerTypes.begin();
          iTrig != m_TriggerTypes.end(); ++iTrig )
    {
      info() << " " << (LHCb::ODIN::TriggerType)*iTrig;
    }
    info() << endmsg;
    if (m_FillHistos)
    {
      info() << "Histogram for inclusive nHit counter between "
             << m_HistoNHitMin << " and " << m_HistoNHitMax << endmsg;
    }
  }

  std::string taeEventList;
  for ( std::vector<std::string>::const_iterator taeIter = m_taeEvents.begin();
        taeIter != m_taeEvents.end(); ++taeIter )
  {
    taeEventList.append(*taeIter);
  }
  if ( !taeEventList.empty() )
    info() << "Add to histogram name : " << taeEventList << endmsg;

  if ( m_FillHistos )
  {
    m_nHits    = book1D("NHitInclusive"     , "Number of hits in RICH "  + taeEventList,
                        m_HistoNHitMin - 0.5, m_HistoNHitMax - 0.5, 100);
    m_nHitsR1  = book1D("NHitInclusiveRich1", "Number of hits in Rich1 " + taeEventList,
                        m_HistoNHitMin - 0.5, m_HistoNHitMax - 0.5, 100);
    m_nHitsR2  = book1D("NHitInclusiveRich2", "Number of hits in Rich2 " + taeEventList,
                        m_HistoNHitMin - 0.5, m_HistoNHitMax - 0.5, 100);
    m_R1R2Corr = book2D("NHitR1R2Corr", "RICH2 V RICH1 Hit Occupancies " + taeEventList,
                        m_HistoNHitMin - 0.5, m_HistoNHitMax - 0.5, 50,
                        m_HistoNHitMin - 0.5, m_HistoNHitMax - 0.5, 50 );

    // Fill histograms with only physics and random triggers.
    const std::string name  = "NHitInclusivePhysAndRandom";
    const std::string title = "Number of physics and random hits in RICH";
    double min = m_HistoNHitMin - 0.5;
    double max = m_HistoNHitMax - 0.5;

    _nHitsPhysAndRandom      = book1D( name          , title + " "  + taeEventList, min, max, 100 );
    _nHitsPhysAndRandomRich1 = book1D( name + "Rich1", title + "1 " + taeEventList, min, max, 100 );
    _nHitsPhysAndRandomRich2 = book1D( name + "Rich2", title + "2 " + taeEventList, min, max, 100 );

    Book2DHisto ( Rich::Rich1, Rich::top   ,  taeEventList );
    Book2DHisto ( Rich::Rich1, Rich::bottom,  taeEventList );
    Book2DHisto ( Rich::Rich2, Rich::left  ,  taeEventList );
    Book2DHisto ( Rich::Rich2, Rich::right ,  taeEventList );
  }

  m_NHitMap.clear();

  //
  // publish counters
  //
  if ( m_FillHistos && m_EnableCounters )
  {
    declareInfo( "NHit_RICH1", m_counterRich1, "Average number of RICH1 hits" );
    declareInfo( "NHit_RICH2", m_counterRich2, "Average number of RICH2 hits" );
  }

  //
  // get Time of startup
  //
  m_TimeLastCounterUpdate = m_TimeStart = m_TimeLastUpdate = time(nullptr);

  //
  // let user know we're here
  //
  if ( m_useCamera )
  {
    if ( msgLevel(MSG::VERBOSE) )
      verbose() << "Send message to CAMERA" << endmsg;
    cameraTool()->Append("TEXT",m_Name.c_str());
    cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Initialized");
  }

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HPDNHitMonitor::execute()
{
  StatusCode sc = StatusCode::SUCCESS;

  // by default no events are 'selected'
  // only bad events should be selected (sent to the event snapshot alg)
  setFilterPassed(false);

  // cached messages to add to camera, for inclusion in a possible event snapshot
  std::vector<std::string> snapshotMessages;

  if ( 1 == ++m_nEvts )
  {
    const std::string m = "First event seen";
    info() << m << endmsg;
    if ( m_useCamera )
    {
      cameraTool()->Append("TEXT",m_Name.c_str());
      cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,m);
    }
  }

  if ( msgLevel(MSG::DEBUG) )
    debug() << "==> Execute for event " << m_nEvts << endmsg;

  //
  // local variables
  //
  unsigned int nAllHits      = 0;  // number of hits seen in total so far in this event
  unsigned int nAllHitsRich1 = 0;
  unsigned int nAllHitsRich2 = 0;

  unsigned int nAllHitsPhysAndRandom      = 0; // Same as above, but just for trigger
  unsigned int nAllHitsPhysAndRandomRich1 = 0; //    types 0 and 3, i.e. physics (0)
  unsigned int nAllHitsPhysAndRandomRich2 = 0; //    and random (3) triggers.

  unsigned int nHits         = 0;  //                for this HPD

  std::vector<bool> hasHitsFlag(deRichSys()->allPDRichSmartIDs().size(), false);

  m_NHitMap.clear();

  //
  // check if we should send messages to Camera
  //
  const time_t currentTime = time(nullptr);
  const time_t deltaT = currentTime - m_TimeLastUpdate;
  const bool sendReports = ( (m_UpdateTimerInterval >0) && (deltaT >= m_UpdateTimerInterval) );

  //
  // check ODIN information
  //
  LHCb::ODIN * odin = nullptr;
  if (exist<LHCb::ODIN>( LHCb::ODINLocation::Default ))
  {
    odin = get<LHCb::ODIN>( LHCb::ODINLocation::Default );

    if (msgLevel(MSG::VERBOSE) )
    {
      verbose() << " ODIN event time " << odin->eventTime()
                << " GPS time "        << odin->gpsTime()
                << " runNr "           << odin->runNumber()
                << " orbit Nr "        << odin->orbitNumber()
                << " event type "      << odin->eventType()
                << " event number "    << odin->eventNumber()
                << " bunch ID "        << odin->bunchId()
                << " detector status " << odin->detectorStatus()
                << endmsg;
      const Gaudi::Time eventTime = odin->eventTime();
      verbose() << "EventTime "
                << eventTime.year(true)    << "-"
                << eventTime.month(true)+1 << "-"
                << eventTime.day(true)     << " "
                << eventTime.hour(true)    << "h"
                << eventTime.minute(true)  << "m"
                << eventTime.second(true)  << "s"
                << eventTime.nsecond()     << endmsg;
    }

    //
    // check trigger type
    //
    bool monitorEvent = false;
    for ( std::vector<int>::const_iterator iTrig = m_TriggerTypes.begin();
          iTrig != m_TriggerTypes.end(); ++iTrig )
    {
      if ( odin->triggerType() == (*iTrig) )
      {
        if (msgLevel(MSG::VERBOSE) )
          verbose() << "Monitor event with Trigger type " << odin->triggerType() << endmsg;
        monitorEvent = true;
        break;
      }
      else
      {
        if (msgLevel(MSG::VERBOSE) )
          verbose() << "Do NOT monitor event with Trigger type " << odin->triggerType() << endmsg;
      }
    }
    if (!monitorEvent)
      return StatusCode::SUCCESS;

  }
  else
  {
    info() << "No ODIN information -> don't continue" << endmsg;
    return StatusCode::SUCCESS;
  }
  // to be sure
  if ( !odin ) return StatusCode::SUCCESS;

  //
  // main loop - get all RICH SmartIDs corresponding to HPDs/hits
  //
  const Rich::DAQ::L1Map &l1Map = smartIDDecoder()->allRichSmartIDs(m_taeEvents);
  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "L1 map has size " << l1Map.size() << endmsg;

  for ( Rich::DAQ::L1Map::const_iterator iL1Map = l1Map.begin();
        iL1Map != l1Map.end(); ++iL1Map )
  {

    const Rich::DAQ::Level1HardwareID   l1HardID    = iL1Map->first;
    const Rich::DAQ::IngressMap        &ingressMap  = iL1Map->second;
    if  ( msgLevel(MSG::VERBOSE) )
      verbose() << "IngressMap has size " << ingressMap.size() << endmsg;

    for ( Rich::DAQ::IngressMap::const_iterator iIngressMap = ingressMap.begin();
          iIngressMap != ingressMap.end(); ++iIngressMap )
    {

      const Rich::DAQ::L1IngressID & l1IngressID = iIngressMap->first;
      const Rich::DAQ::IngressInfo & ingressInfo = iIngressMap->second;

      const Rich::DAQ::HPDMap & hpdMap = ingressInfo.hpdData();
      if  ( msgLevel(MSG::VERBOSE) )
        verbose() << "HPD map has size " << hpdMap.size() << endmsg;

      for ( Rich::DAQ::HPDMap::const_iterator iHPDMap = hpdMap.begin();
            iHPDMap != hpdMap.end(); ++iHPDMap )
      {

        const Rich::DAQ::Level1Input           &l1Input        = iHPDMap->first; // SmartID of HPD with hits
        const Rich::DAQ::L1InputWithinIngress  &l1InputIngress = l1Input.l1InputWithinIngress();
        const Rich::DAQ::HPDInfo               &hpdInfo        = iHPDMap->second;
        const LHCb::RichSmartID                &smartIDHPD     = hpdInfo.hpdID();

        // check for valid HPD
        if (!smartIDHPD.isValid() )
        {
          if ( msgLevel(MSG::VERBOSE) )
            verbose() << "HPD SmartID is not valid, continue." << endmsg;
          continue;
        } //SmartID valid

        if (hpdInfo.header().inhibit())
        {
          if ( msgLevel(MSG::VERBOSE) )
            verbose() << "HPD is inhibited, continue " << endmsg;
          continue;
        } //if inhibit

        const Rich::DAQ::HPDHardwareID   hardID = deRichSys()->hardwareID(smartIDHPD); // corresponding hardware ID
        const LHCb::RichSmartID::Vector & hitSmartIDs = hpdInfo.smartIDs();            // vector of SmartIDs with hits
        const Rich::DetectorType   richDetector = smartIDHPD.rich();

        nAllHits          += hitSmartIDs.size();
        nHits              = hitSmartIDs.size();
        m_NHitMap[hardID] += nHits;

        if ( msgLevel(MSG::VERBOSE) )
        {
          verbose() << "Now monitor HPD with SmartID " << smartIDHPD
                    << " hardware ID " << hardID
                    << " L0 ID " << deRichSys()->level0ID(smartIDHPD)
                    << " #hits " << nHits
                    << " #hits so far " << m_NHitMap[hardID]
                    << endmsg;
          for ( LHCb::RichSmartID::Vector::const_iterator iHit = hitSmartIDs.begin();
                iHit != hitSmartIDs.end(); ++iHit )
          {
            verbose() << "Next hit at" << (*iHit) << endmsg;
          }
        }

        const bool triggerSelected = ( odin->triggerType() == LHCb::ODIN::PhysicsTrigger     ||
                                       odin->triggerType() == LHCb::ODIN::TechnicalTrigger   ||
                                       odin->triggerType() == LHCb::ODIN::CalibrationTrigger );

        if ( richDetector == Rich::Rich1 )
        {
          nAllHitsRich1 += hitSmartIDs.size();
          if ( triggerSelected )
            nAllHitsPhysAndRandomRich1 += hitSmartIDs.size();
        } //if Rich1

        if ( richDetector == Rich::Rich2 )
        {
          nAllHitsRich2 += hitSmartIDs.size();
          if ( triggerSelected )
            nAllHitsPhysAndRandomRich2 += hitSmartIDs.size();
        } //if Rich2

        if ( triggerSelected )
        {
          nAllHitsPhysAndRandom += hitSmartIDs.size();
        }

        if  ( m_NHitMap[hardID] >= m_HitThreshold &&
              m_RemoveFaultyHpdUKL1 )
        {
          std::ostringstream errStream;
          errStream << "Request HPD to be disabled, Too many hits " << m_NHitMap[hardID]
                    << " logical L1 ID  "   << deRichSys()->level1LogicalID(l1HardID)
                    << " L1 Input Ingress " << l1InputIngress
                    << " L1 Ingress ID "    << l1IngressID
                    << " RICH "             << richDetector
                    << " SmartID "          << smartIDHPD
                    << " HardID "           << hardID;
          warning() << errStream.str() << endmsg;
          const std::string shortMsg = "Request HPD disable: too many hits";
          m_CameraMessageWarning.insert(std::make_pair(shortMsg,errStream.str()));
          hpdDisableTool() -> DisableHPD(smartIDHPD,"Too many Hits");
          setFilterPassed(true);
          snapshotMessages.push_back(shortMsg);
          snapshotMessages.push_back(errStream.str());
        }

        //
        // get expected occupancy for that HPD and compare to actual #hits
        //
        const double expectedOccupancy = slowHPDOcc()->averageOccupancy(smartIDHPD);
        const double actualHit         = fastHPDOcc()->averageOccupancy(smartIDHPD);
        const double pull = ( expectedOccupancy > 0 ?
                              (actualHit - expectedOccupancy)/expectedOccupancy : 0.0 );

        // if pull deviates too much send a warning
        // but wait for the first events to allow the moving average to update
        if ( m_nEvts > m_MovingAverageEvents )
        {
          if ( pull > m_PullThreshold )
          {
            std::ostringstream message;
            message << smartIDHPD << " saw " << actualHit << " hits. Deviates from expected "
                    << expectedOccupancy << " pull " << pull;
            if ( msgLevel(MSG::VERBOSE) ) verbose() << message.str() << endmsg;
            const std::string shortMsg = "Unusual hit occupancy";
            m_CameraMessageWarning.insert(std::make_pair(shortMsg,message.str()));
            setFilterPassed(true);
            snapshotMessages.push_back(shortMsg);
            snapshotMessages.push_back(message.str());
            ++m_unusalOccCount;
            ++m_unusualHPDOccCount[smartIDHPD];
          }

          if ( msgLevel(MSG::VERBOSE) )
            verbose() << "HPD " << smartIDHPD << " expected occupancy " << expectedOccupancy
                      << " actual " << actualHit << " pull " <<  pull << endmsg;
        }

        //
        // Checks for stuck pixel rows or columns
        //
        if ( hitSmartIDs.size() >= m_minBadHPDPixClusSize )
        {

          // Loop over all pixels in this HPD and count number in each row/column
          std::map<unsigned int,unsigned int> rowPixCount;
          for ( LHCb::RichSmartID::Vector::const_iterator iPixel = hitSmartIDs.begin();
                iPixel != hitSmartIDs.end(); ++iPixel )
          {
            if ( (*iPixel).isValid() )
            {
              ++rowPixCount[         (*iPixel).pixelRow() ];
              ++rowPixCount[ 10000 + (*iPixel).pixelCol() ];
            }
          }

          // see if one row or column is over the threshold
          bool foundBadCluster = false;
          unsigned int nPixsInStuckClus = 0;
          for ( std::map<unsigned int,unsigned int>::const_iterator iM = rowPixCount.begin();
                iM != rowPixCount.end(); ++iM )
          {
            if ( (*iM).second >= m_minBadHPDPixClusSize )
            {
              foundBadCluster  = true;
              nPixsInStuckClus = (*iM).second;
              break;
            }
          }

          // If found a bad cluster, report it
          if ( foundBadCluster )
          {
            std::ostringstream message;
            message << smartIDHPD << " # pixels = " << nPixsInStuckClus;
            const std::string shortMsg = "Possible stuck HPD pixel column/row";
            m_CameraMessageWarning.insert(std::make_pair(shortMsg,message.str()));
            if ( msgLevel(MSG::VERBOSE) ) verbose() << message.str() << endmsg;
            hpdDisableTool() -> DisableHPD(smartIDHPD,shortMsg);
            setFilterPassed(true);
            snapshotMessages.push_back(shortMsg);
            snapshotMessages.push_back(message.str());
          }

        }

        //
        // Fill number of hits into histograms
        //
        if (m_FillHistos)
        {
          if ( smartIDHPD.isValid()                        &&
               smartIDHPD.rich()  != Rich::InvalidDetector &&
               smartIDHPD.panel() != Rich::InvalidSide     )
          {
            // Retrieve the rich panel (top|bottom for rich1, left|right for rich2).
            const Rich::Side panel = smartIDHPD.panel();

            const Rich::DAQ::HPDCopyNumber copyNum = deRichSys()->copyNumber( smartIDHPD );

            const Rich::SmartIDGlobalOrdering globOrdering(smartIDHPD);
            const int x = globOrdering.globalPdX();
            const int y = globOrdering.globalPdY();

            const int nX = globOrdering.totalPDX();
            //const int nY = globOrdering.totalPDY();

            m_2dNhit[smartIDHPD.rich()][smartIDHPD.panel()]->fill( x, y, actualHit );

            //const std::string histoNamePull = histoNameBase +  "_Pull";
            AIDA::IHistogram2D *histo = m_2dPull[smartIDHPD.rich()][smartIDHPD.panel()];
            const double binContent = histo->binHeight(x, y);
            const double newWT      = (binContent + pull)/2.0;
            TH2D* rootHisto = Gaudi::Utils::Aida2ROOT::aida2root(histo);
            rootHisto->SetBinContent(x, y, newWT);
            rootHisto->SetMinimum(-fabs(m_PullThreshold));
            rootHisto->SetMaximum(+fabs(m_PullThreshold));

            // New histograms of copy number, only for physics and random triggers.
            if ( triggerSelected )
            {
              if (copyNum.data() < hasHitsFlag.size()) hasHitsFlag[copyNum.data()] = true;
              _copyNumHisto->fill( copyNum.data(), actualHit );
              if ( ( richDetector == Rich::Rich1 ) && ( panel == Rich::bottom ) )
                _copyNumHistoRich1B->fill( 0.1+copyNum.data(), actualHit );
              if ( ( richDetector == Rich::Rich1 ) && ( panel == Rich::top    ) )
                _copyNumHistoRich1T->fill( 0.1+copyNum.data(), actualHit );
              if ( ( richDetector == Rich::Rich2 ) && ( panel == Rich::right  ) )
                _copyNumHistoRich2R->fill( 0.1+copyNum.data(), actualHit );
              if ( ( richDetector == Rich::Rich2 ) && ( panel == Rich::left   ) )
                _copyNumHistoRich2L->fill( 0.1+copyNum.data(), actualHit );
            }

            const int bin = 1 + x*nX + y;
            //plot1D( bin, histoName1D, histoName1D, 0, 0, 0, actualHit );
            m_2dHPDHits[smartIDHPD.rich()][smartIDHPD.panel()]->fill( bin, actualHit );

          } // if valid
        } // fillHisto

      }//for iHPDMap

    } //for iIngressMap

  }//for iL1Map

  // Look for events where the RICH1/RICH2 hit ratio deviates far from 1
  if ( nAllHitsRich2>0 && nAllHitsRich1>0 )
  {
    const double hitRatio = ( (double)nAllHitsRich2/(double)nAllHitsRich1 );
    if ( ( nAllHitsRich1 > m_RichMinHitRatio || nAllHitsRich2 > m_RichMinHitRatio ) &&
         ( hitRatio > m_RichHitRatioMax || hitRatio < m_RichHitRatioMin           ) )
    {
      std::ostringstream message;
      message <<  "# RICH1 hits = " << nAllHitsRich1
              << " # RICH2 hits = " << nAllHitsRich2
              << " RICH2/RICH1 ratio = " << hitRatio;
      if ( msgLevel(MSG::VERBOSE) )
        verbose() << message.str() << endmsg;
      std::ostringstream shortMsg;
      shortMsg << "Unusual RICH2/RICH1 hit ratio";
      m_CameraMessageWarning.insert(std::make_pair(shortMsg.str(),message.str()));
      setFilterPassed(true);
      snapshotMessages.push_back(shortMsg.str());
      snapshotMessages.push_back(message.str());
    }
  }

  if  ( msgLevel(MSG::VERBOSE) )
    verbose() << "number of hits in this event " << nAllHits << endmsg;
  //
  // update & publish histograms
  //
  if (m_FillHistos)
  {
    //fix the HPDs without any hits
    for (size_t copy = 0; copy < hasHitsFlag.size(); ++copy)
      if (!hasHitsFlag[copy])
      {
        _copyNumHisto->fill(copy, 0);
        if (copy < _copyNumHistoRich1T->axis().upperEdge())
          _copyNumHistoRich1T->fill(0.1+copy, 0);
        else if (copy < _copyNumHistoRich1B->axis().upperEdge())
          _copyNumHistoRich1B->fill(0.1+copy, 0);
        else if (copy < _copyNumHistoRich2L->axis().upperEdge())
          _copyNumHistoRich2L->fill(0.1+copy, 0);
        else
          _copyNumHistoRich2R->fill(0.1+copy, 0);

      }
    // update histogram showing #hit distribution
    //if ( nAllHits>0 )
    {
      m_nHits->fill(nAllHits);
    }
    //if ( nAllHitsRich1>0 )
    {
      m_nHitsR1->fill(nAllHitsRich1);
    }
    //if ( nAllHitsRich2>0 )
    {
      m_nHitsR2->fill(nAllHitsRich2);
    }
    //if ( nAllHitsRich1>0 || nAllHitsRich2>0 )
    {
      m_R1R2Corr->fill( nAllHitsRich1, nAllHitsRich2 );
    }

    // Update histograms with number of hits for physics and random triggers.
    if ( nAllHitsPhysAndRandom )
      _nHitsPhysAndRandom     ->fill( nAllHitsPhysAndRandom      );
    if ( nAllHitsPhysAndRandomRich1 )
      _nHitsPhysAndRandomRich1->fill( nAllHitsPhysAndRandomRich1 );
    if ( nAllHitsPhysAndRandomRich2 )
      _nHitsPhysAndRandomRich2->fill( nAllHitsPhysAndRandomRich2 );
  }

  // Update Occupancies sent via DIM
  if ( m_FillHistos && 
       (currentTime-m_TimeLastCounterUpdate) >= m_CounterUpdateRate )
  {
    m_TimeLastCounterUpdate = currentTime;
    if ( m_nHitsR1->allEntries() >= m_CounterUpdateMinEntries )
    {
      m_counterRich1 = m_nHitsR1->mean();
      debug() << "Mean # Rich1 Hits " << m_counterRich1 << endmsg;
    }
    if ( m_nHitsR2->allEntries() >= m_CounterUpdateMinEntries )
    {
      m_counterRich2 = m_nHitsR2->mean();
      debug() << "Mean # Rich2 Hits " << m_counterRich2 << endmsg;
    }
  }

  if ( sendReports )
  {
    // Error reports
    if ( msgLevel(MSG::VERBOSE) ) { verbose() << "Send error report" << endmsg; }
    sc = sc && SendErrorReport(ICameraTool::INFO   ,m_CameraMessageInfo   ,odin);
    sc = sc && SendErrorReport(ICameraTool::WARNING,m_CameraMessageWarning,odin);
    sc = sc && SendErrorReport(ICameraTool::ERROR  ,m_CameraMessageError  ,odin);
    unusualEventSummary();
    m_TimeLastUpdate = currentTime;
  }

  // Send cached messages to camera for inclusion in snapshots
  if ( m_useCamera && m_AddSnapshotCameraMsg && !snapshotMessages.empty() )
  {
    cameraTool()->Append("TEXT",odinInfo(odin).c_str());
    for ( std::vector<std::string>::const_iterator iM = snapshotMessages.begin();
          iM != snapshotMessages.end(); ++iM )
    {
      cameraTool()->Append("TEXT",(*iM).c_str());
    }
  }

  return sc;
}

void HPDNHitMonitor::unusualEventSummary()
{
  if ( m_useCamera && m_unusalOccCount > 0 )
  {
    const time_t currentTime  = time(nullptr);
    const time_t deltaT  = currentTime - m_TimeLastUpdate;
    struct tm * timeinfo = localtime ( &deltaT );
    std::ostringstream messageString,timeT;
    messageString << "Seen " << m_unusalOccCount << " 'unusual' events in past ";
    if ( timeinfo->tm_hour-1 > 0 ) { timeT << timeinfo->tm_hour-1 << " hours "; }
    if ( timeinfo->tm_min    > 0 ) { timeT << timeinfo->tm_min << " mins "; }
    if ( timeinfo->tm_sec    > 0 ) { timeT << timeinfo->tm_sec << " secs "; }
    messageString << timeT.str()
                  << "( " << (double)(m_unusalOccCount) / (double)(deltaT) << " Evt/s )";
    info() << messageString.str() << endmsg;
    cameraTool()->Append("TEXT",("In past "+timeT.str()).c_str());
    for ( Rich::Map<LHCb::RichSmartID,unsigned long>::const_iterator iHPD = 
            m_unusualHPDOccCount.begin();
          iHPD != m_unusualHPDOccCount.end(); ++iHPD )
    {
      std::ostringstream hpdS;
      hpdS << iHPD->first << " saw " << iHPD->second << " 'unusual' events ";
      hpdS << "( " << (double)(iHPD->second) / (double)(deltaT) << " Evt/s )";
      cameraTool()->Append("TEXT",hpdS.str().c_str());
    }
    cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,messageString.str());
  }
  m_unusalOccCount = 0;
  m_unusualHPDOccCount.clear();
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode HPDNHitMonitor::finalize()
{
  unusualEventSummary();
  if ( m_useCamera )
  {
    cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Finalized");
  }
  return Rich::HistoAlgBase::finalize();
}

//=============================================================================
//=============================================================================
std::string
HPDNHitMonitor::GetRichPanelHistId(const Rich::DetectorType rich,
                                   const Rich::Side panel)
{
  // create the name
  std::ostringstream id;
  id << Rich::text(rich)
     << Rich::text(rich, panel);
  return id.str();
}// getRichPanelHistId()

//=============================================================================

StatusCode
HPDNHitMonitor::SendErrorReport( const ICameraTool::MessageLevel    cameraLevel,
                                 std::multimap<std::string,std::string>& errMap,
                                 const LHCb::ODIN *                      odin )
{

  if ( !errMap.empty() )
  {

    if ( m_useCamera && msgLevel(MSG::VERBOSE) )
      verbose() << "send " << errMap.size()
                << " alerts to CAMERA with level " << (int)cameraLevel << endmsg;

    while ( !errMap.empty() )
    {
      // get the error message (key) of the first entry
      const std::string& errKey = (*errMap.begin()).first;

      // messages to send to camera
      std::map<std::string,unsigned long long> messages;

      // get all elements with that key
      for ( std::multimap<std::string,std::string>::const_iterator iErr = errMap.equal_range(errKey).first;
            iErr != errMap.equal_range(errKey).second; ++iErr )
      {
        const std::string& errMsg = (*iErr).second;
        if ( msgLevel(MSG::VERBOSE) )
          verbose() << "Retrieved message " << errKey << " " << errMsg << endmsg;
        ++messages[errMsg];
      }
      if ( m_useCamera )
      {
        cameraTool()->Append("TEXT",errKey.c_str());
        for ( std::map<std::string,unsigned long long>::const_iterator iM = messages.begin();
              iM != messages.end(); ++iM )
        {
          std::ostringstream mess;
          mess << iM->first << " | " << iM->second << " time(s)";
          cameraTool()->Append("TEXT",mess.str().c_str());
        }
        cameraTool()->Append("TEXT",odinInfo(odin).c_str());
        cameraTool()->SendAndClearTS(cameraLevel,m_Name,errKey);
      }

      // remove this message from the map
      errMap.erase(errKey);

    } // while not empty

    if ( !errMap.empty() )
    {
      info() << "Error map not cleared properly - size of map " << errMap.size() << endmsg;
    } //

  } // have something to send

  return StatusCode::SUCCESS;
}

//=============================================================================

StatusCode HPDNHitMonitor::Book2DHisto ( const Rich::DetectorType rich,
                                         const Rich::Side panel,
                                         const std::string& taeEventList )
{

  const std::string idString = GetRichPanelHistId(rich,panel);

  const LHCb::RichSmartID smartID(rich, panel);
  Rich::SmartIDGlobalOrdering gOrdering(smartID, Rich::SmartIDGlobalOrdering::LHCbMode);

  //
  // determine histogram borders
  //
  double hxmin(-0.5);
  double hxmax(Rich::Rich2==rich ? 8.5:13.5);
  const int hxnbins(static_cast<int>(hxmax-hxmin));
  //   double hxmin(gOrdering.minHPDX()-0.5);
  //   double hxmax(gOrdering.maxHPDX()-0.5);
  //   int hxnbins(gOrdering.totalPDX());
  if ( (Rich::Rich2==rich) && (Rich::left==panel) )
  {
    hxmin += (hxmax + 0.5);
    hxmax += (hxmax + 0.5);
  }

  double hymin(-0.5);
  double hymax(Rich::Rich2==rich ? 15.5:6.5);
  const int hynbins(static_cast<int>(hymax-hymin));
  //   double hymin(gOrdering.minHPDY()-0.5);
  //   double hymax(gOrdering.maxHPDY()-0.5);
  //   int hynbins(gOrdering.totalPDY());
  if ( (Rich::Rich1==rich) && (Rich::top==panel) )
  {
    hymin += (hymax + 0.5);
    hymax += (hymax + 0.5);
  }

  const int nX = gOrdering.totalPDX();
  const int nY = gOrdering.totalPDY();

  //
  // book histograms
  //
  m_2dPull[rich][panel] = book2D( idString + "_Pull"    , idString + "_Pull"    + taeEventList,
                                  hxmin, hxmax, hxnbins, hymin, hymax, hynbins );
  m_2dNhit[rich][panel] = book2D( idString + "_NHit"    , idString + "_NHit"    + taeEventList,
                                  hxmin, hxmax, hxnbins, hymin, hymax, hynbins );
  m_2dHPDHits[rich][panel] = book1D( idString + "_HPDHits" , idString + "_HPDHits" + taeEventList,
                                     0, nX*nY, nX*nY );

  _copyNumHisto       = bookProfile1D( "CopyNumberRich"  , "Rich copy number", 0, 484, 484 );

  _copyNumHistoRich1T = bookProfile1D( "CopyNumberRich1T", "HPD Occupancies | RICH1 Upper",   0,  98,  98 ); // top
  _copyNumHistoRich1B = bookProfile1D( "CopyNumberRich1B", "HPD Occupancies | RICH1 Lower",  98, 196,  98 ); // bottom
  _copyNumHistoRich2L = bookProfile1D( "CopyNumberRich2L", "HPD Occupancies | RICH2 A-Side", 196, 340, 144 ); // left
  _copyNumHistoRich2R = bookProfile1D( "CopyNumberRich2R", "HPD Occupancies | RICH2 C-Side", 340, 484, 144 ); // right

  return StatusCode::SUCCESS;
}

//=============================================================================

