// $Id: RichCalibMonitor.cpp,v 1.5 2009-08-26 12:25:58 rogers Exp $
// Include files

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "RichCalibMonitor.h"

// RICH
#include "RichKernel/RichDAQDefinitions.h"

// CAMERA
#include "Camera/ICameraTool.h"

// histograms
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichCalibMonitor
//
// 2007-04-04 : Ulrich Kerzel
//-----------------------------------------------------------------------------

using namespace Rich::Mon;

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( CalibMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  CalibMonitor::CalibMonitor( const std::string& name,
                              ISvcLocator* pSvcLocator)
    : Rich::HistoAlgBase      ( name , pSvcLocator ),
      m_RichSys               (     NULL           ),
      m_SmartIDDecoder        (     NULL           ),
      m_HpdUKL1DisableTool    (     NULL           ),
      m_CamTool               (     NULL           ),
      m_nEvts                 (     0              ),
      m_nEvtsLastUpdate       (     0              )
{
  declareProperty( "FillHistos"          , m_FillHistos          =  true );
  declareProperty( "ExtendedVerbose"     , m_ExtendedVerbose     = false );
  declareProperty( "RemoveFaultyHpdUKL1" , m_RemoveFaultyHpdUKL1 = false );
  declareProperty( "UpdateTimerInterval" , m_UpdateTimerInterval =   600 );
  declareProperty( "CounterUpdateRate",        m_CounterUpdateRate       = 30  );
  declareProperty( "CounterUpdateMinEntries",  m_CounterUpdateMinEntries = 100 );
}

//=============================================================================
// Destructor
//=============================================================================
CalibMonitor::~CalibMonitor() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode CalibMonitor::initialize()
{
  const StatusCode sc = Rich::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  debug() << "==> Initialize" << endmsg;

  //
  // print information about steering options
  //
  info() << "Fill histograms             " << m_FillHistos                << endmsg;
  info() << "Send diable command to UKL1 " << m_RemoveFaultyHpdUKL1       << endmsg;
  //
  // get tools, etc
  //

  // RichDet
  m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, 0, true );

  // CAMERA error reporting tool
  m_CamTool = tool<ICameraTool>("CameraTool");

  //
  // pre-book histograms
  //
  if ( m_FillHistos )
  {
    book1D("TriggerType", "Trigger Types",  -0.5, 7.5, 8);
    book1D("CalibrationType", "Calibration Types",  -0.5, 3.5, 4);
    const std::vector<std::string> caliTypes = boost::assign::list_of("A")("B")("C")("D");
    for ( std::vector<std::string>::const_iterator iC = caliTypes.begin();
          iC != caliTypes.end(); ++iC )
    {
      // book histos
      book1D( "Rich1Occ-Cali"+*iC, "RICH1 Occupancy : Calibration Type "+*iC, -0.5, 2000.-0.5, 100 );
      book1D( "Rich2Occ-Cali"+*iC, "RICH2 Occupancy : Calibration Type "+*iC, -0.5, 2000.-0.5, 100 );
      declareInfo( "NHit_Cali-"+*iC+"_RICH1", m_dimMap[*iC].valueR1, 
                   "Number of hits in RICH1 for Trigger "+*iC );
      declareInfo( "NHit_Cali-"+*iC+"_RICH2", m_dimMap[*iC].valueR2, 
                   "Number of hits in RICH2 for Trigger "+*iC );
    }
  }

  //
  // get Time of startup
  //
  m_TimeLastCounterUpdate = m_TimeStart = m_TimeLastUpdate = time(NULL);

  //
  // misc
  //
  m_TriggerTypeMap.clear();
  m_CalibrationTypeMap.clear();

  //
  // let user know we're here
  //
  verbose() << "send message to CAMERA" << endmsg;
  m_CamTool->Append("TEXT",name().c_str());
  m_CamTool->SendAndClearTS(ICameraTool::INFO,name(),"Initialized");


  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CalibMonitor::execute()
{

  if ( 0 == m_nEvts )
  {
    const std::string m = "First event seen";
    info() << m << endmsg;
    m_CamTool->Append("TEXT",name().c_str());
    m_CamTool->SendAndClearTS(ICameraTool::INFO,name(),m);
    m_TimeStart = m_TimeLastUpdate = time(NULL);
  }

  ++m_nEvts;

  if ( msgLevel(MSG::DEBUG) )
    debug() << "==> Execute for event " << m_nEvts << endmsg;

  //
  // local variables
  //

  const time_t currentTime = time(NULL);
  const time_t deltaT = (currentTime - m_TimeLastUpdate);
  if ( m_UpdateTimerInterval > 0 && deltaT >= m_UpdateTimerInterval )
  {

    struct tm * timeinfo = localtime ( &deltaT );
    const unsigned int nNewEvts = m_nEvts-m_nEvtsLastUpdate;
    std::ostringstream messageString;
    messageString << "Seen " << nNewEvts << " events in past ";
    if ( timeinfo->tm_hour-1 > 0 ) { messageString << timeinfo->tm_hour-1 << " hours "; }
    if ( timeinfo->tm_min    > 0 ) { messageString << timeinfo->tm_min << " mins "; }
    if ( timeinfo->tm_sec    > 0 ) { messageString << timeinfo->tm_sec << " secs "; }
    messageString << "( " << (double)(nNewEvts) / (double)(deltaT)
                  << " Evt/s )";

    // add number of each trigger type
    if ( !m_TriggerTypeMap.empty() )
    {
      std::ostringstream tString;
      tString << "Triggers -";
      for ( TriggerTypeMap::const_iterator iMap = m_TriggerTypeMap.begin();
            iMap != m_TriggerTypeMap.end(); ++iMap )
      {
        tString << " [ " << iMap->second << " " << iMap->first << " ]";
      }
      m_CamTool->Append("TEXT",tString.str().c_str());
      // clear for next set of messages
      m_TriggerTypeMap.clear();
    }

    // add number of each calibration type
    if ( !m_CalibrationTypeMap.empty() )
    {
      std::ostringstream tString;
      tString << "Calibration Types -";
      for ( CalibrationTypeMap::const_iterator iMap = m_CalibrationTypeMap.begin();
            iMap != m_CalibrationTypeMap.end(); ++iMap )
      {
        tString << " [ " << iMap->second << " " << iMap->first << " ]";
      }
      m_CamTool->Append("TEXT",tString.str().c_str());
      // clear for next set of messages
      m_CalibrationTypeMap.clear();
    }

    debug() << messageString.str() << endmsg;
    m_CamTool->SendAndClearTS(ICameraTool::INFO,name(), messageString.str());

    m_TimeLastUpdate  = currentTime;
    m_nEvtsLastUpdate = m_nEvts;
  } // if updateInterval

  //
  // check if ODIN information is available
  //
  LHCb::ODIN * odin = NULL;
  if ( exist<LHCb::ODIN>(LHCb::ODINLocation::Default) )
  {
    odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);

    if (msgLevel(MSG::VERBOSE) )
    {
      verbose() << " ODIN event time " << odin->eventTime()
                << " GPS time "        << odin->gpsTime()
                << " runNr "           << odin->runNumber()
                << " orbit Nr "        << odin->orbitNumber()
                << " event type "      << odin->eventType()
                << " triger type "     << odin->triggerType()
                << " event number "    << odin->eventNumber()
                << " bunch ID "        << odin->bunchId()
                << " detector status " << odin->detectorStatus()
                << endmsg;
    }
  }
  else
  {
    return Warning( "ODIN bank missing", StatusCode::SUCCESS );
  }

  const bool calibTrigger = ( odin->triggerType() == LHCb::ODIN::CalibrationTrigger );

  if ( m_FillHistos )
  {
    plot1D(odin->triggerType(), "TriggerType", "Trigger Types", 0,0,0 );
    if ( calibTrigger )
    {
      plot1D(odin->calibrationType(), "CalibrationType", "Calibration Types", 0,0,0 );
    }
  }

  // count which triggers have been sent
  ++m_TriggerTypeMap[odin->triggerType()];
  if ( calibTrigger ) ++m_CalibrationTypeMap[odin->calibrationType()];

  // Proceed only for calibration triggers
  if ( !calibTrigger ) return StatusCode::SUCCESS;

  // count hits
  Rich::Map<Rich::DetectorType,unsigned int> nHits;

  //
  // main loop - get all RICH SmartIDs corresponding to HPDs/hits
  //
  const Rich::DAQ::L1Map &l1Map = m_SmartIDDecoder->allRichSmartIDs();
  if  ( msgLevel(MSG::VERBOSE) )
    verbose() << "L1 map has size " << l1Map.size() << endmsg;

  for ( Rich::DAQ::L1Map::const_iterator  iL1Map = l1Map.begin();
        iL1Map != l1Map.end(); ++iL1Map )
  {

    //const Rich::DAQ::Level1HardwareID   l1HardID    = iL1Map->first;
    //const Rich::DAQ::Level1LogicalID    l1LogicalID = m_RichSys->level1LogicalID(l1HardID);
    const Rich::DAQ::IngressMap        &ingressMap  = iL1Map->second;
    if  ( msgLevel(MSG::VERBOSE) )
      verbose() << "IngressMap has size " << ingressMap.size() << endmsg;

    for ( Rich::DAQ::IngressMap::const_iterator iIngressMap = ingressMap.begin();
          iIngressMap != ingressMap.end(); ++iIngressMap )
    {

      //const Rich::DAQ::L1IngressID & l1IngressID = iIngressMap->first;
      const Rich::DAQ::IngressInfo & ingressInfo = iIngressMap->second;

      const Rich::DAQ::HPDMap &hpdMap = ingressInfo.hpdData();
      if  ( msgLevel(MSG::VERBOSE) )
        verbose() << "HPD map has size " << hpdMap.size() << endmsg;

      for ( Rich::DAQ::HPDMap::const_iterator iHPDMap = hpdMap.begin();
            iHPDMap != hpdMap.end(); ++iHPDMap )
      {

        //const Rich::DAQ::Level1Input           l1Input          = iHPDMap->first;
        //const Rich::DAQ::L1InputWithinIngress  l1InputIngress   = l1Input.l1InputWithinIngress();
        const Rich::DAQ::HPDInfo               &hpdInfo         = iHPDMap->second;
        const LHCb::RichSmartID &              smartIDHPD       = hpdInfo.hpdID();

        // check for valid HPD
        if (!smartIDHPD.isValid() )
        {
          verbose() << "HPD SmartID is not valid, continue." << endmsg;
          continue;
        } //SmartID valid
        if (hpdInfo.header().inhibit())
        {
          verbose() << "HPD is inhibited, continue " << endmsg;
          continue;
        } // inhibit

        //const Rich::DAQ::HPDHardwareID   hardID       = m_RichSys->hardwareID(smartIDHPD); // corresponding hardware ID
        //const Rich::DAQ::Level0ID        l0ID         = m_RichSys->level0ID(smartIDHPD);   //               L0
        const LHCb::RichSmartID::Vector &hitSmartIDs  = hpdInfo.smartIDs();                // vector of SmartIDs with hits
        //        const Rich::DetectorType         richDetector = smartIDHPD.rich();

        // Count hits
        nHits[smartIDHPD.rich()] += hitSmartIDs.size();

      }//for iHPDMap
    } //for iIngressMap
  }//for iL1Map

  if ( m_FillHistos )
  {
    std::ostringstream type;
    type << odin->calibrationType();

    AIDA::IHistogram1D * r1 =
      plot1D( nHits[Rich::Rich1],
              "Rich1Occ-Cali"+type.str(),
              "RICH1 Occupancy : Calibration Type "+type.str(), -0.5, 2000.5, 2001 );
    AIDA::IHistogram1D * r2 =
      plot1D( nHits[Rich::Rich2],
              "Rich2Occ-Cali"+type.str(),
              "RICH2 Occupancy : Calibration Type "+type.str(), -0.5, 2000.5, 2001 );

    // Update Occupancies sent via DIM
    if ( (currentTime-m_TimeLastCounterUpdate) >= m_CounterUpdateRate )
    {
      m_TimeLastCounterUpdate = currentTime;
      DimCount & dimC = m_dimMap[type.str()];
      if ( r1 && r1->allEntries() >= m_CounterUpdateMinEntries ) 
      { 
        dimC.valueR1 = r1->mean();
      }
      if ( r2 && r2->allEntries() >= m_CounterUpdateMinEntries )  
      {
        dimC.valueR2 = r2->mean(); 
      }
    }

  }

  return StatusCode::SUCCESS;
} // execute

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CalibMonitor::finalize()
{
  m_CamTool->SendAndClearTS(ICameraTool::INFO,name(),"Finalized");

  return Rich::HistoAlgBase::finalize();
}

//=============================================================================
