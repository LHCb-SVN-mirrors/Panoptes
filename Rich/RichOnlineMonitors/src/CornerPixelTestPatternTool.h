#ifndef CornerPixelTestPatternTool_H
#define CornerPixelTestPatternTool_H 1

// Base classes.
#include "ITestPatternTool.h"
#include "GaudiAlg/GaudiHistoTool.h"

namespace Rich {
  namespace Mon {

    /**
     * @class CornerPixelTestPattern
     *
     * Provides information about the corner pixel test pattern,
     * for use with the test pattern monitoring algorithm.
     *
     * @author Gareth Rogers
     * @date   2009-02-20
     */
    class CornerPixelTestPattern : public GaudiHistoTool, virtual public ITestPattern {

    public:
      
      CornerPixelTestPattern(const std::string &type, const std::string &name, const IInterface *parent);
      virtual ~CornerPixelTestPattern();

      virtual StatusCode initialize();
      virtual StatusCode finalize();

      /*!
       * Returns a vector of smart IDs which specify the test pattern hits for the given HPD.
       *
       * @param  hpdId RICH smart ID point which contains at least enough information to
       *           specify a specific HPD.
       * @param  invertRows If true then the row pixel number will be inverted in the produced
       *           pattern i.e. 0->31, 1->30, ..., 30->1 and 31->0. false no change is made.
       *           Defaults to false. No effect on the pattern.
       * @return LHCb::RichSmartID::Vector Each entry in the vector is a pixel in the test pattern.
       *
       * If the smart ID contains subpixel information then an ALICE mode pattern
       * is returned, if not then an LHCb mode pattern.
       */
      virtual LHCb::RichSmartID::Vector testPatternHits(const LHCb::RichSmartID &hpdId, bool=false) const;

      /*!
       * Return the dimensions of a single HPD test pattern.
       *
       * @param  Rich::DetectorType Not required for this pattern as it is symmetric and the
       *           orientation of the specific RICH detector does not matter.
       * @param  nBinsX Number of bins along the x-axis of the histogram.
       * @param  xMin Minimum bin value for the x-axis of the histogram.
       * @param  xMax Maximum bin value for the x-axis of the histogram.
       * @param  nBinsY Number of bins along the y-axis of the histogram.
       * @param  yMin Minimum bin value for the y-axis of the histogram.
       * @param  yMax Maximum bin value for the y-axis of the histogram.
       */
      virtual StatusCode histogramDimensions(Rich::DetectorType, int &nBinsX, double &xMin, double &xMax, int &nBinsY, double &yMin, double &yMax) const;

      /*!
       * Takes a pixel x/y coordinates from a HPD histogram and converts it to a
       * HPD test pattern coordinate.
       *
       * @param  Rich::DetectorType Not required for this pattern as it is symmetric and the
       *           orientation of the specific RICH detector does not matter.
       * @param  bool Not required for this pattern as it does not matter if we are in
       *           LHCb or ALICE mode.
       * @param  hpdX x-coordinate on the HPD to convert to a test pattern x-coordinate.
       * @param  hpdY y-coordinate on the HPD to convert to a test pattern y-coordinate.
       * @param  patternX The x-coordinate in the test pattern space.
       * @param  patternY The y-coordinate in the test pattern space.
       * @param  invertRows If true then the row pixel number will be inverted in the produced
       *           pattern i.e. 0->31, 1->30, ..., 30->1 and 31->0. false no change is made.
       *           Defaults to false. No effect on the pattern.
       *
       * Test pattern HPD coordinates are setup such that the test pattern takes up as
       * few bins as possible while maintaining the test pattern shape to ensure it is
       * recognisable.
       */
      virtual StatusCode plotCoordinates(Rich::DetectorType, bool, int hpdX, int hpdY, int &patternX, int &patternY, bool=false) const;

      /*!
       * Returns the total number of hits on the test pattern.
       *
       * @return int Total number of pixels in the test pattern.
       */
      virtual int totalTestPatternPixels() const;


    private:

      const int m_totalPatternPixels; //!< Total number of hits in the test pattern.
      const int m_colMin; //!< Pixel coordinate of the lowest column number in the pattern.
      const int m_colMax; //!< Pixel coordinate of the largest column number in the pattern.
      const int m_nBinsCol; //!< Number of pixels in a column of the test pattern.
      const int m_rowMin; //!< Pixel coordinate of the lowest row number in the pattern.
      const int m_rowMax; //!< Pixel coordinate of the largest row number in the pattern.
      const int m_nBinsRow; //!< Number of pixels in a row of the test pattern.
    };// class CornerPixelTestPattern
  }// namespace Mon
}// namespace Rich

#endif// CornerPixelTestPatternTool_H
