// Gaudi
#include "GaudiKernel/ToolFactory.h"

// Local
#include "RowPixelTestPatternTool.h"

// Std
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : RowPixelTestPattern
//
// 2009-02-12 : Gareth Rogers
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
namespace Rich {
  namespace Mon {
    DECLARE_TOOL_FACTORY(RowPixelTestPattern)
  }// namespace Mon
}// namespace Rich

//-----------------------------------------------------------------------------
Rich::Mon::RowPixelTestPattern::RowPixelTestPattern(const std::string &type
                                                          , const std::string &name
                                                          , const IInterface *parent)
  : GaudiHistoTool(type, name, parent)
  , m_totalPatternPixels(256)
  , m_colMin(0)
  , m_colMax(32)
  , m_nBinsCol(32)
  , m_rowMin(0)
  , m_rowMax(8)
  , m_nBinsRow(8)
{
  declareInterface<ITestPattern>(this);
}// Rich::Mon::RowPixelTestPattern::RowPixelTestPattern(const std::string &, const std::string &, const IInterface *)

//-----------------------------------------------------------------------------
Rich::Mon::RowPixelTestPattern::~RowPixelTestPattern() {
}// Rich::Mon::RowPixelTestPattern::~RowPixelTestPattern()


//-----------------------------------------------------------------------------
StatusCode Rich::Mon::RowPixelTestPattern::initialize() {
  // Done.
  return GaudiHistoTool::initialize();
}// Rich::Mon::RowPixelTestPattern::initialize()

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::RowPixelTestPattern::finalize() {
  // Done.
  return GaudiHistoTool::finalize();
}// Rich::Mon::RowPixelTestPattern::finalize()


//-----------------------------------------------------------------------------
LHCb::RichSmartID::Vector Rich::Mon::RowPixelTestPattern::testPatternHits(const LHCb::RichSmartID &hpdId, bool invertRows) const {
  // As this is done for a different smart ID, we can't cache the test pattern hits
  // and it must be done each time. Ensure that there is no pixel information in
  // the ID we are given. We want to set only those pixels in the test pattern.
  LHCb::RichSmartID::Vector testPattern(m_totalPatternPixels, hpdId.pdID());
  // Decide if we are in ALICE mode or not.
  // We are given a smart ID. This ID may be a pixel hit, in which case it may
  // or may not be in ALICE mode so have to check for ALICE mode pixel.
  // It may be a HPD in which case we don't know the mode and assume LHCb.
  bool alice(hpdId.pixelSubRowDataIsValid());
  // Start at -1 the illumintated rows are at 3,7,15,19,23,27,31 and these are all 4 apart.
  // There are two exceptions. Row 11 is missing, can just skip this row. Row 0 is the first row, which we handle when row=-1.
  // Actual row and actual sub row allow us to convert where necessary the row from the row loop counters.
  // Row inversion must be handled. The column number always matches the column loop counter as we
  // just run through all the pixels on a row.
  int actualRow(-1);
  int actualSubRow(7);
  // Index keeps track of which of the test pattern pixels we are setting in the vector.
  int index(0);
  for (int row(-1); row<32; row+=4 ) {
    for (int col(0); col<32; ++col) {
      // We are missing the 3rd row. Increment by 4 ourselves and we can carry on executing the loop.
      if (11 == row)
        row += 4;

      actualRow = row;
      actualSubRow = 7;
      if (-1 == row) {
        actualRow = 0;
        actualSubRow = 0;
      }// if(-1==row)
      if (invertRows) {
        actualRow    = 31 - actualRow;
        actualSubRow =  7 - actualSubRow;
      }// if(invertRows)
      
      testPattern[index].setPixelRow(actualRow);
      if (alice)
        testPattern[index].setPixelSubRow(actualSubRow);
      testPattern[index].setPixelCol(col);
      ++index;
    }// for col
  }// for row

  // Done.
  return testPattern;
}// Rich::Mon::RowPixelTestPattern::testPattern(const LHCb::RichSmartID &hpdID)

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::RowPixelTestPattern::histogramDimensions(Rich::DetectorType rich, int &nBinsX, double &xMin, double &xMax, int &nBinsY, double &yMin, double &yMax) const {
  // We will need to know the size of the HPDs to request an appropriate sized RICH.
  // As our HPD is asymmetric in pattern we need to check which RICH we are using.
  if (Rich::Rich1 == rich) {
    nBinsX = m_nBinsRow;
    xMin   = m_rowMin;
    xMax   = m_rowMax;
    nBinsY = m_nBinsCol;
    yMin   = m_colMin;
    yMax   = m_colMax;
  }// if RICH1
  else if (Rich::Rich2 == rich) {
    nBinsX = m_nBinsCol;
    xMin   = m_colMin;
    xMax   = m_colMax;
    nBinsY = m_nBinsRow;
    yMin   = m_rowMin;
    yMax   = m_rowMax;
  }// else if RICH2
  else {
    nBinsX = -1;
    xMin   = -1;
    xMax   = -1;
    nBinsY = -1;
    yMin   = -1;
    yMax   = -1;
  }// else InvalidDetector

  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::RowPixelTestPattern::histogramDimensions()

//-----------------------------------------------------------------------------
StatusCode Rich::Mon::RowPixelTestPattern::plotCoordinates(Rich::DetectorType rich, bool alice, int hpdX, int hpdY, int &patternX, int &patternY, bool invertRows) const {
  // If we are in ALICE mode there is a different separation between the rows.
  int factor(alice ? 32:8);
  // Whether the rows are on X or Y depends on the RICH.
  // We must adjust the row number depending on whether it is ALICE or not.
  if (Rich::Rich1 == rich) {
    // In the case of RICH1 the columns don't change and are on Y.
    patternY = hpdY;
    // Add 1 to the hpdY as the first row is at zero and the second 31. Adding 1 means we don't get zero twice.
    patternX = (hpdX+1) / factor;
    // We are missing a row and this must be handled, which one depends on which way we are counting.
    const int missingRow(invertRows ? 5:3);
    if (missingRow < patternX)
      patternX -= 1;
  }// if RICH1
  else if (Rich::Rich2 == rich) {
    // The are columns on X and the rows on Y.
    patternX = hpdX;//columns don't change.
    // Add 1 to the hpdY as the first row is at zero and the second 31. Adding 1 means we don't get zero twice.
    patternY = (hpdY+1) / factor;
    // We are missing a row and this must be handled, which one depends on which way we are counting.
    const int missingRow(invertRows ? 5:3);
    if (missingRow < patternY)
      patternY -= 1;
  }// elseif RICH2
  else {
    // We don't know which we are dealing with so test pattern must not have been set.
    patternX = -1;
    patternY = -1;
  }// don't know which RICH
  // Done.
  return StatusCode::SUCCESS;
}// Rich::Mon::RowPixelTestPattern::plotCoordinates()

//-----------------------------------------------------------------------------
int Rich::Mon::RowPixelTestPattern::totalTestPatternPixels() const {
  return m_totalPatternPixels;
}// Rich::Mon::RowPixelTestPattern::totalTestPatternPixels()

//=============================================================================
