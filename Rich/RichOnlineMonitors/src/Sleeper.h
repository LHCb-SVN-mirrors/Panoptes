#ifndef RICHMON_SLEEPER_H
#define RICHMON_SLEEPER_H


// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/AlgFactory.h"

namespace Rich
{
  namespace Mon
  {

    class Sleeper : public GaudiAlgorithm {

      static const int OSCISCALE= 10;
    public: // methods
      // Standard constructor
      Sleeper( const std::string& name, ISvcLocator* pSvcLocator );

      virtual ~Sleeper( ); ///< Destructor

      virtual StatusCode initialize();    // Algorithm initialization
      virtual StatusCode execute   ();    // Algorithm execution
      virtual StatusCode finalize  ();    // Algorithm finalization

    private:
      std::string m_myname;
      int m_val1;
      int m_val2;
    };
  }
}

#endif
