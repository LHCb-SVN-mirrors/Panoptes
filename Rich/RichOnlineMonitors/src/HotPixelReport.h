// $Id: HotPixelReport.h,v 1.9 2009-10-09 14:59:28 jonrob Exp $
#ifndef HOTPIXELREPORT_H
#define HOTPIXELREPORT_H 1

#include <iostream>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

#include "IHotPixelReport.h" 

#include "RichKernel/RichHashMap.h"

namespace Rich
{
  namespace Mon
  {

    /** @class HotPixelReport HotPixelReport.h HotPixelReport.h
     *
     *
     *  @author Ross Young
     *  @date   2009-09-15
     */

    class HotPixelReport : public GaudiTool,virtual public IHotPixelReport {
    public:
      /// Standard constructor
      HotPixelReport( const std::string& type,
                      const std::string& name,
                      const IInterface* parent);

      virtual ~HotPixelReport( ); ///< Destructor

      virtual int FillArray( const LHCb::RichSmartID &hitHPD, 
                             const LHCb::RichSmartID::Vector &pixelHits );
      virtual StatusCode PerformTest(int evts, std::list<ErrorReport> * reports);

    private:

      /// Hit count map
      typedef Rich::HashMap<LHCb::RichSmartID,unsigned int> HitCountMap;

    private:

      /// Working count of hits in each channel
      HitCountMap m_hitCount;

      long m_interval;  // Number of events sampled per Hot/Cold pixel analysis
      float m_cutoff;   // Minimum cut to raise error flag of pixel occupancy
      int m_colcutoff;  // Minimum cut to raise error flag of number of hot pixels in a column
      
    };

  }
}

#endif
