// $Id: IHPDCountTool.h,v 1.5 2009-05-12 08:18:38 rogers Exp $
#ifndef RICHONLINEMONITORS_IHPDCOUNTTOOL_H
#define RICHONLINEMONITORS_IHPDCOUNTTOOL_H 1

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IHPDCountTool ( "IHPDCountTool", 1, 0 );

namespace LHCb {
  class RichSmartID;
}//namespace LHCb

namespace Rich {
  namespace Mon {

    class ErrorReport;

    /** @class IHPDCountTool IHPDCountTool.h IHPDCountTool.h
     *
     *  Interface to HPDCountTool
     *
     *  @author Claus Buszello
     *  @date   2007-05-30
     */
    class IHPDCountTool : virtual public IAlgTool {

    public:

      // Return the interface ID
      static const InterfaceID& interfaceID() { return IID_IHPDCountTool; }

      virtual StatusCode performTest(const LHCb::RichSmartID &, int, ErrorReport &) = 0;

    };//class IHPDCountTool

  }//namespace Mon
}//namespace Rich

#endif // RICHONLINEMONITORS_IHPDCOUNTTOOL_H
