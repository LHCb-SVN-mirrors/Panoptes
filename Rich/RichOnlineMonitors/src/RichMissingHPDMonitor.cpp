// $Id: RichMissingHPDMonitor.cpp,v 1.33 2010-02-04 12:17:30 ukerzel Exp $
// Include files

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "RichMissingHPDMonitor.h"

// RICH
#include "RichKernel/RichDAQDefinitions.h"
#include "RichKernel/RichSmartIDCnv.h"

//-----------------------------------------------------------------------------

using namespace Rich::Mon;

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( MissingHPDMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
  MissingHPDMonitor::MissingHPDMonitor( const std::string& name,
                                        ISvcLocator* pSvcLocator)
    : Rich::HistoAlgBase        ( name , pSvcLocator ),
      m_RichSys                 ( NULL               ),
      m_SmartIDDecoder          ( NULL               ),
      m_HpdUKL1DisableTool      ( NULL               ),
      m_CamTool                 ( NULL               ),
      m_taeEvents               ( 1, ""              ),
      m_nEvts                   ( 0                  ),
      m_Name                    ( name               )
{
  m_activeRICH[Rich::Rich1] = true;
  m_activeRICH[Rich::Rich2] = true;
  declareProperty( "RawEventLocations", m_taeEvents   );
  declareProperty( "HPDCheckFreq", m_checkFreq = 1000 );
  declareProperty( "MaxMissingEvents", m_maxMissingEvents = 100000 );
  declareProperty( "VetoNoBias"      , m_vetoNoBias       = true   );
}

//=============================================================================
// Destructor
//=============================================================================
MissingHPDMonitor::~MissingHPDMonitor() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode MissingHPDMonitor::initialize()
{
  const StatusCode sc = Rich::HistoAlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  // Partition
  const char* partitionName = getenv("PARTITION");
  m_Name = name();
  if ( partitionName ) 
  {
    const std::string sPartition(partitionName);
    m_Name += sPartition;
    if ( sPartition == "RICH1" ) m_activeRICH[Rich::Rich2] = false;
    if ( sPartition == "RICH2" ) m_activeRICH[Rich::Rich1] = false;
  }

  // talk to camera
  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "send message to CAMERA" << endmsg;
  cameraTool()->Append("TEXT",m_Name.c_str());
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Initialized");

  // return
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MissingHPDMonitor::execute()
{
  StatusCode sc = StatusCode::SUCCESS;

  // load the ODIN
  const LHCb::ODIN * odin = getIfExists<LHCb::ODIN>( LHCb::ODINLocation::Default );
  if ( !odin ) return sc;

  // Veto NoBias
  if ( m_vetoNoBias )
  {
    const Gaudi::Math::Bit< unsigned int, 2 > bit{};
    if ( bit(odin->eventType()) ) { return sc; }
  }

  // count events and print on the first
  if ( 1 == ++m_nEvts )
  {
    const std::string m = "First event seen";
    info() << m << endmsg;
    cameraTool()->Append("TEXT",m_Name.c_str());
    cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,m);
    m_lastEventCheck = m_nEvts;
  }

  // main loop over HPDs in the data
  const Rich::DAQ::L1Map &l1Map = smartIDDecoder()->allRichSmartIDs(m_taeEvents);
  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "L1 map has size " << l1Map.size() << endmsg;

  for ( Rich::DAQ::L1Map::const_iterator iL1Map = l1Map.begin();
        iL1Map != l1Map.end(); ++iL1Map )
  {
    //const Rich::DAQ::Level1HardwareID   l1HardID    = iL1Map->first;
    const Rich::DAQ::IngressMap        &ingressMap  = iL1Map->second;

    for ( Rich::DAQ::IngressMap::const_iterator iIngressMap = ingressMap.begin();
          iIngressMap != ingressMap.end(); ++iIngressMap )
    {

      const Rich::DAQ::IngressInfo & ingressInfo = iIngressMap->second;
      const Rich::DAQ::HPDMap & hpdMap = ingressInfo.hpdData();
      for ( Rich::DAQ::HPDMap::const_iterator iHPDMap = hpdMap.begin();
            iHPDMap != hpdMap.end(); ++iHPDMap )
      {
        const Rich::DAQ::HPDInfo &hpdInfo    = iHPDMap->second;
        const LHCb::RichSmartID  &smartIDHPD = hpdInfo.hpdID();

        // check for valid HPD
        if ( !smartIDHPD.isValid() ) { continue; }

        // Flag this HPD as having been seen in the data, with the event count number
        m_hpdCount[ smartIDHPD ] = m_nEvts;

      }
    } 
  }

  // Monitor things every now and then
  if ( m_nEvts                    >  m_maxMissingEvents && 
       (m_nEvts-m_lastEventCheck) >= m_checkFreq         )
  {

    // Loop over HPDs expected to be present and flag those missing
    const LHCb::RichSmartID::Vector & activeHpds = deRichSys()->activePDRichSmartIDs();
    for ( LHCb::RichSmartID::Vector::const_iterator iHPD = activeHpds.begin();
          iHPD != activeHpds.end(); ++iHPD )
    {
      // Is this RICH active.
      if ( m_activeRICH[(*iHPD).rich()] )
      {
        if ( ( m_nEvts - m_hpdCount[*iHPD] ) > m_maxMissingEvents )
        {
          // it is missing
          std::ostringstream mess;
          mess << "HPD active but no data seen for " << m_maxMissingEvents << " events or more";
          //hpdDisableTool() -> ReportHPD( *iHPD, mess.str() );
          hpdDisableTool() -> DisableHPD( *iHPD, mess.str() );
          //info() << mess.str() << endmsg;
        }
      }
    }

    // Now, loop over those expected to be missing and flag those present
    const LHCb::RichSmartID::Vector & inactiveHpds = deRichSys()->inactivePDRichSmartIDs();
    for ( LHCb::RichSmartID::Vector::const_iterator iHPD = inactiveHpds.begin();
          iHPD != inactiveHpds.end(); ++iHPD )
    {
      // Is this RICH active.
      if ( m_activeRICH[(*iHPD).rich()] )
      {
        if ( ( m_nEvts - m_hpdCount[*iHPD] ) < m_maxMissingEvents )
        {
          // This HPD is present in the data ...
          std::ostringstream mess;
          mess << "HPD inactive but present in the data during the last " 
               << m_maxMissingEvents << " events";
          hpdDisableTool() -> ReportHPD( *iHPD, mess.str() ); 
          //info() << mess.str() << endmsg;
        }
      }
    }

    // set the last seen event number
    m_lastEventCheck = m_nEvts;

  }

  return sc;
} 

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MissingHPDMonitor::finalize()
{
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Finalized");
  return Rich::HistoAlgBase::finalize();
}
