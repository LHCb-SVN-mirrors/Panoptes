#include "RichMirrCombinFit/MirrCombFit.h"

//Local
#include "RichMirrCombinFit/DThetaFitter.h"
//C++
#include <fstream>
//Boost
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/token_functions.hpp>
#include <boost/tokenizer.hpp>
#include <boost/xpressive/xpressive.hpp>
//ROOT
#include <TKey.h>
#include <TList.h>
#include <TPRegexp.h>
#include <TString.h>

namespace  po = boost::program_options;
using           boost::format;
using           boost::lexical_cast;
using namespace boost::xpressive;
using namespace std;

MirrCombFit::MirrCombFit()
{
  cout <<"Constructing MirrCombFit()"<< endl;
}

MirrCombFit::~MirrCombFit()
{

}


void MirrCombFit::MirrCombList()
{
  // read combAndMirrSubsets
  cout <<"Open file with mirror combinations"<< endl;
  ifstream combAndMirrSubsets;
  combAndMirrSubsets.open( (m_combAndMirrSubsetsFile).c_str() );

  vector<string> combList;
  string combAndMirrSubsetsOneStr;
  string oneLineStr;

  while (  ! combAndMirrSubsets.eof() ) {
    getline( combAndMirrSubsets, oneLineStr );
    combAndMirrSubsetsOneStr += oneLineStr;
    combAndMirrSubsetsOneStr += "\n";
  }
  combAndMirrSubsets.close();

  // print the whole image of the combAndMirrSubsetsFile
  cout <<combAndMirrSubsetsOneStr<< endl;

  sregex reSep        = sregex::compile("[\\s]+");    // any space separator, including end-of-line

  sregex comb_matcher = sregex::compile("[\\d]{4}");  // just 4-digit mirror combination number

  sregex_token_iterator sregTokIt( combAndMirrSubsetsOneStr.begin(), combAndMirrSubsetsOneStr.end(), reSep, -1 );
  sregex_token_iterator strEnd;
  smatch what;
  while ( sregTokIt != strEnd ) {
    string strSregTokIt = (*sregTokIt).str();
    if ( regex_match( strSregTokIt, what, comb_matcher ) ) {
      combList.push_back( (string)what[0] );
      cout <<(string)what[0]<< endl;
    }
    ++sregTokIt;
  }
  sort( combList.begin(), combList.end() );

  m_mirrCombList = combList;
  cout <<"Got m_mirrCombList vector"<< endl;

}


string MirrCombFit::MirrCombNam(int pri, int sec)
{
  string priStr = lexical_cast<string>(pri);
  string secStr = lexical_cast<string>(sec);
  if (pri < 10) priStr = "0"+priStr;
  if (sec < 10) secStr = "0"+secStr;
  string combStr = priStr+secStr;
  return combStr;
}


void MirrCombFit::CreateMapCombNamCombObj()
{
  cout <<"Creating complete map of named mirror combination objects:"<< endl;
  for (  int pri = 0; pri < m_priMirrs; ++pri) {
    for (int sec = 0; sec < m_secMirrs; ++sec) {
      MirrComb combObj(pri, sec);
      string combNam = this->MirrCombNam(pri, sec);
      cout <<" "<<combNam;
      m_mapNamObj[combNam] = combObj;
    }
    cout << endl;
  }
}


void MirrCombFit::HistoList()
{
  // Get list of all histograms in the input file.
  // Cross check these histograms against the list of
  // mirror combinations, and keep any that match.

  TList* listOfKeys = gDirectory->GetListOfKeys();
  TIter next( listOfKeys );

  // loop over all histograms
  while ( TKey* key = (TKey*)next() ) {

    // work only with TH2D histograms:
    TString className( Form("%s", key->GetClassName()) );
    if ("TH2D" != className ) {
      continue;
    }

    // get histogram name, and check against list of chosen mirror combinations
    TString h2Name( Form("%s", key->GetName()) );
    TPRegexp regexpNum("[0-9]+");
    TString combNamTStr = h2Name( regexpNum );
    vector<string>::iterator sWhere = find( m_mirrCombList.begin(), m_mirrCombList.end(), combNamTStr );
    if ( sWhere == m_mirrCombList.end() ) {
      continue;
    }

    string h2name = (string)h2Name;
    if ( ! boost::find_first(h2name, "Iso") ) {m_histoNames.push_back(h2Name );}
  }
  // here is the sorted list of all relevant histograms for fitting:
  sort( m_histoNames.begin(), m_histoNames.end() );
}


void MirrCombFit::OpenFile()
{
  // Open file and go to directory with ROOT files.
  f = TFile::Open( m_inputHistosFile.c_str());
  f->cd( ("/RICH/RichAlignMoniR"+m_rich+"Gas/Rich"+m_rich+"Gas").c_str() );
}


void MirrCombFit::FitHistos()
{
  // get list of mirror combinations
  cout <<"Get MirrCombList"<< endl;
  this->MirrCombList();
  // open root file
  this->OpenFile();
  // get list of histograms
  this->HistoList();

  // if the m_useGlobalFitMean flag is !=0, run a global dTheta fit,
  // and use it as initial or fixed value of shift in the full dPhi dTheta fit
  if (m_useGlobalFitMean) {
    TH1D* histoGlobalDeltaTheta = (TH1D*)gDirectory->Get("../deltaThetaUnamb");
    this->SetShift( this->GetGlobalFitMean(histoGlobalDeltaTheta) );
  }

  for (vector<TString>::iterator hNameIt=m_histoNames.begin(); hNameIt<m_histoNames.end(); ++hNameIt) {
    cout <<(*hNameIt)<< endl;
    TH2D* histo = (TH2D*)gDirectory->Get((*hNameIt));
    if (m_phiBinFactor != 1) {
      histo->RebinX(m_phiBinFactor);
    }
    histo->Print();
    this->BinCounter(histo);
    GetFitParams(histo);
  }

  f->Close();
}


void MirrCombFit::BinCounter(TH2D* histoIn)
{
  int safeBins = 0;
  TH1D* projx = histoIn->ProjectionX();
  // actual number of bins in phi (should be 20, but could be more or less, depends on version Rich/RichAlignment)
  int xbins = projx->GetNbinsX();
  // number of bins in deltaTheta (should be 50)
  int ybins = histoIn->GetNbinsY();
  // Now, ideally we want
  double want = 16.0;
  // of every
  double have = 20.0;
  // bins in phi to have at least minPhotonsPerPhiBin photons per phi bin.
  // we now set this threshold via minAverageBinPop in the mirror alignment settings file
  double minPhotonsPerPhiBin = m_minAverageBinPop * ybins;

  // OLD
//double minPhotonsPerPhiBin = 300.0;
  // If we have a different number of bins, we need to scale minPhotonsPerPhiBin:
//double minPhotonsPerPhiBinScaled = have*minPhotonsPerPhiBin/xbins;

  // Loop over the phi bins, count the number of photons in each phi bin.
  for (int binNr=1; binNr<=xbins; ++binNr) {
    double binCont = projx->GetBinContent(binNr);
  //if ( binCont >= minPhotonsPerPhiBinScaled ){
    if ( binCont >= minPhotonsPerPhiBin ) {
      ++safeBins;
      cout <<"Bin "<<binNr<<" has "<<binCont<<" entries."<< endl;
    }
    else {
      cout <<"Bin "<<binNr<<" has "<<binCont<<" entries. ";
      cout <<"This is lower than the "<<minPhotonsPerPhiBin<<" photons needed to include this bin in the fit."<< endl;
    }
  }
//cout <<" "<< safeBins<<" out of "<< xbins <<" are populated with ''enough'' ("<<minPhotonsPerPhiBinScaled<<") photons." << endl;
  cout <<" "<< safeBins<<" out of "<< xbins <<" are populated with ''enough'' ("<<minPhotonsPerPhiBin      <<") photons." << endl;
  double frac = ((double)safeBins) / xbins;
  double threshold = (want/have) - 0.0001; // (the minus 0.01% is just for a safety margin)
  if ( frac < threshold ) {
    cout <<"The number of phi bins populated sufficiently is low; this may be not_good_enough for mirror alignment."<< endl;
  }
}


void MirrCombFit::GlobalFit()
{
  this->OpenFile();

  TH1D* histoGlobalDeltaTheta = (TH1D*)gDirectory->Get("../deltaThetaUnamb");
  cout <<"Getting global delta theta histogram "<<histoGlobalDeltaTheta<< endl;

  DThetaFitter dThetaFitter(m_begRange, m_endRange);
  cout <<"Making fitter "<<histoGlobalDeltaTheta<< endl;

  if (m_zeroGlobalFitMean) {
    dThetaFitter.setConstantMean(0.0);
  }

  dThetaFitter.loadHistogram(histoGlobalDeltaTheta);
  cout <<"Loading histogram"<< endl;

  dThetaFitter.plot(m_plotDir+"Rich"+m_rich+"globalDThetaFit");

  f->Close();
}


double MirrCombFit::GetGlobalFitMean(TH1D* histo)
{
  DThetaFitter dThetaFitter(m_begRange, m_endRange);
  dThetaFitter.loadHistogram(histo);
  if (m_plotOutputLevel > 0) dThetaFitter.plot(m_plotDir+"Rich"+m_rich+"globalDThetaFit_fromGetGlobalFitMean");

  return dThetaFitter.getMean();
}


void MirrCombFit::StopOrContinue()
{
  // THIS FUNCTION IS NO LONGER BEING USED, BUT BEING PRESERVED UNTIL DEEMED TRULY UNNECESSARY.

  // Write a file to say if we are going to stop or continue
  string stop_or_continue("STOP");
  for (MapCombNamCombObj::iterator it = m_mapNamObj.begin(); it != m_mapNamObj.end(); ++it) {

      vector<string>::iterator sWhere = find( m_mirrCombList.begin(), m_mirrCombList.end(), (*it).first );
      if ( sWhere == m_mirrCombList.end() ) {
        continue;
      }
      if (fabs( (*it).second.Y() )*1000 > 0.1) { // is Y misalignment larger than 0.1 mrad?
        cout <<"Y parameter for mirror combination "<<(*it).first<<" has tilt of  "<<format("%| 10.4f|")%((*it).second.Y()*1000)<< endl;
        stop_or_continue = "CONTINUE";
      }
      if (fabs( (*it).second.Z() )*1000 > 0.1) { // is Z misalignment larger than 0.1 mrad?
        cout <<"Z parameter for mirror combination "<<(*it).first<<" has tilt of  "<<format("%| 10.4f|")%((*it).second.Z()*1000)<< endl;
        stop_or_continue = "CONTINUE";
      }
  }

  ofstream stop_or_continue_txt;
  stop_or_continue_txt.open( ("Rich"+m_rich+"_stop_or_continue.txt").c_str() ); // maybe Change this
  stop_or_continue_txt << stop_or_continue << endl;
  stop_or_continue_txt.close();

}


void MirrCombFit::CombinFitDecision()
{
  // Write a file to say if we are going to stop or continue
  string combinfitdecision("CONTINUE");
  for (MapCombNamCombObj::iterator it = m_mapNamObj.begin(); it != m_mapNamObj.end(); ++it) {

    vector<string>::iterator sWhere = find( m_mirrCombList.begin(), m_mirrCombList.end(), (*it).first );
    if ( sWhere == m_mirrCombList.end() ) {
      continue;
    }
    if (fabs((*it).second.Y())*1000 > m_warningFactor*m_stopTolerance) { // is Y misalignment larger than m_warningFactor*m_stopTolerance mrad?
      cout <<"WARNING! WARNING! Y parameter for mirror combination "<<(*it).first<<" has tilt of  "<<format("%| 10.4f|")%((*it).second.Y()*1000)<< endl;
      combinfitdecision = "PAUSE";
    }
    else if (fabs((*it).second.Y())*1000 > m_stopTolerance) { // is Y misalignment larger than m_stopTolerance mrad?
      cout <<"Y parameter for mirror combination "<<(*it).first<<" has tilt of  "<<format("%| 10.4f|")%((*it).second.Y()*1000)<< endl;
    }
    if (fabs((*it).second.Z())*1000 > m_warningFactor*m_stopTolerance) { // is Z misalignment larger than m_warningFactor*m_stopTolerance mrad?
      cout <<"WARNING! WARNING! Z parameter for mirror combination "<<(*it).first<<" has tilt of  "<<format("%| 10.4f|")%((*it).second.Z()*1000)<< endl;
      combinfitdecision = "PAUSE";
    }
    else if (fabs((*it).second.Z())*1000 > m_stopTolerance) { // is Z misalignment larger than m_stopTolerance mrad?
      cout <<"Z parameter for mirror combination "<<(*it).first<<" has tilt of  "<<format("%| 10.4f|")%((*it).second.Z()*1000)<< endl;
    }
  }

  ofstream combinfitdecision_txt;
  combinfitdecision_txt.open( ("Rich"+m_rich+"_combinfitdecision.txt").c_str() );
  combinfitdecision_txt << combinfitdecision << endl;
  combinfitdecision_txt.close();
}


void MirrCombFit::ReadConfFile(string confFile)
{
  int    richDetector      ; // PN - this is read in, but I don't think it's used at all anymore
  double deltaThetaWindow  ; // PN - this is used

  string          strDeltaThetaWindow;
  stringstream stgstmDeltaThetaWindow;

  try {
    // declare the supported options
    po::options_description from_conf_file_desc("Configuration");

    from_conf_file_desc.add_options()
      ("help"                ,                                                                      "produce help message"                                                                                                            )
      ("plotDir"             , po::value<string>(& m_plotDir               )->default_value( ""  ), "working directory where all intermediate files are created"                                                                      )
      ("thisVariant"         , po::value<string>(& m_thisVariant           )                      , "string identifying this case, e.g. \"48m85c_p12p43fix_Gs1Gb1Mp9Wi4_129kPreMC10\""                                                )
      ("richDetector"        , po::value<int   >(& richDetector            )->default_value( 1   ), "number of RICH detector: if equals 1 then RICH1, if equals 2 then RICH2"                                                         )
      ("fixSinusoidShift"    , po::value<int   >(& m_fixSinusoidShift      )->default_value( 1   ), "fix shift parameter in fitting formula A*cos(phi)+B*sin(phi)+shift (default =1 : fix at sinusoidShift); if =0, it is not fixed"  )
      ("sinusoidShift"       , po::value<double>(& m_sinusoidShift         )->default_value( 0.0 ), "shift parameter in fitting formula; if fixSinusoidShift !=0, it is fixed at this value; othrewise this is just initial value"    )
      ("zeroGlobalFitMean"   , po::value<int   >(& m_zeroGlobalFitMean     )->default_value( 0   ), "default =0 : \"mean\" in global fit is not fixed; if !=0 it is fixed at zero"                                                    )
      ("useGlobalFitMean"    , po::value<int   >(& m_useGlobalFitMean      )->default_value( 0   ), "default =0 : don't; otherwise, call GetGlobalFitMean and use that mean as initial (or fixed) value of m_sinusoidShift"           )
      ("combAndMirrSubsets"  , po::value<string>(& m_combAndMirrSubsetsFile)                      , "File with chosen mirrors and combinations subsets"                                                                               )
      ("minAverageBinPop"    , po::value<double>(& m_minAverageBinPop      )->default_value( 6.0 ), "minimum average bin population after merging slices, default=6 (old default was 100)"                                            )
      ("deltaThetaWindow"    , po::value<double>(& deltaThetaWindow        )->default_value( 8.0 ), "delta Theta window in mrad where slices are fitted to find the mode, default=4.0"                                                )
      ("combinFitMethod"     , po::value<int   >(& m_fitMethod             )->default_value( 3   ), "which method of the 2D histogram fitting to use, default=3: slices fit jointly with sinusoidal trajectory of their modes"        )
      ("iterationCount"      , po::value<int   >(& m_iterationCount        )->default_value( 0   ), "this iteration number, starting from 0"                                                                                          )
      ("inputHistos"         , po::value<string>(& m_inputHistosFile       )->default_value( ""  ), "input ROOT file"                                                                                                                 )
      ("backgroundOrder"     , po::value<int   >(& m_backgroundOrder       )->default_value( 2   ), "order for the polynomial background"                                                                                             )
      ("plotOutputLevel"     , po::value<int   >(& m_plotOutputLevel       )->default_value( 2   ), "what plots should be saved (0) plot nothing (1) plot ...(2) only plot fits with chi2 worse than 3.0 (3) plot everything"         )
      ("mirrCombinFitResults", po::value<string>(& m_outputResultsFile     )->default_value( ""  ), "output results file"                                                                                                             )
      ("phiBinFactor"        , po::value<int   >(& m_phiBinFactor          )->default_value( 1   ), "number of phi bins we use for alignment = number of phi bins in the 2D histograms / phiBinFactor"                                )
      ("stopTolerance"       , po::value<double>(& m_stopTolerance         )->default_value( 0.1 ), "tolerance, in mrad, to stop the alignment because all mirror tilts have converged; historically 0.1"                             )
      ("warningFactor"       , po::value<double>(& m_warningFactor         )->default_value( 20  ), "alerts the alignment shifter if any of the mirrors have shifted more than warningFactor*stopTolerance"                           )
    ;
    po::variables_map vm;
    ifstream ifstrConfFile( confFile.c_str() );
    po::store( po::parse_config_file(ifstrConfFile, from_conf_file_desc), vm );
    po::notify( vm );

    m_begRange = -deltaThetaWindow/1000.0;
    m_endRange =  deltaThetaWindow/1000.0;

    if (vm.count("help"              )) {cout <<"Usage: MirrCombinFit.exe config-file-name"           << endl;         cout <<from_conf_file_desc;                      throw "help";}
    if (vm.count("plotDir"           )) {cout <<"plotDir               "<<m_plotDir                   << endl;} else { cout <<"plotDir            was not set."<< endl; throw "plotDir not set";}
    if (vm.count("combAndMirrSubsets")) {cout <<"combAndMirrSubsets    "<<m_combAndMirrSubsetsFile    << endl;} else { cout <<"combAndMirrSubsets was not set."<< endl; throw "comb and mirr set file not set";}
    cout                                      <<"richDetector          "<<richDetector                << endl;
    cout                                      <<"plotDir               "<<m_plotDir                   << endl;
    cout                                      <<"fixSinusoidShift      "<<m_fixSinusoidShift          << endl;
    cout                                      <<"sinusoidShift         "<<m_sinusoidShift             << endl;
    cout                                      <<"zeroGlobalFitMean     "<<m_zeroGlobalFitMean         << endl;
    cout                                      <<"useGlobalFitMean      "<<m_useGlobalFitMean          << endl;
    cout                                      <<"minAverageBinPop      "<<m_minAverageBinPop          << endl;
    stgstmDeltaThetaWindow <<format("%|3.1f|")%deltaThetaWindow;
    strDeltaThetaWindow = stgstmDeltaThetaWindow.str();
    cout                                      <<"deltaThetaWindow      "<<strDeltaThetaWindow<<" mrad"<< endl;
    cout                                      <<"combinFitMethod       "<<m_fitMethod                 << endl;
    cout                                      <<"iterationCount        "<<m_iterationCount            << endl;
    cout                                      <<"inputHistos           "<<m_inputHistosFile           << endl;
    cout                                      <<"backgroundOrder       "<<m_backgroundOrder           << endl;
    cout                                      <<"plotOutputLevel       "<<m_plotOutputLevel           << endl;
    cout                                      <<"mirrCombinFitResults  "<<m_outputResultsFile         << endl;
    cout                                      <<"phiBinFactor          "<<m_phiBinFactor              << endl;
    cout                                      <<"stopTolerance         "<<m_stopTolerance             << endl;
    cout                                      <<"warningFactor         "<<m_warningFactor             << endl;
  }
  catch(exception& e) {
    cout <<e.what()<< endl;
    throw "exception";
  }
}


void MirrCombFit::PrintResults()
{
  ofstream outputFile(m_outputResultsFile.c_str());
  if (outputFile.is_open()) {
    for (MapCombNamCombObj::iterator it=m_mapNamObj.begin(); it != m_mapNamObj.end(); ++it) {
      vector<string>::iterator sWhere = find( m_mirrCombList.begin(), m_mirrCombList.end(), (*it).first );
      if ( sWhere == m_mirrCombList.end() ) {
        continue;
      }
      outputFile
      <<(*it).first<<"  "
      <<format("%| 10.4f|")%((*it).second.Y()    *1000.0)<<"  "<<format("%| 10.4f|")%((*it).second.YErr()    *1000.0)<<"  "
      <<format("%| 10.4f|")%((*it).second.Z()    *1000.0)<<"  "<<format("%| 10.4f|")%((*it).second.ZErr()    *1000.0)<<"  "
      <<format("%| 10.4f|")%((*it).second.Shift()*1000.0)<<"  "<<format("%| 10.4f|")%((*it).second.ShiftErr()*1000.0)<<"\n";
    }
  }
}


void MirrCombFit::SetShift(double shift)
{
  m_sinusoidShift = shift;
}


void MirrCombFit::Print(ostream& out) const
{
  out <<"test\n";
//for (MapCombNamCombObj::const_iterator it = m_mapNamObj.begin(); it!=m_mapNamObj.end(); ++it) {
//
//  out
//  <<(*it).first<<"  "
//  <<format("%| 10.4f|")%((*it).second.Y()    *1000.0)<<"  "<<format("%| 10.4f|")%((*it).second.YErr()    *1000.0)<<"  "
//  <<format("%| 10.4f|")%((*it).second.Z()    *1000.0)<<"  "<<format("%| 10.4f|")%((*it).second.ZErr()    *1000.0)<<"  "
//  <<format("%| 10.4f|")%((*it).second.Shift()*1000.0)<<"  "<<format("%| 10.4f|")%((*it).second.ShiftErr()*1000.0)<<"\n";

//}
}


ostream& operator<<(ostream& out, const MirrCombFit& fit)
{
  fit.Print(out);
  return out;
}
