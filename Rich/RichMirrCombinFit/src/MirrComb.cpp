#include "RichMirrCombinFit/MirrComb.h"

//Boost
#include <boost/lexical_cast.hpp>

using namespace std;

using boost::lexical_cast;


MirrComb::MirrComb(int pri, int sec)
: m_pri(pri), m_sec(sec)
{

}

MirrComb::MirrComb(string combNam)
{
  m_pri = lexical_cast<int>(combNam[0]+combNam[1]);
  m_sec = lexical_cast<int>(combNam[2]+combNam[3]);
}

int MirrComb::Pri()
{
  return m_pri;
}

int MirrComb::Sec()
{
  return m_sec;
}

double MirrComb::Z()
{
  return m_z;
}
void MirrComb::Z(double z)
{
  m_z = z;
}

double MirrComb::Y()
{
  return m_y;
}
void MirrComb::Y(double y)
{
  m_y = y;
}

double MirrComb::ZErr()
{
  return m_zErr;
}

void MirrComb::ZErr(double zErr)
{
  m_zErr = zErr;
}

double MirrComb::YErr()
{
  return m_yErr;
}

void MirrComb::YErr(double yErr)
{
  m_yErr = yErr;
}

double MirrComb::Shift()
{
  return m_sinusoidShift;
}

void MirrComb::Shift(double shift)
{
  m_sinusoidShift = shift;
}

double MirrComb::ShiftErr()
{
  return m_sinusoidShiftErr;
}

void MirrComb::ShiftErr(double shiftErr)
{
  m_sinusoidShiftErr = shiftErr;
}

