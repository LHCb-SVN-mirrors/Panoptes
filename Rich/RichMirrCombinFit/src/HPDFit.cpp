#include "RichMirrCombinFit/HPDFitter.h"
#include "RichMirrCombinFit/RichMirrCombFit.h"

//C++
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sstream>
//Root
#include <TH1.h>
#include <TH2.h>
#include <TKey.h>
#include <TObject.h>
#include <TProfile.h>
//Boost
#include <boost/lexical_cast.hpp>

using           boost::lexical_cast;
using namespace std;


void FitHPDs(int richDetector, TFile* f, double deltaThetaWindow, string outputFile)
{
  vector<string> HPDs;
  string strRichDetector( lexical_cast<string>richDetector );

  TDirectory* dir = f->GetDirectory(("RICH/RichAlignMoniR"+strRichDetector+"Gas/HPDs").c_str());
  dir->pwd();

  cout <<"Opened Directory "<<"RICH/RichAlignMoniR"+strRichDetector+"Gas/HPDs"<< endl;
  TList* list = dir->GetListOfKeys() ;
  if (!list) { printf("<E> No keys found in file\n"); exit(1); }
  TIter next(list);
  TKey* key;
  TObject* obj;

  while ( (key = (TKey*)next()) ) {
      obj = key->ReadObj();
      if ( (strcmp(obj->IsA()->GetName(), "TProfile") != 0)
      &&   (!obj->InheritsFrom("TH2"))
      &&   (!obj->InheritsFrom("TH1"))
      ) {
          printf("<W> Object %s is not 1D or 2D histogram : "
                 "will not be converted\n", obj->GetName());
      }
      printf("Histo name:%s title:%s\n", obj->GetName(), obj->GetTitle());
      string name = obj->GetName();
      cout <<"HPD: "<<name<< endl;
    //if (name == "HPD_100408")
      HPDs.push_back(name);
  }


  vector<string>::iterator HPDIt;

  for (HPDIt = HPDs.begin(); HPDIt<HPDs.end(); HPDIt++) {
    TString histPath;

    histPath = "RICH/RichAlignMoniR"+strRichDetector+"Gas/HPDs/"+*HPDIt;

    TH2F* inHist = static_cast<TH2F*>(f->Get(histPath));

    HPDFitter hpdfit(inHist, lexical_cast<int>(strRichDetector));

    if (inHist->GetEntries() > 1000) {
      // Want only to fit HPDs with even distribution of hits throughout phi.
      unsigned int xbins = inHist->GetNbinsX();
      unsigned int ybins = inHist->GetNbinsY();
      int XbinsLessThanMin = 0;
      for (unsigned int xbin = 0; xbin != xbins; xbin++) {
        double binEntriesX = 0;
        for (unsigned int ybin = 0; ybin != ybins; ybin++) {
          binEntriesX = binEntriesX + inHist->GetBinContent(xbin, ybin);
        }
        if (binEntriesX < 9) {
          XbinsLessThanMin++;
        }
      }

      if (XbinsLessThanMin < 50) {
        hpdfit.SetRange(-1*deltaThetaWindow/1000.0, deltaThetaWindow/1000.0); // Set dTheta Range
        hpdfit.Fit();
      }
    }
    hpdfit.Write((outputFile).c_str());
  }
}

int main(int argc, char* argv[])
{

  cout <<argc   << endl;
  cout <<argv[1]<< endl;
  cout <<argv[2]<< endl;
  cout <<argv[3]<< endl;
  cout <<argv[4]<< endl;

  int rich                   = lexical_cast<int>(argv[1]);
  double deltaThetaWindow = lexical_cast<double>(argv[2]);
  string histoFile        = lexical_cast<string>(argv[3]);
  string outputFile       = lexical_cast<string>(argv[4]);

  TFile* f = new TFile(histoFile.c_str());
  cout <<"Opened file: "<< histoFile << endl;
  FitHPDs(rich, f, deltaThetaWindow, outputFile);

  delete f;
  return 0;
}
