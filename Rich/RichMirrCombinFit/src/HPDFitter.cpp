#include "RichMirrCombinFit/HPDFitter.h"

//C++
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
//ROOT
#include <TF1.h>
#include <TH1D.h>
#include <TMath.h>
#include <TPad.h>
#include <TProfile.h>
//Roofit
#include <RooAbsPdf.h>
#include <RooAddPdf.h>
#include <RooChebychev.h>
#include <RooDataHist.h>
#include <RooGaussian.h>
#include <RooPlot.h>
//Boost
#include <boost/lexical_cast.hpp>

using           boost::lexical_cast;
using namespace RooFit;
using namespace std;


const double HPDFitter::TDRsigma[] = {0.00145, 0.00068};

HPDFitter::HPDFitter(TH2D* histo, int Rich)
: RICH(Rich), sin(0), sinErr(0), cos(0), cosErr(0)
{
  m_histo = histo;
  HPD = m_histo->GetName();
  if      (Rich == 1)  m_dTheta = new RooRealVar("dTheta", "dTheta", 0.0, -0.007,  0.007 );
  else if (Rich == 2)  m_dTheta = new RooRealVar("dTheta", "dTheta", 0.0, -0.0039, 0.0039);
  else if (Rich != 1 && Rich != 2) cerr <<"Rich is not RICH1 or RICH2!!"<< endl;

}


HPDFitter::~HPDFitter()
{
  delete m_histo;
}


void HPDFitter::SetRange(double low, double high)
{
  m_dTheta->setRange(low, high);
}


void HPDFitter::Fit()
{
  int xbins = (int)m_histo->GetNbinsX();

  FitProjection();

  TGraphErrors* cleanPlot = new TGraphErrors;
  // Loop over phi bins
  for (int i=0; i<xbins; ++i) {
    FitSlice(i,cleanPlot);
  }

  Fit2D(cleanPlot);

}


void HPDFitter::FitSlice(int i, TGraphErrors* cleanPlot)
{
  int xbins = (int)m_histo->GetNbinsX();
  double xwidth = 2.0*TMath::Pi()/xbins;

  TH1D* stripe = (TH1D*)m_histo->ProjectionY("stripe", i, i, "o");

  RooDataHist* data = new RooDataHist("data", "data", *m_dTheta, stripe);
//RooDataHist ReducedDataSet("data", "data", NPhotons, inputHisto);

//int NEntries = data->numEntries();

  // Draw data
//RooPlot* DthetaProjection = m_dTheta.frame();

  // signal
  RooRealVar sig_photons_mean("Mean", "mean", m_sinusoidShift, -0.003, 0.003);
  RooRealVar sig_photons_sigma("#sigma", "width of gaussian", TDRsigma[RICH]*1.2, TDRsigma[RICH]*0.4, TDRsigma[RICH]*2.5);

  RooAbsPdf* signal_NPhotons = new RooGaussian("signal_photons", "signal_photons", *m_dTheta, sig_photons_mean, sig_photons_sigma);

  RooRealVar bkg_photons_p0("p0", "p0", 0.0, -100,    100   );
  RooRealVar bkg_photons_p1("p1", "p1", 0.0, -1.0e10, 1.0e10);

  RooArgList bkgArgList(bkg_photons_p0, bkg_photons_p1);

  // Background
  RooAbsPdf* bkg_photons = new RooChebychev("Polynomial", "O(1) Polynomial", *m_dTheta, bkgArgList);

  // Signal fraction
  RooRealVar f("f", "signal fraction", 0.5, 0.0, 1.0);

  // Combine to make PDF
  RooAbsPdf* photon_pdf = new RooAddPdf("photon_pdf", "photon_pdf", RooArgList(*signal_NPhotons, *bkg_photons), f);

//if (NEntries > 100) // Fit only if Entries larger than 100
//{
  photon_pdf->fitTo(*data, PrintLevel(-1), Warnings(kFALSE));
//}

  // Add mean values to 2D plot to fit
  cleanPlot->SetPoint(i, (i+0.5)*xwidth, sig_photons_mean.getValV());
  cleanPlot->SetPointError(i, 0.5*xwidth/sqrt(stripe->GetEntries()), sig_photons_mean.getError());

  string name = lexical_cast<string>(stripe->GetName())+"_"+lexical_cast<string>(m_histo->GetName())+"_"+lexical_cast<string>(i);

  RooPlot* Plot = m_dTheta->frame();
  data->plotOn(Plot);
  photon_pdf->plotOn(Plot, RooFit::LineColor(15));
  photon_pdf->plotOn(Plot, Components(*bkg_photons), LineColor(16+1), LineStyle(2+1));

  Plot->Draw();
  gPad->SaveAs((name+".png").c_str());

  delete stripe;
  delete data;
  delete photon_pdf;
  delete bkg_photons;
  delete signal_NPhotons;
}


void HPDFitter::FitProjection()
{
  TH1D* Projection = (TH1D*)m_histo->ProjectionY("Projection");

  RooDataHist* data = new RooDataHist("data", "data", *m_dTheta, Projection);

  RooRealVar sig_photons_mean("Mean", "mean", 0.0, -0.003, 0.003);
  RooRealVar sig_photons_sigma("#sigma", "width of gaussian", TDRsigma[RICH]*1.2, TDRsigma[RICH]*0.4, TDRsigma[RICH]*2.5);

  RooAbsPdf* signal_NPhotons = new RooGaussian("signal_photons", "signal_photons", *m_dTheta, sig_photons_mean, sig_photons_sigma);

  RooRealVar bkg_photons_p0("p0", "p0", 0.0, -100,    100   );
  RooRealVar bkg_photons_p1("p1", "p1", 0.0, -1.0e10, 1.0e10);

  RooArgList bkgArgList(bkg_photons_p0, bkg_photons_p1);

  // Background
  RooAbsPdf* bkg_photons = new RooChebychev("Polynomial", "O(1) Polynomial", *m_dTheta, bkgArgList);

  // Signal fraction
  RooRealVar f("f", "signal fraction", 0.5, 0.0, 1.0);

  // Combine to make PDF
  RooAbsPdf* photon_pdf = new RooAddPdf("photon_pdf", "photon_pdf", RooArgList(*signal_NPhotons, *bkg_photons), f);

//if (NEntries > 100) // Fit only if Entries larger than 100
//{
  photon_pdf->fitTo(*data);
//}

  string name = lexical_cast<string>(Projection->GetName())+"_"+lexical_cast<string>(m_histo->GetName());

  RooPlot* Plot = m_dTheta->frame();
  data->plotOn(Plot);
  photon_pdf->plotOn(Plot, RooFit::LineColor(15));
  photon_pdf->plotOn(Plot, Components(*bkg_photons), LineColor(16+1), LineStyle(2+1));

  Plot->Draw();
  gPad->SaveAs((name+".png").c_str());

  m_sinusoidShift = sig_photons_mean.getValV();
  delete Projection;
  delete data;
  delete photon_pdf;
  delete bkg_photons;
  delete signal_NPhotons;

}


void HPDFitter::Fit2D(TGraphErrors* cleanPlot)
{
  // Define fit function
  TF1* sinusoid = new TF1("sinusoid", "[0]*cos(x) + [1]*sin(x) + [2]", 0, 2.0*TMath::Pi());

  sinusoid->SetParName(0, "CosCoeff");
  sinusoid->SetParName(1, "SinCoeff");
  sinusoid->SetParName(2, "Shift"   );

  int phiBins = (int)m_histo->GetNbinsX();
//double phiWidth = 2.0*TMath::Pi()/phiBins;
//int theBins = (int)m_histo->GetNbinsY();

  // Fit TGraphErrors with sinusoid

  // Estimate cos amplitude
  double CosAmpEst;
  double x, y1, y2, sy1, sy2;
  cleanPlot->GetPoint(int(phiBins/4),   x, y1);
  cleanPlot->GetPoint(int(phiBins*3/4), x, y2);
  sy1 = cleanPlot->GetErrorY(int(phiBins/4));
  sy2 = cleanPlot->GetErrorY(int(phiBins*3/4));
  CosAmpEst = ((y1/sy1)-(y2/sy2))/((1/sy1)+(1/sy2));

  sinusoid->SetParameter(0, CosAmpEst); // total tilt around Y in rad

  // Estimate sin amplitude
  double SinAmpEst;
  double y3, y4, y5, sy3, sy4, sy5;
  cleanPlot->GetPoint(0, x, y3);
  cleanPlot->GetPoint(int(phiBins/2), x, y4);
  cleanPlot->GetPoint(phiBins-1,      x, y5);
  sy3 = cleanPlot->GetErrorY(0);
  sy4 = cleanPlot->GetErrorY(int(phiBins/2));
  sy5 = cleanPlot->GetErrorY(phiBins-1);
  SinAmpEst = ((y3/sy3)-(y4/sy4)+(y5/sy5))/((1/sy3)+(1/sy4)+(1/sy5));

  sinusoid->SetParameter(1, SinAmpEst); // total tilt around Z in rad

  // Fix sinusoid shift when m_fixSinusoidShift !=0 (default =0)
  if (m_fixSinusoidShift) {
    sinusoid->FixParameter(2, m_sinusoidShift);
  }

  // Fit TGraphErrors
  cleanPlot->Fit(sinusoid, "R");

  cos    = sinusoid->GetParameter(0);
  cosErr = sinusoid->GetParError( 0);
  sin    = sinusoid->GetParameter(1);
  sinErr = sinusoid->GetParError( 1);

  bool writehistos = true;
  if (writehistos) {
    m_histo->Draw("colz");
    cleanPlot->Draw("SAME");
    string name = m_histo->GetName();
    gPad->SaveAs((name+".png").c_str());
  }
}


void HPDFitter::Write(const char* file)
{
  fstream filestr;
  filestr.open (file, fstream::in | fstream::out | fstream::app);
  filestr <<HPDNum()<<"  "<<cos<<"  "<<cosErr<<"  "<<sin<<"  "<<sinErr<<"  "<< endl;
  filestr.close();
}


string HPDFitter::HPDNum()
{
  bool dbThis = true;
  string str ("HPD_");
  size_t found;
  found = HPD.find_first_not_of(str);
  HPD.erase(0,found);

  int richDet = lexical_cast<int>(HPD[0]);
  int box     = lexical_cast<int>(HPD[1]);

  string colStr = lexical_cast<string>(HPD[2])+lexical_cast<string>(HPD[3]);
  string rowStr = lexical_cast<string>(HPD[4])+lexical_cast<string>(HPD[5]);

  int col = lexical_cast<int>(colStr);
  int row = lexical_cast<int>(rowStr);

  if (dbThis)
    {
      cout <<"HPD "<<HPD                    << endl;
      cout <<"rich"<<richDet                << endl;
      cout <<"box "<<box                    << endl;
      cout <<"col "<<col<<" colStr "<<colStr<< endl;
      cout <<"row "<<row<<" rowstr "<<rowStr<< endl;
    }

  int start(0);
  int columns(0);
  int rows(0);

  if (RICH ==1) {
    if (box == 0) start = 0;
    if (box == 1) start = 98;
    columns = 7;
    rows = 14;
    cout <<"RICH"<<RICH<<" cols: "<<columns<<" rows: "<<rows << endl;
  }

  if (RICH ==2) {
    if (box == 0) start = 196;
    if (box == 1) start = 340;
    columns = 9;
    rows = 16;
  }

  int hpdNum = start + rows*col + row;
  if (dbThis) cout <<"HPD int "<<hpdNum<< endl;
  return ( lexical_cast<string>(hpdNum) );
}
