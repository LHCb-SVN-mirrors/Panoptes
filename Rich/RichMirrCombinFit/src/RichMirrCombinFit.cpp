//Local
#include "RichMirrCombinFit/IMirrCombFit.h"
#include "RichMirrCombinFit/LHCbStyle.h"
#include "RichMirrCombinFit/Rich1MirrCombFit.h"
#include "RichMirrCombinFit/Rich2MirrCombFit.h"
//C++
#include <iostream>
#include <string>
//BOOST
#include <boost/lexical_cast.hpp>
//ROOT
#include <Math/MinimizerOptions.h>

using namespace std;

using boost::lexical_cast;


int main(int argc, char* argv[])
{
  // scenario 1:
  //    whatever.exe fitConfigFile RichDetector
  //    then argc = 3, justGlobalFit is always zero (see below)
  // scenario 2:
  //    whatever.exe fitConfigFile RichDetector justGlobalFit_int
  //    then argc = 4, justGlobalFit is justGlobalFit_int (=0/1) (see below)

  LHCbStyle(true);
  gStyle->SetPalette(kBlueYellow); // was kRainBow, which is deprecated at CERN,
                                   // because it is bad for perception and is therefore
                                   // strongly disfavoured from the cognitive science
                                   // point of view:
                                   // "Data Visualization: The End of the Rainbow"

  cout <<"There were "<<argc<<" arguments passed to RichMirrCombinFit.cpp"<< endl;

  string configFile = lexical_cast<string>(argv[1]);

  int rich = lexical_cast<int>(argv[2]);

  int justGlobalFit = 0;
  if (argc > 3) justGlobalFit = lexical_cast<int>(argv[3]);
  cout <<"justGlobalFit = "<<justGlobalFit<< endl;

  // Initialize an instance of ROOT::Math::MinimizerOptions
  ROOT::Math::MinimizerOptions SetThese;

  // Set options for Minimizer Algorithm, Type, and Strategy
  SetThese.SetDefaultMinimizer("Minuit", "Migrad"); // Minuit and Migrad, use this as first option always

  SetThese.SetDefaultStrategy(1);          // Strategy 1, good for regular fits. For many parameter fits
                                           // we switch to Strategy 0. See below.

//SetThese.SetDefaultMinimizer("Minuit2"); // Minuit   2 (causes more fit fails)
//SetThese.SetDefaultStrategy(2);          // Strategy 2 (causes more fit fails)

  // Set options for MaxFunctionCalls and MaxIterations for Minuit
  int MaxFunctionCalls = 2000000; // Call limits seem to be at most 75000, let's use about 20x that
  int MaxIterations    =   20000; // TMinuit uses 500 as default, let's use 20x that
  SetThese.SetDefaultMaxFunctionCalls(MaxFunctionCalls);
  SetThese.SetDefaultMaxIterations(MaxIterations);

  // Print everything so we can see it
  cout <<"DefaultMinimizerType    "<<SetThese.DefaultMinimizerType()   << endl;
  cout <<"DefaultMinimizerAlgo    "<<SetThese.DefaultMinimizerAlgo()   << endl;
  cout <<"DefaultStrategy         "<<SetThese.DefaultStrategy()        << endl;
  cout <<"DefaultMaxFunctionCalls "<<SetThese.DefaultMaxFunctionCalls()<< endl;
  cout <<"DefaultMaxIterations    "<<SetThese.DefaultMaxIterations()   << endl;
  cout << endl;

  IMirrCombFit* combFit = 0;

  if      (rich == 1) {
    combFit = new Rich1MirrCombFit();
    cout <<"Constructed Rich1MirrCombFit()"<< endl;
  }
  else if (rich == 2) {
    combFit = new Rich2MirrCombFit();
    cout <<"Constructed Rich2MirrCombFit()"<< endl;
  }
  else return 0;

  combFit->ReadConfFile(configFile);
  cout <<"Read config file: "<<configFile<< endl;

  /*
  if (justGlobalFit) {
    double shift = 0.0;
    combFit->SetShift(shift);
    combFit->GlobalFit();
  }
  else { // normal case
    double shift = 0.0;
    combFit->SetShift(shift);

    cout <<"Fit Histos"<< endl;
    combFit->FitHistos();

    cout <<"Write Output File"<< endl;
    combFit->PrintResults();

    combFit->StopOrContinue(); // Creates a file that as far as I can tell gets overwritten by RichMirrAlign anyway. Candidate for removal.
  }
  */

  if (true) { // this is intentional

    double shift = 0.0;
    combFit->SetShift(shift);

    cout <<"Monitoring Histos"<< endl; // plots for Data Manager, only the untilted one is really useful though
    combFit->GlobalFit();

    // Switch to Strategy 0 (strategy zero is good for many-parameter systems)
    int Strategy2D = 0;
    SetThese.SetDefaultStrategy(Strategy2D);
  //SetThese.SetDefaultMinimizer("Minuit", "Minimize"); // Use MINIMIZE[see Minuit manual] (maybe fewer fits will fail)

    cout <<"Minuit options have changed for Fit Histos!" << endl;

    cout <<"DefaultMinimizerType    "<<SetThese.DefaultMinimizerType()   << endl;
    cout <<"DefaultMinimizerAlgo    "<<SetThese.DefaultMinimizerAlgo()   << endl;
    cout <<"DefaultStrategy         "<<SetThese.DefaultStrategy()        << endl;
    cout <<"DefaultMaxFunctionCalls "<<SetThese.DefaultMaxFunctionCalls()<< endl;
    cout <<"DefaultMaxIterations    "<<SetThese.DefaultMaxIterations()   << endl;
    cout << endl;

    cout <<"Fit Histos"<< endl; // The important stuff & additional plots for RICH Piquet (not made yet)
    combFit->FitHistos();

    cout <<"Write Output File"<< endl;
    combFit->PrintResults();

    combFit->CombinFitDecision(); // Creates a file that lets you know if mirror combination's tilts are over m_warningFactor*stopTolerance.
  }
  else {
    cout <<"This line should have never printed. There is an error."<< endl;
  }

  return 0;
}
