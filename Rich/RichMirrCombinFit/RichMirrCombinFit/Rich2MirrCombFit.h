#ifndef RICHMIRRCOMBINFIT_RICH2MIRRCOMBFIT_H
#define RICHMIRRCOMBINFIT_RICH2MIRRCOMBFIT_H

//local
#include "RichMirrCombinFit/MirrCombFit.h"
//ROOT
#include <TGraphErrors.h>
#include <TH2D.h>


class Rich2MirrCombFit : public MirrCombFit
{
public:

  Rich2MirrCombFit();
  ~Rich2MirrCombFit();

  virtual void GetFitParams(TH2D*);

  void         FitSurface(bool);

  void         FitBySlice();

  void         FitSlices(TGraphErrors*);

  void         Fit2D(TGraphErrors*);

protected:

private:

  TH2D* m_histo;

};

#endif
