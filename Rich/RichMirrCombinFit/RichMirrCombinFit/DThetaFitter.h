#ifndef RICHMIRRCOMBINFIT_DTHETAFITTER_H
#define RICHMIRRCOMBINFIT_DTHETAFITTER_H

//C++
#include <sstream>
#include <string>
//ROOT
#include <TH1D.h>
//RooFit
#include <RooDataHist.h>
#include <RooRealVar.h>
#include <RooWorkspace.h>


class DThetaFitter{

  template <class typeToString> std::string makeString(const typeToString& thingToString)
  {
    std::stringstream ss;
    ss << thingToString;
    return ss.str();
  }

  RooWorkspace* _wspace;
  RooDataHist*  _dThetaHist;
  double        _dThetaMin;
  double        _dThetaMax;
  int           _fitShape;

  void buildPdfBifurGauss(int backgroundOrder);
  void buildPdfGauss(     int backgroundOrder);

  int fit();

public:

  DThetaFitter(float dThetaMin=-0.007, float dThetaMax=0.007, int fitShape=0,  int backgroundOrder=2);

  double getMean()     const;
  double getMeanErr()  const;

  double getWidth()    const;
  double getWidthErr() const;

  void loadHistogram(std::string filePath, std::string histogramPath="RICH/RichAlignMoniR1Gas/deltaThetaUnamb");
  void loadHistogram(TH1D* histogram);

  double plot(std::string plotDirectory, double plotIfChi2GreaterThan=0.0);

  void setFitShape(int fitShape) { _fitShape = fitShape; }

  void setMeanLimits(double min, double max) {
    RooRealVar* dTheta = _wspace->var ("gaussMean");
    if (min != -1.0) dTheta->setMin(min);
    if (max != -1.0) dTheta->setMax(max);
  }
  void setWidthLimits(double min, double max) {
    RooRealVar* dTheta = _wspace->var ("gaussWidth");
    if (min != -1.0) dTheta->setMin(min);
    if (max != -1.0) dTheta->setMax(max);
  }

  void setConstantMean(double mean);
  void setMean(double mean);
  void setWidth(double width);


  ~DThetaFitter();

};

#endif
