#ifndef RICHMIRRCOMBINFIT_RICH1MIRRCOMBFIT_H
#define RICHMIRRCOMBINFIT_RICH1MIRRCOMBFIT_H

//local
#include "RichMirrCombinFit/MirrCombFit.h"
//ROOT
#include <TGraphErrors.h>
#include <TH2D.h>


class Rich1MirrCombFit : public MirrCombFit
{
public:

  Rich1MirrCombFit();
  ~Rich1MirrCombFit();

  virtual void GetFitParams(TH2D*);

  void         FitSurface(bool);

  void         FitBySlice();

  void         FitSlices(TGraphErrors*);

  void         Fit2D(TGraphErrors*);

protected:

private:

  TH2D* m_histo;

};

#endif
