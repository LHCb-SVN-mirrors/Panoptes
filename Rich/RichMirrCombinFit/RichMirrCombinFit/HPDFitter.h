#ifndef RICHMIRRCOMBINFIT_HPDFITTER_H
#define RICHMIRRCOMBINFIT_HPDFITTER_H

//C++
#include <string>
//ROOT
#include <TGraphErrors.h>
#include <TH2D.h>
//Roofit
#include <RooRealVar.h>


class HPDFitter
{
public:

  HPDFitter(TH2D* histo, int Rich);
  ~HPDFitter();

  std::string HPDNum();
  void        Fit();
  void        FitProjection();
  void        SetRange(double low, double high);
  void        Write(const char* file);

protected:

  void        Fit2D(TGraphErrors* cleanPlot);
  void        FitSlice(int i, TGraphErrors* cleanPlot);

private:

  std::string          HPD;
  RooRealVar*          m_dTheta;
  int                  m_fixSinusoidShift;
  TH2D*                m_histo;
  double               m_sinusoidShift;
  const int            RICH;
  double               sin, sinErr, cos, cosErr;
  static const double  TDRsigma[2];

};

#endif
