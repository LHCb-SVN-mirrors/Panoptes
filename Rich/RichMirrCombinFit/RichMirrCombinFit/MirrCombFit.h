#ifndef RICHMIRRCOMBINFIT_MIRRCOMBFIT_H
#define RICHMIRRCOMBINFIT_MIRRCOMBFIT_H

//Local
#include "RichMirrCombinFit/IMirrCombFit.h"
#include "RichMirrCombinFit/MirrComb.h"
//C++
#include <iostream>
#include <string>
#include <map>
#include <vector>
//ROOT
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>


typedef std::map<std::string, MirrComb> MapCombNamCombObj;

class MirrCombFit : public IMirrCombFit
{
public:

  MirrCombFit();
  virtual ~MirrCombFit();

  void         MirrCombList();

  void         HistoList();

  std::string  MirrCombNam(int pri, int sec);


  virtual void ReadConfFile(std::string confFile);

  virtual void FitHistos();

  virtual void StopOrContinue();

  virtual void CombinFitDecision();

  virtual void PrintResults();

  virtual void GlobalFit();


  virtual void GetFitParams(TH2D*) = 0;


  double       GetGlobalFitMean(TH1D* histo);

  void         SetShift(double shift);

  void         BinCounter(TH2D*);

  void         Print(std::ostream& out) const;


private:

  void         OpenFile();

  TFile* f;


protected:

  void         CreateMapCombNamCombObj();

  int                      m_backgroundOrder;
  double                   m_begRange;
  std::string              m_combAndMirrSubsetsFile;
  double                   m_deltaThetaWindow;
  double                   m_endRange;
  int                      m_fixSinusoidShift;
  std::vector<TString>     m_histoNames;
  int                      m_fitMethod;
  std::string              m_inputHistosFile;
  int                      m_iterationCount;
  double                   m_minAverageBinPop;
  std::vector<std::string> m_mirrCombList;
  MapCombNamCombObj        m_mapNamObj;
  std::string              m_outputResultsFile;
  int                      m_phiBinFactor;
  std::string              m_plotDir;
  int                      m_plotOutputLevel;
  int                      m_priMirrs;
  std::string              m_rich;
  int                      m_secMirrs;
  double                   m_sigmaTDR;
  double                   m_sinusoidShift;
  double                   m_stopTolerance;
  std::string              m_thisVariant;
  int                      m_useGlobalFitMean;
  double                   m_warningFactor;
  int                      m_zeroGlobalFitMean;

};

std::ostream& operator<<(std::ostream& out, const MirrCombFit& fit);

#endif
