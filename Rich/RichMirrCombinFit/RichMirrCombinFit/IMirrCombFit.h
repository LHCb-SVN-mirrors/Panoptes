#ifndef RICHMIRRCOMBINFIT_IMIRRCOMBFIT_H
#define RICHMIRRCOMBINFIT_IMIRRCOMBFIT_H

//C++
#include <string>


class IMirrCombFit
{
public:

  virtual     ~IMirrCombFit() {}

  virtual void ReadConfFile(std::string confFile) = 0;

  virtual void SetShift(double shift) = 0;

  virtual void FitHistos() = 0;

  virtual void StopOrContinue() = 0;

  virtual void CombinFitDecision() = 0;

  virtual void PrintResults() = 0;

  virtual void GlobalFit() = 0;

};

#endif
