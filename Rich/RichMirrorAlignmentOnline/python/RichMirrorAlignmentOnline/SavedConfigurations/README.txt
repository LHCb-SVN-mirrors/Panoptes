Active Configurations

16xxxx_DraftInitialConfig.txt - Draft initial configuration for 2016 running
                                                    should have RICH1 coeffCalibTilt 0.7
                                                    magnifCoeffMode 2 [need to recalculate magnification factors before stable running!]
                                                    solutionMethod 0
                                                    fitMethod 5
                                RECOMMENDED TO START FROM NULLIFIED CONDDB!!!

Archived Configurations

v1r1_Configurations/160221_DefaultConfiguration.txt - same as original/Configuration_orig.txt, but added a couple help lines which are hopefully harmless 
                                                      (but need to be updated with the real information)

v1r1_Configurations/160221_SM0.txt - Change to Anatoly's minuit method with regularization for both RICHes. 
                                     Start from existing FIXED magnification coefficients.

v1r1_Configurations/16xxxx_DraftInitialConfig.txt - Draft initial configuration for 2016 running
                                                    should have RICH1 coeffCalibTilt 0.7 
                                                    magnifCoeffMode 2 [need to recalculate magnification factors before stable running!]
                                                    solutionMethod 0
                                                    fitMethod 5
                                                    RECOMMENDED TO START FROM NULLIFIED CONDDB!!!

original/Configuration_orig.txt - what was there when we started SavedConfigurations

Removed Configurations:

160221_SM0_R1_CCT0.8_MCM2.txt - Change to Anatoly's minuit method with regularization for both RICHes. 
                                 and ONLY for RICH1:
                                  coeffCalibTilt changed from default to 0.8
                                  magnifCoeffMode changed from default to 2 (recalculate magnification factors  in each iteration)

