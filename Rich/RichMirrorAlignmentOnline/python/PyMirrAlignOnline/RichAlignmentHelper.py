import os
import re
import shutil
import time
from   time       import sleep
from   time       import time
from   time       import gmtime, strftime
from   subprocess import *

class RichAlignmentHelper:
    def __init__(self, configuration, _combAndMirrSubsets):
        self.alignConf = configuration
        self.combAndMirrSubsets = _combAndMirrSubsets
        self.thisNameStr = self.alignConf.getProp('nameStr')
        self.tiltNames = self.alignConf.getProp('tiltNames')
        self.magnifDir = self.alignConf.getProp('magnifDir')
        self.workdir = self.alignConf.getProp('WorkDir')
        self.whichRich = self.alignConf.getProp('Rich')
        self.deltaThetaWindow = self.alignConf.getProp('deltaThetaWindow')
        self.minAverageBinPop = self.alignConf.getProp('minAverageBinPop')
        self.phiBinFactor = self.alignConf.getProp('phiBinFactor')
        self.combinFitMethod = self.alignConf.getProp('combinFitMethod')
        self.coeffCalibTilt = self.alignConf.getProp('coeffCalibTilt')
        self.magnFactorsMode = self.alignConf.getProp('magnFactorsMode')
        self.solutionMethod = self.alignConf.getProp('solutionMethod')
        self.fixSinusoidShift = self.alignConf.getProp('fixSinusoidShift')
        self.stopTolerance = self.alignConf.getProp('stopTolerance') 
        self.stopToleranceSec = self.alignConf.getProp('stopToleranceSec') 

    def runMirrCombinFit(self, n_it):
        ps = []
        
        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames
            
        for tiltName in _tiltNames :
            if(tiltName == ""):
                connectStr = ""
            else:
                connectStr = "_"
                
            combinFitDir =  self.workdir + "/Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + "/"
            combinFitPlotDir = combinFitDir + "/plots/"
            if os.path.exists(combinFitDir):
                shutil.rmtree(combinFitDir)
            os.makedirs(combinFitDir)
            os.makedirs(combinFitPlotDir)
            
            combinResultsFile = combinFitDir + "Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            fitConfigFile = combinFitDir + "Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".conf"
            fitOutputFile = combinFitDir + "Rich" + str(self.whichRich) + "MirrCombinFitOut_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            fitStdOutFile = combinFitDir + "Rich" + str(self.whichRich) + "MirrCombinFitStdOut_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            fitStdErrFile = combinFitDir + "Rich" + str(self.whichRich) + "MirrCombinFitStdErr_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            
            histoFile = "RichRecQCHistos_rich" + str(self.whichRich) + "_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".root"
            
            if os.path.exists(fitConfigFile) :
                os.remove(fitConfigFile)
            f = open(fitConfigFile, 'w')
            args = 'plotDir               = ' + combinFitPlotDir + '\n'
            args += 'richDetector          = ' + str(self.whichRich) + '\n'
            args += 'combAndMirrSubsets    = ' + self.combAndMirrSubsets + '\n'
            args += 'minAverageBinPop      = ' + str(self.minAverageBinPop) + '\n'
            args += 'phiBinFactor          = ' + str(self.phiBinFactor) + '\n'
            args += 'deltaThetaWindow      = ' + str(self.deltaThetaWindow) + '\n'
            args += 'combinFitMethod       = ' + str(self.combinFitMethod) + '\n'
            args += 'iterationCount        = ' + str(n_it) + '\n'
            args += 'inputHistos           = ' + str(self.workdir + histoFile) + '\n'
            args += 'mirrCombinFitResults  = ' + str(combinResultsFile) + '\n'
            args += 'thisVariant           = ' + str(self.thisNameStr) + '\n'
            args += 'fixSinusoidShift      = ' + str(self.fixSinusoidShift) + '\n'
            args += 'plotOutputLevel       = 2 \n'
            f.write(args)
            f.close()
            
            myStdOut = open(fitStdOutFile, 'w')
            myStdErr = open(fitStdErrFile, 'w')
            cmd = "`which RichMirrCombinFit.exe` " + fitConfigFile + " " + str(self.whichRich) + " 0 > " + fitOutputFile
            p2 = Popen(cmd, shell=True, executable="/bin/bash", stdout=myStdOut, stderr=myStdErr)
            myStdOut.close()
            myStdErr.close()
            ps.append(p2)
            
            PopenCount = -1;
            while True:
                sleep(30)
                ps_status = [q.poll() for q in ps]
                if all([x is not None for x in ps_status]):
                    PopenTotal = 0
                    for l in ps_status:
                        if l is not None : PopenTotal += 1
                    print "%d of 9 RichMirrCombinFit processes complete" % PopenTotal
                    print strftime("%Y-%m-%d %H:%M:%S", gmtime())
                    print "-------------------"
                    break
                else:
                    PopenDone = 0
                    for l in ps_status:
                        if l is not None : PopenDone += 1
                    if PopenDone > PopenCount:
                        print "%d of 9 RichMirrCombinFit processes complete" % PopenDone
                        print strftime("%Y-%m-%d %H:%M:%S", gmtime())
                        print "-------------------"
                        PopenCount = 0 + PopenDone


    def runRichAlign(self, n_it):
        pNoTilt = []
        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames
            
        for tiltNum, tiltName in enumerate(_tiltNames) :
            if(tiltName == ""):
                connectStr = ""
            else:
                connectStr = "_"
                
            combinFitDir = self.workdir + "/Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + "/"
            print "combinFitDir " + combinFitDir
            combinResultsFile = combinFitDir + "/Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            print "combinResultsFile " + combinResultsFile
            magnificationOuputFile = self.workdir + "Rich" + str(self.whichRich) + "MirrMagnFactors_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            
            p = []
            with open(combinResultsFile) as fitfile:
                line = fitfile.readline()
                while line:
                    pars = line.split()
                    if len(pars) is not 7:
                        line = fitfile.readline()
                        continue
                    newpars = []
                    for i, j in enumerate(pars):
                        if i == 0:
                            newpars.append(j)
                        else:
                            newpars.append(float(j))
                    p.append(newpars)
                    if tiltNum == 0 :
                        pNoTilt.append(newpars)
                    line = fitfile.readline()

            with open(magnificationOuputFile, 'w') as magfile:
                for i, sp in enumerate(p) :
                    tiltSign = 1.0;
                    if tiltName.count('neg') > 0:
                        tiltSign = -1.0
                    mirrPair = sp[0]
                    Y = (sp[1] - pNoTilt[i][1]) / (self.coeffCalibTilt * tiltSign)
                    Z = (sp[3] - pNoTilt[i][3]) / (self.coeffCalibTilt * tiltSign)
                    magfile.write(mirrPair + "  " + str(Y) + "  " + str(Z) + "\n")
        
        
        richMirrAlignConfFile = self.workdir + "Rich" + str(self.whichRich) + "MirrAlign_i" + str(n_it) + ".conf"
        richMirrAlignOutFile = self.workdir + "Rich" + str(self.whichRich) + "MirrAlignOut_i" + str(n_it) + ".txt"
        richMirrAlignNanOutFile = self.workdir + "Rich" + str(self.whichRich) + "MirrAlignNanOut_i" + str(n_it) + ".txt"
        
        combinFitDir = self.workdir + "/Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + "_i" + str(n_it) + "/"
        combinResultsFile = combinFitDir + "/Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + "_i" + str(n_it) + ".txt"
        print "combinFitDir " + combinFitDir
        print "combinResultsFile " + combinResultsFile
        
        currentMirrorXMLFile = self.workdir + "Rich" + str(self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i" + str(n_it) + ".xml"
        nextIterationXMLFile = self.workdir + "Rich" + str(self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i" + str(n_it + 1) + ".xml"
        zeroMirrorXMLFile = self.workdir + "Rich" + str(self.whichRich) + "CondDBUpdate_" + self.thisNameStr + "_i0.xml"
        
        if os.path.exists(richMirrAlignConfFile) :
            os.remove(richMirrAlignConfFile)
        f = open(richMirrAlignConfFile , 'w')
        args = 'workDir              = ' + self.workdir + '\n'
        args += 'thisVariant          = ' + self.thisNameStr + '\n'
        args += 'richDetector         = ' + str(self.whichRich) + '\n'
        args += 'magnFactorsMode      = ' + str(self.magnFactorsMode) + '\n'
        args += 'solutionMethod       = ' + str(self.solutionMethod) + '\n'
        args += 'usePremisaligned     = ' + "false" + '\n'
        args += 'iterationCount       = ' + str(n_it) + '\n'
        args += 'combAndMirrSubsets   = ' + self.combAndMirrSubsets + '\n'
        args += 'mirrCombinFitResults = ' + combinResultsFile + '\n'
        args += 'zerothIterationXML   = ' + zeroMirrorXMLFile + '\n'
        args += 'currentIterationXML  = ' + currentMirrorXMLFile + '\n'
        args += 'nextIterationXML     = ' + nextIterationXMLFile + '\n'
        args += 'stopTolerance        = ' + str(self.stopTolerance) + '\n'
        args += 'stopToleranceSec     = ' + str(self.stopToleranceSec) + '\n'        
        f.write(args)
        f.close()
        
        fitStdOutFile = self.workdir + "/Rich" + str(self.whichRich) + "MirrAlignStdOut_i" + str(n_it) + ".txt"
        fitStdErrFile = self.workdir + "/Rich" + str(self.whichRich) + "MirrAlignStdErr_i" + str(n_it) + ".txt"
        myStdOut = open(fitStdOutFile, 'w')
        myStdErr = open(fitStdErrFile, 'w')
        cmd = "`which RichMirrAlign.exe` " + richMirrAlignConfFile + " > " + richMirrAlignOutFile,
        p = Popen(cmd, shell=True, executable="/bin/bash", stdout=myStdOut, stderr=myStdErr)
        p.communicate()
        myStdOut.close()
        myStdErr.close()


    def checkFitQual(self, n_it):
        if self.magnFactorsMode == 0:
            _tiltNames = [""]
        else:
            _tiltNames = self.tiltNames
            
        for tiltName in _tiltNames :
            if(tiltName == ""):
                connectStr = ""
            else:
                connectStr = "_"

            combinFitDir = self.workdir + "/Rich" + str(self.whichRich) + "MirrCombinFit_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + "/"
            fitOutputFile = combinFitDir + "Rich" + str(self.whichRich) + "MirrCombinFitOut_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            fitNGEFile = combinFitDir + "Rich" + str(self.whichRich) + "MirrCombinFitNGEOut_" + self.thisNameStr + connectStr + tiltName + "_i" + str(n_it) + ".txt"
            
            os.system('cat ' + fitOutputFile + ' | grep " not_good_enough " > ' + fitNGEFile)
            num_lines = sum(1 for line in open(fitNGEFile))
            if (num_lines != 0):
                print "NOT ENOUGH ENTRIES IN THE HISTOS FOR PROPER FITSING!!!"
                print "WILL DO IT ANYWAY FOR NOW :D :D :D"


    def hasConverged(self):
        Rich_stop_or_continue_txt = open(self.workdir + "/Rich" + str(self.whichRich) + "_stop_or_continue.txt")
        verdict = Rich_stop_or_continue_txt.readline().strip().upper()
        Rich_stop_or_continue_txt.close()
        if (verdict == "STOP") :
            return True
        if (verdict != "STOP" and verdict != "STOP_NAN" and verdict != "STOP_NGE") :
            return False
