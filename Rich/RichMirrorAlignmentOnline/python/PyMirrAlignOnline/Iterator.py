from Communicator import *
import distutils
import os
import re
import shutil
import sys
import math
import time
from   time       import sleep
from   time       import time
from   time       import gmtime, strftime
import pydim
import threading
import Configurables
from   subprocess import *
from   distutils  import dir_util


########################################################################################################################                                    
##### Here is where the party starts!
########################################################################################################################     
def run(whichRich):
  ### define the communicator who will communicate with the run-control
  com = Communicator('ALIGNITER')
  state = State.NOT_READY
  com.set_status(state)
  
  from HistoHelper import SaveSetRetriever
  from HistoHelper import HistoCopier
  ssRetr = None  #the SavesetRetriever, this thing will give us the path to the histogram files produced by the analyzers
  histCopier = None  #the Histcopier, it will wait after each minor iteration for the savesets to be written and copy it under the approriate name into workdir
  from XMLFileHelper import XMLFileHelper
  xmlHelper = None  
  
  from RichAlignmentHelper import RichAlignmentHelper
  alignHelper = None

  from SetupHelper import SetupHelper
  setupHelper = None

  ########################################################################################################################
  #####  reading in the seetings from the Configuration file
  ########################################################################################################################
  if whichRich == 1:
    from Configurables import Rich1MirrAlignOnConf
    alignConf = Rich1MirrAlignOnConf()
  elif whichRich == 2:
    from Configurables import Rich2MirrAlignOnConf
    alignConf = Rich2MirrAlignOnConf()
    
  alignConf.__apply_configuration__()
  thisCase = alignConf.getProp('nameStr')
  workdir = alignConf.getProp('WorkDir')
  histdir = alignConf.getProp('HistoDir')
  tiltNames = alignConf.getProp('tiltNames')
  thisNameStr = alignConf.getProp('nameStr')
  magnFactorsMode = alignConf.getProp('magnFactorsMode')
  
  ### this is the place where the analyzers will pick up the xml file for their Brunel jobs
  ### before each new minor iteration the new xml file will be copied here
  currentXML = workdir + "/CondDB_Rich" + str(whichRich) + ".xml"
  combAndMirrSubsets = workdir + "Rich" + str(whichRich) + "CombAndMirrSubsets.txt"
  
  ########################################################################################################################
  ##### iteration counters, n_it for major iteration, m_it for minor iteration
  ########################################################################################################################
  n_it = alignConf.getProp("MajItStart")
  m_it = alignConf.getProp("MinItStart")
  print "INFO: m_it = ", m_it

  conv = False  ### varibale indicates if alignment converged yet
  while True:
    command = com.get_command()
    ########################################################################################################################                                
    ##### this code will be excecuted when you klick configure on the run-control
    ########################################################################################################################       
    if command.startswith('configure') and state == State.NOT_READY:
      setupHelper = SetupHelper(alignConf)
      xmlHelper = XMLFileHelper(alignConf)
      ssretr = SaveSetRetriever("LHCbA", "AligWrk_Rich" + str(whichRich))
      histCopier = HistoCopier(alignConf, ssretr)
      alignHelper = RichAlignmentHelper(alignConf, combAndMirrSubsets)
      state = State.READY
      
    ########################################################################################################################                                
    ##### setting up the alignment and starting the first minor iteration (this only for the very first iteration)           
    ######################################################################################################################## 
    elif (command.startswith('start') and state == State.READY):
      ### write the file for the analyzers to know which iteration they are at
      f = open(alignConf.getProp("ItNrFile"), "w")
      f.write(str(m_it))
      f.close()
      t = time()

      ### if you want to start the alignment from scratch (e.g. at the m_it = 0) 
      if m_it == 0:
        #clean up the working directory
        print "INFO: Setting up the folders and getting the starting XML"
        print "INFO: Removing everything in " + workdir
        for f in os.listdir(workdir):
          if os.path.isdir(workdir + "/" + f):
            shutil.rmtree(workdir + "/" + f)
          else:
            os.remove(workdir + "/" + f)
            
        shutil.copyfile("/group/rich/AlignmentFiles/Rich" + str(whichRich) + "CombAndMirrSubsets.txt", combAndMirrSubsets)
        currentMirrorXMLFile = workdir + "/Rich" + str(whichRich) + "CondDBUpdate_" + thisNameStr + "_i" + str(n_it) + ".xml"
        
        xmlHelper.getStartXML(currentMirrorXMLFile)   ### get the starting-xml from the database 
        if not magnFactorsMode == 0:
          xmlHelper.create_coeff_calibration_xml_files(currentMirrorXMLFile, n_it)    ### create the xml files for the tilted mirror configurations 
        
      ### if you want to start from some arbitrary minor iteration
      else:
        #this method will keep all files needed when starting at m_it > 0
        setupHelper.startMinIt(m_it)

      ### now place the xml file for the current minor iteration in the place where the analyzers will pick it up
      if m_it % 9 == 0:
        pickupXML = workdir + "Rich" + str(whichRich) + "CondDBUpdate_" + thisNameStr + "_i" + str(n_it) + ".xml"
      else:
        pickupXML = workdir + "Rich" + str(whichRich) + "CondDBUpdate_" + thisNameStr + "_" + tiltNames[m_it % 9] + "_i" + str(n_it) + ".xml"
      shutil.copyfile(pickupXML, currentXML)
      
      print "\n \n Start new major Iteration " + str(n_it) + "\n \n"
      print "    Minor iteration " + str(m_it)
      
      # increate the minor iteration counter
      if magnFactorsMode == 0:
        setupHelper.setupMagnifFiles()
        m_it = m_it + 9
      else:
        m_it = m_it + 1
      # set iterator state to running => analyzers will exectute the Brunel jobs now 
      state = State.RUNNING
      
    ######################################################################################################################## 
    ##### here the code for all the following iterations
    ########################################################################################################################  
    elif (command.startswith('pause') and state == State.RUNNING):
      state = State.PAUSED
      com.set_status(state)
      
      # write new minor iteration number to file for the analyzers to read
      f = open(alignConf.getProp("ItNrFile"), "w")
      f.write(str(m_it))
      f.close()
      
      # copy the saveset made by the analyzers in the last iteration to the desired place in workdir
      if magnFactorsMode == 0:
        histCopier.getHistoFile(n_it, m_it - 9);
      else:
        histCopier.getHistoFile(n_it, m_it - 1);
      
      # if the minor iterations for one major iteration have finished perform fits etc: 
      if(m_it % 9 == 0):
        print "    minor iterations for major iteration " + str(n_it) + " finished"
        print "    now merge the rootfiles and perfrom alignment"        
        
        ### run the mirror Alignment
        alignHelper.runMirrCombinFit(n_it)
        alignHelper.checkFitQual(n_it)
        alignHelper.runRichAlign(n_it)
        
        ### pick up the new xml file and format in in a way readable for the analyzers
        newXMLfile = workdir + "Rich" + str(whichRich) + "CondDBUpdate_" + thisNameStr + "_i" + str(n_it + 1) + ".xml"
        xmlHelper.reformatXML(newXMLfile, workdir)
        
        # ## check if the alignment has converged or failed
        conv = alignHelper.hasConverged()
        print " Error: Alignment after ", n_it, " Iteration is converged is ", conv

        if(n_it >= 9 or conv):
          if conv and n_it >= 9:
            print "Alignment has reached its maximum number of iteration and has converged!"
          elif conv:
            print "Alignment has converged after the ", n_it, "th iteration!"
          else:
            print "Alignment has reached its maximum number of iterations and has not converged. :("
          setupHelper.finalize(conv, n_it)
          print "Error: state = READY!!!"
          state = State.READY
        

      # ## if the alignment has not converged and the maximal number of iterations has not been reached,
      # ## set up everything for the next major iteration
        if n_it < 9 and not conv:
          n_it = n_it + 1
          print "\n \n Start new major Iteration " + str(n_it) + "\n \n"
          ### get the xml-file produced by the last fit and create the ones with the tiled mirrors
          currentMirrorXMLFile = workdir + "Rich" + str(whichRich) + "CondDBUpdate_" + thisNameStr + "_i" + str(n_it) + ".xml"
          if not magnFactorsMode == 0:
            xmlHelper.create_coeff_calibration_xml_files(currentMirrorXMLFile, n_it)
              
      ### just start the next minor iteration if not converged yet
      if n_it < 9 and not conv:
        print "Major iteration " + str(n_it)
        print "    Minor iteration " + str(m_it)
        if(m_it % 9 == 0):
          pickupXML = workdir + "Rich" + str(whichRich) + "CondDBUpdate_" + thisNameStr + "_i" + str(n_it) + ".xml"
        else:
          pickupXML = workdir + "Rich" + str(whichRich) + "CondDBUpdate_" + thisNameStr + "_" + tiltNames[m_it % 9] + "_i" + str(n_it) + ".xml"
          
        shutil.copyfile(pickupXML, currentXML)
        if magnFactorsMode == 0:
          m_it = m_it + 9
        else:
          m_it = m_it + 1
        state = State.RUNNING
      sleep(2)
      
    elif command.startswith('stop') and state in (State.RUNNING, State.READY):
      setupHelper.finalize(conv, n_it)
      state = State.READY
    elif command.startswith('reset'):
      state = State.NOT_READY
      break
    else:
      print 'iterator: bad transition from %s to %s' % (state, command)
      state = State.ERROR
      break
    # Set our status
    com.set_status(state)
    # Set our status one last time
  com.set_status(state)


if __name__ == '__main__':
    run()


