import distutils
import os
import re
import shutil
import sys
import math
import time
from   time       import sleep
from   time       import time
from   time       import gmtime, strftime
import pydim
import threading
import Configurables
from   subprocess import *
from   distutils  import dir_util


class XMLFileHelper:
    def __init__(self, configuration):
        self.alignConf = configuration
    
    def create_coeff_calibration_xml_files(self, inputFile, n_it):
        print "INFO: start create coeff"
        writedir = self.alignConf.getProp('WorkDir')
        coeffCalibTilt = self.alignConf.getProp('coeffCalibTilt')
        whichRich = self.alignConf.getProp('Rich')
        thisNameStr = self.alignConf.getProp('nameStr')
        
        import fpformat
        from   array        import array
        from   lxml         import etree
        
        signName = dict({  '-': 'neg'            , '+': 'pos'            })
        signToShift = dict({  '-':-coeffCalibTilt , '+':  coeffCalibTilt })
        mirrTypeName = dict({'pri': 'Sph', 'sec': 'Sec'})
        totMirrNum = dict({'pri':   56 , 'sec':   40 })
        if whichRich == 2 :
            totMirrNum = dict({'pri':   56 , 'sec':   40 })
        if whichRich == 1 :
            totMirrNum = dict({'pri':    4 , 'sec':   16 })
            
        seqNum = dict({  'Y':    1 , 'Z':    2 })
        rotRE = re.compile(r'[\-|\+]?[0-9]+\.?[0-9]*|[\-|\+]?\.[0-9]+')
        rotValues = []
        
        for mirrType in ['pri', 'sec'] :
            for axis in ['Y', 'Z'] :
                for sign in ['-', '+'] :
                    parser = etree.XMLParser(remove_blank_text=True)
                    RichAlignmentConditions = etree.parse(inputFile, parser)
                    for mirrNum in range (totMirrNum[ mirrType ]) :
                        floatRotValues = array('f')
                        if (whichRich == 1) :
                            dRotXYZ = RichAlignmentConditions.xpath("//condition[@name='" + mirrTypeName[ mirrType ] + "Mirror" + str(mirrNum) + "_Align']/paramVector[@name='dRotXYZ']")
                        if (whichRich == 2) :
                            dRotXYZ = RichAlignmentConditions.xpath("//condition[@name='Rich" + str(whichRich) + mirrTypeName[ mirrType ] + "Mirror" + str(mirrNum) + "_Align']/paramVector[@name='dRotXYZ']")
                        rotValues = rotRE.findall(etree.tostring(dRotXYZ[0], pretty_print=True, method="text"))
                        for i in range (3) :
                            floatRotValues.append(float(rotValues[i]))
                        floatRotValues[ seqNum[ axis ] ] += signToShift[ sign ]
                        dRotXYZ[0].text = fpformat.fix(floatRotValues[0], 5) + '*mrad ' + fpformat.fix(floatRotValues[1], 5) + '*mrad ' + fpformat.fix(floatRotValues[2], 5) + '*mrad '
                        if axis == 'Y' :
                            signCombinName = signName[ sign ] + 'YzerZ'
                        else           :
                            signCombinName = 'zerY' + signName[ sign ] + 'Z'
                        newXML_File = open(writedir + '/Rich' + str(whichRich) + 'CondDBUpdate_' + thisNameStr + "_" + mirrType + "_" + signCombinName + "_i" + str(n_it) + ".xml", 'w')
                        newXML_File.write(etree.tostring(RichAlignmentConditions, encoding='iso-8859-1', xml_declaration=True, pretty_print=True, with_tail=False))
                        newXML_File.close()



    def getStartXML(self, startxml):
        workdir = self.alignConf.getProp('WorkDir')
        whichRich = self.alignConf.getProp('Rich')
        
        import os, re
        base_dir = '/group/online/alignment/Rich' + str(whichRich) + '/MirrorAlign/'
        
        re_version = re.compile(r"^v([0-9]+)\.xml$")
        def get_version(entry):
            r = re_version.match(entry)
            if not r:
                return -1
            else:
                return int(r.group(1))

        latest = max(os.listdir(base_dir), key=get_version)
        latest = os.path.join(base_dir, latest)
        
        with open(startxml, 'w') as outfile:
            outfile.write('<?xml version=\'1.0\' encoding=\'iso-8859-1\'?> \n')
            outfile.write('<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n')
            outfile.write('<DDDB> \n')
            with open(latest) as infile:
                for line in infile:
                    outfile.write(line)
                outfile.write('</DDDB> \n')
                    
        print "This files was picked up as the starting xml: ", latest


    def finalXML(self, lastIt):
        workdir = self.alignConf.getProp('WorkDir')
        whichRich = self.alignConf.getProp('Rich')
        
        finalXML = workdir + "/CondDB_Update_Rich" + str(whichRich) + ".xml"
        tocopyXML = workdir + "/CondDB_Rich" + str(whichRich) + "_i" + str(lastIt) + ".xml"
        
        with open(tocopyXML, 'r') as infile:
            with open(finalXML, 'w') as outfile:
                for line in infile:
                    if line != "<?xml version=\'1.0\' encoding=\'iso-8859-1\'?> \n" + "\n" and line != '<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n' and line != '<DDDB> \n' and line != '</DDDB> \n':
                        output.write(line)


    def reformatXML(self, newXMLfile, workdir):
        temp = workdir + "temp.xml"
        counter = 0
        with open(newXMLfile, 'r') as infile:
            with open(temp, 'w') as outfile:
                for line in infile:
                    counter = counter + 1
                    if counter == 1:
                        outfile.write('<?xml version=\'1.0\' encoding=\'iso-8859-1\'?> \n')
                        outfile.write('<!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"> \n')
                        outfile.write('<DDDB> \n')
                    else:
                        outfile.write(line)
            shutil.copyfile(temp, newXMLfile)
            os.remove(temp)
