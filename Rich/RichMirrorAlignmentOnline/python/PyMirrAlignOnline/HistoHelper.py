import time
from   time       import sleep
from   time       import time
from   time       import gmtime, strftime
import pydim
import threading
import shutil
import os

class SaveSetRetriever:
    def __init__(self, part, tsk):
        self.name = part + "/" + tsk + "/SAVESETLOCATION"
        self.format = "C"
        self.SaveSet = ""
        self.lastData = 0.0
        self.__lock = threading.Lock()
        #print "[Error] Constructing SaveSet Retriever using service name " + self.name
        self.__lock.acquire()
        self.__infoID = pydim.dic_info_service(self.name, self.format, self.callback, timeout=10000000, default_value="Default")
        self.__lock.release()
        #print "[Error] Exiting Constructor of SaveSet Retriever"
    
    def callback(self, *args):
        self.__lock.acquire()
        print "Callback Called with args ", args
        if len(args) > 1:
            r = args[1]
            r = r[:r.find('\0')]
            self.SaveSet = r
            self.lastData = time()
        self.__lock.release()

    def getSaveSet(self):
        return self.SaveSet
    
    def getUpdTime(self):
        return self.lastData

class HistoCopier:
    def __init__(self, alignConf, ssretr):
        self.__tiltNames = alignConf.getProp('tiltNames')
        self.__workdir = alignConf.getProp('WorkDir')
        self.__whichRich = alignConf.getProp('Rich')
        self.__nameStr = alignConf.getProp('nameStr')
        self.__ssretr = ssretr
        
    def resultName(self, n_it, m_it):
        if m_it%9 == 0:
            mergename = (self.__workdir +
                         "/RichRecQCHistos_rich" +
                         str(self.__whichRich) +
                         "_"+self.__nameStr +
                         "_i"+str(n_it)+".root")
        else:
            mergename = (self.__workdir +
                         "/RichRecQCHistos_rich" +
                         str(self.__whichRich) +
                         "_"+self.__nameStr +
                         "_"+self.__tiltNames[m_it%9] +
                         "_i"+str(n_it)+".root")
        return mergename
    
    def waitForSaveSet(self, resultName, m_it):
        waitForSs = True
        while(waitForSs):
            SaveSetName = self.__ssretr.getSaveSet()
            if str('00' + str(m_it) ) in SaveSetName:
                waitForSs = False
            sleep (1)
        waitForSs = True
        while(waitForSs):
            if os.path.exists(SaveSetName):
                waitForSs = False
            sleep (1)
        shutil.copyfile(SaveSetName, resultName)
    
    def getHistoFile(self, n_it, m_it):
        resultName = self.resultName(n_it, m_it)
        #from multiprocessing import Process
        self.waitForSaveSet( resultName, m_it)
        #pro = Process(target = self.waitForSaveSet, args = ( resultName, m_it))
        #pro.start()
        #self.__processes[resultName] = [pro, n_it, m_it, False]
