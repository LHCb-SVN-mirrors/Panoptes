__version__ = "$Id: BrunelOnline.py,v 1.25 2010/11/09 12:20:55 frankb Exp $"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys
# Paras added these next 3
import re, importlib, __builtin__
import Configurables as Configs
import Gaudi.Configuration as Gaudi
import GaudiKernel
from GaudiKernel.SystemOfUnits import GeV, MeV
from GaudiKernel.ProcessJobOptions import PrintOff,InstallRootLoggingHandler,logging
from Configurables import CondDB, GaudiSequencer, EventPersistencySvc, HistogramPersistencySvc, EventLoopMgr, OutputStream, Gaudi__SerializeCnvSvc, DstConf
from Communicator import *
import time
#PrintOff(999)
InstallRootLoggingHandler(level=logging.CRITICAL)

processingType ='DataTaking'

GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))
requirement = None

debug = 0
def dummy(*args,**kwd): pass

MSG_VERBOSE = 1
MSG_DEBUG   = 2
MSG_INFO    = 3
MSG_WARNING = 4
MSG_ERROR   = 5
MSG_FATAL   = 6
MSG_ALWAYS  = 7

class saveHistograms(object):
  def __init__(self, runType, saveDir, n):
    self.__runType = runType
    self.__saveDir = saveDir
    self.__n = n
    
  def __call__(self):
    from Configurables import GaudiSequencer#, UpdateAndReset
    app=Gaudi.ApplicationMgr()
    app.ExtSvc += ['MonitorSvc']
    
    from Configurables import MonitorSvc
    MonitorSvc().OutputLevel = MSG_ERROR
    MonitorSvc().UniqueServiceNames = 1
    MonitorSvc().CounterUpdateInterval = -1
    MonitorSvc().DimUpdateInterval = -1
    Online = importOnline()
    MonitorSvc().PartitionName         = Online.PartitionName
    MonitorSvc().ProgramName           = "AligWork_%s_%02d" % (self.__runType, self.__n)
    
def staticVar(varname, value):
    def decorate(func):
        setattr(func, varname, value)
        return func
    return decorate

@staticVar("Online", None)
def importOnline():
  if importOnline.Online:
    print importOnline.Online
    return importOnline.Online
  if 'RUNINFO' in os.environ:
    runinfo = os.environ['RUNINFO']
    d, f = os.path.split(runinfo)
    name, ext = os.path.splitext(f)
    sys.path.insert(1, d)
    Online = importlib.import_module('OnlineEnv')
    sys.path.remove(d)
  else:
    import OnlineEnv as Online
  importOnline.Online = Online
  return Online

#===============================================================================
from Gaudi.Configuration import appendPostConfigAction
class ChangeP(object):
    def __init__(self, minP):
        self.__minP = minP

    def __call__(self):
        from Configurables import  PatForward, PatForwardTool
        for fwd in ("PatForwardHLT1", "PatForwardHLT2"):
            PatForward(fwd).addTool(PatForwardTool)
            PatForward(fwd).PatForwardTool.MinMomentum = self.__minP

#===============================================================================
def patchBrunel(true_online_version, alignConf, n):
  whichRich = alignConf.getProp('Rich')
  workdir =  alignConf.getProp('WorkDir')

  import GaudiConf.DstConf
  import Brunel.Configuration
  Online = importOnline()

  brunel = Brunel.Configuration.Brunel()
  #brunel.HltFilterRegEx = "Hlt1CalibRICHMirror(RICH" + str(whichRich) + ")?Decision"
  brunel.Hlt1FilterCode = "HLT_PASS_RE('Hlt1CalibRICHMirror(RICH%s)?Decision')"%whichRich

  from Configurables import RecMoniConf
  brunel.RecoSequence = ['Decoding', "VELO", "TT", "IT", "OT", "TrHLT1", "Vertex", "TrHLT2", 'RICH']
  RecMoniConf ().MoniSequence = ["RICH"]

  import ConditionsMap
  brunel.OnlineMode = True
  brunel.DDDBtag    = ConditionsMap.DDDBTag
  brunel.CondDBtag = ConditionsMap.CondDBTag
  brunel.Histograms =  "Expert"
  brunel.DataType = "2015"
  brunel.UseDBSnapshot = True

  brunel.OutputType='NONE'

  brunel.PartitionName = os.environ.get('PARTITION_NAME', 'TEST')

  from Configurables import CondDB
  conddb = CondDB()
  conddb.Online = True
  conddb.EnableRunChangeHandler = True
  conddb.UseDBSnapshot = True

  baseloc = '/group/online/hlt/conditions'

  ## Get the dictionary of conditions for the RunChangeHandler service
  ## from the file used by HLT2. Messing with sys.path is evil, but
  ## perhaps the least evil here...
  import sys
  try:
    import All
  except ImportError:
    rd = '/group/online/hlt/conditions/RunChangeHandler'
    sys.path.append(rd)
    import All

  ## Add base path of the files to the files in All.py
  conditionmap = {os.path.join(baseloc, f) : cs for f, cs in All.ConditionMap.iteritems()}
  
  conds = {
    'Rich1' : [
              'Conditions/Alignment/Rich1/SecMirrorMasterTop_Align' ,
              'Conditions/Alignment/Rich1/SecMirrorMasterBot_Align'
              ]
              + ['Conditions/Alignment/Rich1/SphMirror%d_Align' %i for i in range(0, 4)]
              + ['Conditions/Alignment/Rich1/SecMirror%d_Align' %i for i in range(0, 16)],
    'Rich2' : []
              + ['Conditions/Alignment/Rich2/Rich2SphMirrorCont%d_Align' % i for i in range (0,2)]
              + ['Conditions/Alignment/Rich2/Rich2SecMirrorCont%d_Align' % i for i in range (0,2)]
              + ['Conditions/Alignment/Rich2/Rich2SphMirror%d_Align' % i for i in range(0, 56)]
              + ['Conditions/Alignment/Rich2/Rich2SecMirror%d_Align' % i for i in range(0, 40)]
    }

  if hasattr(Online, "AlignXmlDir"):
    xmlDir = Online.AlignXmlDir
  else:
    xmlDir = workdir
  pat = os.path.join(xmlDir, "CondDB_%s.xml")
  ## Update the dictionary of conditions with the alignment specific
  ## ones.
  conditionmap.update({pat % sd : f for (sd, f) in conds.iteritems() if sd == ('Rich' + str(whichRich))})
  
  # On 11 April 2016, Roel sent us an email describing how to change the current default year for runs (2016) to 2015
  # I am not sure if this provides a permanent solution or is a hack so that we can do testing with 2015 data
  # If this is a hack, it must be commented out before 2016 data taking (but after we finish our tests).
  ## Roel's original suggestion was something like this:
  ## conddb.RunChangeHandlerConditions = {k.replace('2016', '2015') : v for k, v in ConditionsMap.RunChangeHandlerConditions.iteritems()}
  ## Claire's solution to fit our code:
  conditionmap = {k.replace('2016', '2015') : v for k, v in conditionmap.iteritems()}

  # Configure RunChangeHandlerSvc by hand to allow different locations of the
  # files. postConfigAction hammer required...
  def applyConds():
    from Configurables import RunChangeHandlerSvc
    rch = RunChangeHandlerSvc()
    Gaudi.ApplicationMgr().ExtSvc.append(rch)
    rch.Conditions = {c : f for f, cs in conditionmap.iteritems() for c in cs}
  from Gaudi.Configuration import appendPostConfigAction
  appendPostConfigAction(applyConds)

  from Configurables import MagneticFieldSvc
  MagneticFieldSvc().UseSetCurrent = True

  # add to the existing map
  # handlerConditions.update(conditionmap)
  # conddb.RunChangeHandlerConditions = handlerConditions

  brunel.WriteFSR  = False # This crashes Jaap's stuff
  EventLoopMgr().OutputLevel = MSG_WARNING
  EventLoopMgr().Warnings    = False

  # Hack by Chris
  #print "# Warning using CKThetaQuartzRefractCorrections = [ 0,-0.0001,0 ]"
  from Configurables import RichRecSysConf
  rConf = RichRecSysConf("RichOfflineRec")
  #rConf.richTools().photonReco().CKThetaQuartzRefractCorrections = [ 0,-0.001,0 ]
  rConf.richTools().photonReco().CKThetaQuartzRefractCorrections = [ -0.00625, 0.000247332, 2.9e-5 ]
  rConf.setProp("PidConfig", "None")

  # Need a postConfigAction to apply the correct condtions
  minP = 20
  if whichRich == 2:
    minP = 40
  appendPostConfigAction(ChangeP(minP * GeV))

  from Configurables import  TrackSys
  TrackSys().setProp( "TrackPatRecAlgorithms", ["FastVelo","VeloTT","ForwardHLT1","ForwardHLT2"] )

  rConf.photonConfig().SelectionMode = "Wide"

  if whichRich == 2:
      rConf.Radiators = [ "Rich2Gas" ]
  if whichRich == 1:
      rConf.Radiators = [ "Rich1Gas" ]

  rConf.trackConfig().TrackCuts = { "Forward" : { "Chi2Cut" : [0,5], "PCut" : [minP,9999999] },
                                    "Match"   : { "Chi2Cut" : [0,5], "PCut" : [minP,9999999] } }
  rConf.TracklessRingAlgs = [ ]

  from Configurables import HistogramPersistencySvc
  HistogramPersistencySvc().OutputFile = ""

  from Configurables import RichRecQCConf
  RichRecQCConf ("OfflineRichMoni").removeMonitor("L1SizeMonitoring"       )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("DBConsistencyCheck"     )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("HotPixelFinder"         )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("PidMonitoring"          )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("PixelMonitoring"        )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("TrackMonitoring"        )
  ## Commented the next line out on 2016-04-08, in order to create PhotonMonitoring plots. #####  
  # RichRecQCConf ("OfflineRichMoni").removeMonitor("PhotonMonitoring"       )
  ## Then added these next 4 lines on 2016-04-08 ###############################################
  # Open up the CK res plot range, for the Wide photon selection
  # RichRecQCConf ("OfflineRichMoni").CKThetaResRange = [ 0.045, 0.008, 0.004 ]
  RichRecQCConf ("OfflineRichMoni").CKThetaResRange = [ 0.05, 0.008, 0.004 ]
  ##############################################################################################
  RichRecQCConf ("OfflineRichMoni").removeMonitor("TracklessRingAngles"    )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("TracklessRingPeakSearch")
  RichRecQCConf ("OfflineRichMoni").removeMonitor("HPDImageShifts"         )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("DataDecodingErrors"     )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("HPDIFBMonitoring"       )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichPixelPositions"     )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("HPDHitPlots"            )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichTrackGeometry"      )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichGhostTracks"        )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichCKThetaResolution"  )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichTrackResolution"    )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichPhotonSignal"       )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichTrackCKResolutions" )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichPhotonGeometry"     )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("PhotonRecoEfficiency"   )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichPhotonTrajectory"   )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichStereoFitterTests"  )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichRayTracingTests"    )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichDataObjectChecks"   )
  RichRecQCConf ("OfflineRichMoni").removeMonitor("RichRecoTiming"         )

  from Configurables import RichAlignmentConf
  RichAlign = RichAlignmentConf ("OfflineRichMoni_RichAlignmentConf")
  if whichRich == 2:
      RichAlign.Radiators =  ["Rich2Gas"]
  if whichRich == 1:
      RichAlign.Radiators =  ["Rich1Gas"]
  RichAlign.HPDList =  [[0],[0],[0]]
  RichAlign.Histograms = "Expert"

  RichAlign.NTupleProduce = False
  RichAlign.R1NTupleProduce = False

  deltatheta = alignConf.getProp("deltaThetaWindow")/1000.
  RichAlign.DeltaThetaRange = [0.04,deltatheta, deltatheta ]

  brunel.OutputLevel       = MSG_INFO
  brunel.PrintFreq         = 100

  HistogramPersistencySvc().OutputLevel = MSG_ERROR
  from Configurables import CondDB
  from Configurables import CondDBAccessSvc
  from Configurables import ApplicationMgr
  from Configurables import HistogramPersistencySvc

  import uuid
  runType="Rich"+str(whichRich) + "_"+str(uuid.uuid4())+"_"+str(n)
  histoDir = workdir
  from Gaudi.Configuration import appendPostConfigAction
  appendPostConfigAction(saveHistograms(runType, histoDir, n))

  print brunel
  return brunel

def setupOnline(directory, filename, prefix, alignConf):
  import OnlineEnv as Online
  from Configurables import LHCb__FILEEvtSelector as es
  app=Gaudi.ApplicationMgr()
  app.AppName = ''
  app.HistogramPersistency = 'NONE'
  app.SvcOptMapping.append('LHCb::FILEEvtSelector/EventSelector')
  app.SvcOptMapping.append('LHCb::FmcMessageSvc/MessageSvc')
  from Configurables import MirrPauseHandler as mp
  mp().ReferenceRunNr = Online.DeferredRuns[0] if hasattr(Online, 'DeferredRuns') else 0
  mp().ItNrFile = alignConf.getProp('ItNrFile')
  app.TopAlg.append("MirrPauseHandler")
  sel = es("EventSelector")
  sel.AllowedRuns = Online.DeferredRuns if hasattr(Online, 'DeferredRuns') else []
  sel.Directory = directory
  sel.FilePrefix = prefix
  sel.Input = filename
  sel.DeleteFiles = False;
  sel.Decode = False
  sel.Pause = True
  sel.PauseSleep = 5000
  app.EvtSel  = sel
  app.EvtMax = alignConf.getProp('EvtMax')
  Online.rawPersistencySvc()
  evtloop = Configs.EventLoopMgr('EventLoopMgr')
  evtloop.Warnings = False
  evtloop.EvtSel = sel
  app.EventLoop = evtloop
  runable = Configs.LHCb__EventRunable("Runable")
  runable.MEPManager        = ""
  app.Runable = runable
  app.AuditAlgorithms = False
  Configs.MonitorSvc().OutputLevel = MSG_ERROR
  Configs.MonitorSvc().UniqueServiceNames = 1
  Configs.MonitorSvc().DimUpdateInterval = -1
  Configs.RootHistCnv__PersSvc("RootHistSvc").OutputLevel = MSG_ERROR
  app.OutputLevel = MSG_INFO
  print sel

#============================================================================================================
def patchMessages():
  import OnlineEnv as Online
  app=Gaudi.ApplicationMgr()
  Configs.AuditorSvc().Auditors = []
  app.MessageSvcType = 'LHCb::FmcMessageSvc'
  if Gaudi.allConfigurables.has_key('MessageSvc'):
    del Gaudi.allConfigurables['MessageSvc']
  msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
  msg.fifoPath      = os.environ['LOGFIFO']
  msg.LoggerOnly    = True
  msg.doPrintAlways = False
  msg.OutputLevel   = MSG_INFO # Online.OutputLevel

#============================================================================================================
def start():
  Online = importOnline()
  import sys
#   Online.end_config(True)
  Online.end_config(False)

#============================================================================================================
def doIt(filename = "", n = 0, whichRich = 1):

  if whichRich ==1:
    from Configurables import Rich1MirrAlignOnConf
    alignConf = Rich1MirrAlignOnConf()
  elif whichRich ==2:
    from Configurables import Rich2MirrAlignOnConf
    alignConf = Rich2MirrAlignOnConf()
  alignConf.__apply_configuration__()

  workdir = alignConf.getProp('WorkDir')
  true_online = os.environ.has_key('LOGFIFO') and (os.environ.has_key('PARTITION') or os.environ.has_key('PARTITION_NAME'))

  if os.path.isdir(filename):
    directory = filename
    prefix = 'Run_'
  else:
    directory = os.path.dirname(filename)
    prefix = os.path.basename(filename)[:4]

  br = patchBrunel(true_online, alignConf, n)
  setupOnline(directory, filename, prefix, alignConf)
  if true_online:
    patchMessages()
  start()
