import os
import shutil
import re
import time
from   time       import time
from   time       import gmtime, strftime



class SetupHelper:
    def __init__(self, _alignConf):
        self.alignConf = _alignConf
        # maybe put combAndMirrSubsets here later somehow
        self.nameStr = self.alignConf.getProp('nameStr')
        self.tiltNames = self.alignConf.getProp('tiltNames')
        self.workdir = self.alignConf.getProp('WorkDir')
        self.whichRich = self.alignConf.getProp('Rich')
        self.magnFactorsMode = self.alignConf.getProp('magnFactorsMode')
        self.minAverageBinPop = self.alignConf.getProp('minAverageBinPop')
        self.phiBinFactor = self.alignConf.getProp('phiBinFactor')
        self.deltaThetaWindow = self.alignConf.getProp('deltaThetaWindow')
        self.combinFitMethod = self.alignConf.getProp('combinFitMethod')
        self.solutionMethod = self.alignConf.getProp('solutionMethod')
        self.coeffCalibTilt = self.alignConf.getProp('coeffCalibTilt')
        self.magnifDir = self.alignConf.getProp('magnifDir') 
        self.fixSinusoidShift = self.alignConf.getProp('fixSinusoidShift') 
        self.stopTolerance = self.alignConf.getProp('stopTolerance') 
        self.stopToleranceSec = self.alignConf.getProp('stopToleranceSec') 


    def setupMagnifFiles(self):
        magnifDir = self.alignConf.getProp('magnifDir')
        for f in os.listdir(magnifDir):
            shutil.copy(magnifDir + "/" + f, self.workdir)

    
    def startMinIt(self, m_it):
        removeIt = (m_it - m_it % 9) / 9
        files = os.listdir(self.workdir)
        
        re_fitFolders = re.compile(r"Rich" + str(self.whichRich) + "MirrCombinFit_" + self.nameStr + "[\w\.]*_i" + str(removeIt))
        re_alignFiles = re.compile(r"Rich" + str(self.whichRich) + "MirrAlign[a-zA-z]*_i" + str(removeIt) + "\.txt")
        fitFolders = [x for x in files if re_fitFolders.match(x) ]
        alignFiles = [x for x in files if re_alignFiles.match(x) ]
        
        if (m_it % 9 == 0):
            connectStr = ""
        else:
            connectStr = "_"
        rootFile = "RichRecQCHistos_rich" + str(self.whichRich) + "_" + self.nameStr + connectStr + self.tiltNames[m_it % 9] + "_i" + str(removeIt) + ".root"
        
        for f in fitFolders:
            shutil.rmtree(self.workdir + "/" + f)
        for f in alignFiles:
            os.remove(self.workdir + "/" + f)
        if os.path.exists(self.workdir + "/" + rootFile):
            os.remove(self.workdir + "/" + rootFile)


    def finalize(self, conv, n_it):
        self.writeSummary(conv, n_it)
        savedir = self.alignConf.getProp('SaveDir') + '/Rich' + str(self.whichRich) + '/' + strftime("%Y%m%d_%H%M%S", gmtime())
        if not os.path.exists(savedir):
            os.makedirs(savedir)
        for f in os.listdir(self.workdir):
            if os.path.isdir(self.workdir + "/" + f):
                shutil.copytree(self.workdir + "/" + f, savedir + '/' + f)
            else:
                shutil.copy(self.workdir + "/" + f, savedir)
        
    def writeSummary(self, conv, n_it):
        summaryFile = self.workdir + "/summary.txt"

        import OnlineEnv as Online
        runs = Online.DeferredRuns
        hasi = ''
        for i in range(0, len(runs)):
            hasi += str(runs[i]) + '  ' 
            
        if os.path.exists(summaryFile) :
            os.remove(summaryFile)
        f = open(summaryFile, 'w')
        args = 'richDetector       = ' + str(self.whichRich) + '\n'
        args += 'converged         = ' + str(conv) + '\n'
        args += 'max iterations    = ' + str(n_it) + '\n'
        args += 'runs              = ' + hasi + '\n'
        args += 'magnFactorsMode   = ' + str(self.magnFactorsMode) + '\n'
        args += 'minAverageBinPop  = ' + str(self.minAverageBinPop) + '\n'
        args += 'phiBinFactor      = ' + str(self.phiBinFactor) + '\n'
        args += 'deltaThetaWindow  = ' + str(self.deltaThetaWindow) + '\n'
        args += 'combinFitMethod   = ' + str(self.combinFitMethod) + '\n'
        args += 'solutionMethod    = ' + str(self.solutionMethod) + '\n'
        args += 'coeffCalibTilt    = ' + str(self.coeffCalibTilt) + '\n'
        args += 'magnifDir         = ' + str(self.magnifDir) + '\n'
        args += 'fixSinusoidShift  = ' + str(self.fixSinusoidShift) + '\n'
        args += 'stopTolerance     = ' + str(self.stopTolerance) + '\n'
        args += 'stopToleranceSec  = ' + str(self.stopToleranceSec) + '\n'
                
        f.write(args)
        f.close()
