/*
 * MirrPauseHandler.cpp
 *
 *  Created on: Oct 23, 2014
 *      Author: beat
 */

#include <stdio.h>
#include <stdlib.h>
//#include "GaudiOnline/OnlineService.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "Gaucho/IGauchoMonitorSvc.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "MDF/OnlineRunInfo.h"
#include "Event/ODIN.h"
#include "DetDesc/RunChangeIncident.h"
#include "GaudiKernel/IMonitorSvc.h"
#include "GaudiKernel/IUpdateable.h"
#include "GaudiKernel/SmartIF.h"

using namespace LHCb;

class MirrPauseHandler: public GaudiAlgorithm, virtual public IIncidentListener
{
  public:
    MirrPauseHandler(const std::string& name, ISvcLocator* sl);
    virtual ~MirrPauseHandler();
    virtual StatusCode initialize();
    virtual StatusCode start();
    virtual StatusCode stop();
    virtual void handle(const Incident& inc);
    StatusCode execute();
    void readReference();
    IGauchoMonitorSvc *m_MonSvc;
//    IIncidentSvc *incidentSvc()
//    {
//      return OnlineService::incidentSvc();
//    };
    std::string m_PartitionName;
    std::string m_RefFileName;
    unsigned long m_Reference;
    ISvcLocator* m_sl;
    IIncidentSvc *m_is;
//    Gaudi::Utils::TypeNameString m_monitorSvcType;
    unsigned long m_currentRun;
    bool m_fireRunChange;
    int m_refRunNr;
   std::string m_itNrFile;
};

DECLARE_ALGORITHM_FACTORY(MirrPauseHandler)

MirrPauseHandler::MirrPauseHandler(const std::string& name, ISvcLocator* sl) :
    GaudiAlgorithm(name, sl), m_MonSvc(0), m_Reference(0),m_fireRunChange(false)
{
  declareProperty("PartitionName",  m_PartitionName="LHCbA");
  declareProperty("ReferenceRunNr",  m_refRunNr);
  declareProperty("ItNrFile", m_itNrFile);
  m_sl = sl;
//  m_monitorSvcType = Gaudi::Utils::TypeNameString("MonitorSvc");

}
StatusCode MirrPauseHandler::initialize()
{
  StatusCode sc;
  GaudiAlgorithm::initialize();
  m_MonSvc = svc<IGauchoMonitorSvc>("MonitorSvc");
//  sc = m_sl->service( MonitorSvc,m_MonSvc,true);
//  sc = service(m_monitorSvcType, m_MonSvc, true);
  if (m_MonSvc == 0)//!sc.isSuccess())
  {
    error()<< "Cannot access monitoring service of type MonitorSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  m_is = svc<IIncidentSvc>("IncidentSvc");
//  sc = m_sl->service("IncidentSvc",m_is,false);
  if (m_is == 0)//!sc.isSuccess())
  {
    error()<< "Cannot access monitoring service of type IncidentSvc." <<endmsg;
    return StatusCode::FAILURE;
  }
  m_is->addListener(this, "UpdateConstants");
  m_is->addListener(this, "DAQ_PAUSE");
  m_is->addListener(this, "BeginEvent");
  m_RefFileName = m_itNrFile;
  //m_RefFileName="/group/online/dataflow/options/"+m_PartitionName+"/RICH_Alignment_Reference_File.txt";
  return StatusCode::SUCCESS;

}

void MirrPauseHandler::handle(const Incident& inc)
{
  if (inc.type() == "DAQ_PAUSE")
  {
    readReference();
    SmartIF<IUpdateableIF> aaa(m_MonSvc);
    if (aaa.isValid())aaa->update(m_refRunNr*10000+m_Reference);
  }
  if( m_fireRunChange && "BeginEvent"==inc.type() ) {
    // throw a runchange incident to make sure it reads the xml
    m_is->fireIncident(RunChangeIncident(name(),m_currentRun)) ;
    m_fireRunChange = false ;
  }

}
StatusCode MirrPauseHandler::execute()
{
  LHCb::RawEvent* m_rawEvt;
  if( exist<LHCb::RawEvent>( LHCb::RawEventLocation::Default ) )
  {
    m_rawEvt= get<LHCb::RawEvent>( LHCb::RawEventLocation::Default );


    LHCb::RawBank::BankType i = LHCb::RawBank::BankType::ODIN;//(16); // 16 is Odin bank ID...
    std::string bname = LHCb::RawBank::typeName(i);
    std::string::size_type odinfound=bname.find("ODIN",0);

    if (odinfound!=std::string::npos)
    {
     const std::vector<LHCb::RawBank*>& b =m_rawEvt->banks(i);
     if ( b.size() > 0 )
     {
       std::vector<LHCb::RawBank*>::const_iterator itB;
      // int k = 0;
       for( itB = b.begin(); itB != b.end(); itB++ )
       {
          //if ( ((k++)%4) == 0 ) info() << endmsg << "  " ;
          const LHCb::RawBank* r = *itB;
   // const LHCb::OnlineRunInfo* ori = 0;
          const LHCb::OnlineRunInfo* ori=r->begin<LHCb::OnlineRunInfo>();
          m_currentRun=ori->Run;
       }
     }
   }



//    if( exist<LHCb::ODIN>( LHCb::ODINLocation::Default ) )
//    {
//      const LHCb::ODIN* odin = get<LHCb::ODIN> ( LHCb::ODINLocation::Default );
//      m_currentRun = odin->runNumber() ;
//    }
  }
  return StatusCode::SUCCESS;
}

MirrPauseHandler::~MirrPauseHandler()
{
}
void MirrPauseHandler::readReference()
{
  FILE *f;
  f = fopen(m_RefFileName.c_str(),"r");
  fscanf(f,"%lu",&m_Reference);
  fclose(f);
}

StatusCode MirrPauseHandler::stop()
{
  m_fireRunChange = true;
  return StatusCode::SUCCESS;
}
StatusCode MirrPauseHandler::start()
{
  m_MonSvc->resetHistos( this  );
  return StatusCode::SUCCESS;
}
