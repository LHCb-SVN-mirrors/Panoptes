################################################################################
# Package: RichLAMSImageAnalysis
################################################################################
gaudi_subdir(RichLAMSImageAnalysis v1r0)

gaudi_depends_on_subdirs(Online/DIM)

find_package(ROOT COMPONENTS Hist Gpad)
find_package(Boost COMPONENTS filesystem)

gaudi_add_executable(RichLAMSImageAnalysis
                     src/*.cpp
                     INCLUDE_DIRS Boost ROOT
                     LINK_LIBRARIES Boost dim ROOT)

