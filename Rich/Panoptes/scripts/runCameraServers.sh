#!/bin/bash

# Only run the CAMERA server if we are on the appropriate host.
SERVER=$1
if [ -z "$SERVER" ]; then
    SERVER="hist01"
fi

if [ "$HOST" = "$SERVER" ]; then
    # Stop any existing servers.
    killall -9 CameraWebServer.exe
    killall -9 CameraServer.exe

    # This is where we save the log files.
    export CAMDIR=/localdisk/scratch/RICH/CAMERA
    # Ensures the CAMERA data directory exists
    mkdir -p $CAMDIR/data
    # Ensure log directory exists.
    mkdir -p $CAMDIR/log

    # Start the Camera server and the web server.
    CameraWebServer.exe -C $PANOPTESROOT/scripts/etc/CameraWebServer.conf > $CAMDIR/log/CameraWebServer.log 2>&1 &
    CameraServer.exe -C $PANOPTESROOT/scripts/etc/CameraServer.conf > $CAMDIR/log/CameraServer.log 2>&1 &

    # Wait until the process is finished
    sleep 2
    # Creates a new warnings.out file when the camera server is started.
    SERVER="hist01.lbdaq.cern.ch:45123"
    $PANOPTESROOT/scripts/newDataDirectory.sh `date +%y%m%d%H%M` $SERVER
else
    echo "CAMERA servers must be run on $SERVER. CAMERA servers not started."
fi
