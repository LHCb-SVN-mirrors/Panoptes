#!/bin/bash

# This setup script is to be used with the LHCb online environment to
# start the CAMERA web server when the computer boots.
# It is a simple script setting up the enviroment to use then
# starting the executable with the exec command. This allows the LHCb
# online scripts to maintain control over the server.

# Start the executable.
exec -a CameraWebServer `which CameraWebServer.exe` -C $PANOPTESROOT/scripts/CernVM/etc/CameraWebServer.conf

exit 0
