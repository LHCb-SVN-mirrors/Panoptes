#!/bin/bash

export DIM_DNS_NODE=localhost

# This is the location that the CAMERA client uses to store the extra
# data received from the CAMERA server.
export CAMCACHE=/tmp/Camera/cache

# Make sure cache Dirs exist
mkdir -p ${CAMCACHE}

# Finally start the GUI
CameraGui.exe localhost:45124 &
