# 
# to be run with SiBias Off to find and dump hot pixels
# 
#
if [ -z DIM_DNS_NODE ]; then
	echo "Please define DIM_DNS_NODE"
	exit 1
fi
# Cleanup any running jobs

# message are send via the DIM Messageservice
# export MSGSVC=MessageSvc
# export MSGSVC=LHCb::DimMessageSvc
export MSGSVC=LHCb::FmcMessageSvc

# TAN - do not touch unless you know what you're doing
export TAN_PORT YES
export TAN_NODE=$(hostname -f)

export BIGTERM='xterm  -ls -132 -geometry 132x45 -title ' 
export WIDETERM='xterm  -sl 10000 -ls -132 -geometry 160x50 -title '
export MINITERM='xterm -sl 100000 -ls -132 -geometry 132x10 -title '

export RICHLOG=/var/tmp/rich/log

# shortcuts for starting jobs

export test_exe=${ONLINEKERNELROOT}/${CMTCONFIG}/test.exe
export gaudi_run="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineStart "
export gaudi_exe="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineTask -msgsvc=${MSGSVC} -auto"  
export gaudi_exe2="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineTask -msgsvc=MessageSvc -auto"  

export UTGID=RichHotPixels ; ${gaudi_exe} -opt=$RICHMONITORINGSYSROOT/options/RichHotPixels.opts -main=$GAUDIONLINEROOT/options/Main.opts # 1>${RICHLOG}/${UTGID}2.log  2>&1 &
#
#
# Change the permissions on the shared memory files so that anyone in RICH group can delete them
#
