#!/bin/bash
. ${GAUDIONLINEROOT}/tests/cmt/preamble.sh
#rm /dev/shm/bm_* /dev/shm/sem.bm_* /dev/shm/TAN* /dev/shm/sem.TAN*
#
#
export NODENAME=`python -c "print '$HOST'.split('.')[0]"`
echo "Pyopts: ${RICH_PYOPTS}"
if test -z "${RICH_PYOPTS}";
  then
    export RICH_PYOPTS=${PANOPTESROOT}/python:${PANOPTESROOT}/options
    export PYTHONPATH=${RICH_PYOPTS}:${PYTHONPATH}
    echo "Tweaking python path: ${RICH_PYOPTS}"
fi;
#
start_py_task MbmEvents "import GaudiOnlineTests;GaudiOnlineTests.runBuffer(buffer='Events')"
#
sleep 6
#
#  Monitors:
#
$BIGTERM MBMMon@${HOST}     -e "export UTGID=${NODENAME}/MBMMon;    exec $gaudi_run libOnlineKernel.so mbm_mon"&

start_py_task Monitor "import GaudiOnlineTests;GaudiOnlineTests.runBufferCons('Events', True, False)"
#
start_py_task RichDAQMon "import RichDAQMon;RichDAQMon.start(mode='OnlineTest')"
#
tail -n 3 ${0}
# start_py_task Mdf2Mbm "import GaudiOnlineTests;GaudiOnlineTests.runMDF2MBMFile(['Events'],fname='/daqarea/lhcb/data/2009/RAW/FULL/FEST/FEST/44164/044164_0000000001.raw')"
