#!/bin/bash

export DIM_DNS_NODE=localhost

export CAMDIR=/tmp/Camera
mkdir -p ${CAMDIR}/data

pkill dns.exe
pkill CameraServer 
pkill CameraWebServer
sleep 1

dns.exe&

$PANOPTESROOT/scripts/CRJ/Cambridge/StartCameraServer.sh&
$PANOPTESROOT/scripts/CRJ/Cambridge/StartCameraWebServer.sh&

sleep 2
echo
echo "Don't forget to set DIM_DNS_NODE=localhost in the terminal you run Panoptes"
echo

exit 0
