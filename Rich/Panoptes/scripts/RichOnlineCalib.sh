#!/bin/bash
#
#  Permanent Rich calibration task running on hist01
#

export CMTCONFIG=@BINARY_TAG@;
cd /group/rich/sw/cmtuser/Panoptes_@CMAKE_PROJECT_VERSION@/build.@BINARY_TAG@/Rich/Panoptes/scripts
. ./setup.vars_@BINARY_TAG@

if [ -r /etc/sysconfig/dim ]; then
   . /etc/sysconfig/dim
   export DIM_DNS_NODE
fi
if [ -z $DIM_DNS_NODE ]; then
   echo [Error] DIM_DNS_NODE undefined and /etc/sysconfig/dim not readable
   # exit 1
fi;
#
# export environment variables
# for logger and partition
#
export DEBUG=0;
export UTGID;
export LOGFIFO=/tmp/logRichCalib.fifo;
export PARTITION="LHCb";
export TNS_ADMIN=/sw/oracle/10.2.0.4/linux64/network/admin;
#
# Temp. Hack to slee if we end-up on hist01-old, which pcSrv cannot distinguish from hist01!
if test `hostname -s` = "hist01-old";
then 
    python -c "import time; time.sleep(100000000);";
fi;
#
if test "${DEBUG}" = "1";
then
  echo "TNSNAMES  $TNS_ADMIN";
  echo "Panoptes is taken from $PANOPTESROOT/$CMTCONFIG using $GAUDIONLINEROOT ";
  echo "LOGFIFO is taken from $LOGFIFO";
  echo "UTGID = ${UTGID}";
  echo "PARTITION = ${PARTITION}";
fi;
#
# Set DIM-DNS here. Looks like it is overwritten else
#
export DIM_DNS_NODE=mona09
#
#
# start Calibration Job 
#
exec -a ${UTGID} \
    GaudiOnlineExe.exe libGaudiOnline.so OnlineTask -auto \
    -msgsvc=LHCb::FmcMessageSvc \
    -tasktype=LHCb::Class1Task \
    -main=/group/online/dataflow/templates/options/Main.opts \
    -opt=command="\
import os,Gaudi,GaudiKernel.ProcessJobOptions;\
from Gaudi.Configuration import importOptions;\
GaudiKernel.ProcessJobOptions.printing_level=999;\
importOptions(os.environ['PANOPTESROOT']+'/options/RichOnlineCalib.py');"

