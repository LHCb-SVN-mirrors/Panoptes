# 
# hltrx.sh
# Markus Frank & Niko Neufeld
# Simple script to run the event builder chain stand-alone
# Requires CMTPROJECTPATH set and setup.sh sourced in $ONLINETASKSROOT/cmt
# 
#
if [ -z DIM_DNS_NODE ]; then
	echo "Please define DIM_DNS_NODE"
	exit 1
fi

 #message are send via the DIM Messageservice
export RICHLOG=/var/tmp/rich/log

# export MSGSVC=MessageSvc
# export MSGSVC=LHCb::DimMessageSvc
export MSGSVC=LHCb::FmcMessageSvc

export test_exe=${ONLINEKERNELROOT}/${CMTCONFIG}/test.exe
export gaudi_run="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineStart "
export gaudi_exe="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineTask -msgsvc=${MSGSVC} -auto"  
export gaudi_exe2="${GAUDIONLINEROOT}/${CMTCONFIG}/Gaudi.exe libGaudiOnline.so OnlineTask -msgsvc=MessageSvc -auto"  


export UTGID=RichPost ; ${gaudi_exe} -opt=$RICHMONITORINGSYSROOT/options/RichOnlineAna2.opts -main=$GAUDIONLINEROOT/options/Main.opts # 1>${RICHLOG}/${UTGID}2.log 2>&1 &


