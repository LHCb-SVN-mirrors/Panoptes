#!/bin/bash

# This setup script is to be used with the LHCb online environment to
# start the CAMERA web server when the computer boots.
# It is a simple script setting up the enviroment to use then
# starting the executable with the exec command. This allows the LHCb
# online scripts to maintain control over the server.

# Define a couple of variables that will point to the CAMERA executable
# and config files.
version=v5r2
Panoptes_release=/group/rich/sw/cmtuser/Panoptes_$version/Rich/Panoptes

# This sources the appropriate script to setup the LHCb environment.
source /group/rich/sw/scripts/setup.sh
# The official LHCb recommended mechanism for setting up a project.
# Setting up the online project gives us access to the CAMERA executable.
source `which SetupProject.sh` Panoptes $version

# Start the executable.
exec -a CameraServerOffline `which CameraServer.exe` -C $Panoptes_release/scripts/etc/CameraServerOffline.conf
