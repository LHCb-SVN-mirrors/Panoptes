#!/bin/bash

#This is the location that the CAMERA client uses to store the extra
#data received from the CAMERA server.
#export CAMCACHE=/tmp/cam/camcache
#mkdir -p $CAMCACHE/data
export CAMCACHE=$HOME
#export CAMCACHE=/dev/shm

#ssh -L 45124:127.0.0.1:45124 lxplus.cern.ch ssh -L 45124:hlte0403:45124 lbgw

# Change of directory to source .rootrc
cd $CAMERAROOT/scripts/

#Takes two arguments:
#$1 - name of the computer running the CAMERA server.
#$2 - the CAMERA warning file to open. This is optional and defaults to warnings.out.
#CameraGui.exe $1 $2 2>&1 | cat > ${HOME}/.camera.log&
CameraGui.exe $1 $2 &

#127.0.0.1 #warnings.out 
cd -
