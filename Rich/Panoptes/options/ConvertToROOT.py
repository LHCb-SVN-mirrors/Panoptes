##############################################################################
# File for running Panoptes with default options 
# Edit one or more of the lines below to change the defaults
##############################################################################
# Syntax is:
#   gaudirun.py Panoptes-Default.py <someDataFiles>.py
##############################################################################

from Panoptes.Configuration   import *
from RichRecSys.Configuration import *
from RichRecQC.Configuration  import *
from RichMarkovRingFinder.Configuration import *

##############################################################################

# Number of events to process
Panoptes().EvtMax     = 10000
# Event number print frequency
Panoptes().printFreq  = 1000

# Data base
db_tag                = "2008-default"
#db_tag                = "DC06-default"
Panoptes().DDDBtag    = db_tag
Panoptes().condDBtag  = db_tag

# Offline or online ??
Panoptes().mode = "Offline" # Use "Online" for online mode

# Reconstruction options
RichRecSysConf().checkProcStatus       = False
RichRecSysConf().initPixels            = False  # RichRecPixel objects
RichRecSysConf().initTracks            = False  # RichRecTrack objects (requires Tracking)
RichRecSysConf().initPhotons           = False  # RichRecPixel objects (requires Tracking)
RichRecSysConf().tracklessRings        = False  # Run the trackless ring finding algs
RichPixelCreatorConfig().pixelCleaning = "None" # Disable pixel cleaning

# PID algorithms
RichRecSysConf().pidConfig       = "None" # Disable PID sequence

# Tracking. Run our own tracking sequence
Panoptes().usePrivateTracking = False

# Monitoring options
# Low level DAQ monitors
RichMonitoringSysConf().monitorNhits          = False
RichMonitoringSysConf().monitorDAQ            = False
RichMonitoringSysConf().monitorHitMaps        = False
RichMonitoringSysConf().monitorOnlineStatus   = False
RichMonitoringSysConf().monitorIFB            = False
RichMonitoringSysConf().monitorCalibration    = False
# Send Event snapshots of events with 'errors'
RichMonitoringSysConf().sendErrorEventSnapshots = False

# High level reconstruction
RichMonitoringSysConf().monitorReconstruction = False
RichRecQCConf().pixelMonitoring               = False  # Monitor reconstructed pixels
RichRecQCConf().photonMonitoring              = False  # Track based photon resolution
RichRecQCConf().pidMonitoring                 = False  # Disable PID monitoring
RichRecQCConf().tracklessRingMonitoring       = False  # Monitor the trackless ring finding
RichRecQCConf().mirrorAlignmentMonitoring     = False  # The mirror alignment monitoring

# Max number hits per HPD panel for ring finding
# RichMarkovRingFinderConf().maxHitsInPanel = 999;

# TAE events
Panoptes().rawEventLocations = [
    # Primary event
    ""
    # Previous events
    #,"Prev1", "Prev2", "Prev3", "Prev4", "Prev5"
    # Successive events
    #,"Next1", "Next2", "Next3", "Next4", "Next5"
    # Previous events
    ,"Prev1"
    # Successive events
    ,"Next1"
    ]

Panoptes().writeOutSelectedEvents = True
Panoptes().selectedEventsDataFile = "/work/jonrob/data/Others_Run_23092.raw"

##############################################################################

# Must be the last line ...
Panoptes().applyConf()

seq = GaudiSequencer("Out")
seq.MeasureTime = True
ApplicationMgr().TopAlg += [seq]
seq.Members = ["Rich::DAQ::LoadRawEvent/LoadEvent"]

# RICH2 run
#EventSelector().Input = [
#    "DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/RICH2/COSMICS/34094/034094_0000085536.raw' SVC='LHCb::MDFSelector'"
#    ]
# Other Sub-dets
EventSelector().Input = [
    "DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/34092/034092_0000085535.raw' SVC='LHCb::MDFSelector'"
#   ,"DATAFILE='PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/34092/034092_0000085542.raw' SVC='LHCb::MDFSelector'"
    ]

OutputStream("RawWriter").RequireAlgs = ["LoadEvent"]
