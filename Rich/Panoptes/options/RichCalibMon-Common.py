
from Panoptes.Configuration   import *
from RichRecSys.Configuration import RichPixelCreatorConfig
from Configurables            import Rich__HPDOccupancyTool

# Defaults
Panoptes().DataType   = "2016"
Panoptes().DDDBtag    = "dddb-20150724"
Panoptes().CondDBtag  = "cond-20160517"
# Try and load tags from online settings
try:
    import ConditionsMap
    print "Setting DB tags from Online ConditionsMap"
    Panoptes().DDDBtag   = ConditionsMap.DDDBTag
    Panoptes().CondDBtag = ConditionsMap.CondDBTag
except ImportError:
    print "Setting DB tags from Defaults"
print "CondDBTag =", Panoptes().CondDBtag, "DDDBTag = ", Panoptes().DDDBtag

## Reconstruction options
RichMonitoringSysConf().RichRecCheckProcStatus          = False 
RichMonitoringSysConf().RichRecInitPixels               = False    # RichRecPixel objects
RichMonitoringSysConf().RichRecInitTracks               = False   # RichRecTrack objects (requires Tracking)
RichMonitoringSysConf().RichRecInitPhotons              = False   # RichRecPixel objects (requires Tracking)
RichMonitoringSysConf().RichRecTracklessRingAlgs        = [""]
RichMonitoringSysConf().RichRecPidConfig                = "None"

# Disable pixel cleaning when creating RichRecPixels
RichPixelCreatorConfig().PixelCleaning = "None"

Panoptes().MonName = "RichCalibMon"

# Tracking. Run our own tracking sequence
Panoptes().UsePrivateTracking = False

  # High level reconstruction (from RichRecQC)
RichMonitoringSysConf().MonitorReconstruction = False
RichMonitoringSysConf().MonitorPixels         = False  # Monitor reconstructed pixels
RichMonitoringSysConf().MonitorPhoton         = False # Track based photon resolution
RichMonitoringSysConf().MonitorPID            = False # Disable PID monitoring
RichMonitoringSysConf().MonitorTracklessRing  = False  # Monitor the trackless ring finding
RichMonitoringSysConf().MonitorMirrorAlign    = False # The mirror alignment monitoring

# Low level DAQ monitors
RichMonitoringSysConf().MonitorNhits            = False
RichMonitoringSysConf().MonitorDAQ              = False
RichMonitoringSysConf().MonitorHitMaps          = True
RichMonitoringSysConf().MonitorIFB              = False

# Hitmaps
RichMonitoringSysConf().HitMapsMon_LowResHitMaps   = True
RichMonitoringSysConf().HitMapsMon_HotPixEnabled   = False
RichMonitoringSysConf().HitMapsMon_HPDCountEnabled = False

# Calibration Farm monitoring
RichMonitoringSysConf().MonitorCalibration      = True
#RichMonitoringSysConf().OutputLevelCalibration  = 1
RichMonitoringSysConf().TestPattern             = "CornerPixels"
RichMonitoringSysConf().MonitorImageMovements   = False

# Event 'snapshots' sent to CAMERA
RichMonitoringSysConf().SendEventSnapshots       = False
#RichMonitoringSysConf().SendNHitEventSnapshots   = False # for #hit monitor
#RichMonitoringSysConf().SendDaqEventSnapshots    = False # for DAQ monitor

# TAE events
Panoptes().RawEventLocations = [
  # Primary event
  ""
  #    # Previous events (extend as needed)
  #    ,"Prev1"
  #    # Successive events (extend as needed)
  #    ,"Next1"
  ]
