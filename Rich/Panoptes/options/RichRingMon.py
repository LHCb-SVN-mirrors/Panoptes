"""
     Run RICH Monitoring in the online environment

     @author C.Jones
"""
__version__ = "$Id: RichRingMon.py,v 1.22 2009-11-27 11:21:21 ukerzel Exp $"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys

partition = os.getenv('PARTITION','')
importpath = "/group/online/dataflow/options/"+partition+"/RECONSTRUCTION"
if os.path.exists(importpath) : sys.path.insert(0,importpath)

from Gaudi.Configuration import *
from Configurables import Panoptes, CondDB, CondDBAccessSvc
from Panoptes.Configuration import *
import glob

def start(mode='Online'):
  """
        Finish configuration and configure Gaudi application manager

        @author M.Frank
  """
  
  importOptions( "$PANOPTESROOT/options/RichRingMon-Common.py" )
  Panoptes().Mode = mode

  # Custom DB slices at the pit
  customDBs = glob.glob('/group/rich/ActiveDBSlices/*.db')
  for db in customDBs:
    CondDB().addLayer( CondDBAccessSvc(os.path.basename(db),
                                       ConnectionString="sqlite_file:"+db+"/LHCBCOND",
                                       DefaultTAG="HEAD") )
  
  # override CondDBTag with the one provided by the Online environment?
  # -> n.b. only works in "online" or "calibration" mode, setting ignored otherwise
  Panoptes().UseOnlineCondDBtag              = False

  # connect to ORACLE conditionsDB
  Panoptes().UseOracle                       = False

  # Ignore the heart beat when not using oracle.
  if not Panoptes().UseOracle:
    CondDB().IgnoreHeartBeat     = True
    CondDB().EnableRunStampCheck = False

  partition = os.getenv('PARTITION','')

  # Set the trigger bits to select/veto
  if partition in ["LHCb"] :
    # LHCb global running
    Panoptes().TriggerMask = [48,55,57,96]
    Panoptes().VetoMask    = [100,104,105,106]
  else :
    # local runs
    Panoptes().TriggerMask = [48,55,57,96]
    Panoptes().VetoMask    = [100,104,105,106]

  # override default settings for FEST
  if partition.upper() == 'FEST':
    RichMonitoringSysConf().DaqMon_RemoveFaultyHPD  = False
    RichMonitoringSysConf().NHitMon_RemoveFaultyHPD = False
    RichMonitoringSysConf().HPDDisable              = False
    Panoptes().DDDBtag                              = "MC09-20090602"
    Panoptes().CondDBtag                            = "sim-20090402-vc-md100"
    Panoptes().Simulation                           = True

  import OnlineEnv as Online
  if mode == 'Online':
    Online.end_config(False)
  else:
    Online.end_config(True)
    
if os.environ.has_key('LOGFIFO'): start()
