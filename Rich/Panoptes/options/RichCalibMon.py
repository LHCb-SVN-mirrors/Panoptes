"""
     Run RICH monitoring on the calibration farm

     @author C.Jones
"""

import os, sys

partition = os.getenv('PARTITION','')
importpath = "/group/online/dataflow/options/"+partition+"/RECONSTRUCTION"
if os.path.exists(importpath) : sys.path.insert(0,importpath)

import Gaudi.Configuration as Gaudi
import Configurables as Configs
from Panoptes.Configuration import *
import glob

def start(mode='Online'):
  """
        Finish configuration and configure Gaudi application manager

        @author C.Jones
  """

  importOptions( "$PANOPTESROOT/options/RichCalibMon-Common.py" )

  ## Import modules
  import OnlineEnv as Online
  from RichRecSys.Configuration import RichPixelCreatorConfig
  from Configurables import CondDB, CondDBAccessSvc
 
  #
  # set mode to Calibration
  #
  mode            = 'Calibration'
  Panoptes().Mode = mode

  # Custom DB slices at the pit
  customDBs = glob.glob('/group/rich/ActiveDBSlices/*.db')
  for db in customDBs:
    CondDB().addLayer( CondDBAccessSvc(os.path.basename(db),
                                       ConnectionString="sqlite_file:"+db+"/LHCBCOND",
                                       DefaultTAG="HEAD") )    

  # override CondDBTag with the one provided by the Online environment?
  # -> n.b. only works in "online" or "calibration" mode, setting ignored otherwise
  Panoptes().UseOnlineCondDBtag              = False

  # connect to ORACLE conditionsDB
  Panoptes().UseOracle                       = False

  # Ignore the heart beat when not using oracle.
  if not Panoptes().UseOracle:
    CondDB().IgnoreHeartBeat     = True
    CondDB().EnableRunStampCheck = False

  # partition = os.getenv('PARTITION','')

  # override default settings for FEST
#   if partition.upper() == 'FEST':
#     Panoptes().DDDBtag                              = "MC09-20090602"
#     Panoptes().CondDBtag                            = "sim-20090402-vc-md100"
#     Panoptes().Simulation                           = True

  if mode == 'Online' or mode == 'Calibration':
    Online.end_config(False)
  else:
    Online.end_config(True)

if os.environ.has_key('LOGFIFO'):
  print "Now try to configure RichCalibMon" 
  start()
else:
  print "LOGFIFO not set?"
