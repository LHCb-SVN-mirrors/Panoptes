import os, sys

print 'RichOnlineCalib.py  ....'

partition = os.environ['PARTITION']
importpath = "/group/online/dataflow/options/"+partition+"/RECONSTRUCTION"
if os.path.exists(importpath) : sys.path.insert(0,importpath)

import Gaudi.Configuration
from Configurables import GaudiSequencer, ApplicationMgr, LHCb__FmcMessageSvc
from Configurables import OMARichRefIndex, OMARichHPDImage, Camera
import OnlineConfig as Online

MSG_VERBOSE = 1
MSG_DEBUG   = 2
MSG_INFO    = 3
MSG_WARNING = 4
MSG_ERROR   = 5
MSG_FATAL   = 6
MSG_ALWAYS  = 7

def setup():
  
  RichOnCalibSeq = GaudiSequencer("RichOnCalibSeq")
  RefIndexAlg = OMARichRefIndex("RefractiveIndexCalibration")
  #HPDImageAlg = OMARichHPDImage("HPDImageAlg") 

  # Rich refrative index calibration job
  RefIndexAlg.InputTasks  = [ "Brunel" ]
  RefIndexAlg.Partition   = partition
  #RefIndexAlg.InputFiles  = [ "/hist/Savesets/2013/LHCb/Brunel/01/20/Brunel-135576-20130120T161302-EOR.root"]
  RefIndexAlg.xmlFilePath = "/group/online/alignment"

  # HPD image calibration job
  #HPDImageAlg.InputTasks  = [ "Brunel" ]
  #HPDImageAlg.Partition   = partition
  ##HPDImageAlg.InputFiles  = [ "/hist/Savesets/2013/LHCb/Brunel/01/20/Brunel-135576-20130120T161302-EOR.root"]
  #HPDImageAlg.xmlFilePath = "/group/online/alignment/" 

  RichOnCalibSeq.Members += [ RefIndexAlg ]
  RichOnCalibSeq.IgnoreFilterPassed = True

  # Output Level of both
  RefIndexAlg.OutputLevel = 3
  #HPDImageAlg.OutputLevel = 3

  # Camera setup
  if not Camera().isPropertySet("CameraServers") :
    if partition in ["RICH","RICH1","RICH2","LHCb"]:
      Camera().setProp("CameraServers",["hist01.lbdaq.cern.ch:45123"])
    else:
      Camera().setProp("CameraServers",["hist01.lbdaq.cern.ch:45121"])
  
  ApplicationMgr().TopAlg += [ RichOnCalibSeq ]
  ApplicationMgr().ExtSvc += [ "LHCb::PublishSvc" ]
  ApplicationMgr().EvtSel = "NONE"
  ApplicationMgr().ExtSvc += [ "MonitorSvc" ]
  #ApplicationMgr().Runable = "LHCb::OnlineRunable/Runable"
  ApplicationMgr().Runable = "LHCb::LazyRunable/LazyRunable"

  from Configurables import LHCbApp
  # Defaults
  LHCbApp().DataType  = "2016"
  LHCbApp().DDDBtag   = "dddb-20150724"
  LHCbApp().CondDBtag = "cond-20160517"
  # Try and load tags from online settings
  try:
    import ConditionsMap
    print "Setting DB tags from Online ConditionsMap"
    LHCbApp().DDDBtag   = ConditionsMap.DDDBTag
    LHCbApp().CondDBtag = ConditionsMap.CondDBTag
  except ImportError:
    print "Setting DB tags from Defaults"
  print "CondDBTag =", LHCbApp().CondDBtag, "DDDBTag = ", LHCbApp().DDDBtag
  
  #print '[WARN] .....RichOnlineCalib.py  setup finished....'

#============================================================================================================
def patchMessages():
  """
        Messages in the online get redirected.
        Setup here the FMC message service

        @author M.Frank
  """
  app=ApplicationMgr()
  #Configs.AuditorSvc().Auditors = []
  app.MessageSvcType = 'LHCb::FmcMessageSvc'
  if Gaudi.Configuration.allConfigurables.has_key('MessageSvc'):
    del Gaudi.Configuration.allConfigurables['MessageSvc']
  msg = LHCb__FmcMessageSvc('MessageSvc')
  msg.fifoPath      = os.environ['LOGFIFO']
  msg.LoggerOnly    = True
  msg.doPrintAlways = False
  msg.OutputLevel   = MSG_INFO # Online.OutputLevel


setup()
patchMessages()
Online.end_config(True)
