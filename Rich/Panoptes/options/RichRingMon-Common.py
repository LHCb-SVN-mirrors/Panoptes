from Panoptes.Configuration import *
from RichRecSys.Configuration import RichPixelCreatorConfig
from Configurables import CondDB, CondDBAccessSvc

# Defaults
Panoptes().DataType   = "2016"
Panoptes().DDDBtag    = "dddb-20150724"
Panoptes().CondDBtag  = "cond-20160517"
# Try and load tags from online settings
try:
    import ConditionsMap
    print "Setting DB tags from Online ConditionsMap"
    Panoptes().DDDBtag   = ConditionsMap.DDDBTag
    Panoptes().CondDBtag = ConditionsMap.CondDBTag
except ImportError:
    print "Setting DB tags from Defaults"
print "CondDBTag =", Panoptes().CondDBtag, "DDDBTag = ", Panoptes().DDDBtag

Panoptes().MonName = "RichRingMon"

## mask off hot pixels
#from Configurables import RichTools
#RichTools().rawDecoder().HotPixelsToMask = [ 4237672320,4237914344,4228645792,4228722368,4229220928,4246319928,4253887144 ]

## Reconstruction options
RichMonitoringSysConf().RichRecCheckProcStatus          = False 
RichMonitoringSysConf().RichRecInitPixels               = True   # RichRecPixel objects
RichMonitoringSysConf().RichRecInitTracks               = False   # RichRecTrack objects (requires Tracking)
RichMonitoringSysConf().RichRecInitPhotons              = False   # RichRecPixel objects (requires Tracking)
RichMonitoringSysConf().RichRecTracklessRingAlgs        = ["ENN"] #["Markov"]
RichMonitoringSysConf().RichRecPidConfig                = "None"

# Disable pixel cleaning when creating RichRecPixels
RichPixelCreatorConfig().PixelCleaning = "None"

# Tracking. Run our own tracking sequence
Panoptes().UsePrivateTracking = False

# High level reconstruction (from RichRecQC)
RichMonitoringSysConf().MonitorReconstruction = True
RichMonitoringSysConf().MonitorPixels         = False # Monitor reconstructed pixels
RichMonitoringSysConf().MonitorTracks         = False # Monitor reconstructed tracks
RichMonitoringSysConf().MonitorPhoton         = False # Track based photon resolution
RichMonitoringSysConf().MonitorPID            = False # Disable PID monitoring
RichMonitoringSysConf().MonitorTracklessRing  = True  # Monitor the trackless ring finding
RichMonitoringSysConf().MonitorMirrorAlign    = False # The mirror alignment monitoring
RichMonitoringSysConf().MonitorRaw            = False #

#don't match rings to tracks  
RichMonitoringSysConf().AssociateRingsToTracks = False

# Low level DAQ monitors
RichMonitoringSysConf().MonitorNhits            = False
RichMonitoringSysConf().MonitorDAQ              = False
RichMonitoringSysConf().MonitorHitMaps          = False
RichMonitoringSysConf().MonitorIFB              = False
RichMonitoringSysConf().MonitorCalibration      = False
RichMonitoringSysConf().MonitorImageMovements   = False

RichMonitoringSysConf().DaqMon_MonitorBXID      = True 
RichMonitoringSysConf().DaqMon_MaxErrorMessages =  500
RichMonitoringSysConf().DaqMon_Plot2DHisto      = False
RichMonitoringSysConf().DaqMon_PrintMessages    = False

# Event 'snapshots' sent to CAMERA
from Configurables import RichEventSnapshotConf
RichMonitoringSysConf().SendEventSnapshots       = True
RichMonitoringSysConf().SendNHitEventSnapshots   = False # for #hit monitor
RichMonitoringSysConf().SendDaqEventSnapshots    = False # for DAQ monitor
RichMonitoringSysConf().SnapshotUpdateInterval   = 900
RichMonitoringSysConf().SnapshotNBins            = 200
# event cuts affect the sum of RICH1 and RICH2 
RichEventSnapshotConf().EventCuts  = { "Rings"        : [ 1,50],  
                                       "Pixels"       : [20,999999],  
                                       "HPDsWithHits" : [ 3,999999],
                                       "HPDHits"      : [ 3,999], #average occupancy ~1%, except very busy regions (~8%)
                                       "Tracks"       : [ 0,999999]
                                                     }
RichEventSnapshotConf().MinRingPixels            =  10   #~20 for gas?
RichEventSnapshotConf().MinHitRich               =  10
RichEventSnapshotConf().MaxHitRich               = 300
#RichMonitoringSysConf().SnapshotRingType     =  "All"
#RichMonitoringSysConf().SnapshotRingType     =  "Best"
RichMonitoringSysConf().SnapshotRingType         =  "Isolated"
RichEventSnapshotConf().MaxSnapshots             = 3 # Per Run

# TAE events - should be done automatically
#Panoptes().RawEventLocations = [
#    "","Next1"
#  # Primary event
#  ""
#  #    # Previous events (extend as needed)
#  #    ,"Prev1"
#  #    # Successive events (extend as needed)
#  #    ,"Next1"
#]
