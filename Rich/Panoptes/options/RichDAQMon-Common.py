from Panoptes.Configuration   import *
from RichRecSys.Configuration import RichPixelCreatorConfig
from Configurables            import Rich__HPDOccupancyTool

# Defaults
Panoptes().DataType   = "2016"
Panoptes().DDDBtag    = "dddb-20150724"
Panoptes().CondDBtag  = "cond-20160517"
# Try and load tags from online settings
try:
    import ConditionsMap
    print "Setting DB tags from Online ConditionsMap"
    Panoptes().DDDBtag   = ConditionsMap.DDDBTag
    Panoptes().CondDBtag = ConditionsMap.CondDBTag
except ImportError:
    print "Setting DB tags from Defaults"
print "CondDBTag =", Panoptes().CondDBtag, "DDDBTag = ", Panoptes().DDDBtag

# Task name...
Panoptes().MonName = "RichDAQMon"

#
# Reconstruction options
#
RichMonitoringSysConf().RichRecCheckProcStatus          = False 
RichMonitoringSysConf().RichRecInitPixels               = False # RichRecPixel objects
RichMonitoringSysConf().RichRecInitTracks               = False # RichRecTrack objects (requires Tracking)
RichMonitoringSysConf().RichRecInitPhotons              = False # RichRecPixel objects (requires Tracking)
RichMonitoringSysConf().RichRecTracklessRingAlgs        = [""]
RichMonitoringSysConf().RichRecPidConfig                = "None"

#
# Disable pixel cleaning when creating RichRecPixels
#
RichPixelCreatorConfig().PixelCleaning = "None"

#
# Tracking. Run our own tracking sequence
#
Panoptes().UsePrivateTracking = False

#
# High level reconstruction (from RichRecQC)
#
RichMonitoringSysConf().MonitorReconstruction = False
RichMonitoringSysConf().MonitorPixels         = False # Monitor reconstructed pixels
RichMonitoringSysConf().MonitorPhoton         = False # Track based photon resolution
RichMonitoringSysConf().MonitorPID            = False # Disable PID monitoring
RichMonitoringSysConf().MonitorTracklessRing  = False # Monitor the trackless ring finding
RichMonitoringSysConf().MonitorMirrorAlign    = False # The mirror alignment monitoring
RichMonitoringSysConf().MonitorRaw            = False # 

#
# Low level DAQ monitors
#
RichMonitoringSysConf().DaqMon_UpdateTimerInterval = 900

RichMonitoringSysConf().RawDataDBCheck          = True
RichMonitoringSysConf().RawDataDecodingCheck    = True
RichMonitoringSysConf().RawDataSizeCheck        = True
RichMonitoringSysConf().MissingHPDCheck         = True
RichMonitoringSysConf().HotPixelCheck           = True
RichMonitoringSysConf().MonitorNhits            = True
RichMonitoringSysConf().MonitorDAQ              = True 
RichMonitoringSysConf().MonitorHitMaps          = True
RichMonitoringSysConf().MonitorIFB              = False 
RichMonitoringSysConf().MonitorCalibration      = False
RichMonitoringSysConf().MonitorImageMovements   = False

RichMonitoringSysConf().DaqMon_MonitorBXID      = True
RichMonitoringSysConf().DaqMon_MaxErrorMessages = 10000
RichMonitoringSysConf().DaqMon_Plot2DHisto      = False
RichMonitoringSysConf().DaqMon_PrintMessages    = True

#RichMonitoringSysConf().OutputLevelDAQMon = 1

#
# Hitmaps
#
RichMonitoringSysConf().HitMapsMon_LowResHitMaps   = True
RichMonitoringSysConf().HitMapsMon_HotPixEnabled   = False
RichMonitoringSysConf().HitMapsMon_HPDCountEnabled = False


RichMonitoringSysConf().NHitMon_RemoveFaultyHPD    = False
RichMonitoringSysConf().NHitMon_HitThreshold       = 50  # #hits above which send disable command to UKL1
RichMonitoringSysConf().NHitMon_HistoNHitMax       = 10000
RichMonitoringSysConf().NHitMon_TriggerTypes       = [ 0, 1, 3, 6, 7 ]
RichMonitoringSysConf().NHitMon_MovingAverageFast  = 20
RichMonitoringSysConf().NHitMon_MovingAverageSlow  = 500
RichMonitoringSysConf().NHitMon_IndividualTriggerMonitors = True

#
# Event 'snapshots' sent to CAMERA
#
RichMonitoringSysConf().SendEventSnapshots         = True
RichMonitoringSysConf().SendNHitEventSnapshots     = True  # for #hit monitor
RichMonitoringSysConf().SendDaqEventSnapshots      = True  # for DAQ monitor
RichMonitoringSysConf().SnapshotUpdateInterval     = 99999
RichMonitoringSysConf().SnapshotNBins              = 200
RichMonitoringSysConf().SnapshotRingType           = "Isolated"

#
#HPD disable tool
#
RichMonitoringSysConf().HPDDisable                       = True
RichMonitoringSysConf().HPDDisable_2DHisto               = True
RichMonitoringSysConf().HPDDisable_UpdateTimerInterval   = 6
RichMonitoringSysConf().HPDDisable_HistoryTime           = 300
RichMonitoringSysConf().HPDDisable_BufferSize            = 1000
RichMonitoringSysConf().HPDDisable_DisableCheckInterval  = 999999
RichMonitoringSysConf().HPDDisable_DisableCheckHeartBeat = 3
RichMonitoringSysConf().HPDDisable_CameraSummaryInterval = 300
RichMonitoringSysConf().HPDDisable_AlwaysDisable         = False

#
# Histogram saver cycle
Panoptes().SaverCycle = 360

# TAE events - should be set automatically
#Panoptes().RawEventLocations = [
#    # Primary event
#  ""
#  # Previous events (extend as needed)
#  ,"Prev1", "Prev2"
#  # Successive events (extend as needed)
#  ,"Next1", "Next2", "Next3"
#  ]

#from Configurables import UpdateManagerSvc
#UpdateManagerSvc().ConditionsOverride += [ "Conditions/ReadoutConf/Rich1/DetectorNumbers := int_v InactiveHPDs = ;"]
#UpdateManagerSvc().ConditionsOverride += [ "Conditions/ReadoutConf/Rich2/DetectorNumbers := int_v InactiveHPDs = ;"]
