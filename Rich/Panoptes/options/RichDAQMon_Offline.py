from Gaudi.Configuration import *
from Configurables import Panoptes, RichMonitoringSysConf, CondDB, CondDBAccessSvc
from Panoptes.Configuration import *
import os, glob

importOptions( '$PANOPTESROOT/options/RichDAQMon-Common.py' )
 
Panoptes().Mode       = 'Offline'
Panoptes().EvtMax     = -1
EventSelector().PrintFreq = 1000

# override CondDBTag with the one provided by the Online environment?
# -> n.b. only works in "online" or "calibration" mode, setting ignored otherwise
Panoptes().UseOnlineCondDBtag      = False

#Panoptes().UseMonitorSvc = True

Panoptes().SaverCycle              = 6000000

# test hex code
hex = Panoptes().hexmask([8,48,55,57,96])
print "Baa ", hex
hex = Panoptes().hexmask([100,104,105,106])
print "Baa ", hex

# For testing only
CondDB().IgnoreHeartBeat = True

# Turn on Monitor service for offline presenter testing
#Panoptes().UseMonitorSvc = True

#RichMonitoringSysConf().RandomTestHPDErrorRate      = 0.001
#RichMonitoringSysConf().RandomTestHPDDisableRate    = 0.001
#RichMonitoringSysConf().RandomTestHPDErrorInterval  = 4000
#RichMonitoringSysConf().RandomTestHPDErrorBurstSize = 2000
#RichMonitoringSysConf().RandomTestHPDDResetInterval = 999999

#RichMonitoringSysConf().DaqMon_SendAlertDirect   = True
#RichMonitoringSysConf().DaqMon_SendAlertMessages = True

#RichMonitoringSysConf().OutputLevelDisable = 1

# Send messages faster to camera, just for testing
interval = 30
RichMonitoringSysConf().HPDDisable_UpdateTimerInterval = interval
RichMonitoringSysConf().DaqMon_UpdateTimerInterval     = interval
RichMonitoringSysConf().NHitMon_UpdateTimerInterval    = interval
RichMonitoringSysConf().HPDDisable_HistoryTime         = 30
RichMonitoringSysConf().HPDDisable_CameraSummaryInterval = 40

#RichMonitoringSysConf().OutputLevelDAQMon = 1

# Lower the threshold for disabling, for tests
#RichMonitoringSysConf().HPDDisable_FailureRateThreshold = -1.0
#RichMonitoringSysConf().HPDDisable_BufferSize = 10

# TAE events - should be set automatically
#Panoptes().RawEventLocations = [
#    # Primary event
#  ""
#      # Previous events (extend as needed)
#      ,"Prev1"
#      # Successive events (extend as needed)
#      ,"Next1"
#  ]

# CAMERA
import socket
host = socket.gethostname()
if host != "localhost" : host = socket.gethostbyaddr(host)[0]
Camera().CameraServers = [ host + ":45123" ]

EventSelector().Input = [
   "DATAFILE='PFN:/daqarea/lhcb/data/2012/RAW/FULL/RICH/TEST/107500/107500_0000000001.raw' SVC='LHCb::MDFSelector'"
]
