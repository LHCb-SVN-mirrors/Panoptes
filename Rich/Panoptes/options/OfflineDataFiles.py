
from Panoptes.Configuration import *
from Gaudi.Configuration import *
import glob
   
# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/Collision16/LHCb/Raw/", # Cambridge
    "/usera/jonesc/NFS/data/Collision15/LHCb/Raw/", # Cambridge
    "/home/chris/LHCb/Data/"                        # CRJ's CernVM
    ]
EventSelector().Input = [ ]
for path in searchPaths :
    files = sorted(glob.glob(path+"*/*.raw"))
    EventSelector().Input += [ "DATAFILE='PFN:"+file+"' SVC='LHCb::MDFSelector'" for file in files ]
    
