##############################################################################
# File for running Panoptes with default options 
# Edit one or more of the lines below to change the defaults
##############################################################################
# Syntax is:
#   gaudirun.py Panoptes-Default.py <someDataFiles>.py
##############################################################################

from Panoptes.Configuration   import *
from RichRecSys.Configuration import *
from RichRecQC.Configuration  import *
from RichMarkovRingFinder.Configuration import *

##############################################################################

# Number of events to process
Panoptes().EvtMax     = 50000
#LHCbApp().skipEvents  = 5500
# Event number print frequency
#Panoptes().PrintFreq  = 1000

# Data base
Panoptes().DDDBtag    = "head-20120413"
Panoptes().CondDBtag  = "cond-20120628"

# Offline or online ??
Panoptes().Mode = "Offline" # Use "Online" for online mode

# Reconstruction options
RichRecSysConf().CheckProcStatus       = False
RichRecSysConf().InitPixels            = False  # RichRecPixel objects
RichRecSysConf().InitTracks            = False  # RichRecTrack objects (requires Tracking)
RichRecSysConf().InitPhotons           = False  # RichRecPixel objects (requires Tracking)
#RichRecSysConf().TracklessRings        = False  # Run the trackless ring finding algs
#RichPixelCreatorConfig().pixelCleaning = "None" # Disable pixel cleaning

# PID algorithms
RichRecSysConf().PidConfig       = "None" # Disable PID sequence

# Tracking. Run our own tracking sequence
Panoptes().UsePrivateTracking = False

# Monitoring options
# Low level DAQ monitors
RichMonitoringSysConf().MonitorNhits          = False
RichMonitoringSysConf().MonitorDAQ            = False
RichMonitoringSysConf().MonitorHitMaps        = False
#RichMonitoringSysConf().MonitorOnlineStatus   = False
RichMonitoringSysConf().MonitorIFB            = False
RichMonitoringSysConf().MonitorCalibration    = False
# Send Event snapshots of events with 'errors'
RichMonitoringSysConf().SendEventSnapshots = False

# High level reconstruction
RichRecQCConf().MoniSequencer = GaudiSequencer("Fake")
RichMonitoringSysConf().MonitorReconstruction = False
RichRecQCConf().PixelMonitoring               = False  # Monitor reconstructed pixels
RichRecQCConf().PhotonMonitoring              = False  # Track based photon resolution
RichRecQCConf().PidMonitoring                 = False  # Disable PID monitoring
RichRecQCConf().TracklessRingMonitoring       = False  # Monitor the trackless ring finding
RichRecQCConf().AlignmentMonitoring           = False  # The mirror alignment monitoring

# Max number hits per HPD panel for ring finding
# RichMarkovRingFinderConf().maxHitsInPanel = 999;

# Merge events
seq = GaudiSequencer("MergeSeq")
seq.MeasureTime = True
ApplicationMgr().TopAlg += [seq]

from Configurables import ( StoreExplorerAlg, LHCb__RawEventTestDump )

EventDataSvc().RootCLID              = 1
EventDataSvc().EnableFaultHandler    = True

from Configurables import Rich__TAEventMergeAlg
merger = Rich__TAEventMergeAlg("TAEventMerge")
merger.NMergeStreams = 1
#merger.OutputLevel = 1
#merger.SpilloverLocations = [ ]
seq.Members += [merger]

#explorer = StoreExplorerAlg("TESExplore")
#explorer.Load                = 1
#explorer.PrintFreq           = 1.0
#explorer.OutputLevel         = 1
#seq.Members += [explorer]

# TAE events
Panoptes().RawEventLocations = [
    # Primary event
    ""
    # Previous events
    ,"Prev1", "Prev2", "Prev3", "Prev4", "Prev5"
    # Successive events
    ,"Next1", "Next2", "Next3", "Next4", "Next5"
    # Previous events
    #,"Prev1"
    # Successive events
    #,"Next1"
    ]

Panoptes().WriteOutSelectedEvents = True
Panoptes().SelectedEventsDataFile = "merged.raw"

##############################################################################

# Main events
EventSelector().Input = [
    "DATAFILE='PFN:/r22/lhcb/data/2009/RAW/FULL/LHCb/BEAM1/62558/062558_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]

# RICH events to add
EventSelector("MergeEventSelector0").Input = [
    "DATAFILE='PFN:/r22/lhcb/data/2009/RAW/FULL/RICH/TEST/62559/062559_0000000001.raw' SVC='LHCb::MDFSelector'"
    ]
#EventSelector("MergeEventSelector0").PrintFreq = Panoptes().printFreq

EventSelector("MergeEventSelector0").PrintFreq = 1
EventSelector().PrintFreq = 1

test = GaudiSequencer("SelectRingEvents")
seq.Members += [test]
OutputStream("RawWriter").RequireAlgs = ["SelectRingEvents"]
OutputStream("RawWriter").OutputLevel = 1
