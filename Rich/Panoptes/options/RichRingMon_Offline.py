from Gaudi.Configuration import *
from Configurables import Panoptes
from Panoptes.Configuration import *

importOptions( "$PANOPTESROOT/options/RichRingMon-Common.py" )

Panoptes().Mode   = 'Offline' 
Panoptes().EvtMax = 100000
EventSelector().PrintFreq = 1000

Panoptes().SaverCycle = 6000000

CondDB().IgnoreHeartBeat = True

# Not automatic offline ....
#Panoptes().RawEventLocations = [
  # Primary event
  #"","Next1"
  #    # Previous events (extend as needed)
  #    ,"Prev1"
  #    # Successive events (extend as needed)
  #    ,"Next1"
#  ]

# CAMERA
import socket
host = socket.gethostname()
if host != "localhost" : host = socket.gethostbyaddr(host)[0]
Camera().CameraServers = [ host + ":45123" ]

#from Configurables import RichENNRingFinderConf
#RichENNRingFinderConf().Panels = [ "AerogelTop","AerogelBottom" ]

# make one snapshot per second to test
RichMonitoringSysConf().SnapshotUpdateInterval   =  1
