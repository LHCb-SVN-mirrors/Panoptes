
from Gaudi.Configuration import *
from Configurables import Panoptes, RichMonitoringSysConf, CondDB
from Panoptes.Configuration import *

importOptions( "$PANOPTESROOT/options/RichCalibMon-Common.py" )

Panoptes()     .Mode      = 'Offline'
Panoptes()     .EvtMax    = 10000
EventSelector().PrintFreq = 1000

Panoptes().SaverCycle = 6000000

# For testing only
CondDB().IgnoreHeartBeat = True

# Turn on Monitor service for offline presenter testing
#Panoptes().UseMonitorSvc = True

# # make one snapshot per second to test
# RichMonitoringSysConf().SnapshotUpdateInterval = 1

# Send messages faster to camera, just for testing
RichMonitoringSysConf().CalibMon_UpdateTimerInterval = 30

# TAE events - should be set automatically
#Panoptes().RawEventLocations = [
#    # Primary event
#  ""
#      # Previous events (extend as needed)
#      ,"Prev1"
#      # Successive events (extend as needed)
#      ,"Next1"
#  ]

# CAMERA
import socket
host = socket.gethostbyaddr( socket.gethostname() )[ 0 ]
Camera().CameraServers = [ host + ":45123" ]
