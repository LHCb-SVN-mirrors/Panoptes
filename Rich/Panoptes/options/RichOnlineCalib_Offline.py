
import glob, os, Gaudi.Configuration

from Configurables import GaudiSequencer, ApplicationMgr, LHCb__FmcMessageSvc
from Configurables import OMARichRefIndex, OMARichHPDImage, Camera

MSG_VERBOSE = 1
MSG_DEBUG   = 2
MSG_INFO    = 3
MSG_WARNING = 4
MSG_ERROR   = 5
MSG_FATAL   = 6
MSG_ALWAYS  = 7

def setup():
  
  RichOnCalibSeq = GaudiSequencer("RichOnCalibSeq")
  RefIndexAlg = OMARichRefIndex("RefractiveIndexCalibration")

  # Input files. Check what is available...
  searchPaths = [
    "/hist/Savesets/2015/LHCb/Brunel/09/15/", # At the pit
    "/usera/jonesc/NFS/data/Savesets/",       # Cambridge
    "/mnt/shared/chris/LHCb/Savesets/"        # CRJ CernVM
    ]
  for path in searchPaths :
    RefIndexAlg.InputFiles += sorted(glob.glob(path+"*EOR.root"))
  
  RefIndexAlg.xmlFilePath = "/tmp/HPDCalib"
  RefIndexAlg.DisableDIMPublish = True

  RichOnCalibSeq.Members += [
      RefIndexAlg,
      #HPDImageAlg
      ]
  RichOnCalibSeq.IgnoreFilterPassed = True

  # Output Level of both
  RefIndexAlg.OutputLevel = 2
  #HPDImageAlg.OutputLevel = 3

  # Camera setup
  import socket
  host = socket.gethostname()
  if host != "localhost" : host = socket.gethostbyaddr(host)[0]
  Camera().CameraServers = [ host + ":45123" ]
  
  ApplicationMgr().TopAlg += [ RichOnCalibSeq ]
  #ApplicationMgr().ExtSvc += [ "LHCb::PublishSvc" ]
  ApplicationMgr().EvtSel = "NONE"
  ApplicationMgr().ExtSvc += [ "MonitorSvc" ]
  #ApplicationMgr().Runable = "LHCb::OnlineRunable/Runable"

  from Configurables import LHCbApp
  LHCbApp().DDDBtag   = "head-20120413"
  LHCbApp().CondDBtag = "cond-20120730"
  print '[WARN] .....RichOnlineCalib.py  setup finished....'

setup()
