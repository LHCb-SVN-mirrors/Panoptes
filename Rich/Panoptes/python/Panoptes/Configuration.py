# @package Panoptes
#  High level configuration tools for Panoptes
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   21/08/2008

__version__ = "$Id: Configuration.py,v 1.52 2009-12-07 10:13:37 ukerzel Exp $"
__author__  = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

from os                              import getenv
from LHCbKernel.Configuration        import *
from Gaudi.Configuration             import appendPostConfigAction
from RichMonitoringSys.Configuration import *
import GaudiKernel.ProcessJobOptions
from Configurables import ( CondDBCnvSvc, MagneticFieldSvc, OutputStream,
                            GaudiSequencer, EventClockSvc, Camera,
                            LHCb__FmcMessageSvc, DDDBConf, LHCbApp,
                            UpdateAndReset, MessageSvc, CondDB, COOLConfSvc )

def disableLFC():
    COOLConfSvc(UseLFCReplicaSvc = False)

## @class Panoptes
#  Configurable for Panoptes application
#  @author Chris Jones <Christopher.Rob.Jones@cern.ch>
#  @date   27/08/2008
class Panoptes(LHCbConfigurableUser):

    __used_configurables__ = [ LHCbApp, MessageSvc, Camera]

    KnownModes          = ['Offline', 'Online', 'OnlineTest', 'Calibration']
    KnownDataTypes      = ['DC06' ,'MC09', '2008', '2009', '2010', '2011', '2012', '2015', '2016' ]
    KnownPartitions     = ['RICH', 'RICH1', 'RICH2', 'LHCb', 'FEST']

    maskRaw             = 0x1   #all raw events accepted by HLT
    maskExpress         = 0x10
    maskReco            = 0x100
    maskHLTTimeout      = 0x200
    maskAccessViolation = 0x400
    
    # Steering options
    __slots__ = {
        "MonName"                        : "RichDAQMon"
       ,"Mode"                           : "Offline"                 # The mode (online, offline, calibration)
       ,"Partition"                      : None
       ,"EvtMax"                         :    -1                     # Maximum number of events to process
       ,"SkipEvents"                     : 0
       ,"DataType"                       : "2016"
       ,"Simulation"                     : False
       ,"UseOracle"                      : False
       ,"DDDBtag"                        :  ""                       # geometry database tag
       ,"CondDBtag"                      :  ""                       # conditions database tag
       ,"UseOnlineCondDBtag"             : False                     # use conddb provided by online environment?
       ,"UseMonitorSvc"                  : False
       ,"UseIncidentSvc"                 : True
       ,"IgnoreEventIDMisMatches"        : False
       ,"SuppressDecodingWarningsErrors" : False
       ,"WriteOutSelectedEvents"         : False
       ,"SelectedEventsDataFile"         : "SelectedEvents.raw"
       ,"UsePrivateTracking"             : False
       ,"FieldOff"                       : False                     # set to True for magnetic field off data
       ,"VeloOpen"                       : False                     # set to True for Velo open data
       ,"RawEventLocations"              : [ ""  ]                   # The RawEvent TES locations to use ( empty means defaults)
       ,"MessageSvcOutputLevel"          : 3
       ,"msgSvc"                         : None
       ,"SaverCycle"                     : 900                       # seconds between saving histograms
       ,"MonitorRawEvents"               : True                      # online monitoring of raw events
       ,"MonitorExpressEvents"           : False                     # online monitoring of events in express stream
       ,"MonitorReconstructedEvents"     : False                     # online monitoring of reconstructed events
       ,"DisableUpdateAndReset"          : False                     # Completely turn off UpdateAndReset
       ,"SaveHistograms"                 : 1                         # Save histograms with UpdateAndReset
       ,"ResetHistogramsAfterSave"       : 1                         # Reset histograms after saving
       ,"TriggerMask"                    : [48,55,57,96]             # The trigger bits to select events
       ,"VetoMask"                       : [100,104,105,106]         # The trigger bits to veto events
        }

    ## Apply the configuration
    def applyConf(self):
        
        # Mode
        mode = self.getProp("Mode")

        # Partition (if running online, None otherwise)
        partition = os.getenv('PARTITION','')
        self.setProp("Partition", partition)

        #sanity checks
        self.checkConfiguration()
        
        if mode == "Online" or mode == 'OnlineTest' or mode == "Calibration":
            if not self.getProp("DisableUpdateAndReset") :
                # Saver configuration
                # N.B. has to be first algorithm in sequence
                updateAndReset                      = UpdateAndReset("UpdateAndReset")
                updateAndReset.saveHistograms       = self.getProp("SaveHistograms")
                updateAndReset.saverCycle           = self.getProp("SaverCycle")
                updateAndReset.resetHistosAfterSave = self.getProp("ResetHistogramsAfterSave")
                updateAndReset.saveSetDir           = "/hist/Savesets"             
                # updateAndReset.OutputLevel          = 1
                saverSeq                            = GaudiSequencer("RichSaver")
                saverSeq.MeasureTime                = True
                saverSeq.Members                   += [updateAndReset]
                ApplicationMgr().TopAlg            += [saverSeq]

            #use MonitorSvc 
            self.setProp("UseMonitorSvc", True)

        if mode == 'Calibration':
            if not self.getProp("DisableUpdateAndReset") :
                updateAndReset.MyName = 'RichDAQCalib'

        if mode == "Online" or mode == "Calibration":
           #import  OnlineEnv
           import MonitoringEnv as Online
           ApplicationMgr().SvcOptMapping.append('LHCb::FmcMessageSvc/MessageSvc')
           ApplicationMgr().MessageSvcType  = 'LHCb::FmcMessageSvc'
           del allConfigurables['MessageSvc'] #remove any previous Gaudi configuration
           self.msgSvc                      = LHCb__FmcMessageSvc('MessageSvc')
           self.msgSvc.fifoPath             = os.getenv('LOGFIFO','')
           #self.msgSvc.OutputLevel          = Online.OutputLevel
           self.msgSvc.doPrintAlways        = False
           self.msgSvc.LoggerOnly           = True

           #TAE configuration
           if Online.TAE != 0:
               #we're in TAE mode
               taeLocs = []
               for slot in range( -(Online.TAE-1)/2, (Online.TAE+1)/2+1):
                   if slot<0:
                       taeLocs.append("Prev"+str(-slot))
                   if slot==0:
                       taeLocs.append("")
                   if slot>0:
                       taeLocs.append("Next"+str(slot))
               self.setProp("RawEventLocations", taeLocs)    
        else:
           self.msgSvc                      = MessageSvc()
           self.msgSvc.OutputLevel          = self.getProp("MessageSvcOutputLevel")
           self.msgSvc.Format               = "% F%20W%S%7W%R%T %0W%M"


            
        # General application setup
        self.commonConfig()

        # The RICH Monitoring configuration
        # pass on configuration settings to RichMonitoringSys
        conf = RichMonitoringSysConf()
        self.setOtherProps(conf,["Mode","Partition","RawEventLocations"])
        
        # Should we use our own tracking ?
        if self.getProp("UsePrivateTracking") :
            tkseq = GaudiSequencer("Tracking")
            tkseq.MeasureTime = True
            ApplicationMgr().TopAlg += [tkseq]
            self.privateTracking(tkseq)
            
        # make a sequence for the monitors
        seq = GaudiSequencer("RichMoni")
        seq.MeasureTime = True
        ApplicationMgr().TopAlg += [seq]

        # Write out selected events
        if self.getProp("WriteOutSelectedEvents") :
            writer             = OutputStream("RawWriter")
            writer.RequireAlgs = ["SelectRingEvents"]
            writer.Preload     = False
            writer.Output      = "DATAFILE='"+self.getProp("SelectedEventsDataFile")+"' TYP='POOL_ROOTTREE' OPT='RECREATE'"
            writer.OutputLevel = 3
            writer.ItemList    = [ "/Event/DAQ#1", "/Event/DAQ/RawEvent#1" ]
            taeLocs = self.getProp("RawEventLocations")
            for loc in taeLocs :
                if loc != "" :
                    writer.OptItemList += [ "/Event/"+loc+"/DAQ#1", "/Event/"+loc+"/DAQ/RawEvent#1" ]
            ApplicationMgr().OutStream.append( writer )

        if mode == "Offline" :
            # Data access
            # CRJ : Don't do by default... Leave to the user to do at the command line (needed for data merging)
            #importOptions( "$PANOPTESROOT/options/OfflineDataFiles.py" )
            self.msgSvc.OutputLevel = self.getProp("MessageSvcOutputLevel")
            self.msgSvc.Format      = '% F%30W%S%7W%R%T %0W%M'
            
# =================================================================================

    def hexmask(self,list_of_bits):
        mask = [0,0,0,0]
        for bit in list_of_bits : 
            mask[bit/32] = mask[bit/32] | (1 << (bit - 32*(bit/32)))
        return format(mask[0],"#x")+","+format(mask[1], "#x")+","+format(mask[2], "#x")+","+format(mask[3], "#x") 

    def commonConfig(self):

        from os import getenv

        from Configurables import (ApplicationMgr,AuditorSvc,SequencerTimerTool)

        mode        = self.getProp("Mode")
        partition   = self.getProp("Partition")

        # General configuration
        DDDBConf()

        IODataManager().AgeLimit = 0

        # Setup for online environment - different for MonitoringFarm and CalibrationFarm
        # Common part first
        # - need online event selector rather than the default one
        #   (however, not yet found at this stage?)
        if mode == "Online" or mode == 'OnlineTest' or mode == "Calibration" :
            #import OnlineEnv as Online
            import MonitoringEnv as Online
            isTAE       = False
            if Online.TAE != 0:
                isTAE = True

            # Prevent showing unwanted error.
            import logging
            logging.getLogger().setLevel( logging.FATAL )

            ApplicationMgr().SvcOptMapping.append('LHCb::OnlineEvtSelector/EventSelector')

        # MonitoringFarm    
        if mode == "Online" or mode == 'OnlineTest':
            
            # Old line
            #req = 'EvType=2;TriggerMask=0x100,0x4,0x0,0x0;VetoMask=0x0,0x0,0x0,0x710;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'

            # New using hexmask to create the masks on the fly
            trigmask = self.hexmask( self.getProp("TriggerMask") )
            vetomask = self.hexmask( self.getProp("VetoMask") )
            req = 'EvType=2;TriggerMask='+trigmask+';VetoMask='+vetomask+';MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
            print req
        
            mepMgr                               = Online.mepManager(Online.PartitionID,Online.PartitionName,['Events'],True)
            ApplicationMgr().EvtSel              = Online.mbmSelector(input='Events',type='ONE',decode=False)
            ApplicationMgr().EvtSel.REQ1         = req
            ApplicationMgr().Runable             = Online.evtRunable(mepMgr)
            ApplicationMgr().ExtSvc.append(mepMgr)

        # CalibrationFarm - different setup for TAE and non-TAE mode   
        if mode == "Calibration":
            if isTAE:
                mepMgr                               = Online.mepManager( Online.PartitionID, Online.PartitionName,
                                                                          ["Events"], partitionBuffers = True )
                ApplicationMgr().EvtSel              = Online.mbmSelector( input = 'Events', type = 'USER', decode = False )
                ApplicationMgr().EvtSel.REQ1         = 'EvType=1;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
                ApplicationMgr().EvtSel.AllowSuspend = False;
                ApplicationMgr().Runable             = Online.evtRunable(mepMgr)
                ApplicationMgr().ExtSvc.append(mepMgr)
            else:
                mepMgr                               = Online.mepManager( Online.PartitionID, Online.PartitionName,
                                                                           ["Events"], partitionBuffers = True )
                ApplicationMgr().EvtSel              = Online.mbmSelector( input = 'Events', type = 'USER', decode = False )
                ApplicationMgr().EvtSel.REQ1         = 'EvType=1;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
                ApplicationMgr().EvtSel.AllowSuspend = False;
                ApplicationMgr().Runable             = Online.evtRunable(mepMgr)
                ApplicationMgr().ExtSvc.append(mepMgr)

            
        # Additional services
        ApplicationMgr().ExtSvc += [ "ToolSvc", "AuditorSvc" ]

        # Get the event time (for CondDb) from ODIN
        EventClockSvc().EventTimeDecoder  = "OdinTimeDecoder"
        EventDataSvc().RootCLID           = 1
        EventDataSvc().EnableFaultHandler = True

        self.defineGeometry()
        self.defineEvents()

        # raw event decoding
        if self.getProp("IgnoreEventIDMisMatches") :
            RichTools().rawDecoder().CheckEventIDs = False
        if self.getProp("SuppressDecodingWarningsErrors") :
            RichTools().rawDecoder().OutputLevel   = 6
        # TAE events
        eventLocs = self.getProp("RawEventLocations")
        if len(eventLocs) > 0 :
            RichTools().smartIDTool().RawEventLocations = eventLocs

        # MDF data
        importOptions( "$STDOPTS/RawDataIO.opts" )

        # Camera
        if not Camera().isPropertySet("CameraServers") :
            # These are the partitions that are running with the hardware,
            # basically unless we know the partition is going to be using
            # a real RICH we default to the offline server.
            if self.getProp("Partition") in ["RICH","RICH1","RICH2","LHCb"]:
                servers = ["hist01.lbdaq.cern.ch:45123"]
                Camera().setProp("CameraServers", servers)
            else:
                servers = ["hist01.lbdaq.cern.ch:45121"]
                Camera().setProp("CameraServers", servers)
        print Camera().CameraServers

        # Monitoring Svc
        if self.getProp("UseMonitorSvc") :
            import MonitoringEnv
            ApplicationMgr().ExtSvc += [ "MonitorSvc" ]
            from Configurables import MonitorSvc
            MonitorSvc().ExpandCounterServices = True
            #MonitorSvc().ExpandNameInfix = "<proc>_<program>"
            #DimName = "<part>_x_"+self.getProp("MonName")+"_<program>"
            DimName = "<part>_x_"+self.getProp("MonName")+"_"
            MonitorSvc().ExpandNameInfix = DimName
            MonitorSvc().PartitionName = MonitoringEnv.PartitionName

        # Incident Svc
        if self.getProp("UseIncidentSvc") :
            ApplicationMgr().ExtSvc += [ "IncidentSvc" ]

        # Histos
        if self.getProp("Mode") == "Offline" :
            ApplicationMgr().HistogramPersistency = "ROOT"
            HistogramPersistencySvc().OutputFile = "PanoptesOfflineHistos.root"
        else:
            ApplicationMgr().HistogramPersistency = "NONE"

        # Use TimingAuditor for timing, suppress printout from SequencerTimerTool
        ApplicationMgr().AuditAlgorithms = True
        AuditorSvc().Auditors += [ 'TimingAuditor' ] 
        SequencerTimerTool().OutputLevel = 4
        
    def defineGeometry(self):
        # check if CondDB tag should be taken from online environment
        mode              = self.getProp("Mode")
        overrideCondDBTag = self.getProp("UseOnlineCondDBtag") 
        if overrideCondDBTag == True and (mode == "Online" or mode == "Calibration"):            
            import MonitoringEnv as Online
            onlineCondDBTag = ""
            # check  if tag variable is available in online environment
            if (Online.condDBTag):
                onlineCondDBTag = Online.condDBTag
                # check if variable is set in the online environment - not always done
                if len(onlineCondDBTag) > 0:
                    self.setProp("CondDBtag",onlineCondDBTag )
        
        #self.setOtherProps(LHCbApp(),["DataType","CondDBtag","DDDBtag","UseOracle","Simulation"])
        if self.getProp("UseOracle"):
            CORAL_XML_DIR='/group/online/condb_viewer'
            ApplicationMgr().Environment["CORAL_AUTH_PATH"]     = CORAL_XML_DIR
            ApplicationMgr().Environment["CORAL_DBLOOKUP_PATH"] = CORAL_XML_DIR
            CondDB(UseOracle = True)
            CondDB().IgnoreHeartBeat = True
            appendPostConfigAction(disableLFC)
        self.setOtherProps(LHCbApp(),["DataType","CondDBtag","DDDBtag","Simulation"])

    def defineEvents(self):
        self.setOtherProps(LHCbApp(),["EvtMax","SkipEvents"])
        ## LHCbApp().EvtMax          = self.getProp("EvtMax")
        ## LHCbApp().SkipEvents      = self.getProp("SkipEvents")
    
    def privateTracking(self,sequence):
        from TrackSys.Configuration   import TrackSys
        trackConf = TrackSys()
        self.setOtherProps(trackConf,["FieldOff","VeloOpen"])
        # TrackSys expects certain sequences to be present ...
        sequence.Members += [ GaudiSequencer("RecoVELOSeq"),
                              GaudiSequencer("RecoTTSeq"),
                              GaudiSequencer("RecoITSeq"),
                              GaudiSequencer("RecoOTSeq"),
                              GaudiSequencer("RecoTrSeq") ]
        del trackConf
        
# =================================================================================
# Details from Markus:
# 0x1   : bit set for all raw events (i.e. "raw" and express)
# 0x10  : bit set for express stream
# 0x100 : bit set for reconstructed events
# 0x200 : HLT timeout       - has to be masked off
# 0x400 : access violation  - has to be masked off
# The HLT produces _one_ stream. All events have bit 0x1 set, the express
# stream _in addition_ 0x10
#
    def defineTriggerMask(self):
        maskTrigger =  0xffffffff 
        
        rawBitSet   =  False
        if self.getProp("MonitorRawEvents"):
            maskTrigger = maskTrigger - self.maskRaw
            rawBitSet   = True

        if self.getProp("MonitorExpressEvents"):
            maskTrigger = maskTrigger - self.maskExpress
            if not rawBitSet:
                maskTrigger = maskTrigger - self.maskRaw

        if self.getProp("MonitorReconstructedEvents"):
            maskTrigger = maskTrigger - self.maskReco

        return str(hex(maskTrigger))    
        

# =================================================================================
    def defineVetoMask(self):

        maskVeto = 0x0 + self.maskHLTTimeout + self.maskAccessViolation
        
        if not self.getProp("MonitorRawEvents"):
            maskVeto = maskVeto + self.maskRaw

        if not self.getProp("MonitorExpressEvents"):
            maskVeto = maskVeto + self.maskExpress

        if not self.getProp("MonitorReconstructedEvents"):
            maskVeto = maskVeto + self.maskReco

        return str(hex(maskVeto))    

# =================================================================================
    def checkConfiguration(self):
        mode            = self.getProp("Mode")
        partition       = self.getProp("Partition")

        monitorRaw      = self.getProp("MonitorRawEvents")
        monitorExpress  = self.getProp("MonitorExpressEvents")
        monitorReco     = self.getProp("MonitorReconstructedEvents")
        
        if mode not in self.KnownModes:
            raise RuntimeError("ERROR : Unknown Panoptes Mode '%s'" % mode)
        
        if mode == 'Online' or mode == 'Calibration':
            if partition not in self.KnownPartitions:
                raise RuntimeError("ERROR : Unkonwn Panoptes Partition '%s'" % partition)

        if (monitorRaw or monitorExpress) and monitorReco:
            print "monitor raw           events %s " % monitorRaw
            print "monitor express       events %s " % monitorExpress
            print "monitor reconstructed events %s " % monitorReco
            raise RuntimeError("ERROR: can't subscribe to both RAW and RECONSTRUCTED streams")
