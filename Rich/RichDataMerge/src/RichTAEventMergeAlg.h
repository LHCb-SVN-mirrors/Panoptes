// $Id: RichTAEventMergeAlg.h,v 1.5 2009-11-25 16:19:41 jonrob Exp $
#ifndef RICHTAEVENTMERGEALG_H
#define RICHTAEVENTMERGEALG_H 1

#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <cmath>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IEvtSelector.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/ISvcManager.h"
#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/LinkManager.h"
#include "GaudiKernel/DataStoreItem.h"
#include "GaudiKernel/IEventTimeDecoder.h"

// boost
#include "boost/lexical_cast.hpp"
#include "boost/assign/list_of.hpp"

// Event
#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/ODIN.h"

namespace Rich
{

  /** @class TAEventMergeAlg RichTAEventMergeAlg.h
   *
   *  Algorithm to read in two MDF files and merge the events into
   *  a single file.
   *
   *  @author Christopher Rob JONES
   *  @date   2008-09-23
   */
  class TAEventMergeAlg : public GaudiHistoAlg
  {

  public:

    /// Standard constructor
    TAEventMergeAlg( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~TAEventMergeAlg( );     ///< Destructor

    virtual StatusCode initialize(); ///< Algorithm initialise
    virtual StatusCode execute();    ///< Algorithm execute
    virtual StatusCode finalize();   ///< Algorithm finalize

  private:

    typedef std::map<std::string,LHCb::RawEvent*> TAEvents;

    class EventData
    {
    public:
      EventData() : gpsTime(0), bunchId(0) { }
      ~EventData()
      {
        for ( TAEvents::iterator iS = taeEvents.begin();
              iS != taeEvents.end(); ++iS )
        { delete iS->second; iS->second = NULL; }
      }
    public:
      LHCb::RawEvent rawEvent;
      ulonglong      gpsTime;
      unsigned int 	 bunchId;
      TAEvents       taeEvents;
    };

    typedef std::list<EventData*> OrderedEventData;
    typedef std::vector<OrderedEventData> MergeEvents;

  private:

    /// Copy spillover events
    void copySpills( TAEvents & taeEvents,
                     const bool mainEvent );

    /// read a new event for the given stream number
    StatusCode newEvent( const unsigned int iStream );

    /// Get the ODIN time tool
    const IEventTimeDecoder * timeTool() const
    {
      if (!m_timeTool) { m_timeTool = tool<IEventTimeDecoder>("OdinTimeDecoder"); }
      return m_timeTool;
    }

    /// Get the ODIN
    const LHCb::ODIN * odin()
    {
      timeTool()->getTime();
      return get<LHCb::ODIN>(LHCb::ODINLocation::Default);
    }

    /// Deep copy a RawEvent object
    StatusCode deepCopyRawEvent ( LHCb::RawEvent*  source,
                                  LHCb::RawEvent*& result,
                                  const bool skipODIN = false );

    /// Purge the event structure
    void purgeEvent();

    /// void print Current ODIN
    void printODIN( const std::string & mess,
                    const MSG::Level level = MSG::DEBUG,  	
                    const LHCb::ODIN * ODIN = NULL );

    /// Create new EventData
    void eventData( EventData& evData );

    /// spillover name
    std::string spillName( const int iS )
    {
      std::ostringstream txt;
      if ( iS > 0 )
      {
        txt << "Next" << iS << "/";
      }
      else if ( iS < 0 )
      {
        txt << "Prev" << -iS << "/";
      }
      return txt.str();
    }

  private:

    /// Pointer to ODIN (Event time) tool
    mutable const IEventTimeDecoder * m_timeTool;

    // Event selector for files to merge
    std::vector<IEvtSelector*> m_eventSel;

    /// EventSelector iterators
    std::vector<IEvtSelector::Context*> m_mergeIt;

    /// Number of data streams to merge
    unsigned int m_nStream;

    // ODINs and RawEvents for the merged events
    MergeEvents* m_mergeEvents;

    /// Min GPS time to merge events
    int m_minGPSTime;

    /// Number of events to save in list
    unsigned m_eventHistorySize;

    /// Was the last main event skipped
    bool m_skippedLastMainEv;

    /// Spillover locations
    std::vector<int> m_spillLocations;

    /// Active spills for current event
    std::set<int> m_activeSpills;

    /// Last main event number
    LHCb::ODIN* m_lastMainODIN;

    /// Last spillover ODINs
    std::vector<LHCb::ODIN>* m_lastSpillODIN;

  };

}

#endif // RICHTAEVENTMERGEALG_H
