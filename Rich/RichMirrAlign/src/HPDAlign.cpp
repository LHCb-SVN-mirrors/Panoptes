#include "HPDAlign.h"

using namespace std;
using             boost::lexical_cast;


HPDAlign::HPDAlign(string InputXMLFile, string InputFitFile, int rich)
:inputXMLFile(InputXMLFile), inputFitFile(InputFitFile),RICH(rich)
{
  int firstHPD = 0;
  if (RICH == 1)
  {
    firstHPD =   0;
    NumHPDs  = 196;                                         // 196 HPDs in RICH1. Numbered 0-195 inclusive
  }
  else if (RICH == 2)
  {
    firstHPD = 196;
    NumHPDs  = 288;                                   // check this Number
  }
  for (int HPD = firstHPD; HPD<NumHPDs+firstHPD; HPD++)
  {
    HPDs[HPD] = new HPDAlignParam(HPD);
  }

  cout << "Read Input XMls" << endl;
  ReadXMLFile(inputXMLFile+"P0.xml");
  ReadXMLFile(inputXMLFile+"P1.xml");

  cout << "Read Fit Results " << endl;
  ReadFitResultFile();
  cout << "Fit Results " << endl;

}

HPDAlign::~HPDAlign()
{
//	delete HPDs;
}

void HPDAlign::ReadXMLFile(string inputFile)
{
  //find out how Anatoly does it
  // Have to read two XML files here
  string line;
  size_t found;
  string str2 ("<condition");                               // look for line with this string contains HPD number
  std::vector<string> lines;
  ifstream xmlFile (inputFile.c_str());
  if (xmlFile.is_open())
  {
    while (xmlFile.good())                                  //while good
    {
      while ( getline(xmlFile, line))
      {
        found=line.find(str2);
        //				if (found!=string::npos) GetHPD(line);
        lines.push_back(line);
      // Find out what the line starts with
      }
    }
    xmlFile.close();                                      // close xml file
  }

  for (unsigned int i = 0; i < lines.size(); i++)
  {
    found=lines[i].find(str2);
    if (found!=string::npos)
    {
      double x = 0;
      double y = 0;
      int j = i + 1;
      //cout << "lines[j]" << lines[j] << endl;
      //cout << "lines[i]" << lines[i] << endl;

      GetInitalValue(lines[j], x,  y);
      int hpdNum = GetHPD(lines[i]);
      HPDs[hpdNum]->InitX(x);                         // Set initial values for x from xml file
      HPDs[hpdNum]->InitY(y);                         // Set initial values for x from xml file
    }
  }
}

int HPDAlign::GetHPD(string& line)
{
  cout << line << endl;
  string str("  <condition classID=\"\" name=\"HPD");
  size_t found;
  found=line.find_first_not_of(str,28);
  line.erase(0,found);

  //	cout << line << endl;

  string str2("_Align\">");
  size_t str2Length = str2.length();
  found=line.find(str2);
  line.erase(found,str2Length);

  //	cout << line << endl;

  int HPD = lexical_cast<int>(line);
  return HPD;
}

void HPDAlign::GetInitalValue(string& line, double& x, double& y)
{

  //erase pre number stuff
  string str("    <paramVector name=\"dPosXYZ\" type=\"double\">  ");

  size_t found;
  found=line.find_first_not_of(str);
  line.erase(0,found);

  //erase    0*mm </paramVector>
  string str2("   0*mm </paramVector>");
  found=line.find(str2);
  size_t len = line.length();
  line.erase(found,len);

  // Erase first *mm
  string str3("*mm");
  found=line.find(str3);
  line.erase(found,3);

  // Erase second *mm
  found=line.find(str3);
  line.erase(found,3);

  // separate values
  std::vector<string> values;
  string value;
  istringstream iss(line);
  while (iss >> value)
  {
    values.push_back(value);
    cout << value << endl;
  }
  x = lexical_cast<double>(values[0]);
  y = lexical_cast<double>(values[1]);
}


void HPDAlign::ReadFitResultFile()
{
  //read file
  string line;
  ifstream fitFile (inputFitFile.c_str());
  //check the file is open
  if (fitFile.is_open())
  {
    while (fitFile.good())                                  //while good
    {
      while ( getline(fitFile, line))
      {
        std::vector<double> data;

        double value;
        istringstream iss(line);

        while (iss >> value)
        {
          data.push_back(value);
        }
        std::cout << data[1] << " " << data[3] << std::endl;
        //data[0] HPD number; HPD[1] Sin Coeff; HPD[3] Cos Coeff;
        HPDs[data[0]]->SetFitResultX(data[1]);
        HPDs[data[0]]->SetFitResultY(data[3]);
      }
    }
    fitFile.close();                                      // close fit result file
  }
}

void HPDAlign::WriteXMLFile(string outputXML)
{
  cout << "WriteXMLFile  " << outputXML << endl;
  int firstHPD = 0;
  if (RICH == 1){ NumHPDs = 196; firstHPD =   0;}
  if (RICH == 2){ NumHPDs = 288; firstHPD = 196;}
  //	if (RICH == 1) NumHPDs = 400;

  WritePreXML(outputXML+"P0.xml");
  WritePreXML(outputXML+"P1.xml");

  for (int HPD = firstHPD; HPD<NumHPDs+firstHPD; HPD++)
  {
    //replace with new parameters
    double x_param = HPDs[HPD]->AlignXParam();
    double y_param = HPDs[HPD]->AlignYParam();

    std::vector<string> Params(5);

    Params[0] = "  <condition classID=\"6\" name=\"HPD"+ lexical_cast<string>(HPD) +"_Align\">\n";
    Params[1] = "    <paramVector name=\"dPosXYZ\" type=\"double\">  "+lexical_cast<string>(x_param) + "*mm   "+ lexical_cast<string>(y_param) +"*mm   0*mm </paramVector>\n";
    Params[2] = "    <paramVector name=\"dRotXYZ\" type=\"double\">  0*mrad 0*mrad 0*mrad </paramVector>\n";
    Params[3] = "    <paramVector name=\"pivotXYZ\" type=\"double\"> 0*mm   0*mm   0*mm </paramVector>\n";
    Params[4] = "  </condition>\n\n";

    string outFile = outputXML;
    if      (HPD <   98)              outFile = outFile+"P0.xml";
    else if (HPD >=  98 && HPD < 196) outFile = outFile+"P1.xml";
    else if (HPD >= 196 && HPD < 340) outFile = outFile+"P0.xml";
    else if (HPD >= 340 && HPD < 484) outFile = outFile+"P1.xml";

    WriteLines(Params,outFile);
  //Write output XML file
  }
  // Write final lines of xml file
  WriteLastLines(outputXML+"P0.xml");
  WriteLastLines(outputXML+"P1.xml");
}

void HPDAlign::WritePreXML(string outputXML)
{
  //Write output XML file
  fstream filestr;
  filestr.open (outputXML.c_str(), ios::out);               //append to file
  filestr << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
  filestr << "<!DOCTYPE DDDB SYSTEM \"../../../DTD/structure.dtd\" >\n";
  filestr << "<DDDB>\n\n";
  filestr.close();
}

void HPDAlign::WriteLines(std::vector<string>& Params, string outFile)
{
  std::vector<string>::iterator it;
  fstream filestr;
  filestr.open (outFile.c_str(), fstream::in | fstream::out | fstream::app);
  for (it = Params.begin(); it < Params.end(); it++)
  {
    filestr << *it;
  }
  filestr.close();
}


void HPDAlign::WriteLastLines(string outputXML)
{
  //Write output XML file
  fstream filestr;
  // append to file
  filestr.open (outputXML.c_str(), fstream::in | fstream::out | fstream::app);
  filestr << "</DDDB>";
  filestr.close();
}
