#ifndef HPDALIGNPARAM_H
# define HPDALIGNPARAM_H

# include <fstream>
# include <iostream>
# include <string>

class HPDAlignParam
{
public:

  HPDAlignParam();

  HPDAlignParam(int hpd);

  ~HPDAlignParam();

  double AlignXParam();

  double AlignYParam();

  int HPD();

  void SetFitResultX(double x);

  void SetFitResultY(double y);

  void InitX(double x);

  void InitY(double y);


private:
  int m_HPD;
  double init_x;
  double init_y;
  double new_x;
  double new_y;
  double fit_x;
  double fit_y;

};

#endif
