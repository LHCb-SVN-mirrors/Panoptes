#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <utility>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/token_functions.hpp>
#include <boost/tokenizer.hpp>
#include <boost/xpressive/xpressive.hpp>

#include <Minuit2/FunctionMinimum.h>
#include <Minuit2/MnContours.h>
#include <Minuit2/MnMigrad.h>
#include <Minuit2/MnMinos.h>
#include <Minuit2/MnPlot.h>
#include <Minuit2/MnPrint.h>
#include <Minuit2/MnUserParameters.h>
#include <Minuit2/MnUserParameterState.h>

#include "RichMirrAlignFcn.h"

// Xerces stuff. ---------------------------------------------------------------
#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xqilla/xqilla-dom3.hpp>

namespace    po = boost::program_options;
using   namespace boost::xpressive;
using             boost::format;
using             boost::lexical_cast;
using               std::cerr;
using               std::cout;
using               std::endl;
using               std::exception;
using               std::fstream;
using               std::ifstream;
using               std::pair;
using               std::map;
using               std::ofstream;
using               std::runtime_error;
using               std::string;
using               std::stringstream;
using               std::vector;

XERCES_CPP_NAMESPACE_USE

//------------------------------------------------------------------------------
void print_yz_table(int& rich, string& mirrType, vector<string>& mirrLay, map<string,double>& name_value, int& printStyle, double& stopTolY, double& stopTolZ)
{
  // This table printer can print a table of "sigmas" w.r.t. "stop tolerances",
  // stopTolY / stopTolZ. We stop aligning after this itertaion, if all
  // additional mirror compensations yielded by this iteration are less than
  // stopTolY / stopTolZ mrad.
  //
  // Therefore, to make the printout easily interpretable with this in mind, the
  // following algorithm is used.
  //
  // If a particular mirror compensation is at, say, -0.148 mrad and the stop tolerance is, say, 0.07,
  // the printout will return -2 (-0.148/0.07 truncated before decimal point) i.e. ~ 2 times the allowed tolerance).
  //
  // But if a particular mirror compensation is at, say, -0.034 mrad,
  // the printout will return 0 (-0.034/0.07 truncated before decimal point and without sign) i.e. within the allowed tolerance).

  unsigned int priNumRows=0;
  unsigned int priNumCols=0;
  unsigned int secNumRows=0;
  unsigned int secNumCols=0;

  if      (rich==1) {
    priNumRows = 2;
    priNumCols = 2;

    secNumRows = 4;
    secNumCols = 4;
  }
  else if (rich==2) {
    priNumRows = 7;
    priNumCols = 8;

    secNumRows = 5;
    secNumCols = 8;
  }

  unsigned int numRows(0);
  unsigned int numCols(0);

  if      ("pri"==mirrType) {
    cout <<"                          Y                                                  Z"<< endl;

    numRows = priNumRows;
    numCols = priNumCols;
  }
  else if ("sec"==mirrType) {
    cout <<"                          Y                                                  Z"<< endl;

    numRows = secNumRows;
    numCols = secNumCols;
  }

  stringstream table;

  string firstSpace;
  string secondSpace;

  string boostFormat;

  sregex patternPrefix;
  string substPrefix;

  sregex patternSuffix;
  string substSuffix;

  sregex patternMinus0;
  string subst0;


  if      (printStyle==1) {
    // Detailed Anatoly-style printout with one digit after decimal point:
    // when, e.g. stop tolerance = 0.07,
    // the following substitutions are prepared so that
    // " 0.1520/0.07 =  2.171..." is printed as " 2.1",
    // "-0.1520/0.07 = -2.171..." is printed as "-2.1",
    // " 0.0680/0.07 =  0.971..." is printed as "  .9"
    // "-0.0680/0.07 = -0.971..." is printed as " -.9"
    // " 0.0069/0.07 =  0.097..." is printed as "  .0"
    // "-0.0069/0.07 = -0.097..." is printed as "  .0"
    if      (rich==1) {
      if      ("pri"==mirrType) {
        firstSpace  = "                  ";
        secondSpace = "                                       ";
      }
      else if ("sec"==mirrType) {
        firstSpace  = "            ";
        secondSpace = "                           ";
      }
    }
    else if (rich==2) {
      firstSpace    = "";
      secondSpace   = "   ";
    }

    boostFormat = "%| 17.12f|";

    patternPrefix = sregex::compile("([ ]+[-]?)0\\.");
    substPrefix = " $1.";

    patternSuffix = sregex::compile("\\.(\\d)[\\d]+");
    substSuffix   = ".$1";

    patternMinus0 = sregex::compile("-\\.0");
    subst0        = " .0";
  }
  else if (printStyle==0) {
    // New-style printout with zero digits after decimal point:
    // when, e.g. stop tolerance = 0.07,
    // the following substitutions are prepared so that
    // " 0.1520/0.07 =  2.171..." is printed as " 2",
    // "-0.1520/0.07 = -2.171..." is printed as "-2",
    // " 0.0680/0.07 =  0.971..." is printed as " 0"
    // "-0.0680/0.07 = -0.971..." is printed as " 0"
    // " 0.0069/0.07 =  0.097..." is printed as " 0"
    // "-0.0069/0.07 = -0.097..." is printed as " 0"
    if      (rich==1) {
      if      ("pri"==mirrType) {
        firstSpace  = "                    ";
        secondSpace = "                                       ";
      }
      else if ("sec"==mirrType) {
        firstSpace  = "              ";
        secondSpace = "                           ";
      }
    }
    else if (rich==2) {
      firstSpace    = "  ";
      secondSpace   = "   ";
    }

    boostFormat = "%| 17.12f|";

    patternPrefix = sregex::compile("0");
    substPrefix   = "0";

    patternSuffix = sregex::compile("\\.[\\d]+");
    substSuffix   = "  ";

    patternMinus0 = sregex::compile("-0");
    subst0        = " 0";
  }

  string truncatedValueStg;

  vector<string>::const_iterator mirrItY = mirrLay.begin();
  vector<string>::const_iterator mirrItZ = mirrLay.begin();

  for (unsigned int row=0; row<numRows; ++row) {
    table <<firstSpace;
    for (unsigned int col=0; col<numCols; ++col) {
      stringstream longValueStgStm;
      double scaledValue = name_value[*mirrItY+"Y"] / stopTolY;
      longValueStgStm <<format(boostFormat)%scaledValue;
      truncatedValueStg = regex_replace( longValueStgStm.str(), patternPrefix, substPrefix );
      truncatedValueStg = regex_replace( truncatedValueStg    , patternSuffix, substSuffix );
      truncatedValueStg = regex_replace( truncatedValueStg    , patternMinus0, subst0      );
      table <<truncatedValueStg;
      ++mirrItY;
    }
    table <<secondSpace;
    for (unsigned int col=0; col<numCols; ++col) {
      stringstream longValueStgStm;
      double scaledValue = name_value[*mirrItZ+"Z"] / stopTolZ;
      longValueStgStm <<format(boostFormat)%scaledValue;
      truncatedValueStg = regex_replace( longValueStgStm.str(), patternPrefix, substPrefix );
      truncatedValueStg = regex_replace( truncatedValueStg    , patternSuffix, substSuffix );
      truncatedValueStg = regex_replace( truncatedValueStg    , patternMinus0, subst0      );
      table <<truncatedValueStg;
      ++mirrItZ;
    }
    table << endl;
  }
  cout <<table.str()<< endl;

}

//------------------------------------------------------------------------------

int main(int argc, char* argv[])
{

  if ( argc == 0 ) { cout <<"Configuration file was not set!"<< endl; return 1;}
  string confFilenam = (string)argv[1];
  cout << endl <<"Configuration file       "+confFilenam<< endl;

  // the configuration file may look something like follows:
  // workDir            = /afs/cern.ch/user/a/asolomin/public/rich_align/workdir

  // thisVariant        = 48m47c_p12p43fix_Gs1Gb1Mp9Wi4.0Cm2Sm0McUni_129kPreMC10
  // thisVariant        = 48m47c_p12p43fix_Gs1Gb1Mp9Wi4.0Cm2Sm0McFix_129kPreMC10
  // thisVariant        = 48m47c_p12p43fix_Gs1Gb1Mp9Wi4.0Cm2Sm0McCt0.3_129kPreMC10

  // richDetector       = 1
  // magnFactorsMode    = 2
  // solutionMethod     = 0
  // usePremisaligned   = true
  // iterationCount     = 0

  string workDir                         ;
  string thisVariant                     ;
  int    richDetector                    ;
  int    magnFactorsMode                 ;
  int    solutionMethod                  ;
  bool   usePremisaligned                ;
  int    iterationCount                  ;
  string combAndMirrSubsetsFile          ;
  string mirrCombinFitResultsFile        ;
  string zerothIterationXMLFile          ;
  string currentIterationXMLFile         ;
  string nextIterationXMLFile            ;
  int    stopToleranceMode               ;
  double nominalResolutionSigma          ;
  double stopSigmaFraction               ;
  double stopTolerancePriY               ;
  double stopTolerancePriZ               ;
  double stopToleranceSecY               ;
  double stopToleranceSecZ               ;
  int    printMode                       ;

  try {
    // declare the supported options
    po::options_description from_conf_file_desc("Configuration");

    from_conf_file_desc.add_options()
      ("help"                     ,                                                                            "produce help message"                                                                                                       )
      ("workDir"                  , po::value<string>(&workDir                 )                             , "working directory where all intermediate files are created"                                                                 )
      ("richDetector"             , po::value<int   >(&richDetector            )                             , "number is number of RICH detector.i.e if equals 1 then RICH1,if equals 2 then RICH2"                                        )
      ("thisVariant"              , po::value<string>(&thisVariant             )                             , "string identifying this case, e.g. \"48m85c_p12p43fix_Gs1Gb1Mp9Wi4_129kPreMC10\""                                           )
      ("mirrCombinFitResults"     , po::value<string>(&mirrCombinFitResultsFile)->default_value( ""         ), "input:  mirror-pair histos fit results produced by the RichMirrCombinFit program"                                           )
      ("zerothIterationXML"       , po::value<string>(&zerothIterationXMLFile  )->default_value( ""         ), "input:  initial XML file used during zeroth iteration"                                                                      )
      ("currentIterationXML"      , po::value<string>(&currentIterationXMLFile )->default_value( ""         ), "input:  XML file in use during current iteration"                                                                           )
      ("nextIterationXML"         , po::value<string>(&nextIterationXMLFile    )->default_value( ""         ), "output: XML file produced at the end of this iteration, to be used during next iteration"                                   )
      ("combAndMirrSubsets"       , po::value<string>(&combAndMirrSubsetsFile  )->default_value( ""         ), "file with subsets of mirror combinations and mirrors"                                                                       )
      ("magnFactorsMode"          , po::value<int   >(&magnFactorsMode         )->default_value( 1          ), "magnification factors types: 0=all equal; 1/2=individual fixed/evaluated-on-fly (default) at each iteration "               )
      ("solutionMethod"           , po::value<int   >(&solutionMethod          )->default_value( 0          ), "method of finding solution for mirror misalignments using default=0: Minut, 1: algebraically by substitution"               )
      ("usePremisaligned"         , po::bool_switch  (&usePremisaligned        )->default_value( false, "no"), "if true, then pre-misaligned xml files will be used (MC case), default=false"                                               )
      ("iterationCount"           , po::value<int   >(&iterationCount          )->default_value( 0          ), "this iteration number, starting from 0"                                                                                     )
      ("stopToleranceMode"        , po::value<int   >(&stopToleranceMode       )->default_value( 0          ), "tolerance mode: 0(def)->calculate them from nominal sigma, average magn factors and stopSigmaFraction, 1->use set below"    )
      ("nominalResolutionSigma"   , po::value<double>(&nominalResolutionSigma  )                             , "RICH-dependent nominal Cherenkov angle distribution sigma; no default here"                                                 )
      ("stopSigmaFraction"        , po::value<double>(&stopSigmaFraction       )->default_value( 0.5        ), "tolerance for total tilt of mirror pair in terms of fraction of the noiminal sigma"                                         )
      ("stopTolerancePriY"        , po::value<double>(&stopTolerancePriY       )                             , "RICH-dependent tolerance for primary   mirrors rotation around Y correction in mrad to stop the alignment. Historically 0.1")
      ("stopTolerancePriZ"        , po::value<double>(&stopTolerancePriZ       )                             , "RICH-dependent tolerance for primary   mirrors rotation around Z correction in mrad to stop the alignment. Historically 0.1")
      ("stopToleranceSecY"        , po::value<double>(&stopToleranceSecY       )                             , "RICH-dependent tolerance for secondary mirrors rotation around Y correction in mrad to stop the alignment. Historically 0.1")
      ("stopToleranceSecZ"        , po::value<double>(&stopToleranceSecZ       )                             , "RICH-dependent tolerance for secondary mirrors rotation around Z correction in mrad to stop the alignment. Historically 0.1")
      ("printMode"                , po::value<int   >(&printMode               )->default_value( 0          ), "results print style (0/1/2): -2.17 will be printed as: 0-> -2, 1-> -2.1, while -0.97 as: 0-> 0, 1-> -.9, 2-> both: 1 then 0")
    ;
    po::variables_map vm;
    ifstream confFile_ifstrm( confFilenam.c_str() );
    po::store( po::parse_config_file(confFile_ifstrm, from_conf_file_desc), vm );
    po::notify( vm );

    if (vm.count("help"        )){ cout <<"Usage: RichMirrAlign.exe <conf_file>"      << endl;         cout <<from_conf_file_desc;                            return 0;}
    if (vm.count("workDir"     )){
          cout <<"workDir                  "<<workDir                                 << endl;} else { cout <<"workDir                  was not set."<< endl; return 1;}
    if (vm.count("richDetector")){
          cout <<"richDetector             "<<richDetector                            << endl;} else { cout <<"richDetector             was not set."<< endl; return 1;}
    if (vm.count("thisVariant" )){
          cout <<"thisVariant                                 "<<thisVariant          << endl;} else { cout <<"thisVariant              was not set."<< endl; return 1;}
    cout       <<"mirrCombinFitResults     "<<mirrCombinFitResultsFile                << endl;
    cout       <<"zerothIterationXML        "<<zerothIterationXMLFile                 << endl;
    cout       <<"currentIterationXML       "<<currentIterationXMLFile                << endl;
    cout       <<"nextIterationXML          "<<nextIterationXMLFile                   << endl;
    cout       <<"combAndMirrSubsets       "<<combAndMirrSubsetsFile                  << endl;
    cout       <<"magnFactorsMode          "<<magnFactorsMode                         << endl;
    cout       <<"solutionMethod           "<<solutionMethod                          << endl;
    string        usePremisaligned_str("no");
    if (          usePremisaligned )          usePremisaligned_str = "yes";
    cout       <<"usePremisaligned         "<<usePremisaligned_str                    << endl;
    cout       <<"iterationCount           "<<iterationCount                          << endl;

    cout       <<"stopToleranceMode        "<<stopToleranceMode                       << endl;
    if (stopToleranceMode==0) {
      if (vm.count( "nominalResolutionSigma")) {
          cout <<"nominalResolutionSigma   "<<format("%|4.2f|")%nominalResolutionSigma<< endl;} else { cout <<"nominalResolutionSigma   was not set."<< endl; return 1;}
    }
    cout       <<"stopSigmaFraction        "<<format("%|4.2f|")%stopSigmaFraction     << endl;

    if (stopToleranceMode==1) {
      if (vm.count("stopTolerancePriY")) {
            cout <<"stopTolerancePriY      "<<format("%|4.2f|")%stopTolerancePriY     << endl;} else { cout <<"stopTolerancePriY        was not set."<< endl; return 1;}
      if (vm.count("stopTolerancePriZ")) {
            cout <<"stopTolerancePriZ      "<<format("%|4.2f|")%stopTolerancePriZ     << endl;} else { cout <<"stopTolerancePriZ        was not set."<< endl; return 1;}
      if (vm.count("stopToleranceSecY")) {
            cout <<"stopToleranceSecY      "<<format("%|4.2f|")%stopToleranceSecY     << endl;} else { cout <<"stopToleranceSecY        was not set."<< endl; return 1;}
      if (vm.count("stopToleranceSecZ")) {
            cout <<"stopToleranceSecZ      "<<format("%|4.2f|")%stopToleranceSecZ     << endl;} else { cout <<"stopToleranceSecZ        was not set."<< endl; return 1;}
    }

    cout       <<"printMode                "<<printMode                               << endl;
  }
  catch( exception& e ) {
    cout <<"program_options exception: "<<e.what()<< endl;
    return 1;
  }
  string iterCount(  lexical_cast<string>(iterationCount) );
  string rNr( lexical_cast<string>(richDetector) );

  unsigned int priMirrTotNum=0;
  unsigned int secMirrTotNum=0;

  unsigned int combTotNum=0;

//unsigned int priParTotNum=0;
//unsigned int secParTotNum=0;

  if      ( richDetector == 1 ) {
    priMirrTotNum =  4;
    secMirrTotNum = 16;

    combTotNum    = 16;

  //priParTotNum  =  4*2;
  //secParTotNum  = 16*2;
  }
  else if ( richDetector == 2 ) {
    priMirrTotNum = 56;
    secMirrTotNum = 40;

    combTotNum    = 94;

  //priParTotNum  = 56*2;
  //secParTotNum  = 40*2;
  }
  //----------------------------------------------------------------------------

//unsigned int counter( 0 ); // common counter

  //----------------------------------------------------------------------------
  // The following is commented out by Matt Coombes for the simplification, but
  // the correct mode of operation is when the file names contain also necessary
  // meta-information, permitting to figure out the settings and - to reproduce
  // the results if necessary. That's exactly what was eliminated here.

//sregex mirrCombinSubset_matcher = sregex::compile("(\\d+m\\d+c(_[\\w\\d]+fix)?)_");
//string mirrCombinSubsetStr;
//smatch what;
//regex_search( thisVariant, what, mirrCombinSubset_matcher );
//mirrCombinSubsetStr = what[1].str();
//string  combAndMirrSubsetsFile("Rich"+rNr+"combAndMirrSubsets_"+mirrCombinSubsetStr+".txt");

  cout << endl;
  cout <<"file with mirror combinations list: "<<combAndMirrSubsetsFile<< endl;
  cout << endl;
  //----------------------------------------------------------------------------

  ifstream combAndMirrSubsets;
  combAndMirrSubsets.open( (combAndMirrSubsetsFile).c_str() );

  vector<string>                 combSubset;

  vector<string>                 priNamesSubset;
  vector<string>::const_iterator priNamesSubsetIt;
  vector<string>                 secNamesSubset;
  vector<string>::const_iterator secNamesSubsetIt;

  string                         oneLineStr;
  string                         combAndMirrSubsetsOneStr;

  // read combAndMirrSubsets
  while ( ! combAndMirrSubsets.eof() ) {
    getline( combAndMirrSubsets, oneLineStr );
    combAndMirrSubsetsOneStr += oneLineStr;
    combAndMirrSubsetsOneStr += "\n";
  }
  combAndMirrSubsets.close();

  // print the whole image of the combAndMirrSubsetsFile
  cout <<combAndMirrSubsetsOneStr<< endl;


  sregex reSep             = sregex::compile("[\\s]+");    // any space separator, including end-of-line

  sregex  mirrComb_matcher = sregex::compile("[\\d]{4}");  // just 4-digit mirror combination number

  sregex    number_matcher = sregex::compile("[\\d]+");    // any size number; will be used to capture
                                                           // only 2-digit mirror numbers, not 4-digit
                                                           // mirror combination numbers

  sregex secondary_matcher = sregex::compile("secondary"); // to detect place where the secondary mirror
                                                           // numbers start

  smatch what;
  sregex_token_iterator strEnd;

  // Extract chosen mirror combinations from the combAndMirrSubsetsOneStr and store them
  // in combSubset
  sregex_token_iterator comb_sregTokIt( combAndMirrSubsetsOneStr.begin(), combAndMirrSubsetsOneStr.end(), reSep, -1 );
  /*
  cout <<"combAndMirrSubsetsOneStr: combination, primary, secondary"<< endl;
  */
  //
  while ( comb_sregTokIt != strEnd ) {
    /*
    cout <<"*comb_sregTokIt = "+*comb_sregTokIt<< endl;
    */
    //
    string combination = *comb_sregTokIt;

    if ( regex_match( combination, what, mirrComb_matcher ) ) {
      /*
      cout <<"what = "+what[0]<< endl;
      */
      //
      combSubset.push_back( (string)what[0] );

      // use primary mirror number in the current combination
      // to understand which side this combination belongs to:
    //unsigned int priInComb = lexical_cast<unsigned int>(combination.substr(0, 2));
    //unsigned int side;
    //if ((richDetector==1 && priInComb<=1) || (richDetector==2 && priInComb<=27)) {
    //  side = 0;
    //}
    //else {
    //  side = 1;
    //}
      /*
      cout <<"side = "+lexical_cast<string>(side)+"   combination = "+combination<< endl;
      */
      //
    //combSubsetSides.at(side).push_back(combination);
      /*
      string priNr = combination.substr(0, 2);
      string secNr = combination.substr(2, 2);
      cout <<"priNr+secNr   = "+priNr+secNr<< endl;

      vector<string>::const_iterator mirrFromCombin;
      vector<string>::const_iterator mirrFromSubset;
      mirrFromCombin = find( priNamesTotset.begin(), priNamesTotset.end(), priNr );
      mirrFromSubset = find( priNamesSubset.begin(), priNamesSubset.end(), priNr );
      if ( mirrFromCombin == priNamesTotset.end() && mirrFromSubset != priNamesSubset.end() ) {
        priNamesTotset.push_back( priNr );
      }
      mirrFromCombin = find( secNamesTotset.begin(), secNamesTotset.end(), secNr );
      mirrFromSubset = find( secNamesSubset.begin(), secNamesSubset.end(), secNr );
      if ( mirrFromCombin == secNamesTotset.end() && mirrFromSubset != secNamesSubset.end() ) {
        secNamesTotset.push_back( secNr );
      }
      */
      //
    }
    else {
      /*
      cout <<"what = "+what[0]<< endl;
      */
      //
    }
    ++comb_sregTokIt;
  }

  // Extract selected mirror numbers from the combAndMirrSubsetsOneStr and store them
  // in priNamesSubset and secNamesSubset vectors as strings, e.g. "pri07" or
  // "sec12".
//cout <<"combAndMirrSubsetsOneStr"<< endl;
  sregex_token_iterator combAndMirrSubsetsOneStr_sregTokIt( combAndMirrSubsetsOneStr.begin(), combAndMirrSubsetsOneStr.end(), reSep, -1 );
  bool soFarPrimary(true);
  while ( combAndMirrSubsetsOneStr_sregTokIt != strEnd ) {
    string combAndMirrSubsetsOneStr_sregTokItStr = boost::to_lower_copy( (string)(*combAndMirrSubsetsOneStr_sregTokIt) );
    /*
    cout <<combAndMirrSubsetsOneStr_sregTokItStr<< endl;
    */
    //
    if      ( regex_match(  combAndMirrSubsetsOneStr_sregTokItStr, what, secondary_matcher ) ) {
      soFarPrimary = false;
    }
    else if ( regex_search( combAndMirrSubsetsOneStr_sregTokItStr, what,    number_matcher ) ) {
      string Nr = (string)what[0];
      if (Nr.size() == 2) {   // check if it's a 2-digit mirror number, not 4-digit combination number
        if ( soFarPrimary ) { // primary
          priNamesSubset.push_back("pri"+Nr);
        }
        else {                // secondary
          secNamesSubset.push_back("sec"+Nr);
        }
      }
    }
    ++combAndMirrSubsetsOneStr_sregTokIt;
  }

  // Prepare variables for the total rotations of mirror combinations detected
  // after fitting of the histograms and the corresponding magnification
  // factors:

  // Prepare names of files with the magnification factors:
  vector<string>  magnFactors_files;

  if      ( magnFactorsMode == -1 ) {
    // Foolish "universal" magnification factors, the same for all mirrors, just to prove their absurdness:
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_negYzerZ_universal.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_posYzerZ_universal.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_zerYnegZ_universal.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_zerYposZ_universal.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_negYzerZ_universal.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_posYzerZ_universal.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_zerYnegZ_universal.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_zerYposZ_universal.txt");
  }
  else if ( magnFactorsMode ==  0 ) {
    // Magnification factors are from MC or from previous times, and not updated in any way here:
  //string online_str("online");
  //if (thisVariant.find(online_str) != std::string::npos) {
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_negYzerZ_predefined.txt");
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_posYzerZ_predefined.txt");
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_zerYnegZ_predefined.txt");
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_pri_zerYposZ_predefined.txt");
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_negYzerZ_predefined.txt");
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_posYzerZ_predefined.txt");
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_zerYnegZ_predefined.txt");
  //  magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_sec_zerYposZ_predefined.txt");
  //}
  //else {
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_negYzerZ_predefined.txt");
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_posYzerZ_predefined.txt");
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_zerYnegZ_predefined.txt");
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_zerYposZ_predefined.txt");
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_negYzerZ_predefined.txt");
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_posYzerZ_predefined.txt");
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_zerYnegZ_predefined.txt");
      magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_zerYposZ_predefined.txt");
  //}
  }
  else if ( magnFactorsMode ==  1 ) {
    // Magnification factors are from the zeroth iteration, but not updated further on:
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_negYzerZ_i0.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_posYzerZ_i0.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_zerYnegZ_i0.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_zerYposZ_i0.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_negYzerZ_i0.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_posYzerZ_i0.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_zerYnegZ_i0.txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_zerYposZ_i0.txt");
  }
  else if ( magnFactorsMode ==  2 ) {
    //Magnification factors are updated from iteration to iteration, starting from the zeroth iteration:
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_negYzerZ_i"+iterCount+".txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_posYzerZ_i"+iterCount+".txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_zerYnegZ_i"+iterCount+".txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_pri_zerYposZ_i"+iterCount+".txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_negYzerZ_i"+iterCount+".txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_posYzerZ_i"+iterCount+".txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_zerYnegZ_i"+iterCount+".txt");
    magnFactors_files.push_back("Rich"+rNr+"MirrMagnFactors_"+thisVariant+"_sec_zerYposZ_i"+iterCount+".txt");
  }

  // Handy names:

  vector<string> magnFactors_fileKinds;

  magnFactors_fileKinds.push_back("pri_negYzerZ");
  magnFactors_fileKinds.push_back("pri_posYzerZ");
  magnFactors_fileKinds.push_back("pri_zerYnegZ");
  magnFactors_fileKinds.push_back("pri_zerYposZ");
  magnFactors_fileKinds.push_back("sec_negYzerZ");
  magnFactors_fileKinds.push_back("sec_posYzerZ");
  magnFactors_fileKinds.push_back("sec_zerYnegZ");
  magnFactors_fileKinds.push_back("sec_zerYposZ");

  map<string, string> magnFactors_kinds;

  magnFactors_kinds["pri_negYzerZ"] = "priYn";
  magnFactors_kinds["pri_posYzerZ"] = "priYp";
  magnFactors_kinds["pri_zerYnegZ"] = "priZn";
  magnFactors_kinds["pri_zerYposZ"] = "priZp";
  magnFactors_kinds["sec_negYzerZ"] = "secYn";
  magnFactors_kinds["sec_posYzerZ"] = "secYp";
  magnFactors_kinds["sec_zerYnegZ"] = "secZn";
  magnFactors_kinds["sec_zerYposZ"] = "secZp";

  map<string, double> magnFactor;

  // Read-in the magnification factors:
  ifstream magnFactors_ifstrm;

  for (vector<string>::const_iterator mffname =magnFactors_files.begin();
                                      mffname!=magnFactors_files.end();
                                    ++mffname) {
    // Here we find correspondence between the "long" notation, e.g.
    // pri_negYzerZ, and the short one, e.g. priYn, in the file names:
    string mfkind;
    for (map<string,string>::const_iterator mfk =magnFactors_kinds.begin();
                                            mfk!=magnFactors_kinds.end();
                                          ++mfk) {
      if ( ((*mffname).find(mfk->first))!=std::string::npos ) {
        mfkind = mfk->second;
        break;
      }
    }
    magnFactors_ifstrm.open( (workDir+"/"+(*mffname)).c_str() );
    string                       comb;
    double                               magnFactorY,   magnFactorZ;
    while (magnFactors_ifstrm >> comb >> magnFactorY >> magnFactorZ ) {
      magnFactor[comb+mfkind+"_Y"]     = magnFactorY; // e.g. 1209priYn_Y
      magnFactor[comb+mfkind+"_Z"]     = magnFactorZ; // e.g. 1209priYn_Z
    }
    magnFactors_ifstrm.close();
  }

//for (map<string, double>::const_iterator mf =magnFactor.begin();
//                                         mf!=magnFactor.end();
//                                       ++mf) {
//  cout <<format("magnFactor["+mf->first+"] = %| 5.2f|")%mf->second<< endl;
//}

  // Now we should average pairs of magnification factors
  // one     - with negative calibrational rotation and
  // another - with positive calibrational rotation,
  // e.g. instead of pair
  //                      1209priYn_Y and 1209priYp_Y
  // we will have now
  //     1209priY_Y = 0.5(1209priYn_Y  +  1209priYp_Y)

  // map: [shortened combination+kind name] -> [averaged over +/- calibrational rotations magnification factor]
  map<string, double> magnFactAv;

  // map: [kind name (only major magnfication factors, i.e. Y_Y and Z_Z)]
  // ->   [averaged over +/- calibrational rotations and over all mirror combinations]
  map<string, double> magnFactAvAll;

  for (vector<string>::const_iterator comb =combSubset.begin();
                                      comb!=combSubset.end();
                                    ++comb) {
    magnFactAv[*comb+"priY_Y"] = 0.5*(magnFactor[*comb+"priYn_Y"] + magnFactor[*comb+"priYp_Y"]);
    magnFactAv[*comb+"priY_Z"] = 0.5*(magnFactor[*comb+"priYn_Z"] + magnFactor[*comb+"priYp_Z"]);
    magnFactAv[*comb+"priZ_Y"] = 0.5*(magnFactor[*comb+"priZn_Y"] + magnFactor[*comb+"priZp_Y"]);
    magnFactAv[*comb+"priZ_Z"] = 0.5*(magnFactor[*comb+"priZn_Z"] + magnFactor[*comb+"priZp_Z"]);
    magnFactAv[*comb+"secY_Y"] = 0.5*(magnFactor[*comb+"secYn_Y"] + magnFactor[*comb+"secYp_Y"]);
    magnFactAv[*comb+"secY_Z"] = 0.5*(magnFactor[*comb+"secYn_Z"] + magnFactor[*comb+"secYp_Z"]);
    magnFactAv[*comb+"secZ_Y"] = 0.5*(magnFactor[*comb+"secZn_Y"] + magnFactor[*comb+"secZp_Y"]);
    magnFactAv[*comb+"secZ_Z"] = 0.5*(magnFactor[*comb+"secZn_Z"] + magnFactor[*comb+"secZp_Z"]);

    magnFactAvAll["priY_Y"] += magnFactAv[*comb+"priY_Y"];
    magnFactAvAll["priZ_Z"] += magnFactAv[*comb+"priZ_Z"];
    magnFactAvAll["secY_Y"] += magnFactAv[*comb+"secY_Y"];
    magnFactAvAll["secZ_Z"] += magnFactAv[*comb+"secZ_Z"];

  }
  // and now we divide them by the number of combinations in use to finalize
  // the averaging; they are around:        RICH1  RICH2

  magnFactAvAll["priY_Y"] /= combTotNum; //  1.86   2.05
  magnFactAvAll["secY_Y"] /= combTotNum; // -0.55  -1.04

  magnFactAvAll["priZ_Z"] /= combTotNum; //  2.02   1.83
  magnFactAvAll["secZ_Z"] /= combTotNum; //  0.81   0.61

  cout <<format("averaged magnification factor priY_Y = %| 5.2f|")%magnFactAvAll["priY_Y"]<< endl;
  cout <<format("averaged magnification factor secY_Y = %| 5.2f|")%magnFactAvAll["secY_Y"]<< endl;

  cout <<format("averaged magnification factor priZ_Z = %| 5.2f|")%magnFactAvAll["priZ_Z"]<< endl;
  cout <<format("averaged magnification factor secZ_Z = %| 5.2f|")%magnFactAvAll["secZ_Z"]<< endl;


  // let's calculate the tolerances if stopToleranceMode == 0
  if (stopToleranceMode == 0) {
    // total stop tolerance is equal to a fraction (typically 0.5)
    // of the "ideal" sigma of the Ch-kov angle distribution:
    double stopToleranceTotal = nominalResolutionSigma*stopSigmaFraction;

    // now we calculate tolerances fairly among the contributers,
    // inverse-proportional to their average magnification factors,
    // i.e. each contribution = 1/2 of the total stop tolerance

    stopTolerancePriY = 0.5*stopToleranceTotal / fabs(magnFactAvAll["priY_Y"]);
    stopToleranceSecY = 0.5*stopToleranceTotal / fabs(magnFactAvAll["secY_Y"]);

    stopTolerancePriZ = 0.5*stopToleranceTotal / fabs(magnFactAvAll["priZ_Z"]);
    stopToleranceSecZ = 0.5*stopToleranceTotal / fabs(magnFactAvAll["secZ_Z"]);
  }

  cout <<format("stopTolerancePriY = %| 5.2f|")%stopTolerancePriY<< endl;
  cout <<format("stopToleranceSecY = %| 5.2f|")%stopToleranceSecY<< endl;

  cout <<format("stopTolerancePriZ = %| 5.2f|")%stopTolerancePriZ<< endl;
  cout <<format("stopToleranceSecZ = %| 5.2f|")%stopToleranceSecZ<< endl;

//for (map<string, double>::const_iterator mfa =magnFactAv.begin();
//                                         mfa!=magnFactAv.end();
//                                       ++mfa) {
//  cout <<format("magnFactAv["+mfa->first+"] = %| 5.2f|")%mfa->second<< endl;
//}

  // Read-in fit results from RichMirrCombinFit, i.e. detected combined primary
  // and secondary mirror rotations and constant terms:
  ifstream mirrCombinRots;

  string thisVariantFull = thisVariant+"_i"+iterCount;

//mirrCombinRots.open( (workDir+"/Rich"+rNr+"MirrCombinTotRots"+thisVariantFull+".txt").c_str() );

  mirrCombinRots.open( (mirrCombinFitResultsFile).c_str() );

//unsigned int lines(0);

  map<string, double> combRot;

  string                   comb;
  double                           rotRoundY,   rotRoundY_err,   rotRoundZ,   rotRoundZ_err,   constTerm,   constTerm_err;

  while (mirrCombinRots >> comb >> rotRoundY >> rotRoundY_err >> rotRoundZ >> rotRoundZ_err >> constTerm >> constTerm_err) {
    combRot[comb+"Y"   ] = rotRoundY    ;
    combRot[comb+"Yerr"] = rotRoundY_err;
    combRot[comb+"Z"   ] = rotRoundZ    ;
    combRot[comb+"Zerr"] = rotRoundZ_err;
    combRot[comb+"C"   ] = constTerm    ;
    combRot[comb+"Cerr"] = constTerm_err;

  //if (lines < 2 ) {
  //  cout
  //  <<format("comb: %|04d|") %comb
  //  <<format(    "    rotY=%| 6.3f|")%combRot[comb+"Y"   ]
  //  <<format( "    rotYErr=%| 6.3f|")%combRot[comb+"Yerr"]
  //  <<format(    "    rotZ=%| 6.3f|")%combRot[comb+"Z"   ]
  //  <<format( "    rotZErr=%| 6.3f|")%combRot[comb+"Zerr"]
  //  <<format(   "    const=%| 6.3f|")%combRot[comb+"C"   ]
  //  <<format("    constErr=%| 6.3f|")%combRot[comb+"Cerr"]
  //  <<endl;
  //}
  //++lines;

  }
  mirrCombinRots.close();

  cout
  << endl
  << endl
  <<"Start of the case  "<<thisVariantFull
  << endl;
  //==========================================================================
  map<string, double>      parName_initValue   ;
  map<string, double>      parName_prevValue   ;
  map<string, double>      parName_currValue   ;

  vector<string>           priNamesLay         ;
  vector<string>           secNamesLay         ;

  if      ( richDetector==1 ) {
    //-------------------------------------
    //         RICH1 mirror segments
    //      under alignment monitoring
    //=====================================
    //               primary
    //
    //   quadrant 0           quadrant 1
    //        (00)    side 0   (01)
    //   ---------------o---------------
    //        (03)    side 1   (02)
    //   quadrant 3           quadrant 2
    //=====================================
    //              secondary
    //
    //   quadrant 0           quadrant 1
    //        00    01     04    05
    //                side 0
    //        02    03     06    07
    //   ---------------o---------------
    //        08    09     12    13
    //                side 1
    //        10    11     14    15
    //   quadrant 3           quadrant 2
    //=====================================
    // NB! mirrors with numbers in "()" are not being adjusted in the
    // algebraic method, and corections of their misalignment
    // compensations remain = 0.

    string  priLay[4] = {
    "pri00",  "pri01",
    "pri03",  "pri02"
    };
    for (unsigned int i=0; i<priMirrTotNum; ++i) {
      priNamesLay.push_back(priLay[i]);

      parName_initValue[ priLay[i]+"Y" ] = 0.;
      parName_prevValue[ priLay[i]+"Y" ] = 0.;
      parName_currValue[ priLay[i]+"Y" ] = 0.;

      parName_initValue[ priLay[i]+"Z" ] = 0.;
      parName_prevValue[ priLay[i]+"Z" ] = 0.;
      parName_currValue[ priLay[i]+"Z" ] = 0.;
    }
    string  secLay[16] = {
    "sec00",  "sec01",  "sec04",  "sec05",
    "sec02",  "sec03",  "sec06",  "sec07",
    "sec08",  "sec09",  "sec12",  "sec13",
    "sec10",  "sec11",  "sec14",  "sec15"
    };
    for (unsigned int i=0; i<secMirrTotNum; ++i) {
      secNamesLay.push_back(secLay[i]);

      parName_initValue[ secLay[i]+"Y" ] = 0.;
      parName_prevValue[ secLay[i]+"Y" ] = 0.;
      parName_currValue[ secLay[i]+"Y" ] = 0.;

      parName_initValue[ secLay[i]+"Z" ] = 0.;
      parName_prevValue[ secLay[i]+"Z" ] = 0.;
      parName_currValue[ secLay[i]+"Z" ] = 0.;
    }
  }
  else if ( richDetector==2 ) {
    //---------------------------------------------------------
    //    RICH2 mirror segments under alignment monitoring
    //=========================================================
    //                         primary
    //           side 0                       side 1
    //
    //    27    26    25    24    |    55    54    53    52
    //    23    22    21    20    |    51    50    49    48
    //    19    18    17    16    |    47    46    45    44
    //    15    14    13   (12)   o   (43)   42    41    40
    //    11    10    09    08    |    39    38    37    36
    //    07    06    05    04    |    35    34    33    32
    //    03    02    01    00    |    31    30    29    28
    //=========================================================
    //                        secondary
    //           side 0                       side 1
    //
    //    19    18    17    16    |    39    38    37    36
    //    15    14    13    12    |    35    34    33    32
    //    11    10    09    08    o    31    30    29    28
    //    07    06    05    04    |    27    26    25    24
    //    03    02    01    00    |    23    22    21    20
    //=========================================================
    // NB! mirror numbers in "()" are not being adjusted, and corections of
    // their misalignment compensations remain = 0.

    string  priLay[56] = {
    "pri27",  "pri26",  "pri25",  "pri24",  "pri55",  "pri54",  "pri53",  "pri52",
    "pri23",  "pri22",  "pri21",  "pri20",  "pri51",  "pri50",  "pri49",  "pri48",
    "pri19",  "pri18",  "pri17",  "pri16",  "pri47",  "pri46",  "pri45",  "pri44",
    "pri15",  "pri14",  "pri13",  "pri12",  "pri43",  "pri42",  "pri41",  "pri40",
    "pri11",  "pri10",  "pri09",  "pri08",  "pri39",  "pri38",  "pri37",  "pri36",
    "pri07",  "pri06",  "pri05",  "pri04",  "pri35",  "pri34",  "pri33",  "pri32",
    "pri03",  "pri02",  "pri01",  "pri00",  "pri31",  "pri30",  "pri29",  "pri28"
    };
    for (unsigned int i=0; i<priMirrTotNum; ++i) {
      priNamesLay.push_back(priLay[i]);

      parName_initValue[ priLay[i]+"Y" ] = 0.;
      parName_prevValue[ priLay[i]+"Y" ] = 0.;
      parName_currValue[ priLay[i]+"Y" ] = 0.;

      parName_initValue[ priLay[i]+"Z" ] = 0.;
      parName_prevValue[ priLay[i]+"Z" ] = 0.;
      parName_currValue[ priLay[i]+"Z" ] = 0.;
    }
    string  secLay[40] = {
    "sec19",  "sec18",  "sec17",  "sec16",  "sec39",  "sec38",  "sec37",  "sec36",
    "sec15",  "sec14",  "sec13",  "sec12",  "sec35",  "sec34",  "sec33",  "sec32",
    "sec11",  "sec10",  "sec09",  "sec08",  "sec31",  "sec30",  "sec29",  "sec28",
    "sec07",  "sec06",  "sec05",  "sec04",  "sec27",  "sec26",  "sec25",  "sec24",
    "sec03",  "sec02",  "sec01",  "sec00",  "sec23",  "sec22",  "sec21",  "sec20"
    };
    for (unsigned int i=0; i<secMirrTotNum; ++i) {
      secNamesLay.push_back(secLay[i]);

      parName_initValue[ secLay[i]+"Y" ] = 0.;
      parName_prevValue[ secLay[i]+"Y" ] = 0.;
      parName_currValue[ secLay[i]+"Y" ] = 0.;

      parName_initValue[ secLay[i]+"Z" ] = 0.;
      parName_prevValue[ secLay[i]+"Z" ] = 0.;
      parName_currValue[ secLay[i]+"Z" ] = 0.;
    }
  }
  // The following is only proof-of-the-concept testing with RICH2 pre-misaligned mirrors:
  if ( usePremisaligned  && iterationCount==0 ) {
    parName_initValue["pri27Y"]=-1.0;parName_initValue["pri26Y"]= 0.7;parName_initValue["pri25Y"]= 0.0;parName_initValue["pri24Y"]= 1.0;parName_initValue["pri55Y"]=-1.0;parName_initValue["pri54Y"]= 0.7;parName_initValue["pri53Y"]= 0.0;parName_initValue["pri52Y"]= 1.0;
    parName_initValue["pri23Y"]=-0.7;parName_initValue["pri22Y"]= 0.0;parName_initValue["pri21Y"]=-1.0;parName_initValue["pri20Y"]= 0.7;parName_initValue["pri51Y"]=-0.7;parName_initValue["pri50Y"]= 0.0;parName_initValue["pri49Y"]=-1.0;parName_initValue["pri48Y"]= 0.7;
    parName_initValue["pri19Y"]= 0.0;parName_initValue["pri18Y"]= 1.0;parName_initValue["pri17Y"]=-0.7;parName_initValue["pri16Y"]= 0.0;parName_initValue["pri47Y"]= 0.0;parName_initValue["pri46Y"]= 1.0;parName_initValue["pri45Y"]=-0.7;parName_initValue["pri44Y"]= 0.0;
    parName_initValue["pri15Y"]=-1.0;parName_initValue["pri14Y"]= 0.7;parName_initValue["pri13Y"]= 0.0;parName_initValue["pri12Y"]= 1.0;parName_initValue["pri43Y"]=-1.0;parName_initValue["pri42Y"]= 0.7;parName_initValue["pri41Y"]= 0.0;parName_initValue["pri40Y"]= 1.0;
    parName_initValue["pri11Y"]=-0.7;parName_initValue["pri10Y"]= 0.0;parName_initValue["pri09Y"]=-1.0;parName_initValue["pri08Y"]= 0.7;parName_initValue["pri39Y"]=-0.7;parName_initValue["pri38Y"]= 0.0;parName_initValue["pri37Y"]=-1.0;parName_initValue["pri36Y"]= 0.7;
    parName_initValue["pri07Y"]= 0.0;parName_initValue["pri06Y"]= 1.0;parName_initValue["pri05Y"]=-0.7;parName_initValue["pri04Y"]= 0.0;parName_initValue["pri35Y"]= 0.0;parName_initValue["pri34Y"]= 1.0;parName_initValue["pri33Y"]=-0.7;parName_initValue["pri32Y"]= 0.0;
    parName_initValue["pri03Y"]=-1.0;parName_initValue["pri02Y"]= 0.7;parName_initValue["pri01Y"]= 0.0;parName_initValue["pri00Y"]= 1.0;parName_initValue["pri31Y"]=-1.0;parName_initValue["pri30Y"]= 0.7;parName_initValue["pri29Y"]= 0.0;parName_initValue["pri28Y"]= 1.0;

    parName_initValue["sec19Y"]=-0.7;parName_initValue["sec18Y"]= 0.0;parName_initValue["sec17Y"]=-1.0;parName_initValue["sec16Y"]= 0.7;parName_initValue["sec39Y"]=-0.7;parName_initValue["sec38Y"]= 0.0;parName_initValue["sec37Y"]=-1.0;parName_initValue["sec36Y"]= 0.7;
    parName_initValue["sec15Y"]= 0.0;parName_initValue["sec14Y"]= 1.0;parName_initValue["sec13Y"]=-0.7;parName_initValue["sec12Y"]= 0.0;parName_initValue["sec35Y"]= 0.0;parName_initValue["sec34Y"]= 1.0;parName_initValue["sec33Y"]=-0.7;parName_initValue["sec32Y"]= 0.0;
    parName_initValue["sec11Y"]=-1.0;parName_initValue["sec10Y"]= 0.7;parName_initValue["sec09Y"]= 0.0;parName_initValue["sec08Y"]= 1.0;parName_initValue["sec31Y"]=-1.0;parName_initValue["sec30Y"]= 0.7;parName_initValue["sec29Y"]= 0.0;parName_initValue["sec28Y"]= 1.0;
    parName_initValue["sec07Y"]=-0.7;parName_initValue["sec06Y"]= 0.0;parName_initValue["sec05Y"]=-1.0;parName_initValue["sec04Y"]= 0.7;parName_initValue["sec27Y"]=-0.7;parName_initValue["sec26Y"]= 0.0;parName_initValue["sec25Y"]=-1.0;parName_initValue["sec24Y"]= 0.7;
    parName_initValue["sec03Y"]= 0.0;parName_initValue["sec02Y"]= 1.0;parName_initValue["sec01Y"]=-0.7;parName_initValue["sec00Y"]= 0.0;parName_initValue["sec23Y"]= 0.0;parName_initValue["sec22Y"]= 1.0;parName_initValue["sec21Y"]=-0.7;parName_initValue["sec20Y"]= 0.0;


    parName_initValue["pri27Z"]= 1.0;parName_initValue["pri26Z"]= 0.0;parName_initValue["pri25Z"]= 0.7;parName_initValue["pri24Z"]=-1.0;parName_initValue["pri55Z"]= 1.0;parName_initValue["pri54Z"]= 0.0;parName_initValue["pri53Z"]= 0.7;parName_initValue["pri52Z"]=-1.0;
    parName_initValue["pri23Z"]= 0.0;parName_initValue["pri22Z"]=-0.7;parName_initValue["pri21Z"]= 1.0;parName_initValue["pri20Z"]= 0.0;parName_initValue["pri51Z"]= 0.0;parName_initValue["pri50Z"]=-0.7;parName_initValue["pri49Z"]= 1.0;parName_initValue["pri48Z"]= 0.0;
    parName_initValue["pri19Z"]= 0.7;parName_initValue["pri18Z"]=-1.0;parName_initValue["pri17Z"]= 0.0;parName_initValue["pri16Z"]=-0.7;parName_initValue["pri47Z"]= 0.7;parName_initValue["pri46Z"]=-1.0;parName_initValue["pri45Z"]= 0.0;parName_initValue["pri44Z"]=-0.7;
    parName_initValue["pri15Z"]= 1.0;parName_initValue["pri14Z"]= 0.0;parName_initValue["pri13Z"]= 0.7;parName_initValue["pri12Z"]=-1.0;parName_initValue["pri43Z"]= 1.0;parName_initValue["pri42Z"]= 0.0;parName_initValue["pri41Z"]= 0.7;parName_initValue["pri40Z"]=-1.0;
    parName_initValue["pri11Z"]= 0.0;parName_initValue["pri10Z"]=-0.7;parName_initValue["pri09Z"]= 1.0;parName_initValue["pri08Z"]= 0.0;parName_initValue["pri39Z"]= 0.0;parName_initValue["pri38Z"]=-0.7;parName_initValue["pri37Z"]= 1.0;parName_initValue["pri36Z"]= 0.0;
    parName_initValue["pri07Z"]= 0.7;parName_initValue["pri06Z"]=-1.0;parName_initValue["pri05Z"]= 0.0;parName_initValue["pri04Z"]=-0.7;parName_initValue["pri35Z"]= 0.7;parName_initValue["pri34Z"]=-1.0;parName_initValue["pri33Z"]= 0.0;parName_initValue["pri32Z"]=-0.7;
    parName_initValue["pri03Z"]= 1.0;parName_initValue["pri02Z"]= 0.0;parName_initValue["pri01Z"]= 0.7;parName_initValue["pri00Z"]=-1.0;parName_initValue["pri31Z"]= 1.0;parName_initValue["pri30Z"]= 0.0;parName_initValue["pri29Z"]= 0.7;parName_initValue["pri28Z"]=-1.0;

    parName_initValue["sec19Z"]= 0.0;parName_initValue["sec18Z"]=-0.7;parName_initValue["sec17Z"]= 1.0;parName_initValue["sec16Z"]= 0.0;parName_initValue["sec39Z"]= 0.0;parName_initValue["sec38Z"]=-0.7;parName_initValue["sec37Z"]= 1.0;parName_initValue["sec36Z"]= 0.0;
    parName_initValue["sec15Z"]= 0.7;parName_initValue["sec14Z"]=-1.0;parName_initValue["sec13Z"]= 0.0;parName_initValue["sec12Z"]=-0.7;parName_initValue["sec35Z"]= 0.7;parName_initValue["sec34Z"]=-1.0;parName_initValue["sec33Z"]= 0.0;parName_initValue["sec32Z"]=-0.7;
    parName_initValue["sec11Z"]= 1.0;parName_initValue["sec10Z"]= 0.0;parName_initValue["sec09Z"]= 0.7;parName_initValue["sec08Z"]=-1.0;parName_initValue["sec31Z"]= 1.0;parName_initValue["sec30Z"]= 0.0;parName_initValue["sec29Z"]= 0.7;parName_initValue["sec28Z"]=-1.0;
    parName_initValue["sec07Z"]= 0.0;parName_initValue["sec06Z"]=-0.7;parName_initValue["sec05Z"]= 1.0;parName_initValue["sec04Z"]= 0.0;parName_initValue["sec27Z"]= 0.0;parName_initValue["sec26Z"]=-0.7;parName_initValue["sec25Z"]= 1.0;parName_initValue["sec24Z"]= 0.0;
    parName_initValue["sec03Z"]= 0.7;parName_initValue["sec02Z"]=-1.0;parName_initValue["sec01Z"]= 0.0;parName_initValue["sec00Z"]=-0.7;parName_initValue["sec23Z"]= 0.7;parName_initValue["sec22Z"]=-1.0;parName_initValue["sec21Z"]= 0.0;parName_initValue["sec20Z"]=-0.7;
  }
  //==========================================================================
  // The following section makes use of Minuit2 to solve the system of equations
  // by the least squares method.

  // The least squares method of solution of the system of equations is only
  // applicable to RICH2. In the RICH1 case we have just equations, each for
  // one unknown, and the solution is straightforward, i.e. algebraic.

  if      ( solutionMethod == 0 ) {

    ROOT::Minuit2::MnUserParameters params;

    string priNr  ;
    string priNrY ;
    string priNrZ ;

    string secNr  ;
    string secNrY ;
    string secNrZ ;

    map<string, unsigned int> name_order;

    unsigned int order = 0;

    for (unsigned int i=0; i<priMirrTotNum; ++i) {
      priNr  = priNamesLay[i];
      priNrY = priNr+"Y";
      priNrZ = priNr+"Z";
      params.Add( priNrY.c_str(), parName_initValue[priNrY], 0.001 );
      params.Add( priNrZ.c_str(), parName_initValue[priNrZ], 0.001 );
      name_order[priNrY] = order;
    //cout <<format("name_order.find(priNrY)->first = %s   name_order.find(priNrY)->second = %|2d|")%name_order.find(priNrY)->first %name_order.find(priNrY)->second<< endl;
      ++order;
      name_order[priNrZ] = order;
    //cout <<format("name_order.find(priNrZ)->first = %s   name_order.find(priNrZ)->second = %|2d|")%name_order.find(priNrZ)->first %name_order.find(priNrZ)->second<< endl;
      ++order;
    }
    for (unsigned int i=0; i<secMirrTotNum; ++i) {
      secNr  = secNamesLay[i];
      secNrY = secNr+"Y";
      secNrZ = secNr+"Z";
      params.Add( secNrY.c_str(), parName_initValue[secNrY], 0.001 );
      params.Add( secNrZ.c_str(), parName_initValue[secNrZ], 0.001 );
      name_order[secNrY] = order;
    //cout <<format("name_order.find(secNrY)->first = %s   name_order.find(secNrY)->second = %|2d|")%name_order.find(secNrY)->first %name_order.find(secNrY)->second<< endl;
      ++order;
      name_order[secNrZ] = order;
    //cout <<format("name_order.find(secNrZ)->first = %s   name_order.find(secNrZ)->second = %|2d|")%name_order.find(secNrZ)->first %name_order.find(secNrZ)->second<< endl;
      ++order;
    }

  //for (map<string, unsigned int>::const_iterator n_o =name_order.begin();
  //                                               n_o!=name_order.end();
  //                                               ++n_o) {
  //  cout <<format("n_o->first = %s   n_o->second = %|2d|")%n_o->first %n_o->second<< endl;
  //}

  //for (vector<string>::const_iterator css =combSubset.begin();
  //                                    css!=combSubset.end();
  //                                    ++css) {
  //  cout <<"combSubset: "+*css<< endl;
  //}

    // Instantiate the fitting function:
    RichMirrAlignFcn myFcn(combSubset, combRot, magnFactAv, magnFactAvAll, name_order);

    // Instantiate Migrad:
    ROOT::Minuit2::MnMigrad migrad( myFcn, params );

    // Fix parameters of the mirrors that are not in the subset:

    for (unsigned int i=0; i<priMirrTotNum; ++i) {
     priNr  = priNamesLay[i];
      if (find(priNamesSubset.begin(), priNamesSubset.end(), priNr) == priNamesSubset.end()) {
        priNrY = priNr+"Y";
        priNrZ = priNr+"Z";
        migrad.Fix( priNrY.c_str() );
        migrad.Fix( priNrZ.c_str() );
      }
    }
    for (unsigned int i=0; i<secMirrTotNum; ++i) {
      secNr  = secNamesLay[i];
      if (find(secNamesSubset.begin(), secNamesSubset.end(), secNr) == secNamesSubset.end()) {
        secNrY = secNr+"Y";
        secNrZ = secNr+"Z";
        migrad.Fix( secNrY.c_str() );
        migrad.Fix( secNrZ.c_str() );
      }
    }

    // Do the minimization:
    ROOT::Minuit2::FunctionMinimum min = migrad ();

    // Print the results:
    cout <<"Minimum   : "<<min.Fval()          << endl;
    cout <<"Parameters: "<<min.UserParameters()<< endl;

    // Store the result in our private vector for further convenience:
    const ROOT::Minuit2::MnUserParameters outParams = min.UserParameters();

    for (unsigned int i=0; i<priMirrTotNum; ++i) {
      priNr  = priNamesLay[i];
      priNrY = priNr+"Y";
      priNrZ = priNr+"Z";
      parName_currValue[priNrY] = outParams.Value(priNrY);
    //cout <<format("  outParams.Value("+priNrY+")) = %| 7.3f|")%outParams.Value(priNrY);
    //cout <<format("parName_currValue["+priNrY+"]  = %| 7.3f|")%parName_currValue[priNrY]<< endl;
      parName_currValue[priNrZ] = outParams.Value(priNrZ);
    }
    for (unsigned int i=0; i<secMirrTotNum; ++i) {
      secNr  = secNamesLay[i];
      secNrY = secNr+"Y";
      secNrZ = secNr+"Z";
      parName_currValue[secNrY] = outParams.Value(secNrY);
    //cout <<format("  outParams.Value("+secNrY+")) = %| 7.3f|")%outParams.Value(secNrY);
    //cout <<format("parName_currValue["+secNrY+"]  = %| 7.3f|")%parName_currValue[secNrY]<< endl;
      parName_currValue[secNrZ] = outParams.Value(secNrZ);
    }

  //for (map<string,double>::const_iterator pN_cV =parName_currValue.begin();
  //                                        pN_cV!=parName_currValue.end();
  //                                        ++pN_cV) {
  //  cout <<format("pN_cV->first = %s   pN_cV->second = %| 7.3f|")%pN_cV->first %pN_cV->second<< endl;
  //}

  }

  // End of the Minuit-based solution section
  //==========================================================================
  // Next section is the algebraic method:

  else if ( solutionMethod == 1 ) {

    if      ( richDetector == 1 ) {

      // All in RICH1 is formally analogous to RICH2. However, in case of
      // RICH1 the optical system is "degenerate": each primary mirror talks
      // only with the four secondary mirrors of its quadrant, and therefore,
      // instead of system of equations for mirror pairs, we have just
      // individual equations for the secondary mirror rotations, while each
      // primary mirror remains intact.

      parName_currValue["pri00Y"] = parName_initValue["pri00Y"];
      parName_currValue["pri00Z"] = parName_initValue["pri00Z"];
      parName_currValue["pri01Y"] = parName_initValue["pri01Y"];
      parName_currValue["pri01Z"] = parName_initValue["pri01Z"];

      parName_currValue["pri02Y"] = parName_initValue["pri02Y"];
      parName_currValue["pri02Z"] = parName_initValue["pri02Z"];
      parName_currValue["pri03Y"] = parName_initValue["pri03Y"];
      parName_currValue["pri03Z"] = parName_initValue["pri03Z"];

      parName_currValue["sec00Y"] = parName_initValue["sec00Y"];
      parName_currValue["sec00Z"] = parName_initValue["sec00Z"];
      parName_currValue["sec01Y"] = parName_initValue["sec01Y"];
      parName_currValue["sec01Z"] = parName_initValue["sec01Z"];
      parName_currValue["sec02Y"] = parName_initValue["sec02Y"];
      parName_currValue["sec02Z"] = parName_initValue["sec02Z"];
      parName_currValue["sec03Y"] = parName_initValue["sec03Y"];
      parName_currValue["sec03Z"] = parName_initValue["sec03Z"];
      parName_currValue["sec04Y"] = parName_initValue["sec04Y"];
      parName_currValue["sec04Z"] = parName_initValue["sec04Z"];
      parName_currValue["sec05Y"] = parName_initValue["sec05Y"];
      parName_currValue["sec05Z"] = parName_initValue["sec05Z"];
      parName_currValue["sec06Y"] = parName_initValue["sec06Y"];
      parName_currValue["sec06Z"] = parName_initValue["sec06Z"];
      parName_currValue["sec07Y"] = parName_initValue["sec07Y"];
      parName_currValue["sec07Z"] = parName_initValue["sec07Z"];

      parName_currValue["sec08Y"] = parName_initValue["sec08Y"];
      parName_currValue["sec08Z"] = parName_initValue["sec08Z"];
      parName_currValue["sec09Y"] = parName_initValue["sec09Y"];
      parName_currValue["sec09Z"] = parName_initValue["sec09Z"];
      parName_currValue["sec10Y"] = parName_initValue["sec10Y"];
      parName_currValue["sec10Z"] = parName_initValue["sec10Z"];
      parName_currValue["sec11Y"] = parName_initValue["sec11Y"];
      parName_currValue["sec11Z"] = parName_initValue["sec11Z"];
      parName_currValue["sec12Y"] = parName_initValue["sec12Y"];
      parName_currValue["sec12Z"] = parName_initValue["sec12Z"];
      parName_currValue["sec13Y"] = parName_initValue["sec13Y"];
      parName_currValue["sec13Z"] = parName_initValue["sec13Z"];
      parName_currValue["sec14Y"] = parName_initValue["sec14Y"];
      parName_currValue["sec14Z"] = parName_initValue["sec14Z"];
      parName_currValue["sec15Y"] = parName_initValue["sec15Y"];
      parName_currValue["sec15Z"] = parName_initValue["sec15Z"];

      //------------------------------------------------------------------------

      string equationOrder[16] = {
      "pri00-->    sec00",
      "pri00-->    sec01",
      "pri00-->    sec02",
      "pri00-->    sec03",

      "pri01-->    sec04",
      "pri01-->    sec05",
      "pri01-->    sec06",
      "pri01-->    sec07",

      "pri03-->    sec08",
      "pri03-->    sec09",
      "pri03-->    sec10",
      "pri03-->    sec11",

      "pri02-->    sec12",
      "pri02-->    sec13",
      "pri02-->    sec14",
      "pri02-->    sec15"
      };

      for (unsigned int i=0; i<16; ++i) { // number of equations (mirror combinations) on each side
        string priNr = equationOrder[i].substr( 0, 5);
        string pri   =            priNr.substr( 3, 2);
        string arrow = equationOrder[i].substr( 5, 7);
        string secNr = equationOrder[i].substr(12, 5);
        string sec   =            secNr.substr( 3, 2);
        string comb  = pri+sec;

      //cout <<format("magnFactors_pri_negYzerZ[Y]=%| 10.5f|")%magnFactAv[comb+"priYn_Y"]<<format(" magnFactors_pri_negYzerZ[Z]=%| 10.5f|")%magnFactAv[comb+"priYn_Z"]<< endl;
      //cout <<format("magnFactors_pri_posYzerZ[Y]=%| 10.5f|")%magnFactAv[comb+"priYp_Y"]<<format(" magnFactors_pri_posYzerZ[Z]=%| 10.5f|")%magnFactAv[comb+"priYp_Z"]<< endl;
      //cout <<format("magnFactors_pri_zerYnegZ[Y]=%| 10.5f|")%magnFactAv[comb+"priZn_Y"]<<format(" magnFactors_pri_zerYnegZ[Z]=%| 10.5f|")%magnFactAv[comb+"priZn_Z"]<< endl;
      //cout <<format("magnFactors_pri_zerYposZ[Y]=%| 10.5f|")%magnFactAv[comb+"priZp_Y"]<<format(" magnFactors_pri_zerYposZ[Z]=%| 10.5f|")%magnFactAv[comb+"priZp_Z"]<< endl;
      //cout <<format("magnFactors_sec_negYzerZ[Y]=%| 10.5f|")%magnFactAv[comb+"secYn_Y"]<<format(" magnFactors_sec_negYzerZ[Z]=%| 10.5f|")%magnFactAv[comb+"secYn_Z"]<< endl;
      //cout <<format("magnFactors_sec_posYzerZ[Y]=%| 10.5f|")%magnFactAv[comb+"secYp_Y"]<<format(" magnFactors_sec_posYzerZ[Z]=%| 10.5f|")%magnFactAv[comb+"secYp_Z"]<< endl;
      //cout <<format("magnFactors_sec_zerYnegZ[Y]=%| 10.5f|")%magnFactAv[comb+"secZn_Y"]<<format(" magnFactors_sec_zerYnegZ[Z]=%| 10.5f|")%magnFactAv[comb+"secZn_Z"]<< endl;
      //cout <<format("magnFactors_sec_zerYposZ[Y]=%| 10.5f|")%magnFactAv[comb+"secZp_Y"]<<format(" magnFactors_sec_zerYposZ[Z]=%| 10.5f|")%magnFactAv[comb+"secZp_Z"]<< endl;

        if      ( arrow=="-->    " ) { // Actually, this is the only case for RICH1 in this context.
          // ay + cz = e
          // by + dz = f
          double a = magnFactAv[comb+"secY_Y"];
          double c = magnFactAv[comb+"secZ_Y"];
          double b = magnFactAv[comb+"secY_Z"];
          double d = magnFactAv[comb+"secZ_Z"];
        //cout <<priNr+", "+secNr+", equation coefficients: "<<format("a=%| 10.5f|, c=%| 10.5f|, e=%| 10.5f|, b=%| 10.5f|, d=%| 10.5f|, f=%| 10.5f|")
        //                                                            %a           %c           %e           %b           %d           %f<< endl;

          // e = try - (a_p*y_p + c_p*z_p)
          // f = trz - (b_p*y_p + d_p*z_p)
          double e = combRot[comb+"Y"] - magnFactAv[comb+"priY_Y"]*parName_currValue[priNr+"Y"]
                                       - magnFactAv[comb+"priZ_Y"]*parName_currValue[priNr+"Z"];
          double f = combRot[comb+"Z"] - magnFactAv[comb+"priY_Z"]*parName_currValue[priNr+"Y"]
                                       - magnFactAv[comb+"priZ_Z"]*parName_currValue[priNr+"Z"];

          parName_currValue[secNr+"Y"] = (e*d - f*c)/(a*d - b*c);
          parName_currValue[secNr+"Z"] = (a*f - b*e)/(a*d - b*c);
        }
        cout
        <<equationOrder[i]                                    <<"  "
        <<format("%| 10.5f|")%( parName_currValue[priNr+"Y"] )<<"  "
        <<format("%| 10.5f|")%( parName_currValue[priNr+"Z"] )<<"  "
        <<arrow                                               <<"  "
        <<format("%| 10.5f|")%( parName_currValue[secNr+"Y"] )<<"  "
        <<format("%| 10.5f|")%( parName_currValue[secNr+"Z"] )<< endl;
      }
    }  // end of RICH1

    // RICH2
    else if ( richDetector == 2 ) {

      string equationOrder[94] = {
      "pri12-->    sec08",  // from most-central pri12 to most-central sec08

      "pri16    <--sec08",  // from sec08 up
      "pri16-->    sec12",
      "pri20    <--sec12",
      "pri20-->    sec16",
      "pri24    <--sec16",

      "pri08    <--sec08",  // from sec08 down
      "pri08-->    sec04",
      "pri04    <--sec04",
      "pri04-->    sec00",
      "pri00    <--sec00",

      "pri12-->    sec09",  // from most-central pri12 to next-to-most-central sec09


      "pri17    <--sec09",  // from sec09 up
      "pri17-->    sec13",
      "pri21    <--sec13",
      "pri21-->    sec17",
      "pri25    <--sec17",

      "pri09    <--sec09",  // from sec09 down
      "pri09-->    sec05",
      "pri05    <--sec05",
      "pri05-->    sec01",
      "pri01    <--sec01",

      "pri13    <--sec09",  // from sec09 towards periphery of the primary
      "pri13-->    sec10",

      "pri18    <--sec10",  // from sec10 up
      "pri18-->    sec14",
      "pri22    <--sec14",
      "pri22-->    sec18",
      "pri26    <--sec18",
      "pri22-->    sec19",
      "pri27    <--sec19",

      "pri10    <--sec10",  // from sec10 down
      "pri10-->    sec06",
      "pri06    <--sec06",
      "pri06-->    sec02",
      "pri02    <--sec02",
      "pri06-->    sec03",
      "pri03    <--sec03",

      "pri14    <--sec10",  // from sec10 towards periphery of the primary
      "pri14-->    sec11",

      "pri19    <--sec11",  // from sec11 up
      "pri19-->    sec15",
      "pri23    <--sec15",

      "pri11    <--sec11",  // from sec11 down
      "pri11-->    sec07",
      "pri07    <--sec07",

      "pri15    <--sec11",  // from sec11 towards periphery of the primary

      "pri43-->    sec31",  // from most-central pri43 to most-central sec31

      "pri39    <--sec31",  // from sec31 up
      "pri39-->    sec27",
      "pri35    <--sec27",
      "pri35-->    sec23",
      "pri31    <--sec23",

      "pri47    <--sec31",  // from sec31 down
      "pri47-->    sec35",
      "pri51    <--sec35",
      "pri51-->    sec39",
      "pri55    <--sec39",

      "pri43-->    sec30",  // from most-central pri43 to next-to-most-central sec30


      "pri38    <--sec30",  // from sec30 up
      "pri38-->    sec26",
      "pri34    <--sec26",
      "pri34-->    sec22",
      "pri30    <--sec22",

      "pri46    <--sec30",  // from sec30 down
      "pri46-->    sec34",
      "pri50    <--sec34",
      "pri50-->    sec38",
      "pri54    <--sec38",

      "pri42    <--sec30",  // from sec30 towards periphery of the primary
      "pri42-->    sec29",

      "pri37    <--sec29",  // from sec29 up
      "pri37-->    sec25",
      "pri33    <--sec25",
      "pri33-->    sec21",
      "pri29    <--sec21",
      "pri33-->    sec20",
      "pri28    <--sec20",

      "pri45    <--sec29",  // from sec29 down
      "pri45-->    sec33",
      "pri49    <--sec33",
      "pri49-->    sec37",
      "pri53    <--sec37",
      "pri49-->    sec36",
      "pri52    <--sec36",

      "pri41    <--sec29",  // from sec29 towards periphery of the primary
      "pri41-->    sec28",

      "pri36    <--sec28",  // from sec28 up
      "pri36-->    sec24",
      "pri32    <--sec24",

      "pri44    <--sec28",  // from sec28 down
      "pri44-->    sec32",
      "pri48    <--sec32",

      "pri40    <--sec28"   // from sec28 towards periphery of the primary
      };
      parName_currValue["pri12Y"] = parName_initValue["pri12Y"];
      parName_currValue["pri12Z"] = parName_initValue["pri12Z"];
      parName_currValue["pri43Y"] = parName_initValue["pri43Y"];
      parName_currValue["pri43Z"] = parName_initValue["pri43Z"];

      for (unsigned int i=0; i<94; ++i) {
        string priNr = equationOrder[i].substr( 0, 5);
        string pri   =            priNr.substr( 3, 2);
        string arrow = equationOrder[i].substr( 5, 7);
        string secNr = equationOrder[i].substr(12, 5);
        string sec   =            secNr.substr( 3, 2);
        string comb  = pri+sec;

        if      ( arrow=="-->    " ) {
          // at this point the primary mirror terms are already known,
          // and so we substitute them into the equations to calculate
          // the secondary mirror terms
          // ay + cz = e
          // by + dz = f
          double a = magnFactAv[comb+"secY_Y"];
          double c = magnFactAv[comb+"secZ_Y"];
          double b = magnFactAv[comb+"secY_Z"];
          double d = magnFactAv[comb+"secZ_Z"];

          // e = try - a_p*y_p - c_p*z_p
          // f = trz - b_p*y_p - d_p*z_p
          double e = combRot[comb+"Y"] - magnFactAv[comb+"priY_Y"]*parName_currValue[priNr+"Y"]
                                       - magnFactAv[comb+"priZ_Y"]*parName_currValue[priNr+"Z"];
          double f = combRot[comb+"Z"] - magnFactAv[comb+"priY_Z"]*parName_currValue[priNr+"Y"]
                                       - magnFactAv[comb+"priZ_Z"]*parName_currValue[priNr+"Z"];
        //cout <<priNr+", "+secNr+", equation coefficients: "<<format("a=%| 10.5f|, c=%| 10.5f|, e=%| 10.5f|, b=%| 10.5f|, d=%| 10.5f|, f=%| 10.5f|")
        //                                                            %a           %c           %e           %b           %d           %f<< endl;

          // now all necessary preparations are done, so we use Cramer's rule
          // to solve the system of equations and find the misalignment rotations
          // of the secondary partner, which will be used, in turn, for further
          // substitution down the chain.
          parName_currValue[secNr+"Y"] = (e*d - f*c)/(a*d - b*c);
          parName_currValue[secNr+"Z"] = (a*f - b*e)/(a*d - b*c);
        }
        else if ( arrow=="    <--" ) {
          // at this point the secondary mirror terms are already known,
          // and so we substitute them into the equations to calculate
          // the primary mirror terms

          // ay + cz = e
          // by + dz = f
          double a = magnFactAv[comb+"priY_Y"];
          double c = magnFactAv[comb+"priZ_Y"];
          double b = magnFactAv[comb+"priY_Z"];
          double d = magnFactAv[comb+"priZ_Z"];

          // e = try - a_s*y_s - c_s*z_s
          // f = trz - b_s*y_s - d_s*z_s
          double e = combRot[comb+"Y"] - magnFactAv[comb+"secY_Y"]*parName_currValue[secNr+"Y"]
                                       - magnFactAv[comb+"secZ_Y"]*parName_currValue[secNr+"Z"];
          double f = combRot[comb+"Z"] - magnFactAv[comb+"secY_Z"]*parName_currValue[secNr+"Y"]
                                       - magnFactAv[comb+"secZ_Z"]*parName_currValue[secNr+"Z"];
        //cout <<priNr+", "+secNr+", equation coefficients: "<<format("a=%| 10.5f|, c=%| 10.5f|, e=%| 10.5f|, b=%| 10.5f|, d=%| 10.5f|, f=%| 10.5f|")
        //                                                            %a           %c           %e           %b           %d           %f<< endl;

          // now all necessary preparations are done, so we use Cramer's rule
          // to solve the system of equations and find the misalignment rotations
          // of the secondary partner, which will be used, in turn, for further
          // substitution down the chain.
          parName_currValue[priNr+"Y"] = (e*d - f*c)/(a*d - b*c);
          parName_currValue[priNr+"Z"] = (a*f - b*e)/(a*d - b*c);
        }
        cout
        <<equationOrder[i]                                    <<"  "
        <<format("%| 10.5f|")%( parName_currValue[priNr+"Y"] )<<"  "
        <<format("%| 10.5f|")%( parName_currValue[priNr+"Z"] )<<"  "
        <<arrow                                               <<"  "
        <<format("%| 10.5f|")%( parName_currValue[secNr+"Y"] )<<"  "
        <<format("%| 10.5f|")%( parName_currValue[secNr+"Z"] )<< endl;
      }
    }
  }
  //==========================================================================
  // Now that the fitting has ended, we will raise the flag to end the
  // alignment iterations loop if all the newly obtained in this iteration
  // additional corrections are < stopTolerancePriY ... etc. mrad

  string stop_or_continue("STOP");

  for (map<string,double>::const_iterator pN_cV =parName_currValue.begin();
                                          pN_cV!=parName_currValue.end();
                                        ++pN_cV) {
    if (
      (
        ((pN_cV->first).find("pri") != std::string::npos)
        &&
        (
          (((pN_cV->first).find("Y") != std::string::npos) && (fabs(pN_cV->second) > stopTolerancePriY))
          ||
          (((pN_cV->first).find("Z") != std::string::npos) && (fabs(pN_cV->second) > stopTolerancePriZ))
        )
      )
      ||
      (
        ((pN_cV->first).find("sec") != std::string::npos)
        &&
        (
          (((pN_cV->first).find("Y") != std::string::npos) && (fabs(pN_cV->second) > stopToleranceSecY))
          ||
          (((pN_cV->first).find("Z") != std::string::npos) && (fabs(pN_cV->second) > stopToleranceSecY))
        )
      )
    ) {
      stop_or_continue = "CONTINUE";
      break;
    }
  }

  cout <<endl <<"Verdict is "+stop_or_continue<< endl<< endl;

  // write the verdict to the stop_or_continue.txt file
  ofstream stop_or_continue_txt;
  stop_or_continue_txt.open( (workDir+"/Rich"+rNr+"_stop_or_continue.txt").c_str() );
  stop_or_continue_txt <<stop_or_continue<< endl;
  stop_or_continue_txt.close();

  cout <<"File Rich"+rNr+"_stop_or_continue.txt written"<< endl<< endl;

  //==========================================================================
  // Mirror rotational misalignment compensation corrections:
  // we assign with the opposite sign the obtained parameters (which are the
  // measured misalignments) to the this-iteration corrections of the
  // misalignment compensations; these corrections will be added to the
  // compensations used during this iteration and will be used during the next
  // iteration if they are not yet small enough.

  map<string, double> old_MisalignCompensation;        // previous misalignment compensations, used in zeroth iteration as input
  map<string, double> used_MisalignCompensation;       // misalignment compensations, used in this iteration as input
  map<string, double> updated_MisalignCompensation;    // misalignment compensations yielded by this iteration for usage in the next iteration

  map<string, double> all_Iterations_MisalCompensCorr; // cumulative misalignment compensation corrections from all iterations, including this
  map<string, double> this_Iteration_MisalCompensCorr; // only this iteration misalignment compensation additional corrections

  for (map<string,double>::const_iterator pN_cV =parName_currValue.begin();
                                          pN_cV!=parName_currValue.end();
                                        ++pN_cV) {
    this_Iteration_MisalCompensCorr[pN_cV->first] = -(pN_cV->second);
  //cout <<format("this_Iteration_MisalCompensCorr["+pN_cV->first+"] = %| 8.5f|")%this_Iteration_MisalCompensCorr[pN_cV->first]<< endl;
  }
  //==========================================================================
  cout<< endl;
  //==========================================================================
  //==========================================================================
  //==========================================================================
  //==========================================================================
  //==========================================================================
  // Xerces stuff.
  //==========================================================================
  //==========================================================================
  //==========================================================================
  //==========================================================================
  //==========================================================================

  // the resulting xml file will get iteration number incremented,
  // equal to the next iteration number:

  cout<< endl;

//string thisVariantFull_i0   = thisVariant+"_i0";
//string thisVariantFull_next = thisVariant+"_i"+lexical_cast<string>(iterationCount+1);

//string                                     oldFileStr = workDir+"/Mirrors_rich"+rNr+"_"+thisVariantFull_i0  +".xml";
//cout <<       "oldFile:      "            +oldFileStr<< endl;
//const char*    oldFile    =                oldFileStr.c_str();
  const char*    oldFile    =    zerothIterationXMLFile.c_str();
  cout <<       "oldFile:      "+zerothIterationXMLFile<< endl;

//string                                   inputFileStr = workDir+"/Mirrors_rich"+rNr+"_"+thisVariantFull     +".xml";
//cout <<      "inputFile:    "           +inputFileStr<< endl;
//const char*   inputFile    =             inputFileStr.c_str();
  const char*   inputFile    =  currentIterationXMLFile.c_str();
  cout <<      "inputFile:    "+currentIterationXMLFile<< endl;

//string                                  outputFileStr = workDir+"/Mirrors_rich"+rNr+"_"+thisVariantFull_next+".xml";
//cout <<     "outputFile:   "           +outputFileStr<< endl;
//const char*  outputFile    =            outputFileStr.c_str();
  const char*  outputFile    =     nextIterationXMLFile.c_str();
  cout <<     "outputFile:   "    +nextIterationXMLFile<< endl;

  cout<< endl;

  smatch matches;

  // The following regex manipulations part is an unfortunate consequence of the
  // fact, that individual rotations were inconsiderately (and strictly speaking
  // wrongly) not made separate entities...

  // to match something like " -1.02* mrad  -0.25 *mrad  0.0*mrad ":
  sregex dRotXYZText_MatchPattern = sregex::compile(
  "(\\s*)"                 // matches[1]
  "(\\-?\\s*[0-9\\.]+)"    // matches[2]
  "(\\s*\\*\\s*mrad\\s*)"  // matches[3]
  "(\\-?\\s*[0-9\\.]+)"    // matches[4] rotation around Y
  "(\\s*\\*\\s*mrad\\s*)"  // matches[5]
  "(\\-?\\s*[0-9\\.]+)"    // matches[6] rotation around Z
  "(\\s*\\*\\s*mrad\\s*)"  // matches[7]
  );

  // mirror types:
  string mirrTypes[2] = {"pri", "sec"};

  // prepare dictionary of mirror type names
  map<string, string> mirrType_typeName;
  mirrType_typeName["pri"] = "Sph";
  mirrType_typeName["sec"] = "Sec";

  string mirrType;
  string typeName;

  // Prepare dictionary of total mirror numbers:
  map<string, unsigned int> mirrType_totNum;
  mirrType_totNum["pri"] = priMirrTotNum;
  mirrType_totNum["sec"] = secMirrTotNum;

  // Initialise Xerces-C and XQilla using XQillaPlatformUtils:
  XQillaPlatformUtils::initialize();

  // Get the XQilla DOMImplementation object:
  DOMImplementation* xqillaImplementation = DOMImplementationRegistry::getDOMImplementation( X("XPath2 3.0") );

  try {
    // Create a DOMLSParser object:
    AutoRelease<DOMLSParser> parser( xqillaImplementation->createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, 0) );
    parser->getDomConfig()->setParameter(XMLUni::fgDOMNamespaces,            false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesSchema,             false);
    parser->getDomConfig()->setParameter(XMLUni::fgDOMValidateIfSchema,      false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesSchemaFullChecking, false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesSkipDTDValidation,  false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesLoadExternalDTD,    false);
    // Parse the DOMDocument of the XML file "inputFile" that was used during this iteration:
    DOMDocument* document = parser->parseURI( inputFile );
    if(document == 0) {
      cout <<"Document not found."<< endl;
      return 1;
    }
    DOMElement* docElem = document->getDocumentElement();
    // Create a DOMLSSerializer to output the nodes:
    AutoRelease<DOMLSSerializer> serializer( xqillaImplementation->createLSSerializer() );
    serializer->getDomConfig()->setParameter( XMLUni::fgDOMWRTFormatPrettyPrint, false );

    AutoRelease<DOMLSOutput> theOutputDesc( xqillaImplementation->createLSOutput() );
    // The updated misalignment compensation corrections resulting from this iteration
    // will be written to the "outputFile" XML file:
    XMLFormatTarget* myFormTarget = new LocalFileFormatTarget( outputFile );
    theOutputDesc->setByteStream( myFormTarget );
    theOutputDesc->setEncoding( X("ISO-8859-1") );
    // Loop over dRotXYZ elements of all mirrors:
    for (unsigned int i=0; i<=1; ++i) {
      mirrType = mirrTypes[i];
      typeName = mirrType_typeName[mirrType];

      for ( unsigned int mirrNri=0; mirrNri < mirrType_totNum[mirrType]; ++mirrNri ) {

        string mirrNriStr = lexical_cast<string>(mirrNri);

        string mirrNr = mirrNriStr;
        if (mirrNri<=9) mirrNr = "0"+mirrNriStr;

        string typeNrY = mirrType+mirrNr+"Y";
        string typeNrZ = mirrType+mirrNr+"Z";

        string xpathStr;

        if      ( richDetector == 1) {
          xpathStr = "//condition[@name=\""     +typeName+"Mirror"+mirrNriStr+"_Align\"]/paramVector[@name=\"dRotXYZ\"]";
        }
        else if ( richDetector == 2) {
          xpathStr = "//condition[@name=\"Rich2"+typeName+"Mirror"+mirrNriStr+"_Align\"]/paramVector[@name=\"dRotXYZ\"]";
        }
        if (mirrNr=="00") cout <<xpathStr<< endl;
        AutoRelease<DOMXPathExpression>              expression( document->createExpression(X(xpathStr.c_str()), 0) );
        AutoRelease<DOMXPathResult> thisParamVector( expression->evaluate(docElem, DOMXPathResult::FIRST_RESULT_TYPE, 0) );

        DOMNode*                        dRotXYZNode = thisParamVector->getNodeValue();
        DOMNode*      dRotXYZTextNode = dRotXYZNode->getFirstChild();
        const XMLCh*  dRotXYZXCh      = thisParamVector->getStringValue();
        char*         dRotXYZTextCStr = XMLString::transcode( dRotXYZXCh );
        if (mirrNr=="00") cout <<"dRotXYZTextCStr :       \""<<dRotXYZTextCStr<<"\""<< endl;

        string        dRotXYZText_Str( (string)dRotXYZTextCStr );
        regex_search( dRotXYZText_Str,  matches,  dRotXYZText_MatchPattern );
        if (mirrNr=="00") {
          cout <<"matches.size(): "<<matches.size()<< endl;
          for (unsigned int i=0;   i<matches.size(); ++i) {
            cout <<"$"<<i<<" = \""+matches[i].str()+"\""<< endl;
          }
        }

        // Update the used in this iteration misalignment compensations with the
        // new fitted misalignment compensations that are simply corrections
        // to the used ones. In other words, improve the misalignment
        // compensations that will be used in a next iteration, or simply will
        // stay like this from now on, if current alignment session finishes after
        // this iteration:

        if (mirrNr=="00") cout <<"matches[4] "<<matches[4]<< endl;
        if (mirrNr=="00") cout <<"matches[6] "<<matches[6]<< endl;

        // Used in this iteration compensations:
        used_MisalignCompensation[typeNrY] = lexical_cast<double>( matches[4] );
        used_MisalignCompensation[typeNrZ] = lexical_cast<double>( matches[6] );
        // Updated compensations resulting from this iteration:
        updated_MisalignCompensation[typeNrY] = used_MisalignCompensation[typeNrY] + this_Iteration_MisalCompensCorr[typeNrY];
        updated_MisalignCompensation[typeNrZ] = used_MisalignCompensation[typeNrZ] + this_Iteration_MisalCompensCorr[typeNrZ];

        if (mirrNr=="00") cout <<format("this_Iteration_MisalCompensCorr["+typeNrY+"] =%| 8.5f| ")%this_Iteration_MisalCompensCorr[typeNrY]<< endl;
        if (mirrNr=="00") cout <<format("used_MisalignCompensation      ["+typeNrY+"] =%| 8.5f| ")      %used_MisalignCompensation[typeNrY]<< endl;
        if (mirrNr=="00") cout <<format("updated_MisalignCompensation   ["+typeNrY+"] =%| 8.5f| ")   %updated_MisalignCompensation[typeNrY]<< endl;

        if (mirrNr=="00") cout <<format("this_Iteration_MisalCompensCorr["+typeNrZ+"] =%| 8.5f| ")%this_Iteration_MisalCompensCorr[typeNrZ]<< endl;
        if (mirrNr=="00") cout <<format("used_MisalignCompensation      ["+typeNrZ+"] =%| 8.5f| ")      %used_MisalignCompensation[typeNrZ]<< endl;
        if (mirrNr=="00") cout <<format("updated_MisalignCompensation   ["+typeNrZ+"] =%| 8.5f| ")   %updated_MisalignCompensation[typeNrZ]<< endl;

        if (mirrNr=="00") cout <<"matches[2] "<<matches[2]<< endl;

        // Update the paramVector body:
        stringstream newText_stgstm;
        newText_stgstm
        <<format("%| 10.5f|")
                  %lexical_cast< double >( matches[2] )
        <<"*mrad "
        <<format("%| 10.5f|")
                  %updated_MisalignCompensation[typeNrY]
        <<"*mrad "
        <<format("%| 10.5f|")
                  %updated_MisalignCompensation[typeNrZ]
        <<"*mrad";

        if (mirrNr=="00") cout <<"newText_stgstm.str():  \""<<newText_stgstm.str()<<"\""<< endl;

        dRotXYZTextNode->setNodeValue( X( (newText_stgstm.str()).c_str() ) );
      } // End loop over mirror numbers.
    } // End loop over mirror types.
    // Finally, write out the new XML file for the next iteration:
    cout <<"==========================================="<< endl;
    cout <<"Write output file"<< endl;
    serializer->write(document, theOutputDesc);
    cout <<"Output file complete"<< endl;
    cout <<"==========================================="<< endl;
  }
  catch(XQillaException& e) {
    cout <<"XQillaException: "<<UTF8(e.getString())<< endl;
    return 1;
  }
  // Now read the file with the previous compensations used by
  // the very first (zeroth) iteration of this alignment session,
  // and then calculate the cumulative corrections resulted from
  // the current iteration:
  try {
    // Create a DOMLSParser object:
    AutoRelease<DOMLSParser> parser( xqillaImplementation->createLSParser(DOMImplementationLS::MODE_SYNCHRONOUS, 0) );
    parser->getDomConfig()->setParameter(XMLUni::fgDOMNamespaces,            false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesSchema,             false);
    parser->getDomConfig()->setParameter(XMLUni::fgDOMValidateIfSchema,      false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesSchemaFullChecking, false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesSkipDTDValidation,  false);
    parser->getDomConfig()->setParameter(XMLUni::fgXercesLoadExternalDTD,    false);
    // Parse the DOMDocument of the XML file "oldFile" that was used during the very first iteration:
    DOMDocument* document = parser->parseURI( oldFile );
    if(document == 0) {
      cout <<"Document not found."<< endl;
      return 1;
    }
    DOMElement* docElem = document->getDocumentElement();
    // Loop over dRotXYZ elements of all mirrors:
    for (unsigned int i=0; i<=1; ++i) {
      mirrType = mirrTypes[i];
      typeName = mirrType_typeName[mirrType];

      for ( unsigned int mirrNri=0; mirrNri < mirrType_totNum[mirrType]; ++mirrNri ) {

        string mirrNriStr = lexical_cast<string>(mirrNri);

        string mirrNr = mirrNriStr;
        if (mirrNri<=9) mirrNr = "0"+mirrNriStr;

        string typeNrY = mirrType+mirrNr+"Y";
        string typeNrZ = mirrType+mirrNr+"Z";

        string xpathStr;

        if      ( richDetector == 1) {
          xpathStr = "//condition[@name=\""     +typeName+"Mirror"+mirrNriStr+"_Align\"]/paramVector[@name=\"dRotXYZ\"]";
        }
        else if ( richDetector == 2) {
          xpathStr = "//condition[@name=\"Rich2"+typeName+"Mirror"+mirrNriStr+"_Align\"]/paramVector[@name=\"dRotXYZ\"]";
        }
        if (mirrNr=="00") cout <<xpathStr<< endl;
        AutoRelease<DOMXPathExpression>              expression( document->createExpression(X(xpathStr.c_str()), 0) );
        AutoRelease<DOMXPathResult> thisParamVector( expression->evaluate(docElem, DOMXPathResult::FIRST_RESULT_TYPE, 0) );

        const XMLCh*  dRotXYZXCh      = thisParamVector->getStringValue();
        char*         dRotXYZTextCStr = XMLString::transcode( dRotXYZXCh );
        if (mirrNr=="00") cout <<"dRotXYZTextCStr :       \""<<dRotXYZTextCStr<<"\""<< endl;

        string        dRotXYZText_Str( (string)dRotXYZTextCStr );
        regex_search( dRotXYZText_Str,  matches,  dRotXYZText_MatchPattern );
        if (mirrNr=="00") {
          cout <<"matches.size(): "<<matches.size()<< endl;
          for (unsigned int i=0;   i<matches.size(); ++i) {
            cout <<"$"<<i<<" = \""+matches[i].str()+"\""<< endl;
          }
        }

        // Previous compensations, still in the CONDDB, that were used during the very first itertaion:
        old_MisalignCompensation[typeNrY] = lexical_cast<double>( matches[4] );
        old_MisalignCompensation[typeNrZ] = lexical_cast<double>( matches[6] );
        // Accumulated compensation corrections from all iterations, including this iteration:
        all_Iterations_MisalCompensCorr[typeNrY] = updated_MisalignCompensation[typeNrY] - old_MisalignCompensation[typeNrY];
        all_Iterations_MisalCompensCorr[typeNrZ] = updated_MisalignCompensation[typeNrZ] - old_MisalignCompensation[typeNrZ];
        /*
        */
        if (mirrNr=="00") cout <<mirrType+mirrNr<< endl;
        if (mirrNr=="00") cout <<format("previous compensations (used in zeroth iteration)         round Y: %| 10.5f|")%       old_MisalignCompensation[typeNrY];
        if (mirrNr=="00") cout <<format(                                                       ",  round Z: %| 10.5f|")%       old_MisalignCompensation[typeNrZ]<< endl;
      //if (mirrNr=="00") cout <<format("used in this iteration compensations                      round Y: %| 10.5f|")%      used_MisalignCompensation[typeNrY];
      //if (mirrNr=="00") cout <<format(                                                       ",  round Z: %| 10.5f|")%      used_MisalignCompensation[typeNrZ]<< endl;
        if (mirrNr=="00") cout <<format("cumulative corrections to previous compensations          round Y: %| 10.5f|")%all_Iterations_MisalCompensCorr[typeNrY];
        if (mirrNr=="00") cout <<format(                                                       ",  round Z: %| 10.5f|")%all_Iterations_MisalCompensCorr[typeNrZ]<< endl;
        if (mirrNr=="00") cout <<format("updated compensations                                     round Y: %| 10.5f|")%   updated_MisalignCompensation[typeNrY];
        if (mirrNr=="00") cout <<format(                                                       ",  round Z: %| 10.5f|")%   updated_MisalignCompensation[typeNrZ]<< endl;
        if (mirrNr=="00") cout <<format("this iteration's additional corrections to compensations  round Y: %| 10.5f|")%this_Iteration_MisalCompensCorr[typeNrY];
        if (mirrNr=="00") cout <<format(                                                       ",  round Z: %| 10.5f|")%this_Iteration_MisalCompensCorr[typeNrZ]<< endl;
        //
      } // End loop over mirror numbers.
    } // End loop over mirror types.
  }
  catch(XQillaException &e) {
    cout <<"XQillaException: "<<UTF8(e.getString())<< endl;
    return 1;
  }
  // Terminate Xerces-C and XQilla using XQillaPlatformUtils:
  XQillaPlatformUtils::terminate();
  cout <<"==========================================="<< endl;

  //==========================================================================
  /*
  priNamesSubsetIt = priNamesSubset.begin();

  for (unsigned int i=0; i<priMirrTotNum; ++i) {
    cout <<*priNamesSubsetIt<< endl;
    ++priNamesSubsetIt;
  }

  secNamesSubsetIt = secNamesSubset.begin();

  for (unsigned int i=0; i<secMirrTotNum; ++i) {
    cout <<*secNamesSubsetIt<< endl;
    ++secNamesSubsetIt;
  }
  */
  //
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------

  // Final summary printout for monitoring:
  cout << endl;
//cout <<"                                                  RICH"+rNr<< endl;
  cout << endl;
  cout
  <<"Summary of the case: "+thisVariantFull
  <<"  iteration: "+lexical_cast<string>(iterationCount)<< endl;
  cout << endl;
  cout << endl;

  // define how to print
  vector<int> printStyles;

  if      (printMode==0) {
    printStyles.push_back(0);
  }
  else if (printMode==1) {
    printStyles.push_back(1);
  }
  else if (printMode==2) {
    printStyles.push_back(1);
    printStyles.push_back(0);
  }

  string printStyle_str[] = {
  "                          One-glance printout with no digits after decimal point",
  "                           Detailed printout with one digit after decimal point"
  };


  for (auto printStyle=printStyles.begin(); printStyle<printStyles.end(); ++printStyle) {

    cout << endl;
    cout << endl;
    cout <<printStyle_str[*printStyle]<< endl;

    for (unsigned int i=0; i<=1; ++i) {

      string mirrType = mirrTypes[i];

      vector<string> mirrLay;

      cout << endl;

      double stopToleranceY;
      double stopToleranceZ;

      string sTY;
      string sTZ;

      if      ("pri"==mirrType) {
        mirrLay = priNamesLay;
        cout <<"                                              RICH"+rNr+" Primary"<< endl;
        stopToleranceY                              = stopTolerancePriY;
        stringstream sTssY; sTssY <<format("%|4.2f|")%stopTolerancePriY;
        sTY = sTssY.str();
        stopToleranceZ                              = stopTolerancePriZ;
        stringstream sTssZ; sTssZ <<format("%|4.2f|")%stopTolerancePriZ;
        sTZ = sTssZ.str();
        cout <<"                 divided by the tolerances  Y:"+sTY+"  Z:"+sTZ+" mrad, truncated, not rounded"                     << endl;
      }
      else if ("sec"==mirrType) {
        mirrLay = secNamesLay;
        cout <<"                                             RICH"+rNr+" Secondary"<< endl;
        stopToleranceY                              = stopToleranceSecY;
        stringstream sTssY; sTssY <<format("%|4.2f|")%stopToleranceSecY;
        sTY = sTssY.str();
        stopToleranceZ                              = stopToleranceSecZ;
        stringstream sTssZ; sTssZ <<format("%|4.2f|")%stopToleranceSecZ;
        sTZ = sTssZ.str();
        cout <<"                 divided by the tolerances  Y:"+sTY+"  Z:"+sTZ+" mrad, truncated, not rounded"                     << endl;
      }

      cout << endl;

      cout <<"                             previous compensations (used in 0th iteration)"                                         << endl;
      print_yz_table(richDetector, mirrType, mirrLay, old_MisalignCompensation,        *printStyle, stopToleranceY, stopToleranceZ);

      cout <<"                            cumulative corrections to previous compensations"                                        << endl;
      print_yz_table(richDetector, mirrType, mirrLay, all_Iterations_MisalCompensCorr, *printStyle, stopToleranceY, stopToleranceZ);

      cout <<"                                          updated compensations"                                                     << endl;
      print_yz_table(richDetector, mirrType, mirrLay, updated_MisalignCompensation,    *printStyle, stopToleranceY, stopToleranceZ);

      cout <<"                      only this iteration's additional corrections to compensations"                                 << endl;
      print_yz_table(richDetector, mirrType, mirrLay, this_Iteration_MisalCompensCorr, *printStyle, stopToleranceY, stopToleranceZ);

    }
  }
  //--------------------------------------------------------------------------

  cout <<"End"<< endl;

  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  return(0);
}
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
