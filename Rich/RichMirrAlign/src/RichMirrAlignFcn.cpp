#include <cmath>
#include <iostream>
#include <vector>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/math/special_functions/pow.hpp>
#include "RichMirrAlignFcn.h"

using namespace boost::math;
using           boost::format;
using           boost::lexical_cast;
using             std::cout;
using             std::endl;
using             std::map;
using             std::string;
using             std::vector;
//-----------------------------------------------------------------------------
// Implementation file for class : RichMirrAlignFcn
//
// 2004-11-08 : Antonis Papanestis
//-----------------------------------------------------------------------------

// Standard constructor, initializes variables
RichMirrAlignFcn::RichMirrAlignFcn(
const vector<string>               & combSubset          ,
const    map<string, double>       & combRot             ,
const    map<string, double>       & magnFactAv          ,
const    map<string, double>       & magnFactAvAll       ,
const    map<string, unsigned int> & name_order
) :
m_combSubset                       ( combSubset         ),
m_combRot                          ( combRot            ),
m_magnFactAv                       ( magnFactAv         ),
m_magnFactAvAll                    ( magnFactAvAll      ),
m_name_order                       ( name_order         ),

m_theErrorDef                      ( 1.0                ) {

//cout <<"Hello from RichMirrAlignFcn! Initialization."<< endl;

}

// Destructor:
RichMirrAlignFcn::~RichMirrAlignFcn() {}

// operator():
double RichMirrAlignFcn::operator() (const vector<double>& par) const {
  /*
  cout <<"Hello from RichMirrAlignFcn! Iteration."<< endl;
  */
  //

  /*
  unsigned int i=0;
  for (vector<double>::const_iterator parIt =par.begin();
                                      parIt!=par.end();
                                      parIt++) {
    cout <<format("par[%|02d|] =%| 6.3f|")
                       %i       %*parIt<< endl;
    i++;
  }
  */
  //

  double D = 0.0;
  double N = 0.0;
  double S = 0.0;

  for ( vector<string>::const_iterator comb =m_combSubset.begin();
                                       comb!=m_combSubset.end();
                                       comb++ ) {

    double combY    = m_combRot.find(*comb+"Y"   )->second;
  //double combYerr = m_combRot.find(*comb+"Yerr")->second;
    double combZ    = m_combRot.find(*comb+"Z"   )->second;
  //double combZerr = m_combRot.find(*comb+"Zerr")->second;
    /*
    cout <<*comb+"   ";
    cout <<format("combY =%| 6.3f|    combYerr =%| 6.3f|    combZ =%| 6.3f|    combZerr =%| 6.3f|    ")%combY %combYerr %combZ %combZerr<< endl;
    */
    //
    string priNr = "pri"+(*comb).substr(0, 2);
    string secNr = "sec"+(*comb).substr(2, 2);
    /*
    cout <<priNr+"   "+secNr+"   ";
    */
    //
    string priNrY = priNr+"Y";
    string secNrY = secNr+"Y";
    string priNrZ = priNr+"Z";
    string secNrZ = secNr+"Z";
    /*
    cout <<priNrY+"   "+secNrY+"   "+priNrZ+"   "+secNrZ+"   "<< endl;
    */
    //
    unsigned int priNrYi = m_name_order.find(priNrY)->second;
    unsigned int secNrYi = m_name_order.find(secNrY)->second;
    unsigned int priNrZi = m_name_order.find(priNrZ)->second;
    unsigned int secNrZi = m_name_order.find(secNrZ)->second;
    /*
    cout <<format("priNrYi =%|2d|    secNrYi =%|2d|    priNrZi =%|2d|    secNrZi =%|2d|    ")%priNrYi %secNrYi %priNrZi %secNrZi;
    */
    //
    double priY = par[priNrYi];
    double secY = par[secNrYi];
    double priZ = par[priNrZi];
    double secZ = par[secNrZi];
    /*
    cout <<format("priY =%| 6.3f|    secY =%| 6.3f|    priZ =%| 6.3f|    secZ =%| 6.3f|    ")%priY %secY %priZ %secZ<< endl;
    */
    //
  //double priYerr = err[priNrYi];
  //double secYerr = err[secNrYi];
  //double priZerr = err[priNrZi];
  //double secZerr = err[secNrZi];

    double mag_priY_Y = m_magnFactAv.find(*comb+"priY_Y")->second;
    double mag_priY_Z = m_magnFactAv.find(*comb+"priY_Z")->second;
    double mag_priZ_Y = m_magnFactAv.find(*comb+"priZ_Y")->second;
    double mag_priZ_Z = m_magnFactAv.find(*comb+"priZ_Z")->second;
    double mag_secY_Y = m_magnFactAv.find(*comb+"secY_Y")->second;
    double mag_secY_Z = m_magnFactAv.find(*comb+"secY_Z")->second;
    double mag_secZ_Y = m_magnFactAv.find(*comb+"secZ_Y")->second;
    double mag_secZ_Z = m_magnFactAv.find(*comb+"secZ_Z")->second;
    /*
    cout <<format("mag_priY_Y =%| 6.3f|    mag_secY_Y =%| 6.3f|    mag_priZ_Y =%| 6.3f|    mag_secZ_Y =%| 6.3f|    ")%mag_priY_Y %mag_secY_Y %mag_priZ_Y %mag_secZ_Y;
    cout <<format("mag_priY_Z =%| 6.3f|    mag_secY_Z =%| 6.3f|    mag_priZ_Z =%| 6.3f|    mag_secZ_Z =%| 6.3f|    ")%mag_priY_Z %mag_secY_Z %mag_priZ_Z %mag_secZ_Z<< endl;
    */
    //

    // Sum of the squares of (differences between the right- and left-hand sides)
    // divided by their dispersions
    // for Y and Z separately:

     double diffY =              combY     -       (mag_priY_Y*priY     +        mag_secY_Y*secY     +  // major terms
                                                    mag_priZ_Y*priZ     +        mag_secZ_Y*secZ)    ;  // minor terms

  //D += pow<2>(diffY) / (pow<2>(combYerr) + pow<2>(mag_priY_Y*priYerr) + pow<2>(mag_secY_Y*secYerr) +  // major terms
  //                                         pow<2>(mag_priZ_Y*priZerr) + pow<2>(mag_secZ_Y*secZerr));  // minor terms

  //D += pow<2>(diffY) /  pow<2>(combYerr);
    D += pow<2>(diffY)                    ;
    /*
    cout <<format("S diffY =%|8.3f|   ")%S;
    */
    //
    double diffZ =               combZ     -       (mag_priZ_Z*priZ     +        mag_secZ_Z*secZ     +  // major terms
                                                    mag_priY_Z*priY     +        mag_secY_Z*secY)    ;  // minor terms

  //D += pow<2>(diffZ) / (pow<2>(combZerr) + pow<2>(mag_priZ_Z*priZerr) + pow<2>(mag_secZ_Z*secZerr) +  // major terms
  //                                         pow<2>(mag_priY_Z*priYerr) + pow<2>(mag_secY_Z*secYerr));  // minor terms

  //D += pow<2>(diffZ) /  pow<2>(combZerr);
    D += pow<2>(diffZ)                    ;
    /*
    cout <<format("D diffZ =%|8.3f|   ")%S;
    */
    //
    // Finally, the regularizing sum of the weighted solution:
  //N += (pow<2>(priY / priYerr) +
  //      pow<2>(secY / secYerr) +
  //      pow<2>(priZ / priZerr) +
  //      pow<2>(secZ / secZerr));
    // Finally, the regularizing sum of the un-weighted solution:
  //N += (pow<2>(priY)           +
  //      pow<2>(secY)           +
  //      pow<2>(priZ)           +
  //      pow<2>(secZ)          );
    // Finally, the regularizing sum of the "magnified" solution.
    // Each component of the solution vector is multiplied by the
    // respective magnification factor, averaged over all mirror
    // combinations and calibrational tilts:
    N += ( pow<2>(m_magnFactAvAll.find("priY_Y")->second * priY)
         + pow<2>(m_magnFactAvAll.find("secY_Y")->second * secY)
         + pow<2>(m_magnFactAvAll.find("priZ_Z")->second * priZ)
         + pow<2>(m_magnFactAvAll.find("secZ_Z")->second * secZ)
         );
    /*
    cout <<format("N =%|8.3f|   ")%N;
    */
    //
    S += (D + N);
  }
  /*
  cout <<format("D =%|8.3f|   N =%|8.3f|   S =%|8.3f|")%D%N%S<< endl;
  */
  //

  return S  ;
}
