/** @class RichMirrAlignFcn RichMirrAlignFcn.h
 *
 *  @author Antonis Papanestis
 *  @date   2004-11-08
 */
#ifndef RICHALIGN_RICHMIRRALIGNFCN_H
#define RICHALIGN_RICHMIRRALIGNFCN_H 1

#include <map>
#include <vector>
#include <Minuit2/FCNBase.h>

class RichMirrAlignFcn : public ROOT::Minuit2::FCNBase {

public:
  /// Standard constructor
  RichMirrAlignFcn(
  const std::vector<std::string>               & combSubset    ,
  const    std::map<std::string, double>       & combRot       ,
  const    std::map<std::string, double>       & magnFactAv    ,
  const    std::map<std::string, double>       & magnFactAvAll ,
  const    std::map<std::string, unsigned int> & name_order
  );

  virtual ~RichMirrAlignFcn(); ///< Destructor

  virtual double Up() const { return m_theErrorDef; }

  virtual double operator() (const std::vector<double>& params) const;

protected:

private:
        std::vector<std::string>               m_combSubset    ;
           std::map<std::string, double>       m_combRot       ;
           std::map<std::string, double>       m_magnFactAv    ;
           std::map<std::string, double>       m_magnFactAvAll ;
           std::map<std::string, unsigned int> m_name_order    ;

                                 double        m_theErrorDef   ;
};
#endif // RICHALIGN_RICHMIRRALIGNFCN_H
