#ifndef HPDALIGN_H
# define HPDALIGN_H

# include <fstream>
# include <iostream>
# include <string>
# include <vector>
# include <map>
# include <boost/lexical_cast.hpp>

# include "HPDAlignParam.h"

class HPDAlign
{
public:

  HPDAlign(std::string InputXMLFile, std::string InputFitFile, int rich);

  ~HPDAlign();

  void ReadXMLFile(std::string inputFile);

  void ReadFitResultFile();

  void WriteXMLFile(std::string outputXML);

protected:
  int GetHPD(std::string& line);

  void GetInitalValue(std::string& line, double& x, double& y);

  void WriteLastLines(std::string outputXML);

  void WritePreXML(std::string outputXML);

  void WriteLines(std::vector<std::string>& Params, std::string outFile);


private:
  std::string inputXMLFile;
  std::string inputFitFile;
  int RICH;
  std::map<int,HPDAlignParam*> HPDs;
  int NumHPDs;

};

#endif
