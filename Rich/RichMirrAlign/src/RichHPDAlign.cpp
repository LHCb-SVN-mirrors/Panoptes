//Local
#include "HPDAlign.h"


#include <cmath>
#include <cstring>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/token_functions.hpp>
#include <boost/tokenizer.hpp>
#include <boost/xpressive/xpressive.hpp>



namespace    po = boost::program_options;
using   namespace boost::xpressive;
using             boost::format;
using             boost::lexical_cast;
using               std::cerr;
using               std::cout;
using               std::endl;
using               std::exception;
using               std::fstream;
using               std::ifstream;
using               std::ofstream;
using               std::map;
using               std::runtime_error;
using               std::string;
using               std::stringstream;
using               std::vector;



void HPDAligner(string inputXML, string inputFit, string outputXML, int Rich)
{

  cout << "outputXML " << outputXML << endl;
  cout <<  "inputXML " <<  inputXML << endl;
  cout <<  "inputFit " <<  inputFit << endl;

  HPDAlign hpdAlign(inputXML, inputFit, Rich);
  //Write output .xml
  std::cout << "Write XML File" << std::endl;
  hpdAlign.WriteXMLFile(outputXML);
  std::cout << "File Written " <<  outputXML << std::endl;

}

int main(int argc, char* argv[]) {

  int rich = lexical_cast<int>(argv[1]);                    //rich detector


  if (argc < 1)
  {
    std::cout << "ERROR too few parameters" << std::endl;
  }
  // currentHPDXMLFile;
  std::string   inputHPDXMLFileStr = lexical_cast<std::string>(argv[2]);
  // nextIterationHPDXMLFile;
  std::string  outputHPDXMLFileStr = lexical_cast<std::string>(argv[3]);
  //workDir+"/HPDs_rich"+strRichDetector+"_i"+lexical_cast<string>(iterationCount)+".txt";
  std::string   inputHPDFitFileStr = lexical_cast<std::string>(argv[4]);
  //  std::string fitresults = lexical_cast<std::string>(argv[5]);

  HPDAligner(inputHPDXMLFileStr, inputHPDFitFileStr, outputHPDXMLFileStr, rich);

  return 0;
}
