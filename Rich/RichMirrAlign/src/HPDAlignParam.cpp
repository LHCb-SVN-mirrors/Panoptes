#include "HPDAlignParam.h"

using namespace std;

HPDAlignParam::HPDAlignParam()
:init_x(0.0), init_y(0.0), new_x(0.0), new_y(0.0), fit_x(0.0), fit_y(0.0)
{

}

HPDAlignParam::HPDAlignParam(int hpd)
:m_HPD(hpd), init_x(0.0), init_y(0.0), new_x(0.0), new_y(0.0), fit_x(0.0), fit_y(0.0)
{

}

int HPDAlignParam::HPD()
{
  return m_HPD;
}

void HPDAlignParam::SetFitResultX(double x)
{
  fit_x = 1000*x;
}

void HPDAlignParam::SetFitResultY(double y)
{
  fit_y = 1000*y;
}

void HPDAlignParam::InitX(double x)
{
  init_x = x;
}

void HPDAlignParam::InitY(double y)
{
  init_y = y;
}

double HPDAlignParam::AlignXParam()
{
  double alignXparam(0.0);
  if      (m_HPD <   98)               alignXparam = init_x + fit_x;
  else if (m_HPD >=  98 && m_HPD < 196)alignXparam = init_x - fit_x;
  else if (m_HPD >= 196 && m_HPD < 340)alignXparam = init_x - fit_x;
  else if (m_HPD >= 340 && m_HPD < 484)alignXparam = init_x - fit_x;
  return alignXparam;
}

double HPDAlignParam::AlignYParam()
{
  double alignYparam(0.0);
  if      (m_HPD <   98)               alignYparam = init_y + fit_y;
  else if (m_HPD >=  98 && m_HPD < 196)alignYparam = init_y - fit_y;
  else if (m_HPD >= 196 && m_HPD < 340)alignYparam = init_y - fit_y;
  else if (m_HPD >= 340 && m_HPD < 484)alignYparam = init_y - fit_y;
  return alignYparam;
}
