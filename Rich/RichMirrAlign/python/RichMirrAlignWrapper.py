#!/usr/bin/env python
import sys 
import os
from   subprocess import *
import getopt

#print sys.argv[0:]

varOpts = getopt.getopt( sys.argv[1:], 'h:p:u:v:o:w:r:', ['home=', 'pathToScripts=', 'userReleaseArea=', 'setupProjectPanoptesVersion=', 'setupProjectPanoptesOptions=', 'workDir=', 'rich='] ) 
#print varOpts

for varOptKey, varOptValue in varOpts[0]:
   if   varOptKey in ['-h','--home'                       ] :
      home                                                = varOptValue
   elif varOptKey in ['-p','--pathToScripts'              ] :
      pathToScripts                                       = varOptValue
   elif varOptKey in ['-u','--userReleaseArea'            ] :
      userReleaseArea                                     = varOptValue
   elif varOptKey in ['-v','--setupProjectPanoptesVersion'] :
      setupProjectPanoptesVersion                         = varOptValue
   elif varOptKey in ['-o','--setupProjectPanoptesOptions'] :
      setupProjectPanoptesOptions                         = varOptValue
   elif varOptKey in ['-w','--workDir'                    ] :
      workDir                                             = varOptValue
   elif varOptKey in ['-r','--rich'                       ] :
      rich                                                = varOptValue
      
p  = Popen("export   HOME="+home+";"
          +"source "+pathToScripts+"/LbLogin.sh;"
          +"export   User_release_area="+userReleaseArea+";"
          +"source  `which SetupProject.sh`  Panoptes "+setupProjectPanoptesVersion+" "+setupProjectPanoptesOptions+";"
          +"$RICHMIRRALIGNROOT/$CMTCONFIG/RichMirrAlign.exe "+workDir+"/Rich"+rich+"MirrAlign.conf > "+workDir+"/Rich"+rich+"MirrAlign.out",
            shell=True,
            executable="/bin/bash"
          )
sts  = os.waitpid(p.pid, 0)
