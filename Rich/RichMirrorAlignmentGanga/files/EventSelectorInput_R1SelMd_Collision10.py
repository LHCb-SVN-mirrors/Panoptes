from Gaudi.Configuration import *

EventSelector().Input   = [
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455916/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455917/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455918/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455919/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455920/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455921/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455922/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455923/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455924/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455925/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455926/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455927/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455928/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455929/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455930/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455931/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'",
    "   DATAFILE='LFN:/lhcb/user/t/thampson/2010_06/9455/9455932/AlignEventsRich1.dst' TYP='POOL_ROOTTREE' OPT='READ'"]
