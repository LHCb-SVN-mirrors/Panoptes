
from RichKernel.Configuration import *
from Configurables import ( GaudiSequencer
                            , Rich__Mon__HitMapsMonitor
                            )

class RichHitMapMonitorConf(RichConfigurableUser):

    ## Steering options
    __slots__ = {
        "Context"                     : "Offline"
        , "HitMapMonSequencer"        : GaudiSequencer("HitMapMonDefaultSequencer")
        ,"HitMapsMon_HitsPerHPDMaps"  : False # True to enable the production of the 2D histogram showing the number of hits per HPD.
        ,"HitMapsMon_LowResHitMaps"   : False # True to enable the production of the 2D histogram showing the hits per RICH panel, but with multiple pixels per bin.
        ,"HitMapsMon_HighResHitMaps"  : False # True to enable the production of the 2D histogram showing the hits per RICH panel with 1 HPD pixel per bin.
        ,"HitMapsMon_IndividualMaps"  : False # True to produce a 2D histogram with pixel per bin for each individual histogram seen in the data.
        ,"HitMapsMon_ScaleFactor"     : -1    # The number of pixels to place per bin in the low resolution hit map. Affects the x and y axis simultaneously. -1 to take algorithm default.
        ,"HitMapsMon_HotPixEnabled"   : True  # If True then on every monitored event we will update the hot pixel mask.
        ,"HitMapsMon_HPDCountEnabled" : True  # Look for Hot/Cold HPDs in every monitored event if True
        ,"OutputLevelHitmaps"         : 3
        , "HitMapsMon_MonFreq"        : 1
        , "HitMapMon_HotPixEventInterval" : 10000
        }
        

    def applyConf(self):

        # ===================================================================
        #     RichHitMapMonitor
        # ===================================================================
        sequence = self.getProp("HitMapMonSequencer")

        if sequence.name() == "HitMapMonDefaultSequencer" :
            print "=================>>>>> WARNING <<<<<<<<<<========================"
            print "No sequence defined for RichHitMapMonitorConf"
            print "HitMapMonSequencer %s" % sequence.name()

        from Configurables import Rich__Mon__HitMapsMonitor
        HitMapMon                      = Rich__Mon__HitMapsMonitor("HitMapMon")
        HitMapMon.OutputLevel           = self.getProp("OutputLevelHitmaps")
        HitMapMon.HotPixEventInterval   = self.getProp("HitMapMon_HotPixEventInterval")
        HitMapMon.HitsPerHPDMaps        = self.getProp("HitMapsMon_HitsPerHPDMaps")
        HitMapMon.LowResolutionHitMaps  = self.getProp("HitMapsMon_LowResHitMaps")
        HitMapMon.HighResolutionHitMaps = self.getProp("HitMapsMon_HighResHitMaps")
        HitMapMon.IndividualHpdMaps     = self.getProp("HitMapsMon_IndividualMaps")
        if -1 != self.getProp("HitMapsMon_ScaleFactor"):
            HitMapMon.LowResolutionScaleFactor = self.getProp("HitMapsMon_ScaleFactor")

        sequence.Members   += [HitMapMon]
        
        # This include file sets the Pixels to be masked
        # use NoHotPixels.opts to mask no pixels at all
        # #include  "$RICHMONITORINGSYSROOT/options/HotPixels11.opts"
        # #include  "$RICHMONITORINGSYSROOT/options/NoHotPixels.opts"

        # Configure the HitMapMonitor
        
        # Run the HitMapMonitor on every Nth event
        # -1 turns it off
        # The frequencies below are on top of this on
        HitMapMon.MonFreq = self.getProp("HitMapsMon_MonFreq")

        #
        # Hot pixel monitoring
        #
        HitMapMon.HotPixelEnabled = self.getProp("HitMapsMon_HotPixEnabled")
        HitMapMon.HPDCountEnabled = self.getProp("HitMapsMon_HPDCountEnabled")

        # Now configure the tools
        from Configurables import Rich__Mon__HotPixelReport
        HotPixelReport = Rich__Mon__HotPixelReport("HotPixelReport")
        from Configurables import Rich__Mon__HPDCountTool
        HPDCountTool   = Rich__Mon__HPDCountTool("HPDCountTool")
        
        # Pixels with an occupancy above CutOff are reported as Hot
        # For SiBias off this number has to be very high (.5 - .9) to make any sense
        # For SiBias on this number has to be very low (<.001 rather 1 in 10000)
        # Minimum threshold occupancy for hot pixel
        HotPixelReport.CutOff = 0.2
        #Minimum threshold #pixels for hot pixel column
        HotPixelReport.ColumnCutOff = 5
        
        
        # The number of events that a HPD can be noisey before an error message is reported.
        HPDCountTool.NoiseyTolerance = 1000
        # Number of events to use in the moving average.
        HPDCountTool.MovingAverageDataEvents = 50
        
