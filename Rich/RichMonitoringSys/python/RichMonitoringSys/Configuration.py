## @package RichMonitoringSys
#  High level Configuration tools for RICH Monitoring
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   21/08/2008

__version__ = "$Id: Configuration.py,v 1.59 2010-02-12 13:55:15 ryoung Exp $"
__author__  = "Chris Jones <Christopher.Rob.Jones@cern.ch>"

from RichKernel.Configuration            import *
from EventSnapshot                       import RichEventSnapshotConf
from Configurables                       import ( GaudiSequencer                         ,
                                                  Rich__Mon__SingleEventSnapshot         ,
                                                  Rich__Mon__HPDNHitMonitor              ,
                                                  Rich__Mon__DAQMonitor                  ,
                                                  Rich__Mon__CameraRunFolder             ,
                                                  Rich__Mon__HitMapsMonitor              ,
                                                  Rich__Mon__CalibMonitor                ,
                                                  Rich__Mon__HpdUkL1DisableTool          ,
                                                  Rich__Mon__RichHPDImageMovement        ,
                                                  Rich__HPDOccupancyTool                 )                

from RichRecSys.Configuration            import RichRecSysConf
from RichRecQC.Configuration             import RichRecQCConf
from RichHitMapMonitor                   import RichHitMapMonitorConf

## @class RichMonitoringSysConf
#  High level Configuration tools for RICH Data Quality monitoring
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   15/08/2008
class RichMonitoringSysConf(RichConfigurableUser):

    __used_configurables__ = [ RichRecSysConf        ,
                               RichRecQCConf         ,
                               RichHitMapMonitorConf ,
                               RichEventSnapshotConf ,
                               ]

    KnownModes      = ['Offline', 'Online', 'OnlineTest', 'Calibration']
    KnownPartitions = ['RICH', 'RICH1', 'RICH2', 'LHCb', 'FEST']

    ## Steering options
    __slots__ = {
        "Mode"                                    : "Offline"  # The mode (online, offline, calibration)
       ,"Partition"                               :    ""      # Partition to run in online
       ,"UsePhysicsEvents"                        :   True     # filter Physics events
       ,"EnableCameraRunFolder"                   :   True
       ,"RawDataDBCheck"                          :   False    # perform Raw data DB check
       ,"RawDataDecodingCheck"                    :   False    # perform Raw data decoding checks
       ,"RawDataSizeCheck"                        :   False    # perform Raw data size checks
       ,"HotPixelCheck"                           :   False    # perform a search for hot pixels
       ,"MissingHPDCheck"                         :   False
       ,"MonitorNhits"                            :   True
       ,"MonitorDAQ"                              :   True
       ,"MonitorHitMaps"                          :   True
       ,"MonitorImageMovements"                   :   True
       ,"MonitorDarkIFB"                          :   False 
       ,"MonitorIFB"                              :   True
       ,"MonitorReconstruction"                   :   True
       ,"MonitorCalibration"                      :   True      # Running on the Calibration Farm
       ,"MonitorTracks"                           :   True      # from RichRecQC
       ,"MonitorPixels"                           :   True      # from RichRecQC
       ,"MonitorPhoton"                           :   False     # from RichRecQC
       ,"MonitorPID"                              :   False     # from RichRecQC
       ,"MonitorTracklessRing"                    :   False     # from RichRecQC
       ,"MonitorMirrorAlign"                      :   False     # from RichRecQC
       ,"MonitorRaw"                              :   True      # from RichRecQC
       ,"RichRecCheckProcStatus"                  :   False     # from RichRecSys:
       ,"RichRecInitPixels"                       :   True      # from RichRecSys: 
       ,"RichRecInitTracks"                       :   False     # from RichRecSys: RichRecTrack objects (requires Tracking)
       ,"RichRecInitPhotons"                      :   False     # from RichRecSys: RichRecPixel objects (requires Tracking)
       ,"RichRecTracklessRingAlgs"                :   ['']      # from RichRecSys: Run the trackless ring finding algs
       ,"RichRecPidConfig"                        :   "None"
       ,"AssociateRingsToTracks"                  :   False 
       ,"SendEventSnapshots"                      :   True      # Send event snapshots when event errors are detected
       ,"SendNHitEventSnapshots"                  :   True      # Send event snapshots when event errors are detected
       ,"SendDaqEventSnapshots"                   :   True  
       ,"SnapshotUpdateInterval"                  :   300       # time after which to send another snapshot (in seconds)
       ,"SnapshotNBins"                           :   200
       ,"SnapshotRingType"                        : "Isolated" 
       ,"Context"                                 : "Offline"   # The mode (online, offline, calibration)
       ,"RawEventLocations"                       : [""]        # The RawEvent TES locations to use ( empty means defaults)
       ,"OutputLevelNhits"                        :      3
       ,"OutputLevelDAQMon"                       :      3
       ,"OutputLevelHitmaps"                      :      3
       ,"OutputLevelIFB"                          :      3
       ,"OutputLevelCalibration"                  :      3
       ,"OutputLevelTestPattern"                  :      3
       ,"OutputLevelCameraRunFolder"              :      3
       ,"OutputLevelDisable"                      :      3
       ,"OutputLevelHPDImageMovement"             :      3
       ,"DaqMon_MonitorBXID"                      :   True
       ,"DaqMon_UpdateTimerInterval"              :   900
       ,"DaqMon_MaxErrorMessages"                 :   500
       ,"DaqMon_PrintMessages"                    :   False
       ,"DaqMon_RemoveFaultyHPD"                  :   True # send disable commands to UKL1
       ,"DaqMon_Plot2DHisto"                      :   False
       ,"DaqMon_SendAlertDirect"                  :   False
       ,"DaqMon_SendAlertMessages"                :   True
       ,"TestPattern"                             : ""     # The pattern to search for CornerPixels, RowPixels and HalfDiagonalPixels are accepted.
       ,"HitMapsMon_HitsPerHPDMaps"               : False # True to enable the production of the 2D histogram showing the number of hits per HPD.
       ,"HitMapsMon_LowResHitMaps"                : False # True to enable the production of the 2D histogram showing the hits per RICH panel, but with multiple pixels per bin.
       ,"HitMapsMon_HighResHitMaps"               : False # True to enable the production of the 2D histogram showing the hits per RICH panel with 1 HPD pixel per bin.
       ,"HitMapsMon_IndividualMaps"               : False # True to produce a 2D histogram with pixel per bin for each individual histogram seen in the data.
       ,"HitMapsMon_ScaleFactor"                  : -1 # The number of pixels to place per bin in the low resolution hit map. Affects the x and y axis simultaneously. -1 to take algorithm default.
       ,"HitMapsMon_HotPixEnabled"                : True  # If True then on every monitored event we will update the hot pixel mask.
       ,"HitMapsMon_HPDCountEnabled"              : True  # Look for Hot/Cold HPDs in every monitored event if True        
       ,"NHitMon_RemoveFaultyHPD"                 :  False # send disable commands to UKL1
       ,"NHitMon_HitThreshold"                    : 50     # Threshold for too many hits before sending disable command to UKL1  
       ,"NHitMon_FillHistograms"                  :  True  # fill histograms (inclusive / trend) 
       ,"NHitMon_HistoNHitMin"                    :  0     # lower edge of histogram
       ,"NHitMon_HistoNHitMax"                    : 10000  # upper edge of histogram
       ,"NHitMon_ExtendedVerbose"                 :  False
       ,"NHitMon_PullThreshold"                   :   5    # alert threshold for pull of measured vs expected number of hits
       ,"NHitMon_TriggerTypes"                    : [0]    # which ODIN trigger types to monitor, e.g. 0-> Physics, etc.
       ,"NHitMon_MovingAverageFast"               :  20    # for pull calculation
       ,"NHitMon_MovingAverageSlow"               : 500
       ,"NHitMon_IndividualTriggerMonitors"       : True
       ,"NHitMon_UpdateTimerInterval"             : 900
       ,"HPDDisable"                              : True   # Switch sending disable commands to UKL1 (from HpdDisableTool) on or off
       ,"HPDDisable_2DHisto"                      : False  # 2D histogram showing which HPDs are being disabled
       ,"HPDDisable_HistoryTime"                  : 600    # seconds after which old disable requests are being rejected
       ,"HPDDisable_UpdateTimerInterval"          : 30
       ,"HPDDisable_DisableCheckInterval"         : 100
       ,"HPDDisable_DisableCheckHeartBeat"        : 2
       ,"HPDDisable_BufferSize"                   : 1000
       ,"HPDDisable_CameraSummaryInterval"        : 300
       ,"HPDDisable_AlwaysDisable"                : False
       ,"HPDDisable_FailureRateThreshold"         : 0.01
       ,"HPDImageMovement_UpdateFrequency"        : 1000   #
       ,"HPDImageMovement_MakeHistos"             : False  #
       ,"HPDImageMovement_HistoUpdateFrequency"   : 1000   #
       ,"HPDImageMovement_DisplaySmartIDWarnings" : False  #
       ,"HPDImageMovement_UseCutBasedMethod"      : False  #
       ,"HPDImageMovement_CutThreshold"           : 0.1
       ,"CalibMon_UpdateTimerInterval"            : 900
       ,"RandomTestHPDErrorRate"                  : -1.0
       ,"RandomTestHPDDisableRate"                : -1.0
       ,"RandomTestHPDErrorInterval"              : 5000
       ,"RandomTestHPDErrorBurstSize"             : 1100
       ,"RandomTestHPDDResetInterval"             : 10000
        }

    def _addOrRemoveMonitor(self,histo,flag):
        if flag : RichRecQCConf().addMonitor(histo)
        else    : RichRecQCConf().removeMonitor(histo)

    ## Apply the configuration
    def applyConf(self):

        # check configuration
        self.checkConfiguration()

        # add a sequence
        MonSeq                           = GaudiSequencer("RichMoni")
        MonSeq.MeasureTime               = True

        # only use physics events
        if self.getProp("UsePhysicsEvents"):
            from Configurables import HltRoutingBitsFilter
            physFilter       = HltRoutingBitsFilter ("PhysicsFilter", RequireMask = [0x0, 0x4, 0x0])
            MonSeq.Members  += [ physFilter ]
        
        self.configureSequences(MonSeq)

    def configureSequences(self,sequence):

        mode      = self.getProp("Mode")
        partition = self.getProp("Partition")
        #if mode == "Online":
        #    self.setProp("Context", "Online")
        
        
        # Set the context for the overall sequence
        sequence.Context = self.getProp("Context")

        # Want all sub-sequence to run in all cases
        sequence.IgnoreFilterPassed = True

        # pre-defined sequence on Calibration Farm
        # disable other monitors
        if mode == "Calibration":
            self.setProp("MonitorCalibration"      , True)
            self.setProp("MonitorNhits"            , False)
            self.setProp("MonitorDAQ"              , False)
            self.setProp("MonitorImageMovements"   , False)
            self.setProp("MonitorReconstruction"   , False)
            self.setProp("SendEventSnapshots"      , False)
            self.setProp("SendNHitEventSnapshots"  , False)
            self.setProp("SendDaqEventSnapshots"   , False)
            self.setProp("HPDDisable"              , False)
            self.setProp("MonitorPixels"           , False)  # from RichRecQC
            self.setProp("MonitorPhoton"           , False)  # from RichRecQC
            self.setProp("MonitorPID"              , False)  # from RichRecQC
            self.setProp("MonitorRaw"              , False)  # from RichRecQC
            self.setProp("MonitorTracklessRing"    , False)  # from RichRecQC
            self.setProp("MonitorMirrorAlign"      , False)  # from RichRecQC
            self.setProp("RichRecInitTracks"       , False)  # from RichRecSys
            self.setProp("RichRecInitPhotons"      , False)  # from RichRecSys
            self.setProp("RichRecTracklessRingAlgs", [''])   # from RichRecSys
            self.setProp("RichRecPidConfig"        , "None") # from RichRecSys

        # switch off disabling of HPDs
        if mode == "Offline" or partition.upper() == "FEST":
            self.setProp("NHitMon_RemoveFaultyHPD" , False)
            self.setProp("DaqMon_RemoveFaultyHPD"  , False)
            self.setProp("HPDDisable"              , False)

        # BxIDs are not correct in FEST - ignore
        if partition.upper() == "FEST":
            self.setProp("DaqMon_MonitorBXID", False)
            
        # low level monitors
        if (mode == "Offline") or (mode == "Online") or (mode == "OnlineTest") :
            
            daqseq             = GaudiSequencer("RichDAQ")
            daqseq.MeasureTime = True
            sequence.Members  += [daqseq]
            self.daqMonitors(daqseq)

            # Reconstruction monitors
            if self.getProp("MonitorReconstruction") :
                recseq             = GaudiSequencer("RichREC")
                recseq.MeasureTime = True
                sequence.Members  += [recseq]
                self.reconstructionMonitors(recseq)
                
        if (mode == "Offline") or (mode == "Calibration") :        
            # for the Calibration Farm
            if self.getProp("MonitorCalibration") :
                calibSeq              = GaudiSequencer("RichCalib")
                calibSeq.MeasureTime  = True
                sequence.Members     += [calibSeq]
                self.calibrationMonitors(calibSeq)

        if mode == "Offline":
            if ( self.getProp("RandomTestHPDErrorRate")   > 0 or
                 self.getProp("RandomTestHPDDisableRate") > 0 ) :
                from Configurables import Rich__Mon__RandomTestErrors
                randomErrs = Rich__Mon__RandomTestErrors("TestErrors")
                randomErrs.ErrorRate      = self.getProp("RandomTestHPDErrorRate")
                randomErrs.DisableRate    = self.getProp("RandomTestHPDDisableRate")
                randomErrs.ErrorInterval  = self.getProp("RandomTestHPDErrorInterval")
                randomErrs.ErrorBurstSize = self.getProp("RandomTestHPDErrorBurstSize")
                randomErrs.ResetInterval  = self.getProp("RandomTestHPDDResetInterval")
                sequence.Members += [randomErrs]            

        # Add CameraRunFolder to the sequence
        if self.getProp("EnableCameraRunFolder") :
            cameraRunFolder = Rich__Mon__CameraRunFolder("CameraRunFolder")
            cameraRunFolder.OutputLevel  = self.getProp("OutputLevelCameraRunFolder")
            sequence.Members += [cameraRunFolder]

        if (None!=self.getProp("TestPattern")) and (""!=self.getProp("TestPattern")):
            testPatternSeq = GaudiSequencer("TestPatternMon")
            sequence.Members += [testPatternSeq]
            self.testPatternMonitor(testPatternSeq)

        # configure HPD disabling      
        HPDDisableTool = Rich__Mon__HpdUkL1DisableTool("RichUKL1Disable")
        HPDDisableTool.SendDisableCommands   = self.getProp("HPDDisable")
        HPDDisableTool.Plot2DHisto           = self.getProp("HPDDisable_2DHisto")
        HPDDisableTool.HistoryTime           = self.getProp("HPDDisable_HistoryTime")
        HPDDisableTool.OutputLevel           = self.getProp("OutputLevelDisable")
        HPDDisableTool.UpdateTimerInterval   = self.getProp("HPDDisable_UpdateTimerInterval")
        HPDDisableTool.DisableCheckInterval  = self.getProp("HPDDisable_DisableCheckInterval")
        HPDDisableTool.BufferSize            = self.getProp("HPDDisable_BufferSize")
        HPDDisableTool.DisableCheckHeartBeat = self.getProp("HPDDisable_DisableCheckHeartBeat")
        HPDDisableTool.CameraSummaryInterval = self.getProp("HPDDisable_CameraSummaryInterval")
        HPDDisableTool.AlwaysDisable         = self.getProp("HPDDisable_AlwaysDisable")
        HPDDisableTool.FailureRateThreshold  = self.getProp("HPDDisable_FailureRateThreshold")
        
    ## High level monitors for reconstructed information
    def reconstructionMonitors(self,sequence):

        # Configure the RICH reconstruction tools
        RichRecSysConf().Context               = "Offline"
        RichRecSysConf().CheckProcStatus       = self.getProp("RichRecCheckProcStatus")
        RichRecSysConf().InitPixels            = self.getProp("RichRecInitPixels")          # RichRecPixel objects
        RichRecSysConf().InitTracks            = self.getProp("RichRecInitTracks")          # RichRecTrack objects (requires Tracking)
        RichRecSysConf().InitPhotons           = self.getProp("RichRecInitPhotons")         # RichRecPixel objects (requires Tracking)
        RichRecSysConf().TracklessRingAlgs     = self.getProp("RichRecTracklessRingAlgs")   # Run the trackless ring finding algs
        RichRecSysConf().PidConfig             = self.getProp("RichRecPidConfig")
        RichRecSeq                             = GaudiSequencer("RichRec")
        RichRecSeq.MeasureTime                 = True
        sequence.Members                      += [RichRecSeq]
        RichRecSysConf().RecoSequencer         = RichRecSeq
        RichRecSysConf().trackConfig().InputTracksLocation = "" # No tracks required
        #RichRecSysConf().Radiators = [ ]

        # The reconstruction monitoring                  
        RichRecQCConf().Context                           = "Offline"
        self._addOrRemoveMonitor("PixelMonitoring",self.getProp("MonitorPixels"))                  # Monitor reconstructed pixels
        self._addOrRemoveMonitor("TrackMonitoring",self.getProp("MonitorTracks"))                  # Monitor reconstructed tracks
        self._addOrRemoveMonitor("PhotonMonitoring",self.getProp("MonitorPhoton"))                 # Track based photon resolution
        self._addOrRemoveMonitor("PidMonitoring",self.getProp("MonitorPID"))                       # Disable PID monitoring
        self._addOrRemoveMonitor("TracklessRingAngles",self.getProp("MonitorTracklessRing"))       # Monitor the trackless ring finding
        self._addOrRemoveMonitor("TracklessRingPeakSearch",self.getProp("MonitorTracklessRing"))   # Monitor the trackless ring finding
        self._addOrRemoveMonitor("AlignmentMonitoring",self.getProp("MonitorMirrorAlign"))         # The mirror alignment monitoring
        self._addOrRemoveMonitor("HPDIFBMonitoring",self.getProp("MonitorIFB"))
        self._addOrRemoveMonitor("L1SizeMonitoring",self.getProp("MonitorRaw"))
        self._addOrRemoveMonitor("DBConsistencyCheck",self.getProp("MonitorRaw"))
        self._addOrRemoveMonitor("HotPixelFinder",self.getProp("MonitorRaw"))
        self._addOrRemoveMonitor("DataDecodingErrors",self.getProp("MonitorRaw"))

        RichRecMoniSeq                                    = GaudiSequencer("RichRecMoni")
        RichRecMoniSeq.MeasureTime                        = True
        sequence.Members                                 += [RichRecMoniSeq]
        RichRecQCConf().MoniSequencer                     = RichRecMoniSeq

        # Event snapshots
        if self.getProp("SendEventSnapshots") :
            self.setOtherProp(RichEventSnapshotConf(),"Context")
            RichEventSnapshotConf().UpdateInterval        = self.getProp("SnapshotUpdateInterval") 
            RichEventSnapshotConf().NBins                 = self.getProp("SnapshotNBins") 
            RichEventSnapshotConf().RingType              = self.getProp("SnapshotRingType") 
            RichEventSnapShotSeq                          = GaudiSequencer("RichEventSnapShot")
            RichEventSnapShotSeq.MeasureTime = True     
            sequence.Members                             += [RichEventSnapShotSeq]
            RichEventSnapshotConf().SnapshotSequencer     = RichEventSnapShotSeq
            RichEventSnapshotConf().RawEventLocations     = self.getProp("RawEventLocations")

    ## Config an instance of the NHit monitor
    def nHitMonitor(self,name,taeLocs,TriggerTypes):
        RichNHitMon                           = Rich__Mon__HPDNHitMonitor(name)
        RichNHitMon.RawEventLocations         = taeLocs
        RichNHitMon.FillHistos                = self.getProp("NHitMon_FillHistograms")
        RichNHitMon.ExtendedVerbose           = self.getProp("NHitMon_ExtendedVerbose")
        RichNHitMon.HistoNHitMin              = self.getProp("NHitMon_HistoNHitMin")
        RichNHitMon.HistoNHitMax              = self.getProp("NHitMon_HistoNHitMax")
        RichNHitMon.RemoveFaultyHpdUKL1       = self.getProp("NHitMon_RemoveFaultyHPD")
        RichNHitMon.HitThreshold              = self.getProp("NHitMon_HitThreshold")
        RichNHitMon.OutputLevel               = self.getProp("OutputLevelNhits")
        RichNHitMon.PullThreshold             = self.getProp("NHitMon_PullThreshold")
        RichNHitMon.MovingAverageEvents       = self.getProp("NHitMon_MovingAverageSlow")
        RichNHitMon.UpdateTimerInterval       = self.getProp("NHitMon_UpdateTimerInterval") 
        RichNHitMon.TriggerTypes              = TriggerTypes
        return RichNHitMon

    ## Low level DAQ monitors
    def daqMonitors(self,sequence):

        # mode (online or offline)
        mode = self.getProp("Mode")

        # Want all sub-sequence to run in all cases
        sequence.IgnoreFilterPassed = True

        taeLocs                 = self.getProp("RawEventLocations")
        triggerTypes            = self.getProp("NHitMon_TriggerTypes")

        if self.getProp("MonitorDAQ") :

            seq = GaudiSequencer("DAQSeq")
            seq.MeasureTime                 = True
            sequence.Members               += [seq]

            # Add Data and DB consistency check
            if self.getProp("RawDataDBCheck"):
                from Configurables import Rich__DAQ__DataDBCheck
                seq.Members += [Rich__DAQ__DataDBCheck("RichRawDataDBCheck")]

            # Add Data decoding checks
            if self.getProp("RawDataDecodingCheck"):
                from Configurables import Rich__DAQ__DataDecodingErrorMoni
                seq.Members += [Rich__DAQ__DataDecodingErrorMoni("RichRawDataDecodeCheck")]

            # Raw data size checks
            if self.getProp("RawDataSizeCheck"):
                from Configurables import Rich__DAQ__RawDataSize
                seq.Members += [Rich__DAQ__RawDataSize("RichRawDataSizeCheck")]

            # Missing HPDs
            if self.getProp("MissingHPDCheck"):
                from Configurables import Rich__Mon__MissingHPDMonitor
                seq.Members += [Rich__Mon__MissingHPDMonitor("RichMissingHPDCheck")]

            # Hot pixels
            if self.getProp("MissingHPDCheck"):
                from Configurables import Rich__Mon__HPDAnalysisAlg, Rich__HPDHotPixelFinder
                hotpix = Rich__Mon__HPDAnalysisAlg("RichHotPixels")
                hotpix.HPDAnalysisTools = ["RichHotPixelFinder"]
                hotpix.addTool( Rich__HPDHotPixelFinder, "RichHotPixelFinder" )
                hotpix.RichHotPixelFinder.NEventsForAverage = 2000
                hotpix.EventPrescale = 50
                seq.Members += [hotpix]
                                            
            # Online DAQ monitor - loop over configured TAE locations and make one sequence per location
            for taeLoc in taeLocs :
                taeLocName = taeLoc
                if taeLocName == "" : taeLocName = "Central"
                taeseq             = GaudiSequencer("DAQMonitor"+taeLocName+"EventSeq")
                taeseq.MeasureTime = True
                seq.Members       += [taeseq]
                daqMonName                      = "DAQMonitor"+taeLocName
                RichDAQMon                      =  Rich__Mon__DAQMonitor(daqMonName)
                RichDAQMon.UpdateTimerInterval  =  self.getProp("DaqMon_UpdateTimerInterval") 
                RichDAQMon.SendAlertMessages    =  self.getProp("DaqMon_SendAlertMessages") 
                RichDAQMon.SendAlertDirect      =  self.getProp("DaqMon_SendAlertDirect") 
                RichDAQMon.OdinMinGpsTime       =  1
                RichDAQMon.AlertLevelBxID       =  3
                RichDAQMon.CheckOdin            =  True
                RichDAQMon.RemoveFaultyHpdUKL1  =  self.getProp("DaqMon_RemoveFaultyHPD") 
                RichDAQMon.OutputLevel          =  self.getProp("OutputLevelDAQMon")
                RichDAQMon.MonitorBxID          =  self.getProp("DaqMon_MonitorBXID")
                RichDAQMon.MaxErrorMessages     =  self.getProp("DaqMon_MaxErrorMessages")
                RichDAQMon.Plot2DHisto          =  self.getProp("DaqMon_Plot2DHisto")
                RichDAQMon.PrintMessages        =  self.getProp("DaqMon_PrintMessages")
                RichDAQMon.TAELocation          =  taeLoc
                taeseq.Members                 +=  [RichDAQMon]
                if taeLoc == "" and self.getProp("SendDaqEventSnapshots") :
                    RichDAQMon.AddSnapshotCameraMsg = True
                    snap = Rich__Mon__SingleEventSnapshot(daqMonName+"EvtSnapshot")
                    snap.Message           = "DAQ Error in this event."
                    snap.RawEventLocations = [taeLoc]
                    snap.RingLocation      = "" # No rings to show
                    snap.MinHitRich        = 0
                    snap.MaxHitRich        = 999999
                    snap.UpdateInterval    = 60
                    snap.MinRings          = 0
                    taeseq.Members        += [snap]

        # monitor HPD image movement
        if self.getProp("MonitorImageMovements"):
            seq = GaudiSequencer("HPDImgMovSeq")
            seq.MeasureTime         = True
            seq.IgnoreFilterPassed  = True
            seq.Members            += self.imageMovementMonitors("RichHPDImgMove")
            sequence.Members       += [seq]

        # monitor number of hits
        if self.getProp("MonitorNhits") :

            snapShots               = self.getProp("SendNHitEventSnapshots")                 
            seq                     = GaudiSequencer("NHitSeq")
            seq.MeasureTime         = True
            seq.IgnoreFilterPassed  = True
            sequence.Members       += [seq]
            moniname                = "CombinedNHitMonitor"
            taeseq                  = GaudiSequencer(moniname+"Seq")
            taeseq.MeasureTime      = True
            seq.Members            += [taeseq]
            nhitMoni = self.nHitMonitor(moniname,taeLocs,triggerTypes)
            nhitMoni.UseCamera = True
            nhitMoni.EnableDIMCounters = True
            HPDOccupancyFast = Rich__HPDOccupancyTool("ToolSvc.HPDOccupancyFast")
            HPDOccupancySlow = Rich__HPDOccupancyTool("ToolSvc.HPDOccupancySlow")
            HPDOccupancyFast.PrintXML = False
            HPDOccupancyFast.UseRunningOccupancies = True
            HPDOccupancyFast.MemoryFactor = self.getProp("NHitMon_MovingAverageFast")
            HPDOccupancySlow.PrintXML = False
            HPDOccupancySlow.UseRunningOccupancies = True
            HPDOccupancySlow.MemoryFactor = self.getProp("NHitMon_MovingAverageSlow")  
            taeseq.Members         += [ nhitMoni ]
            if snapShots :
                nhitMoni.AddSnapshotCameraMsg = True
                snap                   = Rich__Mon__SingleEventSnapshot(moniname+"EvtSnapshot")
                snap.Message           = moniname + " event snapshot"
                snap.RingLocation      = "" # No rings to show
                snap.RawEventLocations = taeLocs
                snap.MinHitRich        = 0
                snap.MaxHitRich        = 999999
                snap.UpdateInterval    = 60
                snap.MinRings          = 0
                taeseq.Members        += [snap]
            if self.getProp("NHitMon_IndividualTriggerMonitors") :
                # Loop over configured Trigger Types
                for triggerType in triggerTypes :
                    # Loop over configured TAE locations and make one sequence per location
                    for taeLoc in taeLocs :
                        taeLocName = taeLoc
                        if taeLocName == "" : taeLocName = "Central"
                        moniname           = taeLocName+"Trigger"+str(triggerType)+"NHitMonitor"
                        taeseq             = GaudiSequencer(moniname+"Seq")
                        taeseq.MeasureTime = True
                        seq.Members       += [taeseq]
                        moni = self.nHitMonitor(moniname,[taeLoc],[triggerType])
                        moni.UseCamera = False
                        moni.EnableDIMCounters = False
                        moni.MinBadHPDClusSize = 999999 # Disable cluster finding
                        taeseq.Members    += [ moni ]

        if self.getProp("MonitorHitMaps") :
            RichHitMapMonitorConf().HitMapMonSequencer = sequence
            self.setOtherProps(RichHitMapMonitorConf(), ["HitMapsMon_HitsPerHPDMaps",
                                                         "HitMapsMon_LowResHitMaps",
                                                         "HitMapsMon_HighResHitMaps",
                                                         "HitMapsMon_IndividualMaps",
                                                         "OutputLevelHitmaps",
                                                         "HitMapsMon_HotPixEnabled",
                                                         "HitMapsMon_HPDCountEnabled"])

#        if self.getProp("MonitorIFB") :
#            from Configurables import Rich__Mon__HPDIonFeedbackMoni
#            RichIFBMon             = Rich__Mon__HPDIonFeedbackMoni("RichIFBMon")
#            RichIFBMon.OutputLevel = self.getProp("OutputLevelIFB")
#            sequence.Members      += [RichIFBMon]

    def imageMovementMonitors(self, name):
        #RichImageMovementMon = Rich__Mon__RichHPDImageMovement(name)
        #RichImageMovementMon.OutputLevel            = self.getProp("OutputLevelHPDImageMovement")
        #RichImageMovementMon.UpdateFrequency        = self.getProp("HPDImageMovement_UpdateFrequency")
        #RichImageMovementMon.MakeHistos             = self.getProp("HPDImageMovement_MakeHistos")
        #RichImageMovementMon.HistoUpdateFrequency   = self.getProp("HPDImageMovement_HistoUpdateFrequency")
        #RichImageMovementMon.DisplaySmartIDWarnings = self.getProp("HPDImageMovement_DisplaySmartIDWarnings")
        #RichImageMovementMon.UseCutBasedMethod      = self.getProp("HPDImageMovement_UseCutBasedMethod")
        #RichImageMovementMon.CutThreshold           = self.getProp("HPDImageMovement_CutThreshold")
        #return RichImageMovementMon
        from Configurables import Rich__HPDImage__OnlineSummary
        imageSummary = Rich__HPDImage__OnlineSummary("RichHPDImageSummary")
        imageSummary.Keep2DHistograms = False
        imageSummary.FinalHPDFit      = False
        return [imageSummary]
    
    ## on the Calibration Farm
    def calibrationMonitors(self, sequence):

        if self.getProp("MonitorHitMaps") :
            RichHitMapMonitorConf().HitMapMonSequencer = sequence
            self.setOtherProps(RichHitMapMonitorConf(), ["HitMapsMon_HitsPerHPDMaps",
                                                         "HitMapsMon_LowResHitMaps",
                                                         "HitMapsMon_HighResHitMaps",
                                                         "HitMapsMon_IndividualMaps",
                                                         "OutputLevelHitmaps",
                                                         "HitMapsMon_HotPixEnabled",
                                                         "HitMapsMon_HPDCountEnabled"])

        if self.getProp("MonitorCalibration") :
            RichCalibMon = Rich__Mon__CalibMonitor("RichCalibMon")
            RichCalibMon.UpdateTimerInterval    =  self.getProp("CalibMon_UpdateTimerInterval") 
            RichCalibMon.FillHistos             =  True
            RichCalibMon.RemoveFaultyHpdUKL1    =  False
            RichCalibMon.ExtendedVerbose        =  False
            RichCalibMon.OutputLevel            =  self.getProp("OutputLevelCalibration")
            sequence.Members += [RichCalibMon]


    def testPatternMonitor(self, sequence):
        # If we are given a test pattern then we will add the test pattern algorithm
        # to the calibration monitoring sequence, if it is an empty string or unrecognised
        # then we will not do anything.
        monTestPattern = False
        nickname = "TestPattern"
        patternType = self.getProp("TestPattern")
        pattern = None
        context = self.getProp("Mode")
        if "CornerPixels" == patternType:
            from Configurables import Rich__Mon__CornerPixelTestPattern
            pattern = Rich__Mon__CornerPixelTestPattern("ToolSvc."+context+"_"+nickname)
            monTestPattern = True
            if self.getProp("MonitorDarkIFB") :
                self.ionfeedbackMonitor(sequence)  # Create (dark) IFB Monitor with CornerPixel test pattern
        elif "RowPixels" == patternType:
            from Configurables import Rich__Mon__RowPixelTestPattern
            pattern = Rich__Mon__RowPixelTestPattern("ToolSvc."+context+"_"+nickname)
            monTestPattern = True
        elif "HalfDiagonalPixels" == patternType:
            from Configurables import Rich__Mon__HalfDiagonalPixelTestPattern
            pattern = Rich__Mon__HalfDiagonalPixelTestPattern("ToolSvc."+context+"_"+nickname)
            monTestPattern = True
        # Just ignore all other cases and don't add a test pattern.
        elif "" == patternType:
            log.warning("Empty string has been specified for the test pattern. Test pattern will not be analysed.")
        else:
            log.warning("Test pattern '"+patternType+"' not recognised. Test pattern will not be analysed.")
            
        if monTestPattern:
            self.toolRegistry().Tools += [pattern.getType()+"/"+nickname]
            from Configurables import Rich__Mon__TestPatternMonitor
            testPatternMon              = Rich__Mon__TestPatternMonitor("RichTestPatternMon")
            testPatternMon.OutputLevel  = self.getProp("OutputLevelTestPattern")
            testPatternMon.UpdateTimerInterval = self.getProp("CalibMon_UpdateTimerInterval") 
            sequence.Members += [testPatternMon]

    def ionfeedbackMonitor(self, sequence):
        from Configurables import Rich__Mon__HPDIonFeedbackMoni
        RichIFBMon                       = Rich__Mon__HPDIonFeedbackMoni("RichHPDIFBMoni")
        RichIFBMon.OutputLevel           = self.getProp("OutputLevelIFB")
        RichIFBMon.IsDark                = True
        RichIFBMon.EventSize             = 3000000
        RichIFBMon.IonFeedbackALICE      = 5
        RichIFBMon.IonFeedbackLHCB       = 5
        RichIFBMon.WantIFB               = True
        RichIFBMon.WantHitmaps           = False      
        RichIFBMon.WantQuickHitmap       = False
        sequence.Members      += [RichIFBMon]
        
    # =================================================================================
    def checkConfiguration(self):
        mode      = self.getProp("Mode")
        partition = self.getProp("Partition")
        
        if mode not in self.KnownModes:
            raise RuntimeError("ERROR : Unknown Panoptes Mode '%s'" % mode)
        
        if mode == 'Online' or mode == 'Calibration':
            if partition not in self.KnownPartitions:
                raise RuntimeError("ERROR : Unkonwn Panoptes Partition '%s'" % partition)
                            
