
from RichKernel.Configuration import *
from Configurables import ( GaudiSequencer,
                            Rich__Rec__EventSelectionAlg,
                            Rich__Mon__SingleEventSnapshot )
            
## @class RichEventSnapshotConf
#  Event 'snapshot' configuration
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   15/08/2008
class RichEventSnapshotConf( RichConfigurableUser ):

    __used_configurables__ = [ GaudiSequencer ]

  ## Steering options
    __slots__ = {
        "Context"                : "Offline"
        ,"SnapshotSequencer"     : GaudiSequencer("RichEventSnapshotsDefaultSequencer")
        ,"UpdateInterval"        :  60
        ,"NBins"                 : 200
        ,"RingType"              : "Isolated" # "Rec/Rich/Markov/RingsIsolated"
        ,"MinRingPixels"         :   3
        ,"MinHitRich"            :   1
        ,"MaxHitRich"            : 500
        ,"EventCuts"             : { "Rings"           : [0,999999],
                                     "Pixels"          : [0,999999],
                                     "Tracks"          : [0,999999],
                                     "HPDsWithHits"    : [0,999999],
                                     "HPDHits"         : [0,999999]
                                     }
        ,"RawEventLocations"     : [ "" ] # The RawEvent TES locations to use ( empty means defaults)
        ,"MaxSnapshots"          : 5000
        }

    ## Configure an event snapshot sequence 
    def applyConf(self):

        sequence = self.getProp("SnapshotSequencer")
        
        from Configurables import RichRecSysConf

        ringtypes = RichRecSysConf().TracklessRingAlgs
        context = self.getProp("Context") 
        
        for type in ringtypes :
            
            # Add an event selection alg, to select events with isolated rings
            snapSeq              = GaudiSequencer("RichEventSnapshots"+type)
            snapSeq.Context      = self.getProp("Context") 
            snapSeq.MeasureTime  = True
            sequence.Members    += [snapSeq]
            
            ringloc = "Rec/Rich/"+type+"/"+context+"/Rings"+self.getProp("RingType")
            
            selAlg = Rich__Rec__EventSelectionAlg("Select"+type+"RingEvents")
            for name,cut in self.getProp("EventCuts").iteritems() :
                selAlg.setProp("Min"+name,cut[0])
                selAlg.setProp("Max"+name,cut[1])
                selAlg.RingLocation    = ringloc
                snapSeq.Members       += [selAlg]
                
            # Add snapshot alg
            snap                  = Rich__Mon__SingleEventSnapshot(type+"RingEventSnapShot")
            snap.UpdateInterval   = self.getProp("UpdateInterval")
            snap.NBins            = self.getProp("NBins")
            snap.MinRingPixels    = self.getProp("MinRingPixels")
            snap.MinHitRich       = self.getProp("MinHitRich")
            snap.MaxHitRich       = self.getProp("MaxHitRich")
            snap.Message          = "Potentially interesting event selected using '"+ringloc+"' Rings"
            snap.RingLocation     = ringloc
            snap.MaxSnapshots     = self.getProp("MaxSnapshots")
            snapSeq.Members      += [snap]
            
            # Set TAE locations
            snap.RawEventLocations = self.getProp("RawEventLocations")
