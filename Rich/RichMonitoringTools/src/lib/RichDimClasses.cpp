
// local
#include "RichMonitoringTools/RichDimClasses.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichDimClasses
//
// 2015-06-24 : Chris Jones
//-----------------------------------------------------------------------------

std::string Rich::Mon::get_utgid()
{
  const std::string& utgid   = RTL::processName();
  std::string  utgidNew      = utgid;

  // now format the UTGID such that the name of the monitoring node is
  // replaced by x (such that the name published to DIM is always the
  // same and can be "guessed" without querying the DNS server)
  // Only if there are 3 "_" in the UTGID to be consistent with the MonitorSvc

  std::size_t pos1 = std::string::npos; // position of first underscore
  std::size_t pos2 = std::string::npos;
  std::size_t pos3 = std::string::npos;
  std::size_t pos4 = std::string::npos; // should stay at npos
  pos1 = utgidNew.find("_");
  if (pos1 != std::string::npos)
  {
    pos2 = utgidNew.find("_", pos1+1);
    if (pos2 != std::string::npos)
    {
      pos3 = utgidNew.find("_", pos2+1);
      if (pos3 != std::string::npos)
      {
        pos4 = utgidNew.find("_", pos3+1);
      } // if pos3
    } // if pos2
  } // if pos1
  if ( pos1 != std::string::npos &&
       pos2 != std::string::npos &&
       pos3 != std::string::npos &&
       pos4 == std::string::npos )
  {
    utgidNew = utgidNew.replace( pos1+1, pos2-pos1-1, "x" );
  }

  return utgidNew;
}

//=============================================================================
