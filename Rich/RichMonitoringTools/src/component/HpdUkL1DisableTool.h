// $Id: HpdUkL1DisableTool.h,v 1.17 2009-10-08 15:17:57 ukerzel Exp $
#ifndef HPDUKL1DISABLETOOL_H
#define HPDUKL1DISABLETOOL_H 1

#include <map>
#include <string>
#include <time.h>

// boost
#include "boost/regex.hpp"
#include "boost/circular_buffer.hpp"
#include "boost/lexical_cast.hpp"

// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// Workaround for https://its.cern.ch/jira/browse/GAUDI-1168
#include "GaudiKernel/Algorithm.h"

// DIM
#include "dis.hxx"
#include "dic.hxx"
#include "RTL/rtl.h"

// CAMERA
#include "Camera/ICameraTool.h"

// histo
#include "AIDA/IHistogram2D.h"

// RunChange incident.
#include "DetDesc/RunChangeIncident.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// RichKernel
#include "RichKernel/RichHistoToolBase.h"
#include "RichKernel/RichDAQDefinitions.h"
#include "RichKernel/RichSmartIDCnv.h"

// Local
#include "RichMonitoringTools/IHpdUkL1DisableTool.h"
#include "RichMonitoringTools/RichDimClasses.h"

// Histogramming
#include "AIDA/IHistogram2D.h"
#include "TH2D.h"
#include "GaudiUtils/Aida2ROOT.h"

namespace Rich
{
  namespace Mon
  {

    /// Instance counter for HpdUkL1DisableTool
    static int s_HpdUkL1DisableTool_InstanceCounter; 

    /** @class HpdUkL1DisableTool HpdUkL1DisableTool.h
     *
     *  Online tool to disable HPDs
     *
     *  @author Ulrich Kerzel
     *  @date   2008-06-30
     */

    class HpdUkL1DisableTool : public Rich::HistoToolBase,
                               public CheckLHCState,
                               virtual public IHpdUkL1DisableTool,
                               virtual public IIncidentListener
    {

    public:

      /// Standard constructor
      HpdUkL1DisableTool( const std::string& type,
                          const std::string& name,
                          const IInterface* parent);

      virtual ~HpdUkL1DisableTool( ); ///< Destructor

      StatusCode initialize ();
      StatusCode finalize   ();

    private:

      /// Class to monitor the L1 DIM strings
      class L1DimString : public DimInfo
      {
      private:
        std::string m_hpdList;
        bool m_newState;
      private:
        void infoHandler() { m_hpdList = std::string(getString()); m_newState = true; }
      public :
        explicit L1DimString( const std::string & name )
          : DimInfo(name.c_str(),(char*)""), m_hpdList(""), m_newState(false) { }
      public:
        bool newState() const { return m_newState; }
        const std::string& hpdList() const { return m_hpdList; }
        void setRead( const bool read = true ) { m_newState = !read; }
      };

      /// Wrap two DIM strings (RICH1 and RICH2) into one
      class L1DimStringWrap
      {
      public:
        L1DimStringWrap( const std::string & name )
          : m_name  ( name               ),
            m_dimR1 ( "RICH1_L1/" + name ),
            m_dimR2 ( "RICH2_L1/" + name ),
            m_cache ( ""                 ) { }
      public:
        const std::string & name() const { return m_name; }
        void setRead( const bool read = true )
        {
          m_dimR1.setRead(read);
          m_dimR2.setRead(read);
        }
        const std::string hpdList() const
        {
          return ( m_dimR1.hpdList().empty() ? m_dimR2.hpdList() :
                   m_dimR2.hpdList().empty() ? m_dimR1.hpdList() :
                   m_dimR1.hpdList() + " " + m_dimR2.hpdList()   );
        }
        bool newState()
        {
          bool updated = false;
          if ( m_dimR1.newState() || m_dimR2.newState() )
          {
            const std::string tmp = hpdList();
            if ( tmp != m_cache ) { updated = true; m_cache = tmp; }
            else                  { setRead();                     }
          }
          return updated;
        }
        void reset()
        {
          m_cache = "";
          m_dimR1.setRead(false);
          m_dimR2.setRead(false);
        }
      private:
        const std::string m_name;
        L1DimString m_dimR1, m_dimR2;
        mutable std::string m_cache;
      };

    private:

      typedef Rich::Mon::IHpdUkL1DisableTool::HPDData HPDData;

    public:

      /// send disable request
      void DisableHPD   (const LHCb::RichSmartID &smartID,
                         const std::string & reason = "");
      /// send disable request
      void DisableHPD   (const HPDData& data,
                         const std::string & reason = "");

      /// report HPD as having some issue but not necessarily so severe you'd want to disable it
      void ReportHPD    (const LHCb::RichSmartID &smartID,
                         const std::string & reason = "");
      /// report HPD as having some issue but not necessarily so severe you'd want to disable it
      void ReportHPD    (const HPDData& data,
                         const std::string & reason = "");

      /// Reset all information
      void ResetAll     ();

    private:

      typedef std::map<HPDData,long int> HPDCountMap;
      typedef std::map<std::string,long int> CountReasonMap;
      class CountReasonMapAndTime
      {
      public:
        CountReasonMapAndTime() : timeLast(0) { }
      public:
        CountReasonMap cMap;
        time_t timeLast;
      };
      typedef std::map<HPDData,CountReasonMapAndTime> HPDCountReasonMap;
      typedef boost::circular_buffer<unsigned long> Buffer;
      typedef std::pair< Buffer, CountReasonMap > BufferAndReasons;
      typedef std::map< HPDData, BufferAndReasons > HPDBuffer;

    private:

      void                   PrintReasons        (const CountReasonMap & reasons);
      void                   ResetHistograms     ();
      void                   RefreshReports      (const bool camPrint = true);
      void                   RemoveOldReports    ();
      void                   RemoveOldDisables   ();
      virtual void           handle              (const Incident &incident);
      void                   DisableAndPublish   ();
      std::string            GetDisableString    (const HPDData & data);
      std::string            GetDisableString    (const LHCb::RichSmartID &smartID);
      StatusCode             Book2DHisto         (const Rich::DetectorType rich,
                                                  const Rich::Side panel);

      void fillDisabledHisto( const LHCb::RichSmartID& smartID,
                              const double errorWeight );

      const LHCb::RichSmartID smartId( const HPDData& data );
      const HPDData hpdData( const LHCb::RichSmartID smartID );

      /// Get a vector of HPDData objects from a DIM string
      const HPDData::Vector hpdDataFromDIM( const std::string & dimS ) const;

      void processUKL1DimString( L1DimStringWrap * dim );

      /// Is the HPD disabling active ?
      inline bool isHpdDisablingActive() const
      {
        return ( m_AlwaysDisable || 
                 lhcbState() == LHCbState::InPhysics || 
                 lhcState() == "PHYSICS"         ||
                 ( lhcStateObj()               && 
                   lhcState() == "PHYS_ADJUST" &&
                   lhcStateObj()->timeSinceLastUpdate() > 5*60 ) 
                 );
      }

      /// (Re)load the UKL1 Dim strings
      void LoadUKL1DIMStrings();

      /// Delete the UKL1 Dim strings
      void DeleteUKL1DIMStrings();

      /// Reset the Dim strings
      void ResetDIMStrings();

      /// Remove a given HPD from the disabled list
      void removeFromDimString( const HPDData& data );

      /// Returns if an HPD has zero disabling weight in the L1 PVSS projects
      inline bool hpdHasZeroWeight( const HPDData& data ) const
      {
        return ( std::find( m_zeroWeightHPDsV.begin(),
                            m_zeroWeightHPDsV.end(),
                            data ) != m_zeroWeightHPDsV.end() );
      }

      /// Checks if a given HPD is already disabled
      bool isDisabled( const LHCb::RichSmartID& hpdID ) const;

      /// Checks if an HPD has been software disabled
      inline bool isSoftwareDisabled( const HPDData& data ) const
      {
        return ( std::find( m_softwareDisabledHPDs.begin(),
                            m_softwareDisabledHPDs.end(),
                            data ) != m_softwareDisabledHPDs.end() );
      }

    private:

      double                      m_failureRateThreshold; ///< Fraction of times and HPD is allowed to fail.
      int                         m_bufferSize;           ///< Size of buffer
      HPDCountReasonMap           m_HpdErrorReportMap;    ///< HPDs reported with errors (via ReportHPD function)
      HPDCountReasonMap           m_DisabledHpdMap;       ///< holds HPDs already disabled
      HPDBuffer                   m_disableCandidates;    ///< Internal list of candidate HPDs for disabling

      std::string                 m_Name;                 ///< algorithm name (including partition)

      DeRichSystem               *m_RichSys;              ///< Pointer to RICH system detector element

      ICameraTool                *m_CamTool;              ///< CAMERA error reporting tool

      std::string                 m_DisabledHpdString;    ///< The string containing the disabled HPDs, to pass to DIM
      DimService                 *m_HPDDisableDimService; ///< The DIM service for disabled HPDs

      bool                        m_Plot2DHisto;           ///< Turn on/off the filling of the 2D plot
      bool                        m_SendDisableCommands;   ///< Switch off sending the disable commands
      int                         m_HistoryTime;           ///< How long to keep record of a given incident (in seconds)
      bool                        m_AlwaysDisable;         ///< Always enable the HPD disabling

      bool                        m_ClearListAtNewRun;     ///< Clear list of disabled HPDs at the beginning of a new run

      time_t                      m_TimeLastUpdate;        ///< The time of the last camera update
      time_t                      m_TimeLastHPDDisable;    ///< The time of the last search for HPDs to disable
      time_t                      m_TimeLastCamSummary;    ///< The time of the last summaries sent to camera
      int                         m_UpdateTimerInterval;   ///< Interval at which to update histograms
      int                         m_CamSummaryInterval;    ///< Interval to send summaries to camera
      int                         m_DisableCheckInterval;  ///< Number of events between checks for new HPDs to disable
      int                         m_DisableCheckHeartBeat; ///< Max time between HPD Disable searches

      /// Number of monitored events
      unsigned long long m_nMonitoredEvents;

      /// L1 GT inhibit
      L1DimStringWrap * m_l1FELinkDown;

      /// L1 disabled by firmware or monitoring
      L1DimStringWrap * m_l1FESquashed;

      /// L1 permanently disabled
      L1DimStringWrap * m_l1FEDisabledByRecipe;

      /// L1 'zero weight' HPDs
      L1DimStringWrap * m_l1ZeroWeight;

      /// Cache pointers to histograms
      AIDA::IHistogram2D * m_hpdHistos[Rich::NRiches][Rich::NPDPanelsPerRICH];

      /// Set of disabled HPDs (as defined by UKL1 DIM strings)
      HPDData::Set m_disabledHPDsV;

      /// Vector of HPDs with zero weights in the L1, so we should not send any warnings for
      HPDData::Vector m_zeroWeightHPDsV;

      /// List of software disabled HPDs
      HPDData::Set m_softwareDisabledHPDs;

    }; // class

    //=============================================================================

    inline const HpdUkL1DisableTool::HPDData
    HpdUkL1DisableTool::hpdData( const LHCb::RichSmartID smartID )
    {
      const Rich::DAQ::Level1Input in = m_RichSys->level1InputNum(smartID);
      return HPDData( smartID.rich(),
                      m_RichSys->level1LogicalID(m_RichSys->level1HardwareID(smartID)),
                      in.ingressID(),
                      in.l1InputWithinIngress() );
    }

    //=============================================================================

    inline std::string HpdUkL1DisableTool::GetDisableString(const HPDData & data)
    {
      std::ostringstream hpdIDString;
      hpdIDString << " ("
                  << (int)data.rich() << ","
                  << data.l1LogID().data() << ","
                  << data.l1IngressInput().data() << ","
                  << data.l1Ingress().data() << ")";
      return hpdIDString.str();
    }

    //=============================================================================

    inline std::string
    HpdUkL1DisableTool::GetDisableString( const LHCb::RichSmartID &smartID )
    {
      return GetDisableString( hpdData(smartID) );
    }

  } //namespace Mon
} // namespace Rich

#endif // HPDUKL1DISABLETOOL_H
