
// local
#include "HpdUkL1DisableTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HpdUkL1DisableTool
//
// 2008-06-30 : Ulrich Kerzelh
//-----------------------------------------------------------------------------

using namespace Rich::Mon;

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( HpdUkL1DisableTool )

  namespace
  {
    static const int HPDWarningLevel = 1;
    static const int HPDErrorLevel   = 2;
  }

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HpdUkL1DisableTool::HpdUkL1DisableTool( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : Rich::HistoToolBase    ( type, name, parent ),
    m_Name                 ( name  ),
    m_RichSys              ( NULL  ),
    m_CamTool              ( NULL  ),
    m_HPDDisableDimService ( NULL  ),
    m_TimeLastUpdate       (   0   ),
    m_TimeLastHPDDisable   (   0   ),
    m_TimeLastCamSummary   (   0   ),
    m_nMonitoredEvents     (   0   ),
    m_l1FELinkDown         ( NULL  ),
    m_l1FESquashed         ( NULL  ),
    m_l1FEDisabledByRecipe ( NULL  ),
    m_l1ZeroWeight         ( NULL  )
{
  // interface
  declareInterface<Rich::Mon::IHpdUkL1DisableTool>(this);

  // Job Options
  declareProperty( "FailureRateThreshold" , m_failureRateThreshold  = 0.01  );
  declareProperty( "Plot2DHisto"          , m_Plot2DHisto           = false );
  declareProperty( "SendDisableCommands"  , m_SendDisableCommands   = true  );
  declareProperty( "HistoryTime"          , m_HistoryTime           = 5*60  ); // 5 mins
  declareProperty( "ClearListAtNewRun"    , m_ClearListAtNewRun     = true  );
  declareProperty( "UpdateTimerInterval"  , m_UpdateTimerInterval   = 30    ); //30 secs
  declareProperty( "BufferSize"           , m_bufferSize            = 1000  );
  declareProperty( "DisableCheckInterval" , m_DisableCheckInterval  = 100   );
  declareProperty( "DisableCheckHeartBeat", m_DisableCheckHeartBeat = 2     ); // 2 secs
  declareProperty( "CameraSummaryInterval", m_CamSummaryInterval    = 5*60  );
  declareProperty( "AlwaysDisable"        , m_AlwaysDisable         = false );

  // Count instances (only one allowed)
  ++s_HpdUkL1DisableTool_InstanceCounter;
}

//=============================================================================
// Destructor
//=============================================================================
HpdUkL1DisableTool::~HpdUkL1DisableTool() {}

//=============================================================================
StatusCode HpdUkL1DisableTool::initialize()
{
  StatusCode sc = Rich::HistoToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  //
  // check instances
  //

  if ( msgLevel(MSG::DEBUG) )
    debug() << "There are now " << s_HpdUkL1DisableTool_InstanceCounter
            << " instances of Rich::Mon::HpdUkL1DisableTool" << endmsg;
  if ( s_HpdUkL1DisableTool_InstanceCounter > 1 )
  {
    return Error("Only one instance of Rich::Mon::HpdUkL1DisableTool allowed");
  }

  //
  // Partition
  //

  const char * partitionName = getenv("PARTITION");
  m_Name = name();
  if ( partitionName )
  {
    info() << "Running in partition " << std::string(partitionName) << endmsg;
    m_Name += std::string(partitionName);
  }

  //
  // DIM Services
  //

  // Get DIM node
  const char * dimNode = getenv("DIM_DNS_NODE");

  // L1 Dim Strings
  LoadUKL1DIMStrings();

  // Initialise DIM state listeners
  initialiseDIMStates();

  // HPD disabling
  if ( m_SendDisableCommands )
  {
    if ( dimNode )
    {
      const std::string serviceName = get_utgid() + "/RICHHPDDisableUKL1";
      info() << "Publish to DIM " << serviceName << endmsg;
      m_HPDDisableDimService = new DimService( serviceName.c_str(), (char*)"" );
    }
    else
    {
      Warning( "DIM_DNS_NODE undefined - No HPD disabling",
               StatusCode::SUCCESS ).ignore();
    }
  }

  //
  // subscribe to incidents
  //

  incSvc()->addListener( this, IncidentType::BeginEvent );
  incSvc()->addListener( this, IncidentType::EndEvent   );
  incSvc()->addListener( this, IncidentType::RunChange  );

  //
  // get tools
  //

  // RichDet
  m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  // CAMERA error reporting tool
  m_CamTool = tool<ICameraTool>("CameraTool");
  if (msgLevel(MSG::VERBOSE)) verbose() << "send message to CAMERA" << endmsg;
  m_CamTool->Append        ("TEXT",m_Name.c_str());
  m_CamTool->SendAndClearTS(ICameraTool::INFO,m_Name,"Initialized");

  // print configuration
  info() << "FailureRateThreshold  " <<  m_failureRateThreshold  << endmsg;
  info() << "Plot2DHisto           " <<  m_Plot2DHisto           << endmsg;
  info() << "SendDisableCommands   " <<  m_SendDisableCommands   << endmsg;
  info() << "HistoryTime           " <<  m_HistoryTime           << endmsg;
  info() << "ClearListAtNewRun     " <<  m_ClearListAtNewRun     << endmsg;
  info() << "BufferSize            " <<  m_bufferSize            << endmsg;
  info() << "DisableCheckInterval  " <<  m_DisableCheckInterval  << endmsg;
  info() << "DisableCheckHeartBeat " <<  m_DisableCheckHeartBeat << endmsg;
  info() << "CamSummaryInterval    " <<  m_CamSummaryInterval    << endmsg;
  info() << "AlwaysDisable         " <<  m_AlwaysDisable         << endmsg;

  // book histograms
  sc = sc && Book2DHisto ( Rich::Rich1, Rich::top    );
  sc = sc && Book2DHisto ( Rich::Rich1, Rich::bottom );
  sc = sc && Book2DHisto ( Rich::Rich2, Rich::left   );
  sc = sc && Book2DHisto ( Rich::Rich2, Rich::right  );

  // get Time of startup
  m_TimeLastUpdate = m_TimeLastHPDDisable = m_TimeLastCamSummary = time(NULL);

  // reset everything
  ResetAll();

  return sc;
}

//=============================================================================

void HpdUkL1DisableTool::ResetDIMStrings()
{
  //if ( m_lhcbState     )        { m_lhcbState->reset();            }
  if ( m_l1FELinkDown )         { m_l1FELinkDown->reset();         }
  if ( m_l1FESquashed )         { m_l1FESquashed->reset();         }
  if ( m_l1FEDisabledByRecipe ) { m_l1FEDisabledByRecipe->reset(); }
  if ( m_l1ZeroWeight )         { m_l1ZeroWeight->reset();         }
}

//=============================================================================

void HpdUkL1DisableTool::LoadUKL1DIMStrings()
{
  // delete current objects, if any
  DeleteUKL1DIMStrings();

  // Get DIM node
  const char * dimNode = getenv("DIM_DNS_NODE");

  // load new ones
  m_l1FELinkDown         = ( dimNode ? new L1DimStringWrap("FELinkDown")         : NULL );
  m_l1FESquashed         = ( dimNode ? new L1DimStringWrap("FESquashed")         : NULL );
  m_l1FEDisabledByRecipe = ( dimNode ? new L1DimStringWrap("FEDisabledByRecipe") : NULL );
  m_l1ZeroWeight         = ( dimNode ? new L1DimStringWrap("FEZeroWeight")       : NULL );

  // force a first reading of dim strings
  ResetDIMStrings();
}

//=============================================================================

void HpdUkL1DisableTool::DeleteUKL1DIMStrings()
{
  delete m_l1FELinkDown;         m_l1FELinkDown         = NULL;
  delete m_l1FESquashed;         m_l1FESquashed         = NULL;
  delete m_l1FEDisabledByRecipe; m_l1FEDisabledByRecipe = NULL;
  delete m_l1ZeroWeight;         m_l1ZeroWeight         = NULL;
}

//=============================================================================

StatusCode HpdUkL1DisableTool::finalize()
{
  // One final report to camera
  RefreshReports();

  // Clear up lists
  ResetAll();

  // delete the DIM server, if active
  delete m_HPDDisableDimService; 
  m_HPDDisableDimService = NULL;

  // Delete the HPD DIM string objects
  DeleteUKL1DIMStrings();

  // clean up state listeners
  finaliseDIMStates();

  // finish and return
  return HistoToolBase::finalize();
}

//=============================================================================

void HpdUkL1DisableTool::DisableHPD ( const LHCb::RichSmartID &smartID,
                                      const std::string & reason )
{
  if ( smartID.isValid() ) { DisableHPD( hpdData(smartID), reason ); }
}

//=============================================================================

void HpdUkL1DisableTool::DisableHPD ( const HPDData& data,
                                      const std::string& reason )
{
  if ( msgLevel(MSG::VERBOSE) )
  {
    const time_t currentTime = time(NULL);
    struct tm * timeinfo = localtime ( &currentTime );
    verbose() << data << " is disabled at time "
              << timeinfo->tm_hour-1 << " hours "
              << timeinfo->tm_min   << " mins "
              << timeinfo->tm_sec   <<  " secs | '"
              << reason << "'"
              << endmsg;
  }

  // Get map entry for this HPD
  BufferAndReasons & b = m_disableCandidates[data];
  
  // Allocate memory for the buffer of the indices of the events
  // for a HPD candidate for disabling.
  if ( ! b.first.capacity() ) b.first.set_capacity( m_bufferSize );
  
  // Keep the index of the event that has generated the disabling request.
  b.first.push_back( m_nMonitoredEvents );

  // save the reason
  ++(b.second[reason]);
}

//=============================================================================

void HpdUkL1DisableTool::ReportHPD ( const LHCb::RichSmartID & smartID,
                                     const std::string & reason )
{
  if ( smartID.isValid() ) { ReportHPD( hpdData(smartID), reason ); }
}

//=============================================================================

void HpdUkL1DisableTool::ReportHPD ( const HPDData & data,
                                     const std::string & reason )
{
  // Ignore reports for disabled HPDs
  if ( m_DisabledHpdMap.find(data) == m_DisabledHpdMap.end() )
  {

    // get current time
    const time_t currentTime = time(NULL);

    if ( msgLevel(MSG::VERBOSE) )
    {
      struct tm * timeinfo = localtime ( &currentTime );
      verbose() << data << " is reported at time "
                << timeinfo->tm_hour-1 << " hours "
                << timeinfo->tm_min   << " mins "
                << timeinfo->tm_sec   <<  " secs | '"
                << reason << "'"
                << endmsg;
    }

    // Get the map entry for this HPD
    CountReasonMapAndTime & b = m_HpdErrorReportMap[data];

    // Count the reasons for this report in this HPD
    ++(b.cMap[reason]);

    // Save the time of the latest report
    b.timeLast = currentTime;

  }
}

//=============================================================================

void HpdUkL1DisableTool::ResetAll()
{
  if ( !m_HpdErrorReportMap.empty() )
  {
    std::ostringstream m;
    m << "Clearing list of " << m_HpdErrorReportMap.size() << " HPD report(s)";
    m_CamTool->SendAndClearTS(ICameraTool::INFO, m_Name, m.str());
    info() << m.str() << endmsg;
  }

  if ( !m_DisabledHpdMap.empty() )
  {
    std::ostringstream m;
    m << "Clearing list of " << m_DisabledHpdMap.size() << " disabled HPD(s)";
    m_CamTool->SendAndClearTS(ICameraTool::INFO, m_Name, m.str());
    info() << m.str() << endmsg;
  }

  m_HpdErrorReportMap.clear();
  m_DisabledHpdMap.clear();
  m_disableCandidates.clear();
  m_DisabledHpdString.clear();
  m_disabledHPDsV.clear();
  m_softwareDisabledHPDs.clear();
  m_zeroWeightHPDsV.clear();

  if ( m_HPDDisableDimService )
  { m_HPDDisableDimService->updateService( (char*)"" ); }

  ResetDIMStrings();

  ResetHistograms();
}

//=============================================================================

void HpdUkL1DisableTool::removeFromDimString( const HPDData& data )
{
  const std::string hpdIDString = GetDisableString(data);
  int sanityCheck(0);
  while( ++sanityCheck < 999 )
  {
    const std::string::size_type pos = m_DisabledHpdString.find(hpdIDString);
    if ( pos == std::string::npos ) break;
    m_DisabledHpdString.replace(pos,hpdIDString.size(),"");
  }
}

//=============================================================================

void HpdUkL1DisableTool::DisableAndPublish()
{
  if ( msgLevel(MSG::VERBOSE) )
    verbose() << "Beginning of DisableAndPublish " << endmsg;

  // If LHC/LHCb state has changed, send a message to camera
  if ( UNLIKELY( stateChange() ) )
  {
    setDimStatesAsRead();
    std::ostringstream mess;
    mess << "State : LHC '" << lhcState() << "' LHCb '" << lhcbState() << "'";
    m_CamTool->SendAndClearTS( ICameraTool::INFO, m_Name, mess.str() );
    info() << mess.str() << endmsg;
  }

  // Current disabling state
  const bool disablingActive = isHpdDisablingActive();

  // Has HPD disabling turned ON/OFF ?
  static bool lastDisableState = disablingActive;
  static bool firstTime = true;
  if ( UNLIKELY( firstTime || disablingActive != lastDisableState ) )
  {
    lastDisableState = disablingActive;
    std::ostringstream mess;
    mess << "HPD SOFTWARE disabling now ";
    if ( disablingActive ) { mess << "ACTIVE"; } else { mess << "INACTIVE"; }
    m_CamTool->SendAndClearTS( ICameraTool::INFO, m_Name, mess.str() );
    info() << mess.str() << endmsg;
    // Reset, to make sure HPDs in error are now picked up
    if ( !firstTime ) { ResetAll(); }
    firstTime = false;
  }

  // If need be, reread the list of zero weight HPDs
  if ( UNLIKELY( m_l1ZeroWeight && m_l1ZeroWeight->newState() ) )
  {
    m_l1ZeroWeight->setRead();
    
    // read the HPD DIM string
    const std::string l1S = m_l1ZeroWeight->hpdList();

    // Decode the string into a list of HPDs ...
    m_zeroWeightHPDsV = hpdDataFromDIM(l1S);

    // printouts
    std::ostringstream dimStringS;
    dimStringS << "Found " << m_zeroWeightHPDsV.size() << " UKL1 " 
               << m_l1ZeroWeight->name() << " HPD(s)";
    m_CamTool->Append("TEXT",l1S.c_str());
    for ( HPDData::Vector::const_iterator iHPD = m_zeroWeightHPDsV.begin();
          iHPD != m_zeroWeightHPDsV.end(); ++iHPD )
    {
      std::ostringstream mess;
      mess << *iHPD << " " << smartId(*iHPD);
      m_CamTool->Append("TEXT",mess.str().c_str());
    }
    m_CamTool->SendAndClearTS( ICameraTool::WARNING, m_Name, dimStringS.str() );
    info() << dimStringS.str() << " '" << l1S << "'" << endmsg;
  }

  // Publish info for disabled HPDs.
  int newHPDsToDisable = 0;
  for ( HPDBuffer::const_iterator cand = m_disableCandidates.begin();
        cand != m_disableCandidates.end(); ++cand )
  {
    const HPDData & data   = cand->first;
    const Buffer & indices = cand->second.first;

    // Only proceed if enough counts
    if ( indices.size() == indices.capacity() )
    {

      const unsigned long min = *std::min_element( indices.begin(), indices.end() );
      const unsigned long max = *std::max_element( indices.begin(), indices.end() );

      const double failureRate =
        ( max != min ? indices.size() / double( max - min ) : 0 );

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "HPD " << data << " has received "
                  << 100. * failureRate << "% of disable requests." << endmsg;

      // Is rate above disable threshold ?
      if ( failureRate > m_failureRateThreshold )
      {
        // get map entry for this HPD
        CountReasonMapAndTime & b = m_DisabledHpdMap[data];

        // is this the first time for this HPD
        const bool firstDisable = b.cMap.empty();

        // Count disables and reasons
        ++(b.cMap["SOFTWARE Disable"]);

        // set the time for the last disable request for this HPD
        b.timeLast = time(NULL);

        // Only disable the first time, but keep count of all requests
        if ( firstDisable )
        {

          // get the string we send to DIM for this HPD
          const std::string hpdIDString = GetDisableString(data);
          if ( !hpdIDString.empty() )
          {

            // new HPD to disable
            ++newHPDsToDisable;

            // Get the smart ID for this HPD
            const LHCb::RichSmartID hpdID = smartId(data);

            // Have zero weight in the L1 disabling ?
            const bool hasZW = hpdHasZeroWeight(data);

            // append an info string to the camera report
            std::ostringstream mess;
            mess << data << " " << hpdID << " Failure Rate = " << failureRate;
            m_CamTool->Append("TEXT",mess.str().c_str());

            // Append the reasons for the disable to this camera report
            PrintReasons( cand->second.second );

            if ( hasZW ) m_CamTool->Append("TEXT"," -> Has ZERO UKL1 Disabling weight");

            // Check if this HPD is already hardware disabled
            const bool disabled = isDisabled(hpdID);
            if ( disabled ) 
            {
              std::ostringstream dmess;
              dmess << "HPD " << hpdID << " is already DISABLED";
              info() << dmess.str() << endmsg;
              m_CamTool->Append("TEXT",dmess.str().c_str());
            }
            
            // if active, add to DIM string and send a error message
            if ( disablingActive && !hasZW )
            {
              m_DisabledHpdString.append( hpdIDString );
              warning() << "HPD SOFTWARE disabled " << mess.str() << endmsg;
              // Add to list of software disabled HPDs
              m_softwareDisabledHPDs.insert( data );            
            }

          }
        }
      }
    }
  }

  // If need be, send to DIM
  if ( UNLIKELY( newHPDsToDisable > 0 ) )
  {
    std::ostringstream mess;
    mess << "Found " << newHPDsToDisable << " new HPD(s) in error. ";
    if ( disablingActive ) { mess << "DISABLING ON";  }
    else                   { mess << "DISABLING OFF"; }
    if ( disablingActive && !m_DisabledHpdString.empty() )
    {
      m_CamTool->SendAndClearTS( ICameraTool::ERROR     , m_Name, mess.str(),
                                 ICameraTool::ERROR_PVSS, m_Name,
                                 "HPD automatically disabled" );
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "Update DIM service with " << m_DisabledHpdString << endmsg;
      if ( m_HPDDisableDimService )
        m_HPDDisableDimService->updateService( (char*) m_DisabledHpdString.c_str() );
    }
    else
    {
      m_CamTool->SendAndClearTS( ICameraTool::ERROR     , m_Name, mess.str() );
    }
  }

  // Clear the list of currently disabled HPDs
  m_disabledHPDsV.clear();

  // reread from DIM
  processUKL1DimString( m_l1FELinkDown );
  processUKL1DimString( m_l1FESquashed );

}

//=============================================================================

bool 
HpdUkL1DisableTool::isDisabled( const LHCb::RichSmartID& hpdID ) const
{
  // First check to see if in FE Squashed list
  const HPDData::Vector feSquashed = hpdDataFromDIM( m_l1FESquashed ?
                                                     m_l1FESquashed->hpdList() : "" );
  bool disabled = std::find( feSquashed.begin(),
                             feSquashed.end(), (unsigned int) hpdID ) != feSquashed.end();

  // If not, try the FE Down list
  if ( !disabled )
  {
    const HPDData::Vector feDown = hpdDataFromDIM( m_l1FELinkDown ?
                                                   m_l1FELinkDown->hpdList() : "" );
    disabled = std::find( feDown.begin(), feDown.end(), (unsigned int) hpdID ) != feDown.end();
  }

  // return the final status
  return disabled;
}

//=============================================================================

void HpdUkL1DisableTool::processUKL1DimString( L1DimStringWrap * dim )
{

  // CRJ : Have to reread each time, to keep the disable time for each HPD
  //       up to date. To do - see if this can be avoided ...
  //if ( UNLIKELY( dim && dim->newState() ) ) 
  if ( dim )
  {
    dim->setRead();

    // read the HPD DIM string
    const std::string l1S = dim->hpdList();
    //const std::string l1S = "(1,03,5,2) (1,3,6,2) (10,9,12,0)"; // testing

    // Decode the string into a list of HPDs ...
    const HPDData::Vector hpds = hpdDataFromDIM(l1S);

//     // Send changed DIM string to camera
//     std::ostringstream dimStringS;
//     dimStringS << "Updated UKL1 " << dim->name() 
//                << " HARDWARE disabled HPD list = '" << l1S << "'";
//     m_CamTool->Append("TEXT",dimStringS.str().c_str());

//     // Debug printout of the decoded HPD data
//     for ( HPDData::Vector::const_iterator iHPD = hpds.begin();
//           iHPD != hpds.end(); ++iHPD )
//     {
//       std::ostringstream mess;
//       mess << *iHPD << " " << smartId(*iHPD);
//       m_CamTool->Append("TEXT",mess.str().c_str());
//     }

//     // Send DIM string changed message to camera
//     std::ostringstream Msg;
//     Msg << "Updated UKL1 " << dim->name() 
//         << " HARDWARE disabled HPD list " << hpds.size() << " HPD(s)";
//     m_CamTool->SendAndClearTS( ICameraTool::INFO, m_Name, Msg.str() );

    // Count the new HPDs disabled
    unsigned int nl1DisHPDs(0);

    // reason
    const std::string reason = "UKL1 " + dim->name() + " HARDWARE disabled";

    // Loop over them to find new disabled HPDs
    for ( HPDData::Vector::const_iterator iHPD = hpds.begin();
          iHPD != hpds.end(); ++iHPD )
    {

      // Get SmartID for this HPD
      const LHCb::RichSmartID smartID = smartId(*iHPD);
      if ( smartID.isValid() )
      {

        // make sure this HPD is in the list of HPDs actually disabled
        m_disabledHPDsV.insert(*iHPD);

        // add a message to the reasons for this HPD
        DisableHPD(*iHPD,reason);

        // Get the entry in the disabled HPD map for this HPD
        CountReasonMapAndTime & b = m_DisabledHpdMap[*iHPD];

        // First time for this HPD ?
        const bool firstDisable = b.cMap.empty();

        // Count disables and reasons
        ++(b.cMap["HARDWARE Disable"]);
        
        // set the time for the last disable request
        b.timeLast = time(NULL);

        // Is this HPD already in the disabled HPD map
        if ( firstDisable )
        { // No, it isn't

          // increment the L1 HPD disabled counts
          ++nl1DisHPDs;

          // Have zero weight in the L1 disabling ?
          const bool hasZW = hpdHasZeroWeight(*iHPD);

          // append an info string to the camera report
          std::ostringstream mess;
          mess << *iHPD << " " << smartID;
          m_CamTool->Append("TEXT",mess.str().c_str());

          // Append the reasons for the disable to this camera report
          PrintReasons( m_disableCandidates[*iHPD].second );

          if ( hasZW ) 
            m_CamTool->Append("TEXT"," -> Has ZERO UKL1 Disabling weight");

          // message stream error (only if in physics mode)
          if ( isHpdDisablingActive() && !hasZW )
            warning() << reason << " " << mess.str() << endmsg;

          // add to overview histogram straight away
          if ( !hasZW )
          {
            fillDisabledHisto( smartID, 
                               isSoftwareDisabled(*iHPD) ? 
                               HPDErrorLevel : HPDWarningLevel );
          }

        }

      }
      else
      {
        std::ostringstream err;
        err << "Invalid RichSmartID for " << *iHPD;
        Error( err.str() ).ignore();
      }

    } // loop over HPDs from DIM

    if ( nl1DisHPDs > 0 )
    {
      std::ostringstream mess;
      mess << "Found " << nl1DisHPDs << " " << reason << " HPD(s)";
      m_CamTool->SendAndClearTS( ICameraTool::ERROR, m_Name, mess.str() );
    }

  } // DIM service is available and updated

}

//=============================================================================

void HpdUkL1DisableTool::handle(const Incident &incident)
{

  if      ( IncidentType::BeginEvent == incident.type() )
  {

    // just count events
    ++m_nMonitoredEvents;

    // if first event, update last update time
    if ( UNLIKELY( 1 == m_nMonitoredEvents ) )
    {
      m_TimeLastUpdate = m_TimeLastHPDDisable = time(NULL);
    }

  }
  else if ( IncidentType::EndEvent   == incident.type() )
  {

    // current time
    const time_t currentTime = time(NULL);

    // Time to look for new HPDs to disable ?
    const bool disableCheck =
      ( ( (m_nMonitoredEvents % m_DisableCheckInterval) == 0 ) ||
        ( m_DisableCheckHeartBeat > 0 &&
          ((currentTime-m_TimeLastHPDDisable) >= m_DisableCheckHeartBeat) ) );
    if ( UNLIKELY(disableCheck) )
    {
      // search for HPDs to disable
      DisableAndPublish();
      // Set time for last update
      m_TimeLastHPDDisable = currentTime;
    }

    // is it time to update histograms and print to camera ?
    if ( UNLIKELY( (m_UpdateTimerInterval > 0) &&
                   ((currentTime-m_TimeLastUpdate) >= m_UpdateTimerInterval) ) )
    {
      // first remove old reports
      RemoveOldReports();

      // remove old disable requests
      RemoveOldDisables();

      // then send messages for those remaining
      const bool camPrint = ( (currentTime-m_TimeLastCamSummary) >= m_CamSummaryInterval );
      RefreshReports(camPrint);
      if ( camPrint ) { m_TimeLastCamSummary = currentTime; }

      // finally set the time of the last update to now
      m_TimeLastUpdate = currentTime;
    }

  }
  else if ( UNLIKELY( IncidentType::RunChange == incident.type() ) )
  {
    // One final summary printout
    RefreshReports();

    // now wipe the slate clean
    if ( m_ClearListAtNewRun ) ResetAll();
  }

}

//=============================================================================

StatusCode HpdUkL1DisableTool::Book2DHisto( const Rich::DetectorType rich,
                                            const Rich::Side panel )
{
  if ( m_Plot2DHisto )
  {

    if (msgLevel(MSG::VERBOSE))
      verbose() << "book histograms with disabled HPDs in " << rich << " " << panel << endmsg;

    const LHCb::RichSmartID smartID(rich,panel);
    const Rich::SmartIDGlobalOrdering gOrdering(smartID,Rich::SmartIDGlobalOrdering::LHCbMode);

    //
    // determine histogram borders
    //
    double hxmin(-0.5);
    double hxmax(Rich::Rich2==rich ? 8.5:13.5);
    int    hxnbins(static_cast<int>(hxmax-hxmin));
    if ((Rich::Rich2==rich) && (Rich::left==panel))
    {
      hxmin += (hxmax + 0.5);
      hxmax += (hxmax + 0.5);
    }// if

    double hymin(-0.5);
    double hymax(Rich::Rich2==rich ? 15.5:6.5);
    int    hynbins(static_cast<int>(hymax-hymin));
    if ((Rich::Rich1==rich) && (Rich::top==panel))
    {
      hymin += (hymax + 0.5);
      hymax += (hymax + 0.5);
    }

    // Finally book histogram
    std::ostringstream id,title;
    id << Rich::text(rich) << Rich::text(rich,panel);
    title << "HPD Warnings / Disable Requests | "
          << Rich::text(rich)
          << " "
          << Rich::text(rich,panel);
    m_hpdHistos[rich][panel] = book2D( id.str(), title.str(),
                                       hxmin, hxmax, hxnbins,
                                       hymin, hymax, hynbins );

  }
  else
  {
    m_hpdHistos[rich][panel] = NULL;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================

void HpdUkL1DisableTool::RefreshReports(const bool camPrint)
{

  const unsigned int nDisabledHPDs = m_disabledHPDsV.size();
  const unsigned int nHPDError     = m_HpdErrorReportMap.size();

  if ( msgLevel(MSG::VERBOSE) )
  {
    verbose() << "Refreshing data -> Currently | # Disabled HPD(s) = " << nDisabledHPDs
              << " | # HPD(s) with error reports = " << nHPDError
              << endmsg;
  }

  Rich::Map<LHCb::RichSmartID,double> HPDErrorValues;

  // for HPDs with Error reports
  if ( nHPDError > 0 )
  {

    // get the time now
    const time_t currentTime = time(NULL);

    std::ostringstream reportMess;
    reportMess << "Summary : " << nHPDError << " active HPD report(s)";
    info() << reportMess.str() << endmsg;

    for ( HPDCountReasonMap::const_iterator iMap = m_HpdErrorReportMap.begin();
          iMap != m_HpdErrorReportMap.end(); ++iMap )
    {
      const LHCb::RichSmartID smartID = smartId(iMap->first);

      // Have zero weight in the L1 disabling ?
      const bool hasZW = hpdHasZeroWeight(iMap->first);

      // Set HPD error value to warning
      if ( !hasZW ) HPDErrorValues[smartID] = HPDWarningLevel;

      // send some info to camera/messaging
      if ( camPrint )
      {

        // HPD ID
        std::ostringstream hpdS;
        hpdS << iMap->first << " " << smartID;
        if ( msgLevel(MSG::VERBOSE) ) verbose() << hpdS.str() << endmsg;
        m_CamTool->Append("TEXT",hpdS.str().c_str());

        // Time since last report
        const time_t timeDelta = currentTime - iMap->second.timeLast;
        struct tm * timeinfo = localtime ( &timeDelta );
        std::ostringstream mess;
        mess << " Time since last report : ";
        if ( timeinfo->tm_hour-1 > 0 ) { mess << timeinfo->tm_hour-1 << " hours ";   }
        if ( timeinfo->tm_min    > 0 ) { mess << timeinfo->tm_min    << " mins "; }
        if ( ( timeinfo->tm_hour-1 == 0 &&
               timeinfo->tm_min    == 0 ) ||
             timeinfo->tm_sec     > 0  ) { mess << timeinfo->tm_sec  << " secs ";   }
        m_CamTool->Append("TEXT",mess.str().c_str());

        // Summarise the reports
        PrintReasons( iMap->second.cMap );

        if ( hasZW ) m_CamTool->Append("TEXT"," -> Has ZERO UKL1 Disabling weight");

      }

    }

    if ( camPrint )
      m_CamTool->SendAndClearTS(ICameraTool::INFO,m_Name,reportMess.str().c_str());

  } // HPD reports

  //  for disabled HPDs
  if ( nDisabledHPDs > 0 )
  {

    std::ostringstream disMessage;
    disMessage << "Summary : " << nDisabledHPDs << " currently disabled HPD(s)";

    bool sendMessToShifter = false;
    for ( HPDData::Set::const_iterator iHPD = m_disabledHPDsV.begin();
          iHPD != m_disabledHPDsV.end(); ++iHPD )
    {
      const LHCb::RichSmartID smartID = smartId(*iHPD);

      // Have zero weight in the L1 disabling ?
      const bool hasZW = hpdHasZeroWeight(*iHPD);
      if ( !hasZW ) sendMessToShifter = true;

      // Set HPD error value to disabled
      if ( !hasZW ) 
      {
        HPDErrorValues[smartID] = ( isSoftwareDisabled(*iHPD) ?
                                    HPDErrorLevel : HPDWarningLevel );        
      }

      // send some info to camera / messaging
      if ( camPrint )
      {
        std::ostringstream hpdS;
        hpdS << *iHPD << " " << smartID;
        m_CamTool->Append("TEXT",hpdS.str().c_str());
        PrintReasons( m_disableCandidates[*iHPD].second );
        if ( hasZW ) m_CamTool->Append("TEXT"," -> Has ZERO UKL1 Disabling weight");
      }

    }

    // send to central error logger only if printing enabled
    if ( camPrint && sendMessToShifter ) { warning() << disMessage.str() << endmsg; }

    if ( camPrint )
    { m_CamTool->SendAndClearTS(ICameraTool::ERROR,m_Name,disMessage.str().c_str()); }
    
  } // disabled HPDs

  // reset the histograms
  ResetHistograms();

  // ... now refill with final HPD error codes
  for ( Rich::Map<LHCb::RichSmartID,double>::const_iterator iM = HPDErrorValues.begin();
        iM != HPDErrorValues.end(); ++iM )
  {
    fillDisabledHisto(iM->first,iM->second);
  }

}

//=============================================================================

const HpdUkL1DisableTool::HPDData::Vector
HpdUkL1DisableTool::hpdDataFromDIM( const std::string & dimS ) const
{
  HPDData::Vector hpds;

  if ( !dimS.empty() )
  {

    // try block to catch exceptions
    try
    {

      boost::regex re("\\s+");
      boost::sregex_token_iterator i( dimS.begin(), dimS.end(), re, -1 ), j;
      while ( i != j )
      {
        const std::string _hpdS = *i;
        if ( _hpdS.size() > 2 )
        {
          const std::string hpdS = _hpdS.substr(1,_hpdS.size()-2);
          boost::regex rre(",");
          boost::sregex_token_iterator ii( hpdS.begin(), hpdS.end(), rre, -1 ), jj;
          std::vector<unsigned int> values;
          values.reserve(4);
          while ( ii != jj )
          {
            const std::string S = *ii;
            values.push_back( boost::lexical_cast<unsigned int>(S) );
            ++ii;
          }
          if ( 4 == values.size() )
          {
            hpds.push_back( HPDData( (Rich::DetectorType)(values[0]-1),
                                     Rich::DAQ::Level1LogicalID(values[1]),
                                     Rich::DAQ::L1IngressID(values[3]),
                                     Rich::DAQ::L1InputWithinIngress(values[2]) ) );
          }
          else
          {
            Error( "Failed to decode UKL1 DIM string '" + *i + "'" ).ignore();
          }
        }
        else
        {
          Error( "Failed to decode UKL1 DIM string '" + *i + "'" ).ignore();
        }
        ++i;
      }

    }
    catch ( const std::exception & exp )
    {
      Error( "Exception decoding UKL1 DIM string " + dimS + " : " + exp.what() ).ignore();
    }

    // erase duplicates and sort
    hpds.erase ( std::unique(hpds.begin(),hpds.end()), hpds.end() );
    std::sort  ( hpds.begin(), hpds.end() );

  }

  return hpds;
}

//=============================================================================

void HpdUkL1DisableTool::RemoveOldDisables()
{
  if ( m_HistoryTime > 0 )
  {

    // get the time now
    const time_t currentTime = time(NULL);

    // list of HPD reports to remove
    typedef std::map<HPDData,time_t> ToRemove;
    ToRemove oldReports;

    // Loop over HPDs in the local disabled list and compare to thos actually
    // currently disabled
    for ( HPDCountReasonMap::const_iterator iMap = m_DisabledHpdMap.begin();
          iMap != m_DisabledHpdMap.end(); ++iMap )
    {
      const time_t timeDiff = currentTime - iMap->second.timeLast;
      if ( timeDiff >= m_HistoryTime &&
           m_disabledHPDsV.find(iMap->first) == m_disabledHPDsV.end() )
      {
        // This HPD is no longer in the disabled DIM lists, 
        // and the request is OLD, so remove
        oldReports[iMap->first] = timeDiff;
      }
    }

    // Remove all those selected as too old
    if ( !oldReports.empty() )
    {
      for ( ToRemove::const_iterator i = oldReports.begin();
            i != oldReports.end(); ++i )
      {

        struct tm * timeinfo = localtime ( &(i->second) );
        std::ostringstream mess;
        mess << "Last disable request for " << i->first << " is ";
        if ( timeinfo->tm_hour-1 > 0 ) { mess << timeinfo->tm_hour-1 << " hours ";   }
        if ( timeinfo->tm_min    > 0 ) { mess << timeinfo->tm_min    << " mins "; }
        if ( ( timeinfo->tm_hour-1 == 0 &&
               timeinfo->tm_min    == 0 ) ||
             timeinfo->tm_sec     > 0  ) { mess << timeinfo->tm_sec    << " secs ";   }
        mess << "old -> Removing all requests";
        m_CamTool->Append("TEXT",mess.str().c_str());

        // remove from internal disabled HPD map
        m_DisabledHpdMap.erase(i->first);

        // remove this HPD from the DIM string
        removeFromDimString(i->first);

      }

      std::ostringstream mess;
      mess << "Removing " << oldReports.size() << " old HPD disable request(s)";
      m_CamTool->SendAndClearTS(ICameraTool::INFO,m_Name,mess.str().c_str());
      info() << mess.str() << endmsg;

      // Update the DIM string
      if ( m_HPDDisableDimService )
        m_HPDDisableDimService->updateService( (char*) m_DisabledHpdString.c_str() );

    }

  }
}

//=============================================================================

void HpdUkL1DisableTool::RemoveOldReports()
{
  if ( m_HistoryTime > 0 )
  {

    // get the time now
    const time_t currentTime = time(NULL);

    // list of HPD reports to remove
    typedef std::map<HPDData,time_t> ToRemove;
    ToRemove oldReports;

    // find HPDs with old reports
    for ( HPDCountReasonMap::const_iterator iMap = m_HpdErrorReportMap.begin();
          iMap != m_HpdErrorReportMap.end(); ++iMap )
    {
      const time_t timeDiff = currentTime - iMap->second.timeLast;
      if ( timeDiff >= m_HistoryTime )
      {
        oldReports[iMap->first] = timeDiff;
      }
    }

    // Remove all those selected as too old
    if ( !oldReports.empty() )
    {
      for ( ToRemove::const_iterator i = oldReports.begin();
            i != oldReports.end(); ++i )
      {
        struct tm * timeinfo = localtime ( &(i->second) );
        std::ostringstream mess;
        mess << "Last Report for " << i->first << " is ";
        if ( timeinfo->tm_hour-1 > 0 ) { mess << timeinfo->tm_hour-1 << " hours ";   }
        if ( timeinfo->tm_min    > 0 ) { mess << timeinfo->tm_min    << " mins "; }
        if ( ( timeinfo->tm_hour-1 == 0 &&
               timeinfo->tm_min    == 0 ) ||
             timeinfo->tm_sec     > 0  ) { mess << timeinfo->tm_sec    << " secs ";   }
        mess << "old -> Removing all reports";
        m_CamTool->Append("TEXT",mess.str().c_str());
        m_HpdErrorReportMap.erase(i->first);
      }
      std::ostringstream mess;
      mess << "Removing " << oldReports.size() << " old HPD report(s)";
      m_CamTool->SendAndClearTS(ICameraTool::INFO,m_Name,mess.str().c_str());
      info() << mess.str() << endmsg;
    }

  }
}

//=============================================================================

const LHCb::RichSmartID
HpdUkL1DisableTool::smartId( const HPDData& data )
{
  LHCb::RichSmartID smartID;

  if ( data.rich() != Rich::InvalidDetector )
  {
    // Use try block to prevent exceptions going any further
    try
    {
      const Rich::DAQ::Level1HardwareID l1HardID  = m_RichSys->level1HardwareID(data.rich(),data.l1LogID());
      const Rich::DAQ::Level1Input      l1Input(data.l1Ingress(),data.l1IngressInput());
      const Rich::DAQ::HPDHardwareID    hpdHardID = m_RichSys->pdHardwareID(l1HardID, l1Input);
      smartID                                     = m_RichSys->richSmartID(hpdHardID);
    }
    catch ( const GaudiException & excpt )
    {
      if (msgLevel(MSG::DEBUG)) debug() << excpt.message() << " " << excpt.tag() << endmsg;
    }
    catch ( const std::exception & excpt )
    {
      if (msgLevel(MSG::DEBUG)) debug() << excpt.what() << endmsg;
    }
  }

  return smartID;
}

//=============================================================================

void HpdUkL1DisableTool::fillDisabledHisto( const LHCb::RichSmartID& smartID,
                                            const double errorWeight )
{
  if ( m_Plot2DHisto && smartID.isValid()       &&
       smartID.rich()  != Rich::InvalidDetector &&
       smartID.panel() != Rich::InvalidSide     )
  {
    const Rich::SmartIDGlobalOrdering globOrdering(smartID);
    const int x = globOrdering.globalPdX();
    const int y = globOrdering.globalPdY();
    if ( msgLevel(MSG::VERBOSE) )
      verbose() << "Filling histogram for " << smartID << " weight=" << errorWeight << endmsg;
    m_hpdHistos[smartID.rich()][smartID.panel()] -> fill( x, y, errorWeight );
  }
}

//=============================================================================

void HpdUkL1DisableTool::ResetHistograms()
{
  if ( m_Plot2DHisto )
  {
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "clear histograms" << endmsg;
    m_hpdHistos[Rich::Rich1][Rich::top]    -> reset();
    m_hpdHistos[Rich::Rich1][Rich::bottom] -> reset();
    m_hpdHistos[Rich::Rich2][Rich::left]   -> reset();
    m_hpdHistos[Rich::Rich2][Rich::right]  -> reset();
  }
}

//=============================================================================

void HpdUkL1DisableTool::PrintReasons(const CountReasonMap & reasons)
{
  for ( CountReasonMap::const_iterator iR = reasons.begin();
        iR != reasons.end(); ++iR )
  {
    std::ostringstream reason;
    reason << "  " << iR->second << " '" << iR->first << "'";
    m_CamTool->Append("TEXT",reason.str().c_str());
    if ( msgLevel(MSG::VERBOSE) ) verbose() << reason.str() << endmsg;
  }
}

//=============================================================================

