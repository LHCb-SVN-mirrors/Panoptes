#ifndef I_RICHMONITORINGTOOLS_HPDUKL1DISABLETOOL_H 
#define I_RICHMONITORINGTOOLS_HPDUKL1DISABLETOOL_H 1

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/IIncidentListener.h"

// RICH
#include <Kernel/RichSmartID.h>

// boost
#include "boost/cstdint.hpp"
#include "boost/format.hpp"

// STL
#include <vector>
#include <set>

static const InterfaceID IID_IHpdUkL1DisableTool ( "Rich::Mon::IHpdUkL1DisableTool", 1, 0 );

namespace Rich
{
  namespace Mon
  {

    /** @class IHpdUkL1DisableTool RichMonitoringTools/IHpdUkL1DisableTool.h
     *
     *  Interface to HPD disabling tool, for Online monitoring
     *
     *  @author Ulrich Kerzel
     *  @date   2008-06-30
     */
    class IHpdUkL1DisableTool : virtual public IAlgTool,
                                virtual public IIncidentListener
    {

    public:

      // Return the interface ID
      static const InterfaceID& interfaceID() { return IID_IHpdUkL1DisableTool; }

    public:

      /// Type for 8 bit packed word
      typedef boost::int8_t   Pack8_t;

      /// Type for 32 bit raw word
      typedef boost::uint32_t Pack32_t;

    public:

      /** @class HPDData RichMonitoringTools//IHpdUkL1DisableTool.h
       *
       *  Class to store HPD data
       *
       *  @author Chris Jones
       *  @date   12/08/2011
       */
      class HPDData
      {

      private:

        /// The data
        union Data
        {
          struct Packed
          {
            Pack8_t l1IngressInput : 8;   ///< The input number
            Pack8_t l1Ingress      : 8;   ///< The Ingress ID
            Pack8_t l1LogID        : 8;   ///< The Level 1 Logical ID
            Pack8_t rich           : 8;   ///< The RICH type
          } packed;     ///< Packed data object
          Pack32_t raw; ///< Representation as an unsigned int
        } data;

      public:

        /// Set the detector type
        inline void setDet( const Rich::DetectorType det )
        {
          data.packed.rich = (Pack8_t)(det);
        }

        /// Set the Level1 ID
        inline void setL1( const Rich::DAQ::Level1LogicalID l1 )
        {
          data.packed.l1LogID = (Pack8_t)(l1.data());
        }

        /// Set the ingress ID
        inline void setIngress( const Rich::DAQ::L1IngressID ingress )
        {
          data.packed.l1Ingress = (Pack8_t)(ingress.data());
        }

        /// Set the ingress ID
        inline void setInput( const Rich::DAQ::L1InputWithinIngress input )
        {
          data.packed.l1IngressInput = (Pack8_t)(input.data());
        }

      public:

        /// Default Constructor
        HPDData() { this->data.raw = 0; setDet( Rich::InvalidDetector ); }

        /// Copy Constructor
        HPDData( const HPDData& _data ) { this->data.raw = (Pack32_t)_data; }

        /// Constructor from data values
        HPDData( const Rich::DetectorType              _rich,
                 const Rich::DAQ::Level1LogicalID      _l1LogID,
                 const Rich::DAQ::L1IngressID          _l1Ingress,
                 const Rich::DAQ::L1InputWithinIngress _l1IngressInput )
        {
          data.raw = 0;
          setDet     ( _rich           );
          setL1      ( _l1LogID        );
          setIngress ( _l1Ingress      );
          setInput   ( _l1IngressInput );
        }

      public:

        /// Access the RICH detector
        inline Rich::DetectorType rich() const
        {
          return (Rich::DetectorType)(data.packed.rich);
        }

        /// Access the L1 Logical ID
        inline Rich::DAQ::Level1LogicalID l1LogID() const
        {
          return (Rich::DAQ::Level1LogicalID)(data.packed.l1LogID);
        }

        /// Access the Ingress ID
        inline Rich::DAQ::L1IngressID l1Ingress() const
        {
          return (Rich::DAQ::L1IngressID)(data.packed.l1Ingress);
        }

        /// Access the input number
        inline Rich::DAQ::L1InputWithinIngress l1IngressInput() const
        {
          return (Rich::DAQ::L1InputWithinIngress)(data.packed.l1IngressInput);
        }

      public:

        /// Return the raw packed int value
        inline operator Pack32_t() const { return data.raw; }

      public:

        /// Equality operator
        inline bool operator== ( const HPDData& data ) const
        {
          return ( (Pack32_t)(*this) == (Pack32_t)data );
        }

        /// Sorting operator
        inline bool operator<  ( const HPDData& data ) const
        {
          return ( (Pack32_t)(*this) <  (Pack32_t)data );
        }

      public:

        /// Print to ostream
        friend inline std::ostream& operator << ( std::ostream& os, const HPDData & data )
        {
          return os << 
            boost::format("{ %5s L1LogID=%02d Ingress=%02d IngressInput=%02d }")
            % Rich::text(data.rich()) % data.l1LogID() 
            % data.l1Ingress() % data.l1IngressInput();
        }

      public:

        /// Define a vector of HPDData objects
        typedef std::vector<HPDData> Vector;

        /// Define a vector of HPDData objects
        typedef std::set<HPDData>    Set;

      };

    public:

      /// send disable request
      virtual void  DisableHPD ( const LHCb::RichSmartID &smartID,
                                 const std::string & reason = "" ) = 0;

      /// send disable request
      virtual void  DisableHPD ( const HPDData& data,
                                 const std::string & reason = "" ) = 0;

      /// report HPD as having some issue but not necessarily so severe you'd want to disable it
      virtual void  ReportHPD  ( const LHCb::RichSmartID &smartID,
                                 const std::string & reason = "" ) = 0;

      /// report HPD as having some issue but not necessarily so severe you'd want to disable it
      virtual void  ReportHPD  ( const HPDData& data,
                                 const std::string & reason = "" ) = 0;

      /// Clear the error reports
      virtual void  ResetAll   () = 0;

    }; // class

  } // namespace Mon
} // namespace Rich

#endif // I_RICHMONITORINGTOOLS_HPDUKL1DISABLETOOL_H
