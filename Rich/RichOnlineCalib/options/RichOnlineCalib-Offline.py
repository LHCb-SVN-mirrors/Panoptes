"""
Scripts to test offline, usage:
SetupPanoptes 
gaudirun.py RichOnlineCalib-Offline.py
"""

from Configurables import GaudiSequencer
RichOnCalibSeq = GaudiSequencer("RichOnCalibSeq")

# 
from Configurables import OMARichRefIndex, OMARichHPDImage

RefIndexAlg = OMARichRefIndex("RefIndexAlg")
HPDImageAlg = OMARichHPDImage("HPDImageAlg") 

#myFiles1 = ["/afs/cern.ch/user/j/jhe/gangadir/workspace/jhe/LocalXML/308/output/RefInCalib-RichCKCalib_pA_BR-v43r2p6_Run-135576.root"]

myFiles0 = [ "/afs/cern.ch/user/j/jhe/scratch0/Rich/Brunel-135576-20130120T161302-EOR.root"]
myFiles1 = [ "/afs/cern.ch/user/j/jhe/scratch0/Rich/Brunel-135576-Offline-EOR.root" ]

RefIndexAlg.InputFiles  = myFiles1
RefIndexAlg.xmlFilePath = "/tmp/jhe/" 

HPDImageAlg.InputFiles = myFiles1
HPDImageAlg.xmlFilePath = "/tmp/jhe/" 

RichOnCalibSeq.Members += [
    #RefIndexAlg,
    HPDImageAlg
    ]

RefIndexAlg.StopAlgSequence = False
HPDImageAlg.StopAlgSequence = False

RichOnCalibSeq.IgnoreFilterPassed = True

RefIndexAlg.OutputLevel = 2
HPDImageAlg.OutputLevel = 2

from Configurables import ApplicationMgr
ApplicationMgr().TopAlg            += [ RichOnCalibSeq ]

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "head-20120413"
LHCbApp().CondDBtag = "cond-20120730"
