from Configurables import GaudiSequencer
RichOnCalibSeq = GaudiSequencer("RichOnCalibSeq")

# 
from Configurables import OMARichRefIndex, OMARichHPDImage

RefIndexAlg = OMARichRefIndex("RefIndexAlg")
HPDImageAlg = OMARichHPDImage("HPDImageAlg") 

# Rich refrative index calibration job
RefIndexAlg.InputTasks  = [ "Brunel" ]
RefIndexAlg.xmlFilePath = "/group/online/alignment/"

# HPD image calibration job
HPDImageAlg.InputTasks  = [ "Brunel" ]
HPDImageAlg.xmlFilePath = "/group/online/alignment/" 

RichOnCalibSeq.Members += [
    RefIndexAlg,
    HPDImageAlg
    ]
RichOnCalibSeq.IgnoreFilterPassed = True

# Output Level of both
RefIndexAlg.OutputLevel = 3
HPDImageAlg.OutputLevel = 3


from Configurables import ApplicationMgr
ApplicationMgr().TopAlg            += [ RichOnCalibSeq ]

ApplicationMgr().EvtSel = "NONE"
ApplicationMgr().ExtSvc += [ "MonitorSvc" ]
ApplicationMgr().Runable = "LHCb::OnlineRunable/Runable"

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "head-20120413"
LHCbApp().CondDBtag = "cond-20120730"
