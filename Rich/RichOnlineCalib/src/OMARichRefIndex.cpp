// Include files 

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// local
#include "OMARichRefIndex.h"

//-----------------------------------------------------------------------------
// Implementation file for class : OMARichRefIndex
//
// 2014-04-10 : Jibo He
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OMARichRefIndex::OMARichRefIndex( const std::string& name,
                                  ISvcLocator* pSvcLocator )
  : AnalysisTask  ( name , pSvcLocator ),
    m_CamTool          ( nullptr  ),
    m_Name             ( name  ),
    m_mergedRun        ( 0, "" ),
    m_lastRun          ( 0, "" ),
    m_runNumber        ( 0     ),
    m_pPublishSvc      ( nullptr  ),
    m_Rich1PubString   ( ""    ),
    m_Rich2PubString   ( ""    ),
    m_Rich1RefIndex    ( 1.0   ),
    m_Rich2RefIndex    ( 1.0   ),
    m_canvas           ( nullptr  ),
    m_inInit           ( false )
{
  declareProperty("Rich1TaskName" , m_Rich1TaskName = "Rich1/Calib" );
  declareProperty("Rich2TaskName" , m_Rich2TaskName = "Rich2/Calib" );
  declareProperty("Rich1ScaleTaskName" , m_Rich1ScaleTaskName = "Rich1RefIndex" );
  declareProperty("Rich2ScaleTaskName" , m_Rich2ScaleTaskName = "Rich2RefIndex" );

  declareProperty("EoRSignal" ,  m_EoRSignal = "-EOR" );

  declareProperty("HistBase"  ,  m_HistBase  = "RICH/RiCKResLongTight/" );
  declareProperty("ResPlot"   ,  m_ResPlot = "ckResAll" );

  declareProperty("RichRads"  ,  m_rads = {"Rich1Gas", "Rich2Gas"} );

  declareProperty("minEntries" , m_minEntries = 100000 );
  declareProperty("nPolFull"   , m_nPolFull = 3 );
  declareProperty("maxErrorForOK", m_maxErrorForOK = 1e-3 );

  declareProperty("RichFitMin" , m_RichFitMin = { -0.0079, -0.00395 } );
  declareProperty("RichFitMax" , m_RichFitMax = {  0.0077,  0.00365 } );

  declareProperty("RefIndexSlope" , m_RefIndexSlope = { 38.1, 65.25 } );

  declareProperty("Precision" , m_Precision = 8 );

  declareProperty("xmlFilePath",  m_xmlFilePath  = "/group/online/alignment" );

  declareProperty("xmlVersionLog" , m_xmlVersionLog = "version.txt" );
  declareProperty("RefIndexSFLog" , m_RefIndexSFLog = "refIndexSF.txt" );
  declareProperty("DIMSummaryFile",
                  m_dimSummaryFile = "DIMUpdateSummary.txt" );

  declareProperty("DisableDIMPublish", m_disableDIMpublish = false );

  declareProperty("CreatePreliminaryCalibration", m_createPrelimCalib = true );

  declareProperty("CreatePDFSummary", m_createPDFsummary = true );

  declareProperty("MaxCachedCalibsInARow", m_maxCachedCalibs = 5 );

  // Partition
  const char* partitionName = getenv("PARTITION");
  if ( partitionName ) { m_Name += std::string(partitionName); }

}

//=============================================================================
// Destructor
//=============================================================================
OMARichRefIndex::~OMARichRefIndex() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode OMARichRefIndex::initialize()
{
  m_inInit = true;

  // ROOT style for PDFs
  if ( m_createPDFsummary ) { setStyle(); }

  StatusCode sc = AnalysisTask::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // write to summary file
  writeToDimSummaryFile( m_Name + " Initialized" );

  // Fix file permissions ...
  //fixFilePermissions();

  // Initialise DIM state listeners
  //initialiseDIMStates();

  // Publish service
  if ( !m_disableDIMpublish )
  {
    // Initialise scale factors to last known values
    m_Rich1RefIndex = getLastRefIndexSF("Rich1Gas");
    m_Rich2RefIndex = getLastRefIndexSF("Rich2Gas");
    // Initialise the publish service
    sc = serviceLocator()->service("LHCb::PublishSvc",m_pPublishSvc,false);
    if ( sc.isSuccess() && m_pPublishSvc )
    {
      info() << "PublishSvc initialized" << endmsg;
      m_pPublishSvc->declarePubItem( m_Rich1TaskName,      m_Rich1PubString );
      m_pPublishSvc->declarePubItem( m_Rich2TaskName,      m_Rich2PubString );
      m_pPublishSvc->declarePubItem( m_Rich1ScaleTaskName, m_Rich1RefIndex  );
      m_pPublishSvc->declarePubItem( m_Rich2ScaleTaskName, m_Rich2RefIndex  );
    }
  }

  // reset the cached histogram stats
  m_histStats.clear();

  // send a message to camera to say we are ready
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Initialized");

  m_inInit = false;
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode OMARichRefIndex::execute()
{
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Analyze
//=============================================================================
StatusCode OMARichRefIndex::analyze (std::string& SaveSet,
                                     std::string Task)
{
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Analyze" << endmsg;

  // Mandatory: call default analyze method
  StatusCode sc = AnalysisTask::analyze( SaveSet, Task );
  if ( sc.isFailure() ) return sc;
  // if we are still in initialize... just return
  // seems to be needed to prevent a crash when the task is restarted ...
  if ( !m_disableDIMpublish && m_inInit ) return sc;

  // // If LHC/LHCb state has changed, send a message to camera
  // if ( UNLIKELY( stateChange() ) )
  // {
  //   setDimStatesAsRead();
  //   std::ostringstream mess;
  //   mess << "State : LHC '" << lhcState() << "' LHCb '" << lhcbState() << "'";
  //   m_CamTool->SendAndClearTS( ICameraTool::INFO, m_Name, mess.str() );
  //   info() << mess.str() << endmsg;
  // }

  // Are we in physics, so send updates
  //const bool sendUpdates = inPhysics();
  const bool sendUpdates = true;

  // cache the previous state
  static bool lastState = sendUpdates;

  // Flag for first time call
  //static bool firstTime = true;

  // // has the state changed ?
  // if ( UNLIKELY( firstTime || sendUpdates != lastState ) )
  // {
  //   std::ostringstream mess;
  //   mess << "Refractive index calibration now ";
  //   if ( sendUpdates ) { mess << "ACTIVE"; } else { mess << "INACTIVE"; }
  //   m_CamTool->SendAndClearTS( ICameraTool::INFO, m_Name, mess.str() );
  //   info() << mess.str() << endmsg;
  // }

  // return now if not in physics or was previously in physics
  if ( lastState || sendUpdates )
  {

    // Check whether it is the last file of one run
    // either with "-EOR" in file name, or run number changed

    // Each time we do fit, the last run should be set to 0
    // so that it will be init'ed again
    // Fix me!? not do fit for the run withtout "-EOR" just before stopping the calib job...

    // Extract the file name from the full path
    const std::string fileName = boost::filesystem::path(SaveSet).stem().string();

    // Extract the run number
    const unsigned int runNumber = getRunNumber( fileName );

    // start the camera message
    std::ostringstream mess1, mess2;
    mess1 << "Saveset: '" << SaveSet << "'";
    mess2 << "Extracted file name: " << fileName << ", Run: " << runNumber;
    if ( msgLevel(MSG::DEBUG) )
    {
      debug() << mess1.str() << endmsg;
      debug() << mess2.str() << endmsg;
    }
    cameraTool()->Append("TEXT",mess1.str());
    cameraTool()->Append("TEXT",mess2.str());

    // check the run number is sensible ...
    if ( UNLIKELY( 0 == runNumber ) )
    {
      const std::string mess =
        "Run number in Brunel Saveset is '0' !! -> Calibration skipped";
      cameraTool()->SendAndClearTS( ICameraTool::ERROR, m_Name, mess );
      error() << mess << endmsg;
    }
    else
    {
      if ( fileName.find(m_EoRSignal) != std::string::npos )
      {
        m_mergedRun = std::make_pair( runNumber, SaveSet );
        m_lastRun   = std::make_pair( 0, "" );
        runCalibration( "Final EOR", m_mergedRun, true );
        // reset the cached histogram stats
        m_histStats.clear();
      } // "-EOR" inside the file name
      else
      {
        // if it is the first time to read a file without "-EOR"
        // or still the same run
        if ( 0 == m_lastRun.first || runNumber == m_lastRun.first )
        {
          //const bool firstSetForRun = ( 0 == m_lastRun.first );
          m_lastRun = std::make_pair( runNumber, SaveSet );
          //runCalibration( "Periodic", m_lastRun, m_createPrelimCalib && firstSetForRun );
          runCalibration( "Periodic", m_lastRun, m_createPrelimCalib );
        } // wait for new file
        else if ( runNumber != m_lastRun.first ) // new Run, use previous file
        {
          std::ostringstream mess;
          mess << "Refractive index calibration for run " << m_lastRun.first
               << " triggered by non-EOR saveset. Check Online Brunel EOR creation...";
          cameraTool()->Append("TEXT",mess.str());
          warning() << mess.str() << endmsg;
          // Run calibration for the last run
          runCalibration( "Final", m_lastRun, true, ICameraTool::WARNING );
          // reset the cached histogram stats
          m_histStats.clear();
          // Run a first calibration for this run
          const auto thisRun = std::make_pair( runNumber, SaveSet );
          runCalibration( "First", thisRun, m_createPrelimCalib  );
          // reset for the next call
          m_mergedRun = m_lastRun;
          m_lastRun   = std::make_pair( 0, "" ) ;
        }
      }
    }

  }

  // unset the first time flag
  //firstTime = false;

  // update the cached state
  lastState = sendUpdates;

  return sc;
}

//=============================================================================
//  Run the calibration
//=============================================================================
void
OMARichRefIndex::runCalibration( const std::string& type,
                                 const std::pair<unsigned int,std::string>& data,
                                 const bool publishCalib,
                                 ICameraTool::MessageLevel level )
{

  const auto runNumber = data.first;
  m_runNumber = runNumber;

  // Open Saveset
  TFile * f = TFile::Open( data.second.c_str(), "READ" );
  if ( f && !f->IsZombie() )
  {

    if ( m_createPDFsummary ) { printCanvas("["); }
    bool keepPDF = false;

    // Loop over radiators
    for ( const auto& rad : m_rads )
    {

      // the histogram name to fit
      const std::string radHistName = m_HistBase + rad + "/" + m_ResPlot ;
      cameraTool()->Append("TEXT","Fit for "+radHistName);

      // try and load the histogram
      TH1D* radHist = (TH1D*)f->Get(radHistName.c_str());
      if ( radHist )
      {
        // check the histogram stats.
        const auto histStats = radHist->GetEntries();
        const bool betterHistStats = histStats > m_histStats[rad];

        // Make sure axis titles are set..
        if ( radHist->GetXaxis() )
        { radHist->GetXaxis()->SetTitle( "delta(Cherenkov Theta) / rad" ); }
        if ( radHist->GetYaxis() )
        { radHist->GetYaxis()->SetTitle( "Entries" ); }

        // send the histogram to camera
        sendToCamera( radHist, runNumber );

        // Draw to canvas
        draw( radHist );

        double scale = 1.0 ;
        if ( checkCKThetaStats(radHist) )
        {
          // Flag PDF should be kept, as at least one fit had enough stats.
          keepPDF = true;
          // run the fit
          const auto fitresult = fitCKThetaHistogram( radHist, rad, m_nPolFull );
          if ( fitresult.first )
          {
            m_cachedCalibCount[rad] = 0; // Normal calibration, so reset count to 0
            scale = nScaleFromShift( fitresult.second, rad );
            std::ostringstream mess;
            mess << rad << " n-1 shift = " << fitresult.second << " scale = " << scale;
            cameraTool()->Append("TEXT",mess.str());
          }
          else
          {
            level = ICameraTool::WARNING;
            ++m_cachedCalibCount[rad]; // Count cached calibrations
            scale = getLastRefIndexSF( rad ) ;
            std::ostringstream mess;
            mess << "Fit FAILED. Using scale from previous run " << scale;
          }
          if ( publishCalib || betterHistStats )
          {
            if ( !updateRefIndexSF( runNumber, rad, scale ) )
            {
              level = ICameraTool::ERROR;
              std::ostringstream mess;
              mess << "Failed to update " << rad << " ref index scale";
              warning() << mess.str() << endmsg;
              cameraTool()->Append("TEXT",mess.str());
            }
            else
            {
              // update the stats for this histogram
              m_histStats[rad] = histStats;
            }
          }
        }
        else // not enough statistics, take the one from previous run
        {
          level = ICameraTool::WARNING;
          ++m_cachedCalibCount[rad]; // Count cached calibrations
          scale = getLastRefIndexSF( rad ) ;
          std::ostringstream mess;
          mess << "Not enough statistics for " << rad
               << " -> Using scale from previous run " << scale;
          cameraTool()->Append("TEXT",mess.str());
          debug() << mess.str() << endmsg;
        }

        // publish results. Only if histogram did not have 0 entries
        if ( 0 != radHist->GetEntries() &&
             ( publishCalib || betterHistStats ) )
        {
          if ( m_cachedCalibCount[rad] > m_maxCachedCalibs )
          {
            level = ICameraTool::WARNING;
            std::ostringstream mess;
            mess << "Too many (" << m_cachedCalibCount[rad]
                 << ") cached calibrations in a row.";
            warning() << mess.str() << endmsg;
            cameraTool()->Append("TEXT",mess.str());
          }
          if ( !xmlWriter( type, runNumber, scale, rad ) )
          {
            level = ICameraTool::ERROR;
            std::ostringstream mess;
            mess << "Failed to write xml file for " << rad;
            warning() << mess.str() << endmsg;
            cameraTool()->Append("TEXT",mess.str());
          }
          else
          {
            std::ostringstream mess;
            mess << "Successfully wrote and published xml file for " << rad;
            cameraTool()->Append("TEXT",mess.str());
          }
        }
        else
        {
          const std::string mess = "XML Writing is DISABLED for this saveset";
          info() << mess << endmsg;
          cameraTool()->Append("TEXT",mess);
        }

        if ( msgLevel(MSG::DEBUG) ) debug() << "Scale factor for " << rad
                                            << ": " << scale
                                            << endmsg ;

      }
      else
      {
        level = ICameraTool::ERROR;
        std::ostringstream mess;
        mess << "Failed to load histogram '" << radHistName << "'";
        warning() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
      }

    }  // Loop over radiators

    if ( m_createPDFsummary )
    {
      // close the PDF
      printCanvas("]");
      // if not  enough stats, remove the file
      if ( !keepPDF )
      {
        std::string mess = "Not enough stats. for calibration to run. Will remove PDF";
        info() << mess << endmsg;
        cameraTool()->Append("TEXT",mess);
        printCanvas("DELETE");
      }
    }

    // close the saveset file
    f->Close();

  }
  else
  {
    level = ICameraTool::ERROR;
    std::ostringstream mess;
    mess << "Failed to open '" << data.second << "'";
    warning() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());
  }

  delete f;

  // send camera message and gaudi message
  std::ostringstream mess;
  mess << type << " Refractive index calibration for Run " << runNumber;
  if ( ICameraTool::ERROR != level ) { mess << " was SUCCESSFUL" ; }
  else                               { mess << " FAILED"; }
  cameraTool()->SendAndClearTS( level, m_Name, mess.str() );
  if      ( ICameraTool::ERROR   == level ) { error() << mess.str() << endmsg; }
  else if ( ICameraTool::WARNING == level ) { info()  << mess.str() << endmsg; }
  else                                      { info()  << mess.str() << endmsg; }

}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode OMARichRefIndex::finalize()
{
  // clean up state listeners
  //finaliseDIMStates();
  // reset the cached histogram stats
  m_histStats.clear();
  // send a message to camera
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Finalized");
  // write to summary file
  writeToDimSummaryFile( m_Name + " Finalized" );
  return AnalysisTask::finalize();  // must be called after all other actions
}

//=============================================================================
// Fitting histogram
//=============================================================================
std::pair<bool,double>
OMARichRefIndex::fitCKThetaHistogram( TH1* hist,
                                      const std::string& rad,
                                      const unsigned int nPolFull )
{
  // which RICH ?
  const bool isR1 = ( "Rich1Gas" == rad );

  // delta for the pre-fit
  const double delta = ( isR1 ? 0.0025 : 0.00105 );

  // Firstly do a prefit
  double xPeak = hist->GetBinCenter( hist->GetMaximumBin() );
  // if peak is too far off from expectation, just use 0.0 as starting point
  if ( fabs(xPeak) > delta/5.0 ) { xPeak = 0.0; }

  // range for the pre-fit
  double fitMin = xPeak - delta;
  double fitMax = xPeak + delta;

  // Do the pre-fit
  const std::string preFitFName = rad +"PreFitF";
  TF1 preFitF( preFitFName.c_str(), "gaus", fitMin, fitMax );
  // Estimate peak from max bin
  preFitF.SetParameter( 1, xPeak );
  // rough guess at resolution. Does not need to be precise
  preFitF.SetParameter( 2, isR1 ? 0.0016 : 0.0007 );
  // run the pre-fit
  debug() << "Performing prefit using " << preFitFName << endmsg;
  hist->Fit( &preFitF, "QRS0" );
  //hist->Fit( &preFitF, "RS0" );

  // Full fit
  bool fitOK = false;

  // Range for the final full fit, with background etc.
  if ( isR1 ) { fitMin = m_RichFitMin[0]; fitMax = m_RichFitMax[0]; }
  else        { fitMin = m_RichFitMin[1]; fitMax = m_RichFitMax[1]; }

  TF1 lastFitF = preFitF ; // save the last fit function
  TF1 bestFitF = preFitF ; // save the best fit function
  int bestNPol(0);
  for ( unsigned int nPol = 1; nPol < nPolFull+1; ++nPol )
  {
    const std::string fFitFName = rad + "fFitF" + std::to_string(nPol);
    const std::string fFitFType = "gaus(0)+pol"+std::to_string(nPol)+"(3)" ;

    TF1 fFitF( fFitFName.c_str(), fFitFType.c_str(), fitMin, fitMax );
    debug() << "Performing full fit using " << fFitFName << endmsg;

    // set the gaussian parameter names
    fFitF.SetParName(0,"Gaus Constant");
    fFitF.SetParName(1,"Gaus Mean");
    fFitF.SetParName(2,"Gaus Sigma");

    const unsigned int nParamsToSet = ( nPol > 1 ? 3 + nPol : 3 );
    debug() << "Setting " << nParamsToSet << " parameters from previous Pol"
            << bestNPol << " fit" << endmsg;
    for ( unsigned int p = 0; p < nParamsToSet; ++p )
    {
      const auto param = lastFitF.GetParameter(p);
      const auto name  = fFitF.GetParName(p);
      debug() << " -> Setting parameter " << name << " to " << param << endmsg;
      fFitF.SetParameter(p,lastFitF.GetParameter(p));
    }

    // run the fit
    hist->Fit( &fFitF, "MQRSE0" );
    //hist->Fit( &fFitF, "MRSE0" );

    // save the last fit
    lastFitF = fFitF;

    // Fit OK?
    fitOK = fitIsOK(fFitF,rad);
    if ( fitOK )
    {
      bestFitF = fFitF;
      bestNPol = nPol;
      std::ostringstream mess;
      mess << "Pol" << nPol << " fit is OK";
      cameraTool()->Append("TEXT",mess.str());
      debug() << mess.str() << endmsg;
    }
    else
    {
      if ( nPol == nPolFull )
      {
        // use last good fit
        std::ostringstream mess;
        mess << "Pol " << nPol << " fit failed.";
        cameraTool()->Append("TEXT",mess.str());
        debug() << mess.str() << endmsg;
        hist->Fit( &fFitF, "RSE0" ); // CRJ : Why do I redo this fit...
        fitOK = true ;
      }
      if ( nPol > 1 ) break ;
    }

  }

  // Add fitted functions to camera message
  if ( fitOK )
  {
    std::ostringstream mess;
    mess << "Final fit used Pol" << bestNPol << " for background";
    info() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());
    // temporary function to draw (seems needed with latest root...)
    TF1 drawFunc( "FinalFullFit",
                  ("gaus(0)+pol"+std::to_string(bestNPol)+"(3)").c_str(),
                  fitMin, fitMax );
    for ( int i = 0; i <= bestNPol+3; ++i )
    {
      const auto param = bestFitF.GetParameter(i);
      const auto err   = bestFitF.GetParError(i);
      const auto name  = bestFitF.GetParName(i);
      std::ostringstream m;
      m << "  " << name << " = " << param << " +- " << err;
      info() << m.str() << endmsg;
      cameraTool()->Append("TEXT",m.str());
      drawFunc.SetParameter(i,param);
      drawFunc.SetParName(i,name);
    }
    // Full fit
    cameraTool()->Append(&drawFunc);
    // Draw full fit
    drawFunc.SetLineColor(kBlue+2);
    hist->Draw();
    drawFunc.Draw("SAME");
    // Add scale factor
    const auto scale = nScaleFromShift(bestFitF.GetParameter(1),rad);
    TText text;
    text.SetNDC();
    text.SetTextSize(0.025);
    text.DrawText( 0.14, 0.840, ("Run "+std::to_string(m_runNumber)).c_str() );
    text.DrawText( 0.14, 0.814, ("SF  "+std::to_string(scale)).c_str() );
    // background only
    if ( bestNPol > 0 )
    {
      debug() << "Creating background function" << endmsg;
      TF1 backFunc( "Background", ("pol"+std::to_string(bestNPol)).c_str(),
                    fitMin, fitMax );
      for ( int i = 0; i <= bestNPol; ++i )
      {
        const auto param = bestFitF.GetParameter(i+3);
        const auto name  = bestFitF.GetParName(i+3);
        debug() << "  " << name << " = " << param << endmsg;
        backFunc.SetParameter(i,param);
        backFunc.SetParName(i,name);
      }
      cameraTool()->Append(&backFunc);
      // Draw background fit
      backFunc.SetLineColor(kRed+3);
      backFunc.Draw("SAME");
      // print canvas to file
      printCanvas();
    }
    else
    {
      // print canvas to file
      printCanvas();
    }
  }
  else
  {
    // print canvas to file
    printCanvas();
  }

  // Final sanity check on the fit results
  // limits on fit sigma and mean, to detect complete failures such as when there
  // is no Cherenkov signal at all
  const bool finalCheck = fitIsOK(bestFitF,rad);
  if ( !finalCheck ) { cameraTool()->Append("TEXT","Fit FAILED final sanity checks"); }
  fitOK &= finalCheck;

  // return final fit parameter for shift
  return std::make_pair( fitOK, fitOK ? bestFitF.GetParameter(1) : 0.0 );
}

//=============================================================================

bool OMARichRefIndex::fitIsOK( const TF1& fFitF,
                               const std::string& rad ) const
{
  // first check fit error
  bool fitOK = fFitF.GetParError(1) < m_maxErrorForOK ;
  if ( fitOK )
  {
    // which RICH ?
    const bool isR1 = ( "Rich1Gas" == rad );
    // Final sanity check on the fit results
    // limits on fit sigma and mean, to detect complete failures such as when there
    // is no Cherenkov signal at all
    const double maxFitMean  = ( isR1 ? 0.006 : 0.003 );
    const double maxFitSigma = ( isR1 ? 0.010 : 0.005 );
    fitOK &= fabs( fFitF.GetParameter(1) ) < maxFitMean;
    fitOK &= fabs( fFitF.GetParameter(2) ) < maxFitSigma;
  }
  // return the final status
  return fitOK;
}

//=============================================================================

double OMARichRefIndex::getLastRefIndexSF( const std::string& rad )
{
  std::string subDet ;
  if      ( "Rich1Gas" == rad ) { subDet = m_Rich1TaskName; }
  else if ( "Rich2Gas" == rad ) { subDet = m_Rich2TaskName; }

  const boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
  const boost::filesystem::path filename( m_RefIndexSFLog );
  const boost::filesystem::path full_path = dir/filename ;

  unsigned int run = 0 ;
  double scale = 1.0 ;

  if ( !boost::filesystem::exists(full_path) )  // meet this for the first run...
  {
    try
    {
      createDir( dir );
      std::ofstream logging( full_path.string().c_str() );
      if ( logging.is_open() )
      {
        logging << run << " " << std::setprecision(m_Precision) << scale << "\n";
        logging.close();
      }
      else
      {
        std::ostringstream mess;
        mess << "Failed to open " << full_path;
        error() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
        //Exception( mess.str() );
      }
    }
    catch ( const boost::filesystem::filesystem_error& expt )
    {
      std::ostringstream mess;
      mess << "Failed to create file " << full_path << " " << expt.what();
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
    }
    return scale ;
  }
  else
  {
    std::fstream file( full_path.string().c_str(), std::ios::in );
    if ( file.is_open() )
    {
      file >> run >> scale;
      file.close();
      std::ostringstream mess;
      mess << "Read from file scale factor " << scale << " for Run " << run;
      cameraTool()->Append("TEXT",mess.str());
    }
    else
    {
      std::ostringstream mess;
      mess << "Failed to open " << full_path;
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
    }
  }

  info() << "Read " << rad << " scale factor (" << scale << ") from previous run cache"
         << endmsg;

  return scale ;
}

//=============================================================================

bool OMARichRefIndex::updateRefIndexSF( const unsigned int runNumber,
                                        const std::string& rad,
                                        const double scale )
{
  bool OK = true;

  const std::string subDet = ( "Rich1Gas" == rad ? m_Rich1TaskName :
                               "Rich2Gas" == rad ? m_Rich2TaskName :
                               "UNKNOWN" );

  const boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
  const boost::filesystem::path filename( m_RefIndexSFLog );
  const boost::filesystem::path full_path = dir/filename ;

  try
  {

    createDir( dir );

    std::ofstream ofile( full_path.string().c_str() );
    if ( ofile.is_open() )
    {
      ofile << runNumber << " " << std::setprecision(m_Precision) << scale << "\n" ;
      ofile.close();
    }
    else
    {
      std::ostringstream mess;
      mess << "Failed to open " << full_path;
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
      OK = false;
    }

  }
  catch ( const boost::filesystem::filesystem_error& expt )
  {
    std::ostringstream mess;
    mess << "Failed to open file " << full_path << " " << expt.what();
    error() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());
    OK = false;
  }

  return OK;
}

//=============================================================================

bool OMARichRefIndex::xmlWriter( const std::string& type,
                                 const unsigned int runNumber,
                                 const double scale,
                                 const std::string& rad )
{
  // Get version
  const auto version  = setVersion( runNumber, rad );
  const auto versionS = std::to_string(version.second);

  bool OK = version.first;

  const bool isR1 = ( "Rich1Gas" == rad );
  const bool isR2 = ( "Rich2Gas" == rad );
  const bool isFinal = ( type == "Final EOR" || type == "Final" );
  bool scaleIsDifferent = false;

  std::string subDet ;
  if ( isR1 )
  {
    subDet = m_Rich1TaskName;
    m_Rich1PubString = std::to_string(runNumber) + " v" + versionS;
    scaleIsDifferent = m_Rich1RefIndex != scale;
    m_Rich1RefIndex  = scale;
  }
  else if ( isR2 )
  {
    subDet = m_Rich2TaskName;
    m_Rich2PubString = std::to_string(runNumber) + " v" + versionS;
    scaleIsDifferent = m_Rich2RefIndex != scale;
    m_Rich2RefIndex  = scale ;
  }

  const boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
  const boost::filesystem::path file( "v" + versionS + ".xml" );
  const boost::filesystem::path full_path = dir/file ;

  if ( msgLevel(MSG::DEBUG) ) debug() << "Writing to " << full_path << endmsg ;

  if ( !boost::filesystem::exists( full_path ) )
  {

    try
    {
      createDir( dir );

      std::ofstream logging( full_path.string().c_str() ) ;
      if ( logging.is_open() )
      {
        logging << "<condition name=\"RefractivityScaleFactor\"> \n" ;
        logging << "<param name=\"CurrentScaleFactor\" type=\"double\">"
                << std::setprecision(m_Precision) << scale
                << "</param> \n";
        logging << "</condition> \n" ;
        logging << " \n" ;
        logging.close();
        std::ostringstream mess;
        mess << "Successfully wrote " << rad << " Scale factor to " << full_path;
        cameraTool()->Append("TEXT",mess.str());
      }
      else
      {
        std::ostringstream mess;
        mess << "Failed to open " << full_path;
        error() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
        OK = false;
      }
    }
    catch ( const boost::filesystem::filesystem_error& expt )
    {
      std::ostringstream mess;
      mess << "Failed to open file " << full_path << " " << expt.what();
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
      OK = false;
    }

    // Publish to DIM
    if ( OK )
    {
      if ( m_pPublishSvc )
      {
        if      ( isR1 )
        {
          info() << "Publishing to DIM : "
                 << m_Rich1TaskName << " = '" << m_Rich1PubString << "'"
                 << endmsg;
          m_pPublishSvc->updateItem(m_Rich1TaskName);
          if ( isFinal && scaleIsDifferent )
          {
            info() << "Publishing to DIM : "
                   << m_Rich1ScaleTaskName << " = " << m_Rich1RefIndex
                   << endmsg;
            m_pPublishSvc->updateItem(m_Rich1ScaleTaskName);
          }
        }
        else if ( isR2 )
        {
          info() << "Publishing to DIM : "
                 << m_Rich2TaskName << " = '" << m_Rich2PubString << "'"
                 << endmsg;
          m_pPublishSvc->updateItem(m_Rich2TaskName);
          if ( isFinal && scaleIsDifferent )
          {
            info() << "Publishing to DIM : "
                   << m_Rich2ScaleTaskName << " = " << m_Rich2RefIndex
                   << endmsg;
            m_pPublishSvc->updateItem(m_Rich2ScaleTaskName);
          }
        }
      }
      std::ostringstream mess;
      mess << rad << " XML conditions published for Run " << runNumber;
      cameraTool()->Append("TEXT",mess.str());
      // update DIM summary file
      std::ostringstream dimmess;
      dimmess << "Run " << runNumber << " |";
      if ( isR1 ) { dimmess << " " << m_Rich1TaskName << " '" << m_Rich1PubString << "'"; }
      if ( isR2 ) { dimmess << " " << m_Rich2TaskName << " '" << m_Rich2PubString << "'"; }
      OK = writeToDimSummaryFile( dimmess.str() );
    }
    else
    {
      std::ostringstream mess;
      mess << rad << " XML conditions NOT published for Run " << runNumber;
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
    }

  }
  else // file already there
  {
    std::ostringstream mess;
    mess << "File " << full_path << " already there, something must be wrong";
    warning() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());
    OK = false;
  }

  return OK;
}
//=============================================================================

bool OMARichRefIndex::writeToDimSummaryFile( const std::string & mess ) const
{
  bool OK = true;
  // Open the summary file in append mode
  std::ofstream file( dimSummaryFile().c_str(), std::ios_base::app );
  if ( file.is_open() )
  {
    file << watermark() << " | " << mess << std::endl;
    file.close();
    //setPerms( dimSummaryFile() );
  }
  else
  {
    std::ostringstream m;
    m << "Problem writing DIM summary file " << dimSummaryFile();
    warning() << m.str() << endmsg;
    cameraTool()->Append("TEXT",m.str());
    OK = false;
  }
  return OK;
}

//=============================================================================

std::pair<bool,unsigned long long>
OMARichRefIndex::setVersion( const unsigned int runNumber,
                             const std::string& rad )
{
  bool OK = true;

  std::string subDet ;
  if      ( "Rich1Gas" == rad ) { subDet = m_Rich1TaskName; }
  else if ( "Rich2Gas" == rad ) { subDet = m_Rich2TaskName; }

  const boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
  const boost::filesystem::path filename( m_xmlVersionLog );
  const boost::filesystem::path full_path = dir/filename ;

  unsigned int run(0);
  unsigned long long version(0);

  if ( msgLevel(MSG::DEBUG) ) debug() << "Version file: "
                                      << full_path.string()
                                      << endmsg;

  if ( !boost::filesystem::exists(full_path) )  // the first time
  {
    try
    {
      version = 0 ;
      createDir(dir);
      std::ofstream logging( full_path.string().c_str() );
      if ( logging.is_open() )
      {
        logging << runNumber << " " << version << "\n";
        logging.close();
      }
      else
      {
        OK = false;
        std::ostringstream mess;
        mess << "Failed to open " << full_path;
        error() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
      }
    }
    catch ( const boost::filesystem::filesystem_error& expt )
    {
      std::ostringstream mess;
      mess << "Failed to open file " << full_path << " " << expt.what();
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
      OK = false;
    }
    return std::make_pair(OK,version);
  }
  else
  {
    std::fstream file( full_path.string().c_str(), std::ios::in | std::ios::out );
    if ( file.is_open() )
    {
      std::istringstream lastLine( getLastLine(file) );

      lastLine >> run >> version ;

      // increase version number
      ++version;

      file << runNumber << " " << version << "\n" ;
      file.close();
    }
    else
    {
      OK = false;
      std::ostringstream mess;
      mess << "Failed to open " << full_path;
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
    }
  }

  //setPerms( full_path );

  std::ostringstream mess;
  mess << "Setting version: " << version << " for run: " << runNumber;
  if ( msgLevel(MSG::DEBUG) ) debug() << mess.str() << endmsg ;
  //cameraTool()->Append("TEXT",mess.str());

  return std::make_pair(OK,version) ;
}

//=============================================================================

// Get the last line from an input file stream
std::string OMARichRefIndex::getLastLine(std::fstream& in)
{
  std::fstream::pos_type pos = in.tellg();

  std::fstream::pos_type lastPos;
  while ( in >> std::ws && ignoreline(in, lastPos) )
  { pos = lastPos; }

  in.clear();
  in.seekg(pos);

  std::string line;
  std::getline(in, line);
  return line;
}

//=============================================================================

void OMARichRefIndex::printCanvas( const std::string& tag ) const
{

  const std::string imageType = "pdf";

  // get the PDF file name for this run
  auto& pdfFile = m_pdfFile[m_runNumber];

  // Opening file ?
  if ( "[" == tag )
  {

    try
    {

      // If file name is not empty (so repeat fit for a run), delete file
      if ( !pdfFile.empty() ) { removeFile(pdfFile); }

      // Directory to save summaries to
      const boost::filesystem::path dir( m_xmlFilePath +
                                         "/RichCalibSummaries/" +
                                         getDateString() + "/RefIndex/" ) ;

      // Create directory if it does not exist
      if ( !boost::filesystem::exists(dir) )
      { boost::filesystem::create_directories(dir); }

      // File name
      const boost::filesystem::path filename( "Run-" +
                                              std::to_string(m_runNumber) +
                                              "." + imageType );
      const boost::filesystem::path full_path = dir/filename ;

      // If PDF exists, remove before remaking
      removeFile(full_path);

      // cache image file name for this run
      pdfFile = full_path.string();

      // Make a new canvas
      deleteCanvas();

      // Open new PDF
      canvas()->Print( ( pdfFile + tag ).c_str(), imageType.c_str() );

      // set watermark string
      m_watermark = watermark();
      info() << "Watermark = " << m_watermark << endmsg;

    }
    catch ( const boost::filesystem::filesystem_error& expt )
    {
      std::ostringstream mess;
      mess << "Failed to create file " << pdfFile << " " << expt.what();
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
      deleteCanvas();
      pdfFile = "";
    }

  }
  // Closing the file ?
  else if ( "]" == tag )
  {

    // Close the file
    canvas()->Print( ( pdfFile + tag ).c_str(), imageType.c_str() );

    // Set group write permissions
    //setPerms( pdfFile );

    // delete canvas
    deleteCanvas();

    // send a message
    std::ostringstream mess;
    mess << "Created  " << pdfFile;
    info() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());

    // limit the PDF map to (last) 10 entries
    while ( m_pdfFile.size() > 10 )
    { m_pdfFile.erase( m_pdfFile.begin() ); }

  }
  else if ( "" == tag )
  {

    // Add time/date watermark
    canvas()->cd();
    TText text;
    text.SetNDC();
    text.SetTextSize(0.012);
    text.SetTextColor(13);
    text.DrawText( 0.01, 0.01, m_watermark.c_str() );

    // Just print
    canvas()->Print( pdfFile.c_str(), imageType.c_str() );

  }
  else if ( "DELETE" == tag && !pdfFile.empty() )
  {

    try { removeFile( pdfFile ); }
    catch ( const boost::filesystem::filesystem_error& expt )
    {
      std::ostringstream mess;
      mess << "Failed to delete file " << pdfFile << " " << expt.what();
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
    }

  }

}

//=============================================================================

void OMARichRefIndex::fixFilePermissions() const
{
  using namespace boost::filesystem;
  const path dir( m_xmlFilePath + "/RichCalibSummaries" );
  recursive_directory_iterator end_itr;
  for ( recursive_directory_iterator i(dir); i != end_itr; ++i )
  { setPerms( i->path() ); }
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( OMARichRefIndex )

//=============================================================================
