#ifndef OMARICHHPDIMAGE_H 
#define OMARICHHPDIMAGE_H 1

// from Gaudi 
#include "DetDesc/Condition.h"
#include "GaudiKernel/IUpdateManagerSvc.h"

// Include files
// from OMALib
#include "OMAlib/AnalysisTask.h"

// Publish to DIM
#include "GaudiKernel/IPublishSvc.h"
#include "GaudiKernel/ServiceLocatorHelper.h"

// RICH
#include "RichDet/DeRichSystem.h"
#include "RichHPDImageAnalysis/HPDFit.h"

// ROOT includes 
#include <TFile.h> 
#include <TH1.h> 
#include <TH2.h> 
#include <TProfile.h>
#include <TF1.h>
#include <TMath.h>
#include <TSystem.h>

#include <boost/filesystem.hpp>
#include <fstream>      // std::ofstream
#include <iostream>

/** @class OMARichHPDImage OMARichHPDImage.h
 *  
 *
 *  @author Jibo He
 *  @date   2014-04-14
 */
class OMARichHPDImage : public AnalysisTask 
{

public: 

  /// Standard constructor
  OMARichHPDImage( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~OMARichHPDImage( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization
  virtual StatusCode analyze   (std::string& SaveSet, 
                                std::string Task); ///< Algorithm analyze

protected:

private:

  //=============================================================================
  // Function to get run number from fileName, rely on the name convention here:
  // https://lbtwiki.cern.ch/bin/view/Online/MonitoringExpertGuide#Savesets 
  //=============================================================================
  inline unsigned int getRunNumber( std::string fileName )
  {
    std::string firstName = fileName.substr(fileName.find_first_of("-")+1);
    std::string runString = firstName.substr(0, firstName.find_first_of("-"));
    
    unsigned int runNumber = atoi( runString.c_str() );
    
    return runNumber;
  }  

  //=============================================================================
  // Function to get SiSensor anlignment 
  //=============================================================================
  inline Condition* getSiSensorAlignment( const Rich::DAQ::HPDCopyNumber& copyNumber)
  {
    const LHCb::RichSmartID smartID = m_RichSys->richSmartID( copyNumber );

    std::string subDet;
    if( smartID.rich() == Rich::DetectorType::Rich1 ) subDet = "Rich1";
    else subDet = "Rich2";
    
    std::string type = "SiSensor";

    std::string alignLoc = "/dd/Conditions/Alignment/"+subDet+"/"+type+ std::to_string( copyNumber.data() )+"_Align";
   
    Condition *myCond = get<Condition>( detSvc(), alignLoc );
    
    return myCond;
    
  }

  //=============================================================================  
  // Functions to get the last line
  //=============================================================================
  inline std::istream& ignoreline(std::fstream& in, std::fstream::pos_type& pos)
  {
    pos = in.tellg();
    return in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }

  inline std::string getLastLine(std::fstream& in)
  {
    std::fstream::pos_type pos = in.tellg();
    
    std::fstream::pos_type lastPos;
    while (in >> std::ws && ignoreline(in, lastPos))
      pos = lastPos;
    
    in.clear();
    in.seekg(pos);
    
    std::string line;
    std::getline(in, line);
    return line;
  }

  //=============================================================================  
  // Function to set version number
  //=============================================================================
  inline unsigned int setVersion( unsigned int runNumber, std::string subDet )
  {

    boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
    boost::filesystem::path filename( m_xmlVersionLog );
    boost::filesystem::path full_path = dir/filename ;

    unsigned int run, version; 

    if ( msgLevel(MSG::DEBUG) ) debug() << "Version file: " 
                                        << full_path.string() 
                                        << endmsg;

    if( !boost::filesystem::exists( full_path ) )  // the first time
    {
      version = 0 ;
      boost::filesystem::create_directories( dir ) ;
      std::ofstream logging ;
      logging.open( full_path.string().c_str() );
      logging << runNumber << " " << version <<"\n";
      logging.close(); 
      
      return version ;

    } 
    else 
    {
      std::fstream file( full_path.string().c_str(), std::ios::in | std::ios::out );      

      std::istringstream lastLine( getLastLine(file) );

      lastLine >> run >> version ;

      // increase version number 
      version ++ ;
      
      file << runNumber << " " << version << "\n" ;
      file.close();
    }

    if ( msgLevel(MSG::DEBUG) ) debug() << "Setting version: " << version  
					<< " for run: " << runNumber
					<< endmsg ;	
    
    return version ;     
  }
  

  //=============================================================================
  // Function to get version of xml files
  //=============================================================================
  inline unsigned int getVersion( unsigned int runNumber, 
                                  const Rich::DAQ::HPDCopyNumber& copyNumber )
  {

    std::string subDet ;
    
    const LHCb::RichSmartID smartID = m_RichSys->richSmartID( copyNumber );
    if     ( smartID.rich() == Rich::DetectorType::Rich1 ) subDet = m_Rich1TaskName;    
    else if( smartID.rich() == Rich::DetectorType::Rich2 ) subDet = m_Rich2TaskName;

    boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
    boost::filesystem::path filename( m_xmlVersionLog );
    boost::filesystem::path full_path = dir/filename ;

    unsigned int run, version; 

    if( !boost::filesystem::exists( full_path ) )  // the first time
    {

      warning() << "Refactive index calibration didn't run!?" << endmsg;
      version = 0 ;
      boost::filesystem::create_directories( dir ) ;
      std::ofstream logging ;
      logging.open( full_path.string().c_str() );
      logging << runNumber << " " << version <<"\n";
      logging.close(); 
      
      return version ;

    } 
    else 
    {
      std::fstream file( full_path.string().c_str(), std::ios::in );

      std::istringstream lastLine( getLastLine(file) );

      lastLine >> run >> version ;
      file.close();
    }

    if( runNumber!= run ) error() << "HPD image xml version doesn't match that of ref index, something very wrong!" 
                                  << endmsg ;

    if ( msgLevel(MSG::DEBUG) ) debug() << "Getting version: " << version  
                                        << " for run: " << runNumber
                                        << endmsg ;     
    
    return version ;     
  }

  //=============================================================================
  // Function to get version of xml files
  //=============================================================================
  inline unsigned int getVersion( unsigned int runNumber, std::string subDet )
  {
    
    boost::filesystem::path dir( m_xmlFilePath + "/"+ subDet ) ;
    boost::filesystem::path filename( m_xmlVersionLog );
    boost::filesystem::path full_path = dir/filename ;

    unsigned int run, version; 

    if( !boost::filesystem::exists( full_path ) )  // the first time
    {

      warning() << "Refactive index calibration didn't run!?" << endmsg;
      version = 0 ;
      boost::filesystem::create_directories( dir ) ;
      std::ofstream logging ;
      logging.open( full_path.string().c_str() );
      logging << runNumber << " " << version <<"\n";
      logging.close(); 
      
      return version ;

    } 
    else 
    {
      std::fstream file( full_path.string().c_str(), std::ios::in );

      std::istringstream lastLine( getLastLine(file) );

      lastLine >> run >> version ;
      file.close();
    }

    if( runNumber!= run ) error() << "HPD image xml version doesn't match that of ref index, something very wrong!" 
                                  << endmsg ;

    if ( msgLevel(MSG::DEBUG) ) debug() << "Getting version: " << version  
                                        << " for run: " << runNumber
                                        << endmsg ;     
    
    return version ;     
  }



  //=============================================================================
  // Function to write out xml file for image shift
  //=============================================================================
  inline bool xmlWriter( Condition* myCond, 
                         const Rich::DAQ::HPDCopyNumber& copyNumber, 
                         unsigned int version )
  {
    std::string subDet ;
    const LHCb::RichSmartID smartID = m_RichSys->richSmartID( copyNumber );
    if     ( smartID.rich() == Rich::DetectorType::Rich1 ) subDet = m_Rich1TaskName;
    else if( smartID.rich() == Rich::DetectorType::Rich2 ) subDet = m_Rich2TaskName;

    boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
    boost::filesystem::path file( "v" + std::to_string(version) + ".xml" );
    boost::filesystem::path full_path = dir/file ;
 
    if ( msgLevel(MSG::DEBUG) ) debug() << "Writing image shift to " << full_path << endmsg ;

    std::ofstream logging;
 
    if( !boost::filesystem::exists( full_path ) ) {  
      warning() << "Refactive index calibration didn't run!?" << endmsg;
      boost::filesystem::create_directories( dir ) ;
    }
    
    logging.open( full_path.string().c_str(), std::ios::app ) ;  // always in append mode
      
    logging << myCond->toXml("", false, m_Precision ) << "\n";
    logging << "\n" ;
    
    logging.close();
    
    return true;

  }
  
  //=============================================================================
  // Function to write out xml file for occupancy
  //=============================================================================
  inline bool writeHpdOccXml( std::vector<std::pair<unsigned int, double>>& hpdOccs, 
                              std::string subDet,
                              unsigned int runNumber )                               
  {    
    // Get version 
    unsigned int version = getVersion( runNumber, subDet );

    if( subDet == m_Rich1TaskName ) 
    {      
      m_Rich1PubString = std::to_string( runNumber ) + " v" + std::to_string(version); 
    } 
    else if( subDet == m_Rich2TaskName )  
    {
       m_Rich2PubString = std::to_string( runNumber ) + " v" + std::to_string(version); 
    }

    // sort firstly 
    std::sort( hpdOccs.begin(), hpdOccs.end() );
    
    std::string occS = "";
    
    for( std::vector<std::pair<unsigned int, double>>::iterator occ = hpdOccs.begin();  
         occ != hpdOccs.end(); occ++ )
    {
      if( occS!= "" ) occS += " " ;
      occS += std::to_string( as_int( (*occ).first ) ) + "/" + to_string_with_precision( (*occ).second ) ;
    }

    boost::filesystem::path dir( m_xmlFilePath + "/" +subDet ) ;
    boost::filesystem::path file( "v" + std::to_string(version) + ".xml" );
    boost::filesystem::path full_path = dir/file ;
 
    if ( msgLevel(MSG::DEBUG) ) debug() << "Writing occupancy to " << full_path << endmsg ;

    std::ofstream logging;
 
    if( !boost::filesystem::exists( full_path ) ) {      
      warning() << "Refactive index calibration and HPD image didn't run!?" << endmsg;
      boost::filesystem::create_directories( dir ) ;
    }
    
    logging.open( full_path.string().c_str(), std::ios::app ) ;  // always in append mode
      
    logging << "<condition classID=\"5\" name=\"AverageHPDOccupancies\">\n";
    logging << "<paramVector name=\"Occupancies\" type=\"std::string\" comment=\"Average HPD occupancy\">"
            << occS << "</paramVector>\n";
    logging << "</condition>\n";
    logging << "\n" ;
      
    logging.close();
    
    if (m_pPublishSvc) {
        info() << "Publish to DIM... " << endmsg; 
        m_pPublishSvc->updateAll();
    }
    info() << runNumber << ":" << full_path.string() << endmsg;

    return true;
    
  }
  
  inline int as_int(unsigned u) {
    return reinterpret_cast<int&>(u); 
  }

  template <typename T>
  std::string to_string_with_precision( const T a_value )
  {
    std::ostringstream out;
    out << std::setprecision(m_Precision) << a_value;
    return out.str();
  }
  

private:
  // task names
  std::string m_Rich2TaskName, m_Rich1TaskName;

  std::string m_EoRSignal;

  std::pair<unsigned int, std::string> m_mergedRun ;  // to store run number and file path 
  std::pair<unsigned int, std::string> m_lastRun ;    // to store run number and file path of last run

  std::string m_HistBase;
  std::string m_OccpHistBase;
  
  int m_minHPDID;
  int m_maxHPDID;

  unsigned int m_minEntries ;

  /// Pointer to Rich Sys Detector Element
  const DeRichSystem * m_RichSys;
  
  // Fit parameters
  Rich::HPDImage::HPDFit::Params m_params;
  
  // HPD Fitter 
  mutable Rich::HPDImage::HPDFit m_fitter;

  // Precision 
  unsigned int m_Precision;

  // xml file
  std::string m_xmlFilePath;
  std::string m_xmlFileName;

  std::string m_xmlVersionLog;

  // to publish 
  IPublishSvc* m_pPublishSvc;
  std::string m_Rich1PubString, m_Rich2PubString;

  // Together with Ref index
  bool m_withRefIndex ;

};
#endif // OMARICHHPDIMAGE_H
