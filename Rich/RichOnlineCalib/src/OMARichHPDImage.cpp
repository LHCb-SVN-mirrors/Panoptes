// Include files 

 // from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "OMARichHPDImage.h"

using namespace Rich::HPDImage;

//-----------------------------------------------------------------------------
// Implementation file for class : OMARichHPDImage
//
// 2014-04-14 : Jibo He
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( OMARichHPDImage )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
OMARichHPDImage::OMARichHPDImage( const std::string& name,
                                  ISvcLocator* pSvcLocator)
: AnalysisTask ( name , pSvcLocator ),
  m_mergedRun(0,""),
  m_lastRun(0,"")
{
  declareProperty("Rich1TaskName" , m_Rich1TaskName = "Rich1/HpdAlign" );
  declareProperty("Rich2TaskName" , m_Rich2TaskName = "Rich2/HpdAlign" );

  declareProperty("EoRSignal" ,  m_EoRSignal = "-EOR" ); // End of Run Signal  

  declareProperty("HistBase"      ,  m_HistBase     = "RICH/RichHPDImageSummary/" );   // Histogram location
  declareProperty("OccpHistBase"  ,  m_OccpHistBase = "RICH/HPDHitsMoni/PDs/NumHits/" );   // Occupancy Histogram location
  declareProperty("minHPDID"   , m_minHPDID =   0 );
  declareProperty("maxHPDID"   , m_maxHPDID = 484 );
  declareProperty("HPDFitType" , m_params.type = "Sobel" );
  declareProperty("minEntries" , m_minEntries = 10000 );
  
  declareProperty("Precision" , m_Precision = 8 );

  declareProperty("xmlFilePath"   ,  m_xmlFilePath  = "/group/online/alignment/" );
  declareProperty("xmlFileName"   ,  m_xmlFileName  = "results.xml" );
  declareProperty("xmlVersionLog" , m_xmlVersionLog = "version.txt" );

  declareProperty("withRefIndex"  , m_withRefIndex = false );

}
//=============================================================================
// Destructor
//=============================================================================
OMARichHPDImage::~OMARichHPDImage() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode OMARichHPDImage::initialize() {
  
  // Init things for this task 
  // must be run here
  m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );
  
  StatusCode sc = serviceLocator()->service("LHCb::PublishSvc", m_pPublishSvc, false);
  if (m_pPublishSvc) { 
     info() << "PublishSvc initialized " << endmsg;
     m_pPublishSvc->declarePubItem(m_Rich1TaskName, m_Rich1PubString );
     m_pPublishSvc->declarePubItem(m_Rich2TaskName, m_Rich2PubString );
  }

  sc = AnalysisTask::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by AnalysisTask

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode OMARichHPDImage::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  return StatusCode::SUCCESS;
}

StatusCode OMARichHPDImage::analyze (std::string& SaveSet, 
                                     std::string Task) 
{

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Analyze" << endmsg;
 
  // Mandatory: call default analyze method
  StatusCode sc = AnalysisTask::analyze(SaveSet, Task);
  if ( sc.isFailure() ) return sc;

  // Check whether it is the last file of one run
  // either with "-EOR" in file name, or run number changed 

  // Each time we do fit, the last run should be set to 0
  // so that it will be init'ed again
  // Fix me!? not do fit for the run withtout "-EOR" just before stopping the calib job... 

  std::string fileName = boost::filesystem::path( SaveSet.c_str() ).stem().string();

  if ( msgLevel(MSG::DEBUG) ) debug() << "Extracted file name: " << fileName 
                                      << ", Run: " << getRunNumber( fileName )
                                      << endmsg;

  if( fileName.find( m_EoRSignal ) != std::string::npos ) {

    m_mergedRun = std::pair<unsigned int, std::string>( getRunNumber( fileName ), SaveSet );
    m_lastRun   = std::pair<unsigned int, std::string>( 0, "") ;

  } // "-EOR" inside the file name  
  else
  {
    // if it is the first time to read a file without "-EOR" 
    // or still the same run
    if( m_lastRun.first ==0 || getRunNumber( fileName ) ==  m_lastRun.first )  
    { 
      m_lastRun = std::pair<unsigned int, std::string>( getRunNumber( fileName ), SaveSet );

      return StatusCode::SUCCESS;
    } // wait for new file
    else if( getRunNumber( fileName ) !=  m_lastRun.first ) // new Run, use previous file
    {
      m_mergedRun = m_lastRun;
      m_lastRun   = std::pair<unsigned int, std::string>( 0, "") ;
    }    
  }


  // Open Saveset 
  TFile *f = new TFile(m_mergedRun.second.c_str(),"READ");

  if( false == f->IsZombie() )
  {    

    unsigned int xmlVersion;

    // To store HPD occupancies 
    std::vector<std::pair<unsigned int, double>> Rich1_OccsVec, Rich2_OccsVec;
    std::pair<unsigned int, double> HpdOcc;

    // if not run together with the ref index job 
    // need to set the version number firstly 
    if(!m_withRefIndex) 
    {      
      xmlVersion = setVersion( m_mergedRun.first, m_Rich1TaskName );
      xmlVersion = setVersion( m_mergedRun.first, m_Rich2TaskName );
    } 
    else // if run together 
    {
      if( m_Rich1TaskName != "Rich1/Calib" ) 
      {
        m_Rich1TaskName = "Rich1/Calib";
        warning() << "Task name should be the same as Ref index task, changed it to: " 
                  << m_Rich1TaskName 
                  << endmsg;
      }
      if( m_Rich2TaskName != "Rich2/Calib" ) 
      {        
        m_Rich2TaskName = "Rich2/Calib";
        warning() << "Task name should be the same as Ref index task, changed it to: " 
                  << m_Rich2TaskName 
                  << endmsg;
      }      
    }

    // Loop over HPD
    for(int hpdID = m_minHPDID; hpdID< m_maxHPDID; ++hpdID)
    {
      // HPD copy number 
      const Rich::DAQ::HPDCopyNumber copyNumber =  Rich::DAQ::HPDCopyNumber( hpdID );
      
      // SmartID
      const LHCb::RichSmartID smartID = m_RichSys->richSmartID( copyNumber );

      if ( msgLevel(MSG::DEBUG) ) debug() << "HPD ID " << hpdID
                                          << ", smartID "  << smartID.toString()
                                          << ", smartID.key() " << smartID.key() 
                                          << endmsg;
      // Get condition
      Condition* siAlign = getSiSensorAlignment( copyNumber );

      // set occupancy to 0 
      HpdOcc.first  = smartID.key() ;
      HpdOcc.second = 0 ;

      // Get image histogram 
      std::string imageHistName = m_HistBase + "Rich_HPD_" + std::to_string( hpdID ) + "_Image";
      TH2D* imageHist = (TH2D*)f->Get( imageHistName.c_str() );

      // Get occupancy histogram
      std::string occpHistName = m_OccpHistBase + "CopyNum-" + std::to_string( hpdID ) ;
      TH1D* occpHist = (TH1D*)f->Get( occpHistName.c_str() );

      //======================================
      // Update occupany firstly 
      //======================================
      if( occpHist ) 
      {
        HpdOcc.first  = smartID.key() ;
        HpdOcc.second = occpHist->GetMean() ;
      } 

      if( smartID.rich() == Rich::DetectorType::Rich1 ) Rich1_OccsVec.push_back( HpdOcc );
      if( smartID.rich() == Rich::DetectorType::Rich2 ) Rich2_OccsVec.push_back( HpdOcc );

      //======================================
      // Now HPD image shift 
      //======================================
      if( !imageHist )  // check image hist is there 
      {
        warning() << "Failed to get HPD image histogram: " << imageHistName
                  << endmsg ;
      }
      else if( !occpHist ) // check occupancy hist is there 
      {
        warning() << "Failed to get HPD image occupancy histogram: " << occpHistName
                  << endmsg ;
      }  
      else if( imageHist->GetEntries() < m_minEntries )  // check whether there is enough entries
      {
        warning() << "Not enough HPD image entries: " << imageHist->GetEntries() << endmsg ;
      }
      else // both hists are there, and enough entries, then do fit and update condition 
      {  
        // Do the fit 
        const HPDFit::Result result = m_fitter.fit( *imageHist, m_params, int( occpHist->GetEntries() ) );
        
        if ( msgLevel(MSG::DEBUG) ) debug() << "In Fit, shift of " << hpdID 
                                            << ": (" << result.x() << ", "  << result.y() << ")"
                                            << ", Radius " << result.radInMM() 
                                            << " entries  "  << imageHist->Integral()
                                            << " occupancy " << occpHist->GetEntries()
                                            << endmsg;       

        // Update condition
        std::string paramName = "dPosXYZ" ;
        std::vector<double> dPosVec = siAlign->paramAsDoubleVect( paramName );

        if ( msgLevel(MSG::DEBUG) ) debug() << "In DB, shift of " << hpdID 
                                            << ": " <<  dPosVec 
                                            << endmsg ;
                
        dPosVec[0] = result.x();
        dPosVec[1] = result.y();
        dPosVec[2] = 0 ;
        
        siAlign->addParam( paramName, dPosVec, "");

        if ( msgLevel(MSG::DEBUG) ) debug() << "Condition to write out " << siAlign->toXml()                                     
                                            << endmsg;

      }
      
      // Now write out xml file
      xmlVersion = getVersion( m_mergedRun.first, copyNumber );
      if( !xmlWriter( siAlign, copyNumber, xmlVersion ) ) warning() << "Failed to write xml file for HPD " << hpdID << endmsg;
        
    }  //Loop over HPD

    // Now write out HPD occupancies to xml file
    if( !writeHpdOccXml( Rich1_OccsVec, m_Rich1TaskName, m_mergedRun.first ) ) 
      warning() << "Failed to write xml file for Rich1 Occupancies" << endmsg;

    if( !writeHpdOccXml( Rich2_OccsVec, m_Rich2TaskName, m_mergedRun.first ) )
      warning() << "Failed to write xml file for Rich2 Occupancies" << endmsg;       
        
    // empty vectors, just for safety
    Rich1_OccsVec.clear();
    Rich2_OccsVec.clear();

    f->Close();
  
  } else {
    warning() << m_mergedRun.second.c_str() << " is zombie "
              << endmsg;
  }

  delete f;
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode OMARichHPDImage::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return AnalysisTask::finalize();  // must be called after all other actions
}

//=============================================================================
