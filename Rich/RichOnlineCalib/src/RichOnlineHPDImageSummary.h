#ifndef RICHHPDIMAGEONLINESUMMARY_H
#define RICHHPDIMAGEONLINESUMMARY_H 1

// Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiUtils/Aida2ROOT.h"

// DIM
#include "dis.hxx"
#include "dic.hxx"
#include "RTL/rtl.h"

// Publish to DIM
#include "GaudiKernel/IPublishSvc.h"
#include "GaudiKernel/ServiceLocatorHelper.h"

// Base class
#include "RichHPDImageAnalysis/RichHPDImageSummaryBase.h"

// CAMERA
#include "Camera/ICameraTool.h"

// RunChange incident.
#include "DetDesc/RunChangeIncident.h"

// Rich Dim Objects
#include "RichMonitoringTools/RichDimClasses.h"

// RICHKernel
#include "RichKernel/RichHPDIdentifier.h"

// Boost
#include <boost/filesystem.hpp>
#include "boost/lexical_cast.hpp"

// STL
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <ctime>
#include <memory>

// ROOT
#include <TH1.h> 
#include <TH2.h> 
#include <TCanvas.h>
#include <TEllipse.h>
#include <TText.h>

// local
#include "rootstyle.h"

namespace Rich
{

  /** @namespace HPDImage
   *
   *  General namespace for RICH HPD image analysis code
   *
   *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
   *  @date   02/03/0211
   */
  namespace HPDImage
  {

    /** @class OnlineSummary RichHPDImageOnlineSummary.h
     *
     *  Monitors the HPD image movements within an HPD
     *
     *  @author Chris Jones Christopher.Rob.Jones@cern.ch
     *  @date   2010-03-16
     */
    class OnlineSummary : public SummaryBase,
                          //public Rich::Mon::CheckLHCState,
                          virtual public IIncidentListener
    {

    public:

      /// Standard constructor
      OnlineSummary( const std::string& name, ISvcLocator* pSvcLocator );

      /// Destructor
      virtual ~OnlineSummary( ); 

      virtual StatusCode initialize(); ///< Algorithm initialization
      virtual StatusCode execute();    ///< Algorithm execute
      virtual StatusCode finalize();   ///< Algorithm finalization

    private:

      /// Condition cache
      using CondCache = std::map< LHCb::RichSmartID, std::pair<double,std::string> >;

      /// Type for HPD histograms
      using PDHistograms = std::pair< std::unique_ptr<TH1D>, TH2D* >;

      /// Type for mapping between HPD and occupancy and image histograms
      using PD2Hists = std::map< LHCb::RichSmartID, PDHistograms >;

      /// Type for saving occupancy data
      using HPDOccData = std::vector< std::pair<LHCb::RichSmartID::KeyType, double> >;

    private:

      /// Storage for HPD fits to report in detail
      class HPDFitResult
      {
      public:
        HPDFitResult( const LHCb::RichSmartID& _id = LHCb::RichSmartID(0),
                      TH2D * image_h = nullptr,
                      const unsigned int _nEvents = 0 ) 
          : id(_id), imageHist(image_h), nEvents(_nEvents) { }
      public:
        LHCb::RichSmartID id; ///< The HPD ID
        TH2D * imageHist = nullptr; ///< The raw image histogram
        std::vector<std::string> messages; ///< List of messages attached to this HPD Fit
        HPDFit::Result fitResult; ///< The fit result
        unsigned int nEvents{0}; ///< Number of events in the fit
      public:
        using Vector = std::vector<HPDFitResult>;
      };

      /// Class to handle sending HPD fit information to PVSS via DIM
      class HPDFitDimData
      {
      private:
        /// Dasic struct to pass via DIM containing the fit (x,y) centre and radius
        struct ImageData { float x,y,r; }; 
        ImageData m_dimdata;
      public:
        /// Default constructor
        HPDFitDimData() { m_dimdata = { 0, 0, 0 }; }
        /// Constructor for a given HPD
        HPDFitDimData( const Rich::DAQ::HPDCopyNumber& copyNumber )
        {
          // Constructor DIM name
          std::ostringstream title;
          //title << Rich::Mon::get_utgid() << "/HPDImage" << copyNumber;
          title << "RichHPDImageMon/HPDImage" << copyNumber;
          // initialise the fit data
          m_dimdata = { 0, 0, 0 };
          // create DIM service
          m_dimS.reset( new DimService( title.str().c_str(), "F:3", 
                                        (void*)&m_dimdata, sizeof(m_dimdata) ) );
        }
      public:
        /// Update the info for this service
        void update( const float x, const float y, const float r )
        {
          if ( m_dimS )
          {
            m_dimdata = { x, y, r };
            m_dimS->updateService( );
          }
        }
      private:
        std::unique_ptr<DimService> m_dimS; ///< The DIM service object
      public:
        /// Map holding the DIM image data for all HPDs
        using Map = std::map< Rich::DAQ::HPDCopyNumber, std::unique_ptr<HPDFitDimData> >;
      };

    private:

      /// Check HPD image fit result is OK
      bool isOK( HPDFitResult& sHPD ) const;

      /// Get Occupancy and image histograms for given smartID
      PDHistograms& getHists( const LHCb::RichSmartID& smartID );

      /// get the HPD DIM Fit data object
      HPDFitDimData * getHPDFitDimData( const Rich::DAQ::HPDCopyNumber & copyNumber );
      
      // Fill HPD histograms 
      void fillHistograms();

      /// Incident callback
      virtual void handle( const Incident &incident );

      /// Load camera tool on demand
      inline ICameraTool * cameraTool() const
      {
        if ( !m_CamTool ) { m_CamTool = tool<ICameraTool>("CameraTool"); }
        return m_CamTool;
      }

      /// Run the HPD calibration ...
      void runCalibration( const std::string& type = "Final" );

      /// Method to determine if it is OK to publish the calibrations or not
      inline bool okToPublishCalibs() const { return m_okToPublish; }

      /// Method to determine if a PDF summary should be printed
      inline bool createPDFsummary() const 
      {
        return okToPublishCalibs() && m_createPDFsummary; 
      }
      
      // Method to check if a given RichSmartID is for an active RICH detector
      inline bool richIsActive( const LHCb::RichSmartID& smartID ) const
      {
        return ( !m_onlyOneRichPart ||
                 ( "RICH1" == m_partition && Rich::Rich1 == smartID.rich() ) ||
                 ( "RICH2" == m_partition && Rich::Rich2 == smartID.rich() ) );
      }
      
      //=============================================================================
      // Function to get SiSensor anlignment 
      //=============================================================================
      Condition* 
      getSiSensorAlignment( const LHCb::RichSmartID& smartID,
                            const Rich::DAQ::HPDCopyNumber& copyNumber) const;

      //=============================================================================  
      // Ignore a line in a file
      //=============================================================================
      inline std::istream& ignoreline(std::fstream& in, 
                                      std::fstream::pos_type& pos) const
      {
        pos = in.tellg();
        return in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      }
      
      //=============================================================================
      /// get the last line from an fstream file
      //=============================================================================
      std::string getLastLine(std::fstream& in) const;
      
      //=============================================================================  
      // Function to set version number
      //=============================================================================
      bool setVersion( const std::string& subDet );

      //=============================================================================
      // Function to get version of xml files
      //=============================================================================
      inline std::pair<bool,unsigned long long> 
      getVersion( const LHCb::RichSmartID& smartID ) const
      {
        return getVersion( smartID.rich() == Rich::DetectorType::Rich1 ?
                           m_Rich1TaskName : m_Rich2TaskName );
      }
      
      //=============================================================================
      // Function to get version of xml files
      //=============================================================================
      inline std::pair<bool,unsigned long long> 
      getVersion( const std::string& subDet ) const { return m_vCache[subDet]; }
  
      //=============================================================================
      // Function to write out xml file for image shift
      //=============================================================================
      bool xmlWriter( const std::string& condition, 
                      const LHCb::RichSmartID& smartID, 
                      const unsigned long long version );

      //=============================================================================
      // Function to write out xml file for image shift
      //=============================================================================
      inline bool xmlWriter( Condition * siAlign, 
                             const LHCb::RichSmartID& smartID,
                             const unsigned long long version )
      {
        return xmlWriter( siAlign->toXml("",false,m_Precision), smartID, version );
      }
      
      //=============================================================================
      // Function to write out xml file for occupancy
      //=============================================================================
      bool writeHpdXml( HPDOccData& hpdOccs, 
                        LHCb::RichSmartID::Vector& inactHPDs,
                        const std::string& subDet );
  
      /// cast an unsigned int into an int
      inline int as_int(unsigned u) const { return reinterpret_cast<int&>(u); }

      template <typename T>
      inline std::string to_string_with_precision( const T& a_value ) const
      {
        std::ostringstream out;
        out << std::setprecision(m_Precision) << a_value;
        return out.str();
      }

      /// Update HPD Condition cache
      inline void updateConditionCache( const LHCb::RichSmartID& smartID,
                                        const Condition* myCond )
      {
        m_hpdCondCache[smartID].second = xmlString(myCond);
      }
      
      /// Update HPD Condition cache
      inline void updateConditionCache( const LHCb::RichSmartID& smartID,
                                        const double occ )
      {
        m_hpdCondCache[smartID].first = occ;
      }
      
      /// Read from the condition cache the HPD image condition
      std::string getCachedImageCondition( const LHCb::RichSmartID& smartID ) const;

      /// Read from the condition cache the HPD occupancy
      double getCachedOcc( const LHCb::RichSmartID& smartID ) const;

      /// Save HPD Condition Cache
      bool writeCachedConditions();

      /// Read HPD Condition Cache
      void readCachedConditions();

      /// Cache file location
      inline std::string cacheFile() const 
      {
        return m_xmlFilePath + "/" + m_hpdCacheFile;
      }

      /// Run stats file location
      inline std::string runStatsFile() const 
      {
        return m_xmlFilePath + "/" + m_runStatsFile;
      }

      /// Inactive HPD cache file
      inline std::string inactiveHPDFile() const
      {
        return m_xmlFilePath + "/" + m_inactiveHPDFile;
      }

      /// Get the path to write summaries to
      inline std::string summaryPath() const
      {
        const std::string dir( m_xmlFilePath + "/RichCalibSummaries/" + 
                               getDateString() + "/HPDImage/" );
        createDir(dir);
        return dir;
      }

      /// DIM summary file location
      inline std::string dimSummaryFile() const
      { 
        return summaryPath() + m_dimSummaryFile;
      }

      /// Append a histogram to a camera message with run number added to title
      template < class HIST >
      inline void sendToCamera( const HIST * hist,
                                const bool addRun = false ) const
      {
        if ( addRun )
        {
          // const cast ... needed for title hack ..
          HIST * nonc_h = const_cast<HIST*>(hist);
          // save title
          const std::string title = nonc_h->GetTitle();
          // update title with run number added
          std::ostringstream newtitle;
          newtitle << title << " | Run " << m_runNumber;
          nonc_h->SetTitle( newtitle.str().c_str() );
          // send to camera
          cameraTool()->Append( nonc_h );
          // reset title
          nonc_h->SetTitle( title.c_str() );
        }
        else
        {   
          cameraTool()->Append( hist );
        }
      }
      
      /// Send a fit result to camera
      inline void sendToCamera( const HPDFit::Result& fitResult ) const
      {
        if ( fitResult.OK() )
        {
          std::vector<float> ringP = { (float)fitResult.col(),
                                       (float)fitResult.row(),
                                       (float)fitResult.radInPix(),
                                       (float)fitResult.radInPix() };
          cameraTool()->Append( "TELLIPSE", &*ringP.begin(), 4*sizeof(float) );
        }
      }

      /// Si condition to XML string
      inline std::string xmlString( const Condition* myCond ) const
      {
        return myCond->toXml( "", false, m_Precision );
      }

      /// Reset the HPD image and Occ histograms
      void resetHistograms();

      /// Get the vector of HPD smartIDs to process
      const LHCb::RichSmartID::Vector& hpdSmartIDsToProcess() const;

      /// End of calibration reset
      inline void resetCalibCounters() 
      {
        m_nEventsSinceCalib = 0;      // reset event count for next time
        m_runNumber = 0;              // correctly determine the next run number
        m_timeLastCalib = time(nullptr); // reset the last calibration time to now
      }

      /// Print Canvas
      void printCanvas( const std::string& tag = "" ) const;

      /// Get year/month/day as a string
      inline std::string getDateString() const
      {
        // Get year/month/day
        const std::time_t t = std::time(nullptr);
        char mbstr[100];
        return ( std::strftime(mbstr,sizeof(mbstr),"%Y/%m/%d",std::localtime(&t)) ?
                 std::string(mbstr) : "" );
      }

      /// Get watermark string
      inline std::string watermark() const
      {
        const std::time_t t = std::time(nullptr);
        char mbstr[100];
        return ( std::strftime(mbstr,sizeof(mbstr),"%c",std::localtime(&t)) ?
                 std::string(mbstr) : "" );
      }

      /// Add run number to histogram title
      template < class HIST >
      inline void addRunInfo( const HIST * hist, const bool add = true ) const
      {
        if ( hist )
        {
          HIST * nonc_hist = const_cast<HIST*>(hist);
          // Cache original title without run number
          static std::map<const HIST*,std::string> titles;
          if ( titles.find(hist) == titles.end() ) { titles[hist] = hist->GetTitle(); }
          if ( add )
          {
            // update title with run number added
            std::ostringstream newtitle;
            newtitle << titles[hist] << " | Run " << m_runNumber;
            nonc_hist->SetTitle( newtitle.str().c_str() );
          }
          else
          {
            // reset without run number
            nonc_hist->SetTitle( titles[hist].c_str() );
          }
        }
      }

      /// Draw a histogram
      template < class HIST >
      inline void draw( HIST * hist,
                        const std::string& opt = "" ) const
      {
        hist->Draw(opt.c_str());
      }

      /// Draw an image fit
      void draw( const HPDFitResult& hpd ) const;

      /// Fix file permissions
      void fixFilePermissions() const;

      /// Set file permissions on a file
      template < class FILE >
      inline void setPerms( const FILE & file ) const
      {
        if ( boost::filesystem::exists(file) )
        {
          debug() << "Setting permissions for " << file << endmsg;
          using namespace boost::filesystem;
          try { permissions( file, add_perms|owner_write|group_write ); }
          catch ( const filesystem_error& expt )
          {
            std::ostringstream mess;
            mess << "Failed to set permissions for " << file << " " << expt.what();
            warning() << mess.str() << endmsg;
            cameraTool()->Append("TEXT",mess.str());
          }
        }
      }
      
      /// Remove a file
      template < class FILE >
      inline void removeFile( const FILE & file ) const
      {
        if ( boost::filesystem::exists(file) )
        {
          std::ostringstream mess;
          mess << "Removing " << file;
          info() << mess.str() << endmsg;
          cameraTool()->Append("TEXT",mess.str());
          boost::filesystem::remove(file);
        }
      }

      /// Create directory
      template < class DIR >
      inline void createDir( const DIR & dir ) const
      {
        if ( !boost::filesystem::exists(dir) ) 
        {
          std::ostringstream mess;
          mess << "Creating " << dir;
          info() << mess.str() << endmsg;
          cameraTool()->Append("TEXT",mess.str());
          boost::filesystem::create_directories(dir); 
        }
      }

      /// Access and create on demand the current TCanvas
      inline TCanvas * canvas() const
      {
        if ( !m_canvas ) { m_canvas = new TCanvas("HPDCanvas","HPDCanvas",1400,1400); }
        return m_canvas;
      }

      /// Delete the current TCanvas
      inline void deleteCanvas() const { delete m_canvas; m_canvas = nullptr; }

      /// Add a line to the DIM summary file
      bool writeCalibVersionsToDimSummaryFile() const;

      /// Write a message to the DIM summary file
      bool writeToDimSummaryFile( const std::string & mess ) const;

    private:

      /// The partition name
      std::string m_partition; 

      /// Algorithm name + partition
      std::string m_Name; 

      /// HPD Fitter
      mutable Rich::HPDImage::HPDFit m_fitter;

      /// Pointer to Rich Sys Detector Element
      const DeRichSystem * m_RichSys = nullptr;

      /// Raw Buffer Decoding tool
      const Rich::DAQ::IRawBufferToSmartIDsTool * m_SmartIDDecoder = nullptr;

      /// Map between HPD Smart ID and occupancy Histogram pointer
      PD2Hists m_hists;

      /// CAMERA reporting tool
      mutable ICameraTool * m_CamTool = nullptr; 

      /// Total overall number of events seen
      unsigned long long m_nEvts{0};

      /// Number of events seen since last calibration was run...
      unsigned long long m_nEventsSinceCalib{0};

      /// Total number of events in current run
      unsigned long long m_nEventsThisRun{0};

      /// Total number of hits collected in the current run
      unsigned long long m_nHitsThisRun{0};

      /// The time the last calibration was run
      time_t m_timeLastCalib{0};

      /// Time interval to send periodic calibration updates
      int m_minTimeBetweenCalibs;

      /// Minimum number of events for a calibration
      unsigned long long m_minEventsForCalib;

      /// Minimum number of events to update the inactive HPD lists
      unsigned long long m_minEventsForInactiveHPDs;

      /// Average number of hits to assume to calculate min number of inactive list update.
      unsigned long long m_avTotalHitsForInactiveHPDs;

      /// Minimum average HPD occupancy to update the inactive HPD lists
      double m_minAvHPDOccForInactiveHPDs;

      /// Flag to turn on/off the saving of summary PDF files
      bool m_createPDFsummary;

      /// Flag to turn on/off the printing of each HPD to the PDF file
      bool m_printAllHPDs;

      /// Min OK HPD fits for PDF to be produced
      unsigned int m_minOKFitsForPDF;

      /// Max Strange HPD fits for PDF to be produced
      unsigned int m_maxStrangeFitsForPDF;

      /// Min summary histogram entries for PDF to be produced
      unsigned int m_minSumHistEntriesForPDF;

      /// run number for last processed run
      unsigned int m_runNumber{0};

      /// RICH1 task name
      std::string m_Rich2TaskName;

      /// RICH2 task name
      std::string m_Rich1TaskName;
      
      /// Minimum number of entries in HPD image plots for fit
      unsigned int m_minImageEntries ;

      /// Minimum number of entries in HPD occ plots
      unsigned int m_minOccEntries ;

      /// Maximum fraction of HPDs for inactive list
      double m_maxFracHPDsForInactiveList;

      /// Precision for XML
      unsigned int m_Precision ;

      // xml file path info
      std::string m_xmlFilePath;
      std::string m_xmlVersionLog;
      
      /// Publish Service
      IPublishSvc* m_pPublishSvc = nullptr;

      /// Service name for Rich1
      std::string m_Rich1PubString;

      /// Service name for Rich2
      std::string m_Rich2PubString;

      /// Turn on/off DIM publishing
      bool m_disableDIMpublish;

      /// Image shift (x,y) summary histogram
      TH2D * m_imageShiftSummaryHist = nullptr;

      /// Image shift (x,y) error summary histogram
      TH2D * m_imageShiftErrSummaryHist = nullptr;

      /// HPD occupancy summary histogram
      TH1D * m_hpdOccSummaryHist = nullptr;

      /// Image shift Radius summary histogram
      TH1D * m_imageRadiusSummaryHist = nullptr;

      /// Cache of last good HPD conditions
      CondCache m_hpdCondCache;

      /// Cached inactive HPD lists
      std::map< std::string, std::string > m_inactiveHPDCache;

      /// Cache file location
      std::string m_hpdCacheFile;

      /// Run stats. file location
      std::string m_runStatsFile;

      /// Inactive HPD cache file
      std::string m_inactiveHPDFile;

      /// DIM Summary file name
      std::string m_dimSummaryFile;

      /// HPD image fit max position for 'strange HPD' report
      double m_maxXYDeviation;

      /// HPD image fit min,max radius for 'strange HPD' report
      std::pair<double,double> m_minMaxRadius;

      /// HPD image fit min,max radius to fail a fit
      std::pair<double,double> m_minMaxRadiusFail;

      /// Max number of 'strange' HPD reports to send for a given calibration
      unsigned int m_maxStrangeHPDReports;

      /// Max number of OK HPD reports to send for a given calibration
      unsigned int m_maxOKHPDReports;

      /** Flag to turn on forcing the running of the HPD calibrations 
       *  regardless of the LHC State. Useful for offline debug running */
      bool m_alwaysRunCalib;

      /// Send the image fit data to DIM ?
      bool m_sendImageDataToDim;

      /// Map holding the DIM image data for all HPDs
      HPDFitDimData::Map m_dimDataMap;

      /** List of all previously calibrated runs for the current process
       *  with the number of events used for that calibration */
      std::map<unsigned int,unsigned long long> m_processedRuns;

      /// Cache the calibration version results for each Rich for speed ...
      mutable std::map< std::string, std::pair<bool,unsigned long long> > m_vCache;

      /** Count the number of calibrations submitted in a row based purely
       *  on cached information (so no HPDs with any hits) */
      unsigned int m_cachedCalibCount{0};

      /// maximum cached calibrations to allow in a row
      unsigned int m_maxCachedCalibs;

      /// cache if this monitor is running on only one RICH detector
      bool m_onlyOneRichPart{false};

      /// cache if it is OK to publish calibrations
      bool m_okToPublish{true};

      /// Current TCanvas for printing
      mutable TCanvas * m_canvas = nullptr;
      
      /// Current PDF file name
      mutable std::string m_pdfFile = "";

      /// run number for current PDF file
      mutable unsigned int m_runNumberPdfFile{0};

      /// Watermark string
      mutable std::string m_watermark;

    };
    
    //=============================================================================
    
    inline Condition*
    OnlineSummary::getSiSensorAlignment( const LHCb::RichSmartID& smartID,
                                         const Rich::DAQ::HPDCopyNumber& copyNumber)
      const
    {
      const std::string subDet = 
        ( smartID.rich() == Rich::DetectorType::Rich1 ? "Rich1" : "Rich2" );     
      const std::string alignLoc = ( "/dd/Conditions/Alignment/" + subDet + "/SiSensor" +
                                     std::to_string(copyNumber.data()) + "_Align" );
      return get<Condition>( detSvc(), alignLoc );
    }
    
    //=============================================================================
    
    inline double 
    OnlineSummary::getCachedOcc( const LHCb::RichSmartID& smartID ) const
    {
      const auto iFindCache = m_hpdCondCache.find(smartID);
      return ( iFindCache == m_hpdCondCache.end() ? 0.0 :
               iFindCache->second.first );
    }
    
    //=============================================================================
    
    inline std::string 
    OnlineSummary::getCachedImageCondition( const LHCb::RichSmartID& smartID ) const
    {
      const auto iFindCache = m_hpdCondCache.find(smartID);
      return ( iFindCache == m_hpdCondCache.end() ? "" :
               iFindCache->second.second );
    }
    
    //=============================================================================

  } // namespae Mon

} // namespace Rich

//=============================================================================

#endif // RICHHPDIMAGEONLINESUMMARY_H
