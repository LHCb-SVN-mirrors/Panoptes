#ifndef OMARICHREFINDEX_H 
#define OMARICHREFINDEX_H 1

// from OMALib
#include "OMAlib/AnalysisTask.h"

// Publish to DIM
#include "GaudiKernel/IPublishSvc.h"
#include "GaudiKernel/ServiceLocatorHelper.h"

// ROOT includes 
#include <TFile.h> 
#include <TH1.h> 
#include <TH2.h> 
#include <TF1.h>
#include <TMath.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TText.h>

// Boost
#include <boost/filesystem.hpp>

// STL
#include <fstream>   
#include <iostream>
#include <ctime>
#include <map>
#include <unordered_map>

// CAMERA
#include "Camera/ICameraTool.h"

// Dim Objects
#include "RichMonitoringTools/RichDimClasses.h"

// local
#include "rootstyle.h"

/** @class OMARichRefIndex OMARichRefIndex.h
 *  
 *  Performs Refractive Index calibration from Brunel Savesets.
 *  
 *  @author Jibo He
 *  @author Chris Jones Christopher.Rob.Jones@cern.ch
 *  @date   2014-04-10
 */
class OMARichRefIndex : public AnalysisTask
                        //public Rich::Mon::CheckLHCState
{

public: 

  /// Standard constructor
  OMARichRefIndex( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~OMARichRefIndex( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization

  virtual StatusCode analyze( std::string& SaveSet, 
                              std::string Task ); ///< Algorithm analyze
private:

  //=============================================================================
  /// Run Calibration
  //=============================================================================
  void runCalibration( const std::string& type,
                       const std::pair<unsigned int,std::string>& data,
                       const bool publishCalib = true,
                       ICameraTool::MessageLevel level = ICameraTool::INFO );
  
  //=============================================================================
  /// Load camera tool on demand
  //=============================================================================
  inline ICameraTool * cameraTool() const
  {
    if ( !m_CamTool ) { m_CamTool = tool<ICameraTool>("CameraTool"); }
    return m_CamTool;
  }
  
  //=============================================================================
  /// Fit the histogram
  //=============================================================================
  std::pair<bool,double> fitCKThetaHistogram( TH1* hist, 
                                              const std::string& rad, 
                                              const unsigned int nPolFull );

  //=============================================================================
  // Check histogram fit result
  //=============================================================================
  bool fitIsOK( const TF1& fFitF,
                const std::string& rad ) const;

  //=============================================================================
  /// Function to get run number from fileName, rely on the name convention here:
  /// https://lbtwiki.cern.ch/bin/view/Online/MonitoringExpertGuide#Savesets 
  //=============================================================================
  inline unsigned int getRunNumber( const std::string& fileName )
  {
    const std::string firstName = fileName.substr(fileName.find_first_of("-")+1);
    const std::string runString = firstName.substr(0,firstName.find_first_of("-"));
    return atoi( runString.c_str() );
  }  

  //=============================================================================
  /// Function to check whether enough entries 
  //=============================================================================
  inline bool checkCKThetaStats( const TH1* hist )
  {
    return hist->GetEntries() >= m_minEntries;
  }
  
  //=============================================================================
  /// Function to calculate scale factor
  //=============================================================================
  inline double nScaleFromShift( const double& shift, const std::string& rad )
  {
    // the slope parameter for the given RICH
    const double slope = 
      ( "Rich1Gas" == rad ? m_RefIndexSlope[0] : m_RefIndexSlope[1] );
    // Calculate and return the scale factor
    return 1.0 + (shift*slope) ;
  }
  
  //=============================================================================
  // Function to get ref index scale from previous run 
  //=============================================================================
  double getLastRefIndexSF( const std::string& rad );

  //=============================================================================
  /// Function to update ref index scale 
  //=============================================================================
  bool updateRefIndexSF( const unsigned int runNumber, 
                         const std::string& rad,
                         const double scale );
  
  //=============================================================================
  /// Function to write out xml file 
  //=============================================================================
  bool xmlWriter( const std::string& type,
                  const unsigned int runNumber, 
                  const double scale,
                  const std::string& rad );
 
  //=============================================================================  
  /// Functions to get the last line
  //=============================================================================
  inline std::istream& ignoreline(std::fstream& in, std::fstream::pos_type& pos)
  {
    pos = in.tellg();
    return in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
  }

  /// Get the last line from an input file stream
  std::string getLastLine(std::fstream& in);

  //=============================================================================  
  /// Function to set version number
  //=============================================================================
  std::pair<bool,unsigned long long> setVersion( const unsigned int runNumber, 
                                                 const std::string& rad );

  //=============================================================================  
  /// Append a histogram to a camera message with run number added to title
  //=============================================================================  
  template < class HIST >
  inline void sendToCamera( HIST * hist, const unsigned int runNumber ) const
  {
    // save title
    const std::string title = hist->GetTitle();
    // update title with run number added
    std::ostringstream newtitle;
    newtitle << title << " | Run " << runNumber;
    hist->SetTitle( newtitle.str().c_str() );
    // send to camera
    cameraTool()->Append( hist, "E,P" );
    // reset title
    hist->SetTitle( title.c_str() );
  }

  /// Draw a histogram
  template < class HIST >
  inline void draw( HIST * hist,
                    const std::string& opt = "" ) const 
  {
    hist->Draw(opt.c_str()); 
  }
  
  /// Print Canvas
  void printCanvas( const std::string& tag = "" ) const;
  
  /// Get year/month/day as a string
  inline std::string getDateString() const
  {
    // Get year, month, day
    const std::time_t t = std::time(nullptr);
    char mbstr[100];
    return ( std::strftime(mbstr,sizeof(mbstr),"%Y/%m/%d",std::localtime(&t)) ?
             std::string(mbstr) : "" );
  }

  /// Get watermark string
  inline std::string watermark() const
  {
    const std::time_t t = std::time(nullptr);
    char mbstr[100];
    return ( std::strftime(mbstr,sizeof(mbstr),"%c",std::localtime(&t)) ?
             std::string(mbstr) : "" );
  }
  
  /// Fix file permissions
  void fixFilePermissions() const;

  /// Set file permissions on a file
  template < class FILE >
  inline void setPerms( const FILE & file ) const
  {
    if ( boost::filesystem::exists(file) )
    {
      debug() << "Setting permissions for " << file << endmsg;
      using namespace boost::filesystem;
      try { permissions( file, add_perms|owner_write|group_write ); }
      catch ( const filesystem_error& expt )
      {
        std::ostringstream mess;
        mess << "Failed to set permissions for " << file << " " << expt.what();
        warning() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
      }
    }
  }

  /// Remove a file
  template < class FILE >
  inline void removeFile( const FILE & file ) const
  {
    if ( boost::filesystem::exists(file) )
    {
      std::ostringstream mess;
      mess << "Removing " << file;
      info() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
      boost::filesystem::remove(file);
    }
  }
  
  /// Create directory
  template < class DIR >
  inline void createDir( const DIR & dir ) const
  {
    if ( !boost::filesystem::exists(dir) ) 
    {
      std::ostringstream mess;
      mess << "Creating " << dir;
      info() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
      boost::filesystem::create_directories(dir); 
    }
  }

  /// Access and create on demand the current TCanvas
  inline TCanvas * canvas() const
  {
    if ( !m_canvas ) 
    { m_canvas = new TCanvas("RefIndCanvas","RefIndCanvas",1200,900); }
    return m_canvas;
  }
  
  /// Delete the current TCanvas
  inline void deleteCanvas() const { delete m_canvas; m_canvas = nullptr; }

  /// Get the path to write summaries to
  inline std::string summaryPath() const
  {
    const std::string dir( m_xmlFilePath + "/RichCalibSummaries/" + 
                           getDateString() + "/RefIndex/" );
    createDir(dir);
    return dir;
  }

  /// DIM summary file location
  inline std::string dimSummaryFile() const
  {
    return summaryPath() + m_dimSummaryFile;
  }

  /// Write a message to the DIM summary file
  bool writeToDimSummaryFile( const std::string & mess ) const;

private:

  /// CAMERA reporting tool
  mutable ICameraTool * m_CamTool; 

  /// Algorithm name + partition
  std::string m_Name; 

  /// RICH1 task name
  std::string m_Rich1TaskName;
  
  /// RICH2 task name
  std::string m_Rich2TaskName;

  /// RICH1 name for n-1 scale factor DIM service
  std::string m_Rich1ScaleTaskName;
  
  /// RICH2 name for n-1 scale factor DIM service
  std::string m_Rich2ScaleTaskName;

  /// Signal to look for to signify an end of run calibration
  std::string m_EoRSignal;

  /// Flag to turn on the creation of a Preliminary calibration for each run
  bool m_createPrelimCalib;

  /// Store run number and file path 
  std::pair<unsigned int, std::string> m_mergedRun ;  

  /// Store run number and file path of last run
  std::pair<unsigned int, std::string> m_lastRun ;   

  /// run number for run being processed
  unsigned int m_runNumber;

  /// The ROOT file directory to use for the calibration (the Gaudi Monitor)
  std::string m_HistBase;

  /// The histogram ID to use
  std::string m_ResPlot;

  /// The radiators to calibration
  std::vector<std::string> m_rads;

  /// Min entries for a fit
  unsigned int m_minEntries;

  /// Polynominal to use for background shape
  unsigned int m_nPolFull;

  /// Make error for OK
  double m_maxErrorForOK;

  std::vector<double> m_RichFitMin;
  std::vector<double> m_RichFitMax;

  /// slope from the shift 
  std::vector<double> m_RefIndexSlope;

  /// Precision for XML file parameters
  unsigned int m_Precision;

  /// xml file path
  std::string m_xmlFilePath;

  /// log file to list all previous XML versions
  std::string m_xmlVersionLog;

  /// log file for previous scale factors
  std::string m_RefIndexSFLog;

  /// DIM Summary file name
  std::string m_dimSummaryFile;

  /// Publishing service
  IPublishSvc* m_pPublishSvc;

  /// String to publish for new calibrations for RICH1
  std::string m_Rich1PubString;
  
  /// String to publish for new calibrations for RICH2
  std::string m_Rich2PubString;  

  /// The RICH1 scale factor value to publish
  double m_Rich1RefIndex;
 
  /// The RICH2 scale factor value to publish
  double m_Rich2RefIndex;

  /// Flag to turn on/off publishing to DIM
  bool m_disableDIMpublish;

  // The stats for the last fits
  std::unordered_map<std::string, unsigned long long> m_histStats;

  /// Flag to turn on/off the saving of summary PDF files
  bool m_createPDFsummary;

  /// Current TCanvas for printing
  mutable TCanvas * m_canvas;
  
  /// PDF file for each run
  mutable std::map< unsigned int, std::string > m_pdfFile;

  /// Watermark string
  mutable std::string m_watermark;

  /// Are we during initialize ?
  bool m_inInit;

  /** Keep a count of the number of calibrations in a row, per radiator,
   *  for which a cached value from a previous run is used */
  std::unordered_map< std::string, unsigned int > m_cachedCalibCount;

  /// maximum cached calibrations to allow in a row
  unsigned int m_maxCachedCalibs;
  
};

#endif // OMARICHREFINDEX_H
