
// Local
#include "RichOnlineHPDImageSummary.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RichHPDImageOnlineSummary
// 2015-05-17 : Jibo He
// 2015-04-09 : Chris Jones
// 2010-03-16 : Thomas BLAKE
//-----------------------------------------------------------------------------

using namespace Rich::HPDImage;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================

OnlineSummary::OnlineSummary( const std::string& name,
                              ISvcLocator* pSvcLocator )
  : SummaryBase  ( name , pSvcLocator  ),
    m_Name       ( name                )
{
  // Partition name
  const char* partition = getenv("PARTITION");
  if ( partition )
  {
    m_partition = std::string(partition);
    m_Name     += m_partition;
    m_onlyOneRichPart = ( "RICH1" == m_partition ||
                          "RICH2" == m_partition );
    m_okToPublish = ( m_partition != "RICH"  &&
                      m_partition != "RICH1" &&
                      m_partition != "RICH2" &&
                      m_partition != "FEST"  );
  }

  // Job Options

  declareProperty("Rich1TaskName" , m_Rich1TaskName = "Rich1/HpdAlign" );
  declareProperty("Rich2TaskName" , m_Rich2TaskName = "Rich2/HpdAlign" );

  declareProperty("MinImageEntries" , m_minImageEntries = 10000 );
  declareProperty("MinOccEntries" ,   m_minOccEntries   = 1000  );

  declareProperty("Precision" , m_Precision = 8 );

  declareProperty("xmlFilePath"   , m_xmlFilePath  = "/group/online/alignment" );
  declareProperty("xmlVersionLog" , m_xmlVersionLog = "version.txt" );

  declareProperty("ConditionCacheFile",
                  m_hpdCacheFile = "RichHPDConditionCache.txt" );

  declareProperty("RunStatsFile",
                  m_runStatsFile = "RichRunStatsCache.txt" );

  declareProperty("InactiveHPDFile",
                  m_inactiveHPDFile = "RichInactiveHPDCache.txt" );

  declareProperty("DIMSummaryFile",
                  m_dimSummaryFile = "DIMUpdateSummary.txt" );

  declareProperty("MinMaxRadiusFail", m_minMaxRadiusFail = std::make_pair(4.7,7.6) );

  declareProperty("MaxHPDXYError",          m_params.maxXYErr = 0.0008 );
  declareProperty("MaxHPDXYErrorDeviation", m_params.maxXYErrDeviation = 0.125 );
  declareProperty("MaxHPDXYDeviation",      m_maxXYDeviation = 1.4 );

  declareProperty("MinMaxRadius", m_minMaxRadius = std::make_pair(5.7,7.1) );

  declareProperty("MaxOKHPDReports", m_maxOKHPDReports = 0 );

  declareProperty("MaxStrangeHPDReports", m_maxStrangeHPDReports = 8 );

  declareProperty("DisableDIMPublish", m_disableDIMpublish = false );

  declareProperty("MinTimeBetweenCalibs", m_minTimeBetweenCalibs = 16*60  ); // 16 mins
  declareProperty("MinEventsForCalib",    m_minEventsForCalib        =  350000 );

  declareProperty("MinEventsForInactiveHPDs",   m_minEventsForInactiveHPDs   = 1000000 );
  declareProperty("AvTotalHitsForInactiveHPDs", m_avTotalHitsForInactiveHPDs = 1000 );
  declareProperty("MinAvHPDOccForInactiveHPDs", m_minAvHPDOccForInactiveHPDs = 2.0 );

  declareProperty("AlwaysRunCalibration", m_alwaysRunCalib = false );

  declareProperty("SendImageDataToDim", m_sendImageDataToDim = true );

  declareProperty("CreatePDFSummary", m_createPDFsummary = true );
  declareProperty("PrintAllHPDsInPDF", m_printAllHPDs = false );
  declareProperty("MinOKHPDFitsForPDF", m_minOKFitsForPDF = 20 );
  declareProperty("MaxStrangeFitsForPDF", m_maxStrangeFitsForPDF = 100 );
  declareProperty("MinSumHistEntriesForPDF", m_minSumHistEntriesForPDF = 5 );

  declareProperty("MaxCachedCalibsInARow", m_maxCachedCalibs = 5 );

  declareProperty("MaxFracInactiveHPDs", m_maxFracHPDsForInactiveList = 0.4 );

}

//============================================================================
// Destructor
//=============================================================================

OnlineSummary::~OnlineSummary() { }

//=============================================================================
// Initialization
//=============================================================================

StatusCode OnlineSummary::initialize()
{
  // ROOT style for PDFs
  if ( m_createPDFsummary ) { setStyle(); }

  StatusCode sc = SummaryBase::initialize();
  if ( sc.isFailure() ) return sc;

  // Fix file permissions ...
  //fixFilePermissions();

  // Listen for incidents when the calibration should be run
  IIncidentSvc * isvc = svc<IIncidentSvc>( "IncidentSvc", true );
  isvc->addListener( this, IncidentType::RunChange );

  // Load the Rich System detector element
  m_RichSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  // load the smart ID tool
  acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, nullptr, true );

  // read cached conditions
  readCachedConditions();

  // Initialise DIM state listeners
  //initialiseDIMStates();

  // initialise the HPD DIM info map, if required
  if ( m_sendImageDataToDim )
  {
    m_dimDataMap.clear();
    for ( const auto& HPD : hpdSmartIDsToProcess() )
    {
      const auto copyN = m_RichSys->copyNumber(HPD);
      getHPDFitDimData( copyN );
    }
  }

  // Publish to DIM
  if ( !m_disableDIMpublish )
  {
    sc = serviceLocator()->service( "LHCb::PublishSvc", m_pPublishSvc, true );
    if ( sc.isSuccess() && m_pPublishSvc )
    {
      debug() << "PublishSvc initialized" << endmsg;
      m_pPublishSvc->declarePubItem( m_Rich1TaskName, m_Rich1PubString );
      m_pPublishSvc->declarePubItem( m_Rich2TaskName, m_Rich2PubString );
    }
  }

  //==========================================
  // Pre-Book occupancy and image histograms
  //==========================================
  for ( const auto& HPD : hpdSmartIDsToProcess() ) { getHists(HPD); }

  // Book 2D histogram to summarise image (x,y) shifts
  m_imageShiftSummaryHist =
    Gaudi::Utils::Aida2ROOT::aida2root( richHisto2D( HistogramID("hpdImageXYShifts"),
                                                     "HPD Image Shifts (x,y)",
                                                     -2.0, 2.0, 50, -2.0, 2.0, 50,
                                                     "x / mm", "y / mm" ) );

  // Book 2D histogram to summarise image (x,y) shift errors
  m_imageShiftErrSummaryHist =
    Gaudi::Utils::Aida2ROOT::aida2root( richHisto2D( HistogramID("hpdImageXYShiftErrs"),
                                                     "HPD Image Shifts (x,y) Errors (log10)",
                                                     -5, -1, 50, -5, -1, 50,
                                                     "log10(x error / mm)", "log10(y error / mm)" ) );

  // HPD occumary summary
  m_hpdOccSummaryHist =
    Gaudi::Utils::Aida2ROOT::aida2root( richHisto1D( HistogramID("hpdOccSummary"),
                                                     "HPD Occupancy Summary",
                                                     -0.5, 40.5, 41,
                                                     "Av. HPD Occupancy", "Entries" ) );

  // HPD Image radius summary
  m_imageRadiusSummaryHist =
    Gaudi::Utils::Aida2ROOT::aida2root( richHisto1D( HistogramID("hpdImageRadius"),
                                                     "HPD Image Radius",
                                                     5.5, 7.5, 50,
                                                     "HPD Image Radius / mm", "Entries") );

  // reset counters for first event
  resetCalibCounters();

  // Print Partition and publishing status
  info() << "Partition = '" << m_partition << "' OK2Publish = " << okToPublishCalibs() << endmsg;

  // write to summary file
  if ( okToPublishCalibs() ) { writeToDimSummaryFile( m_Name + " Initialized" ); }

  // send a message to camera to say we are ready
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Initialized");

  // return final status
  return sc;
}

//=============================================================================

OnlineSummary::HPDFitDimData * 
OnlineSummary::getHPDFitDimData( const Rich::DAQ::HPDCopyNumber & copyNumber )
{
  // find this HPD in the map
  const auto dimdata = m_dimDataMap.find(copyNumber);
  if ( UNLIKELY( dimdata == m_dimDataMap.end() ) )
  {
    auto * data = new HPDFitDimData(copyNumber);
    m_dimDataMap[copyNumber].reset( data );
    return data;
  }
  else
  {
    return dimdata->second.get();
  }
}

//=============================================================================

OnlineSummary::PDHistograms &
OnlineSummary::getHists( const LHCb::RichSmartID& smartID )
{
  const auto iHist = m_hists.find(smartID);
  if ( UNLIKELY( iHist == m_hists.end() ) )
  {
    // copy number
    const auto copyNumber = m_RichSys->copyNumber( smartID );

    // make a new occupancy histogram
    std::ostringstream name;
    name << "Rich_HPD_" << copyNumber.data() << "_Occupancy";
    m_hists[smartID].first.reset( new TH1D( name.str().c_str(), 
                                            name.str().c_str(), 
                                            201, -0.5, 200.5 ) );
    std::ostringstream mess;
    mess << "Created occupancy histogram for HPD " << copyNumber << " " << smartID;
    _ri_debug << mess.str() << endmsg;
    //cameraTool()->Append("TEXT",mess.str());

    // load image histogram
    m_hists[smartID].second = getHPDImageHist( smartID );

    // return
    return m_hists[smartID];
  }
  else
  {
    return iHist->second;
  }
}

//=============================================================================

const LHCb::RichSmartID::Vector& OnlineSummary::hpdSmartIDsToProcess() const
{

  // load the list of all HPD IDs
  const auto& allIDs = m_RichSys->allPDRichSmartIDs();

  // what partition ?
  if ( !m_onlyOneRichPart )
  {
    // just return the full list
    return allIDs;
  }
  else
  {
    // need to filter the list for RICH1 or RICH2

    // make a static temporary container for the filtered list ...
    static LHCb::RichSmartID::Vector ids;
    // make sure it is empty
    ids.clear();
    // guess at half the size of the full list
    ids.reserve( allIDs.size() / 2 );

    // loop and filter
    for ( const auto& ID : allIDs )
    {
      if      ( "RICH1" == m_partition )
      { if ( Rich::Rich1 == ID.rich() ) { ids.push_back(ID); }  }
      else if ( "RICH2" == m_partition )
      { if ( Rich::Rich2 == ID.rich() ) { ids.push_back(ID); }  }
      else
      { ids.push_back(ID); }
    }

    // return
    return ids;
  }

}

//=============================================================================

void OnlineSummary::handle( const Incident & /* incident */ )
{
  // std::ostringstream mess;
  // mess << "Incident " << incident.type();
  // cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,mess.str());

  // Run the calibration
  runCalibration();
  // reset histograms
  resetHistograms();
}

//=============================================================================

void OnlineSummary::resetHistograms()
{
  // reset run event count
  m_nEventsThisRun = 0;
  m_nHitsThisRun   = 0;
  // reset histograms
  for ( auto & hists : m_hists )
  {
    if ( hists.second.first  ) { hists.second.first->Reset(); }
    if ( hists.second.second ) { hists.second.second->Reset(); }
  }
  //cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"HPD histograms reset");
}

//=============================================================================

bool OnlineSummary::isOK( HPDFitResult& sHPD ) const
{
  bool ok = sHPD.fitResult.OK();
  if ( ok )
  {
    if ( sHPD.fitResult.radInMM() < m_minMaxRadiusFail.first  ||
         sHPD.fitResult.radInMM() > m_minMaxRadiusFail.second )
    {
      ok = false;
      sHPD.messages.push_back( " HPD fit forced failed due to extreme image radius" );
    }
  }
  else
  {
    sHPD.messages.push_back( " HPD fit status failed" );
  }
  return ok;
}

//=============================================================================

void OnlineSummary::runCalibration( const std::string& type )
{
  // check run number is reasonable
  if ( UNLIKELY( 0 == m_runNumber ) )
  {
    // if we have also seen events, something is wrong...
    if ( UNLIKELY( m_nEventsSinceCalib > 0 ) )
    {
      std::ostringstream mess;
      mess << "Undefined run number !! -> " << type << " calibration aborted";
      warning() << mess.str() << endmsg;
      cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,mess.str());
    }
    resetHistograms();
    return;
  }

  // Only do anything if some events have been seen since last time.
  if ( UNLIKELY( 0 == m_nEventsSinceCalib ) )
  {
    std::ostringstream mess;
    mess << "No events processed for run " << m_runNumber << " -> "
         << type << " calibration aborted";
    warning() << mess.str() << endmsg;
    cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,mess.str());
    resetHistograms();
    return;
  }

  // is this a previously calibrated run
  const auto iRunCalData = m_processedRuns.find(m_runNumber);
  if ( iRunCalData != m_processedRuns.end() )
  {
    std::ostringstream m;
    m << "Previous #Events = " << iRunCalData->second << " New #Events = " << m_nEventsThisRun;
    cameraTool()->Append("TEXT",m.str());
    // Do we now have more events to calibrate with than last time ?
    if ( iRunCalData->second >= m_nEventsThisRun )
    {
      std::ostringstream t;
      t << "Already calibrated Run " << m_runNumber << " -> Processing skipped";
      cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,t.str());
      info() << t.str() << endmsg;
      resetCalibCounters();
      return;
    }
  }
  {
    std::ostringstream m;
    m << "Running calibration update with " << m_nEventsThisRun << " events";
    cameraTool()->Append("TEXT",m.str());
  }

  // // check if we have already seen this run
  // if ( std::find(m_runList.begin(),m_runList.end(),m_runNumber) != m_runList.end() )
  // {
  //   std::ostringstream mess;
  //   mess << "Have already calibrated run " << m_runNumber << " !! -> Skipping";
  //   warning() << mess.str() << endmsg;
  //   cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,mess.str());
  //   resetHistograms();
  //   return;
  // }
  // else
  // {
  //   m_runList.push_back(m_runNumber);
  // }

  // Check LHC and LHCb state via DIM

  // // If LHC/LHCb state has changed, send a message to camera
  // if ( UNLIKELY( stateChange() ) )
  // {
  //   setDimStatesAsRead();
  //   std::ostringstream mess;
  //   mess << "State : LHC '" << lhcState() << "' LHCb '" << lhcbState() << "'";
  //   m_CamTool->SendAndClearTS( ICameraTool::INFO, m_Name, mess.str() );
  //   info() << mess.str() << endmsg;
  // }

  // Are we in physics, so send updates
  //const bool sendUpdates = inPhysics();
  const bool sendUpdates = true;

  // cache the previous state
  static bool lastState = sendUpdates;

  // Flag for first time call
  //static bool firstTime = true;

  // // has the state changed
  // // do this last so a calibration is done also on a state change from physics
  // if ( UNLIKELY( firstTime || sendUpdates != lastState ) )
  // {
  //   std::ostringstream mess;
  //   mess << "HPD calibration now ";
  //   if ( sendUpdates ) { mess << "ACTIVE"; } else { mess << "INACTIVE"; }
  //   m_CamTool->SendAndClearTS( ICameraTool::INFO, m_Name, mess.str() );
  //   info() << mess.str() << endmsg;
  // }

  // only run the calibration when in physics or if forced.
  if ( m_alwaysRunCalib || lastState || sendUpdates )
  {

    // update the version to use for this calibration run
    const bool versionOK = ( setVersion(m_Rich1TaskName) &&
                             setVersion(m_Rich2TaskName) );

    // To store HPD occupancies
    HPDOccData Rich1_OccsVec, Rich2_OccsVec;

    // reset summary histograms
    m_imageShiftSummaryHist->Reset();
    m_imageShiftErrSummaryHist->Reset();
    m_hpdOccSummaryHist->Reset();
    m_imageRadiusSummaryHist->Reset();

    // add run info to histogram titles
    addRunInfo( m_imageShiftSummaryHist );
    addRunInfo( m_imageShiftErrSummaryHist );
    addRunInfo( m_hpdOccSummaryHist );
    addRunInfo( m_imageRadiusSummaryHist );

    // Flag to check if XML was correctly written
    bool imageXMLOK = true;

    // Message level to use with camera (if not error)
    auto messLevel = ICameraTool::INFO;

    // get the list of all HPD SmartIDs
    const LHCb::RichSmartID::Vector& allPDIDs = hpdSmartIDsToProcess();

    // List of strange HPD fits to report in detail later on
    HPDFitResult::Vector strangeHPDs;
    // List of successful HPD fits
    HPDFitResult::Vector okHPDs;
    okHPDs.reserve( allPDIDs.size() );

    // save this run in the processed list with number of events used.
    m_processedRuns[m_runNumber] = m_nEventsThisRun;

    // is this a final run calibration ?
    const bool finalRunCalib = ( "Final" == type );

    // Has at least one HPD fit been successful ?
    bool atLeastOneFitOK = false;

    // flag to say there was at least one HPD with at least one entry in its
    // HPD image histogram
    bool atLeastOneHPDWithStats = false;

    // Vector to save the list of HPDs that saw no data
    std::map<Rich::DetectorType,LHCb::RichSmartID::Vector> emptyHPDs;

    // map to count the total number of HPDs in each RICH
    std::map<Rich::DetectorType,unsigned int> totalHPDs;

    // Loop over HPDs
    for ( const auto& smartID : allPDIDs )
    {
      // Count HPDs per RICH
      ++totalHPDs[smartID.rich()];

      // message for camera for this HPD
      std::ostringstream hpdMess;
      hpdMess << smartID;

      // Flag to say if this HPD has seen any hits or not
      bool hpdSawHits = true;

      // HPD copy number
      const auto copyNumber = m_RichSys->copyNumber(smartID);
      _ri_debug << "HPD ID " << copyNumber << " "  << smartID << endmsg;
      // Get condition
      Condition* siAlign = getSiSensorAlignment( smartID, copyNumber );
      if ( UNLIKELY(!siAlign) )
      {
        messLevel = ICameraTool::ERROR;
        hpdMess << " FAILED to load SiSensor alignment condition";
        hpdSawHits = false;
      }
      else
      {

        // HPD alignment condition name to update
        const std::string imageXYZparamName = "dPosXYZ";

        // HPD image radius condition to update
        const std::string imageRparamName   = "ImageRadius";

        // HPD image radius condition could be missing, as its a new addition
        // if so add a default value
        if ( !siAlign->exists(imageRparamName) )
        {
          const double defR = 0.0;
          siAlign->addParam( imageRparamName, defR, "" );
        }

        // Create default occupancy data
        auto HpdOcc = std::make_pair( smartID.key(), getCachedOcc(smartID) );

        // Get histograms
        auto & histograms = getHists( smartID );
        // .. the image histogram
        auto * imageHist = histograms.second;
        // .. and the occupancy plot
        auto * occupHist = histograms.first.get();

        // add run info for printing
        if ( m_printAllHPDs ) { addRunInfo( imageHist ); }

        //======================================
        // Update occupancy first
        //======================================
        if ( occupHist )
        {
          _ri_debug << "Number of entries: " << occupHist->GetEntries()
                    << ", for HPD " << smartID.toString()
                    << endmsg ;

          // Did this HPD see any hits ?
          hpdSawHits = ( occupHist->GetEntries() > 0 );

          if ( occupHist->GetEntries() >= m_minOccEntries )
          {
            // fill occupancy data
            HpdOcc.second = occupHist->GetMean();

            // fill occupancy summary plot
            m_hpdOccSummaryHist->Fill( HpdOcc.second );

            // Update 'good' HPD image condition cache
            updateConditionCache( smartID, HpdOcc.second );

            // Add to camera message
            hpdMess << " <occ>= " << HpdOcc.second;
          }

        }
        else
        {
          messLevel = ICameraTool::WARNING;
          hpdMess << " Occupancy histogram missing";
          warning() << "Occupancy histogram not there for HPD " << smartID << endmsg ;
          hpdSawHits = false;
        }

        // save results to write later on
        if      ( smartID.rich() == Rich::DetectorType::Rich1 )
        { Rich1_OccsVec.push_back( HpdOcc ); }
        else if ( smartID.rich() == Rich::DetectorType::Rich2 )
        { Rich2_OccsVec.push_back( HpdOcc ); }

        //======================================
        // Now HPD image shift
        //======================================
        if ( imageHist && 0 != imageHist->GetEntries() )
        {
          atLeastOneHPDWithStats = true;
        }
        bool usedHPDFit = false;
        if ( !imageHist )  // check image hist is there
        {
          hpdMess << " Image histogram missing";
          warning() << "Image histogram not there for HPD " << smartID << endmsg ;
          messLevel = ICameraTool::WARNING;
        }
        else if ( imageHist->GetEntries() < m_minImageEntries )  // check whether there is enough entries
        {
          hpdMess << " Not enough histogram entries";
          _ri_debug << "Not enough HPD image entries: " << imageHist->GetEntries()
                    << "for HPD " << smartID.toString() << endmsg ;
        }
        else // hist is there, and enough entries, then do fit and update condition
        {
          // Look for 'odd' things to report
          HPDFitResult sHPD( smartID, imageHist, occupHist->GetEntries() );

          // Do the fit
          sHPD.fitResult = m_fitter.fit( *imageHist,
                                         m_params,
                                         occupHist->GetEntries() );
          const auto& result = sHPD.fitResult;
          std::ostringstream fitSum;
          fitSum << " Fit OK :"
                 << " X=" << result.x() << "+-" << result.xErr()
                 << " Y=" << result.y() << "+-" << result.yErr()
                 << " R=" << result.radInMM() << "+-" << result.radErrInMM();
          // Add fit result parameters to camera message
          hpdMess << fitSum.str();

          // Is the fit OK ?
          if ( isOK(sHPD) )
          {

            atLeastOneFitOK = true;
            usedHPDFit      = true;

            _ri_debug << "Fit OK for " << copyNumber
                      << ": Shift = (" << result.x() << "+-" << result.xErr()
                      << ", "          << result.y() << "+-" << result.yErr() << ")"
                      << ", Radius " << result.radInMM()
                      << endmsg;

            // fill summary plots
            m_imageShiftSummaryHist   ->Fill( result.x(),    result.y()  );
            m_imageShiftErrSummaryHist->Fill( result.xErr()>0 ? std::log10(result.xErr()) : -999,
                                              result.yErr()>0 ? std::log10(result.yErr()) : -999 );
            m_imageRadiusSummaryHist  ->Fill( result.radInMM()           );

            // Update condition
            std::vector<double> dPosVec = siAlign->paramAsDoubleVect( imageXYZparamName );
            _ri_debug << "In DB, shift of " << copyNumber << ": " <<  dPosVec
                      << endmsg ;

            // update the alignment parameters in the condition
            dPosVec[0] = result.x();
            dPosVec[1] = result.y();
            dPosVec[2] = 0;
            siAlign->addParam( imageXYZparamName, dPosVec, "" );

            // update image radius
            const double newR = result.radInMM();
            siAlign->addParam( imageRparamName, newR, "" );

            // Update 'good' HPD image condition cache
            updateConditionCache( smartID, siAlign );

            // Update image data sent to DIM if this is an end of run fit
            // and not a periodic intermediary one.
            if ( m_sendImageDataToDim && finalRunCalib )
            {
              // Get the DIM data object
              const auto dimdata = getHPDFitDimData(copyNumber);
              dimdata->update( result.x(), result.y(), result.radInMM() );
            }

            // Unusual (x,y) values
            if ( fabs( result.x() ) > m_maxXYDeviation ||
                 fabs( result.y() ) > m_maxXYDeviation )
            {
              sHPD.messages.push_back( " Large (x,y) deviation in HPD fit" );
            }

            // Unusual (x,y) errors
            if ( !m_fitter.errorsOK( result, m_params ) )
            {
              sHPD.messages.push_back( " Unusual HPD image fit (x,y) errors" );
            }

            // Unusual radius
            if ( result.radInMM() < m_minMaxRadius.first ||
                 result.radInMM() > m_minMaxRadius.second )
            {
              sHPD.messages.push_back( " Unusual HPD image fit radius" );
            }

            // if the messages are not empty, add to reports to send
            if ( !sHPD.messages.empty() )
            {
              // add fit info
              sHPD.messages.push_back( fitSum.str() );
              // add to list of odd HPD fits
              strangeHPDs.push_back( sHPD );
            }
            else
            {
              // add fit info
              sHPD.messages.push_back( fitSum.str() );
              // save to OK list
              okHPDs.push_back( sHPD );
            }

          }
          else
          {
            hpdMess << " Fit FAILED";
            // add fit info
            sHPD.messages.push_back( fitSum.str() );
            // save summary object
            strangeHPDs.push_back( sHPD );
          }

        }

        // The condition string
        std::string condS = xmlString(siAlign);

        // if the HPD fit was not done
        if ( !usedHPDFit )
        {
          // Try and look up last good Condition from the cache
          const std::string cachedCond = getCachedImageCondition(smartID);
          if ( !cachedCond.empty() )
          {
            hpdMess << " Using condition cache : " << cachedCond;
            condS = cachedCond;
          }
          else
          {
            // Just use the defaults
            const std::vector<double> dPosVec = siAlign->paramAsDoubleVect(imageXYZparamName);
            const double               radius = siAlign->paramAsDouble(imageRparamName);
            hpdMess << " Using Defaults : X=" << dPosVec[0] << " Y=" << dPosVec[1]
                    << " R=" << radius;
          }
        }

        // Now write out xml file
        if ( okToPublishCalibs() )
        {
          const auto xmlVersion = getVersion( smartID );
          if ( !xmlVersion.first || !xmlWriter( condS, smartID, xmlVersion.second ) )
          {
            imageXMLOK = false;
            std::ostringstream mess;
            mess << "Failed to write xml file for HPD " << copyNumber;
            warning() << mess.str() << endmsg;
            cameraTool()->Append("TEXT",mess.str());
          }
        }

      } // Si alignment condition OK

      // inactive HPDs
      if ( !hpdSawHits ) { emptyHPDs[smartID.rich()].push_back(smartID); }

      // Add summary for this HPD to camera message
      cameraTool()->Append("TEXT",hpdMess.str());

    }  // Loop over HPDs

    // Did we have at least one HPD with a good fit ?
    if ( atLeastOneFitOK )
    {
      // yes, so reset cached count to 0
      m_cachedCalibCount = 0;
    }
    else
    {
      // Count cached calibrations in a row...
      ++m_cachedCalibCount;
    }

    // Append image shift plot
    sendToCamera( m_imageShiftSummaryHist );
    // Append image shift error plot
    sendToCamera( m_imageShiftErrSummaryHist );
    // Append image radius plot
    sendToCamera( m_imageRadiusSummaryHist );
    // Append HPD Occ plot
    sendToCamera( m_hpdOccSummaryHist );

    // Have we enough events to attempt creating a valid inactive HPD list ?
    const auto avHitsPerEvent = ( m_nEventsThisRun>0 ? m_nHitsThisRun/(double)m_nEventsThisRun : 0 );
    const auto meanHPDOcc = ( !allPDIDs.empty() ? avHitsPerEvent / allPDIDs.size() : 0.0 );
    std::ostringstream mocc;
    mocc << "Av. HPD Occ. = " << meanHPDOcc 
         << ", Av. Hits/Event = " << avHitsPerEvent
         << ", #hits = " << m_nHitsThisRun
         << ", #events = " << m_nEventsThisRun;
    info() << mocc.str() << endmsg;
    cameraTool()->Append("TEXT",mocc.str());
    if ( m_nEventsThisRun >= m_minEventsForInactiveHPDs && 
         avHitsPerEvent   >= m_avTotalHitsForInactiveHPDs &&
         m_nHitsThisRun   >= ( m_avTotalHitsForInactiveHPDs * m_minEventsForInactiveHPDs ) &&
         meanHPDOcc       >= m_minAvHPDOccForInactiveHPDs )
    {
      // Check if empty HPD lists are too big
      for ( const Rich::DetectorType rich : { Rich::Rich1, Rich::Rich2 } )
      {
        if ( !emptyHPDs[rich].empty() &&
             emptyHPDs[rich].size() > m_maxFracHPDsForInactiveList * totalHPDs[rich] )
        {
          emptyHPDs[rich].clear();
          std::ostringstream m;
          m << "Too many inactive HPDs in " << rich << ". Setting to empty list.";
          warning() << m.str() << endmsg;
          messLevel = ICameraTool::WARNING;
          cameraTool()->Append("TEXT",m.str());
        }
      }
    }
    else
    {
      cameraTool()->Append("TEXT","Not enough data to create accurate inactive HPD list.");
      emptyHPDs[Rich::Rich1].clear();
      emptyHPDs[Rich::Rich2].clear();
    }

    // Now write out HPD occupancies and inactive HPDs to xml file
    bool r1OccXMLOK(true), r2OccXMLOK(true);
    if ( okToPublishCalibs() )
    {
      r1OccXMLOK = writeHpdXml( Rich1_OccsVec, emptyHPDs[Rich::Rich1], m_Rich1TaskName );
      if ( !r1OccXMLOK )
      {
        std::ostringstream mess;
        mess << "Failed to write xml file for Rich1 Occupancies and/or inactive HPDs";
        warning() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
      }
      r2OccXMLOK = writeHpdXml( Rich2_OccsVec, emptyHPDs[Rich::Rich2], m_Rich2TaskName );
      if ( !r2OccXMLOK )
      {
        std::ostringstream mess;
        mess << "Failed to write xml file for Rich2 Occupancies and/or inactive HPDs";
        warning() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
      }
    }

    // Will we publish an update via DIM ?
    const bool sendDIMupdate = ( atLeastOneHPDWithStats &&
                                 okToPublishCalibs()    &&
                                 r1OccXMLOK && r2OccXMLOK && imageXMLOK );

    // Update the Condition Cache on disk (only when publishing)
    bool cacheWriteOK(true);
    if ( okToPublishCalibs() )
    {
      cacheWriteOK = writeCachedConditions();
      if ( !cacheWriteOK ) { messLevel = ICameraTool::WARNING; }
    }

    // Print to PDF ?
    if ( createPDFsummary() && atLeastOneFitOK &&
         okHPDs.size()      >= m_minOKFitsForPDF &&
         strangeHPDs.size() <= m_maxStrangeFitsForPDF &&
         ( m_imageShiftSummaryHist->GetEntries()    >= m_minSumHistEntriesForPDF ||
           m_imageShiftErrSummaryHist->GetEntries() >= m_minSumHistEntriesForPDF ||
           m_imageRadiusSummaryHist->GetEntries()   >= m_minSumHistEntriesForPDF ||
           m_hpdOccSummaryHist->GetEntries()        >= m_minSumHistEntriesForPDF ) )
    {

      // Open the file
      printCanvas("[");

      // Draw the summaries in a 2 x 2 array
      canvas()->Divide(2,2);
      canvas()->cd(1); draw( m_imageShiftSummaryHist, "zcol" );
      canvas()->cd(2); draw( m_imageShiftErrSummaryHist, "zcol" );
      canvas()->cd(3); draw( m_imageRadiusSummaryHist );
      canvas()->cd(4); draw( m_hpdOccSummaryHist );
      printCanvas();

      // Draw each HPD on a single page
      if ( m_printAllHPDs )
      {
        canvas()->Clear();
        canvas()->Divide(1,1);
        for ( const auto& HPD : okHPDs      ) { draw(HPD); }
        for ( const auto& HPD : strangeHPDs ) { draw(HPD); }
      }

      // Close PDF summary
      printCanvas("]");

    }

    // If printing all HPD images, reset their titles afterwards.
    if ( UNLIKELY(m_printAllHPDs) )
    {
      for ( const auto& smartID : allPDIDs )
      {
        addRunInfo( getHPDImageHist(smartID), false );
      }
    }

    // send camera message
    std::ostringstream mess;
    mess << type << " HPD calibration summary | Run " << m_runNumber;
    if ( versionOK && r1OccXMLOK && r2OccXMLOK && imageXMLOK )
    { mess << " | SUCCESSFUL"; }
    else
    { mess << " | FAILED"; messLevel = ICameraTool::ERROR; }
    cameraTool()->SendAndClearTS(messLevel,m_Name,mess.str());

    // Report any 'strange' HPDs
    if ( UNLIKELY( !strangeHPDs.empty() && m_maxStrangeHPDReports > 0 ) )
    {
      unsigned int nStrange(0);
      for ( const auto& sHPD : strangeHPDs )
      {
        // Prepare HPD info string
        const auto cN = m_RichSys->copyNumber(sHPD.id);
        std::ostringstream mess;
        mess << "HPD " << cN << " " << sHPD.id;
        cameraTool()->Append("TEXT",mess.str());
        for ( const auto& m : sHPD.messages ) { cameraTool()->Append("TEXT",m); }
        if ( nStrange++ < m_maxStrangeHPDReports )
        {
          // Did we use the log-z image
          std::unique_ptr<const TH2D> 
            lZh( !sHPD.fitResult.usedLogZ() ? nullptr :
                 m_fitter.createLogzImage(*sHPD.imageHist) );
          // Get cleaned image
          std::unique_ptr<const TH2D> 
            cleanedH( m_fitter.getFitHistogram( lZh.get() ? *lZh.get() : *sHPD.imageHist,
                                                m_params, sHPD.nEvents ) );
          // Append images and fits to camera
          sendToCamera( lZh.get() ? lZh.get() : sHPD.imageHist, true );
          sendToCamera( sHPD.fitResult );
          sendToCamera( cleanedH.get(), true );
          sendToCamera( sHPD.fitResult );
        }
      }
      std::ostringstream mess;
      mess << strangeHPDs.size() << " unusual " << type
           << " HPD calibration(s) | Run " << m_runNumber;
      cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,mess.str());
    }

    // Send randomly selected sample of OK HPD fits
    if ( UNLIKELY( m_maxOKHPDReports > 0 && !okHPDs.empty() ) )
    {
      const unsigned int nOKHPDsToSend = 2;
      std::random_shuffle( okHPDs.begin(), okHPDs.end() );
      unsigned int nOKHPD(0);
      for ( const auto& okHPD : okHPDs )
      {
        if ( ++nOKHPD <= nOKHPDsToSend )
        {
          // Did we use the log-z image
          std::unique_ptr<const TH2D> lZh
            ( okHPD.fitResult.usedLogZ() ?
              m_fitter.createLogzImage(*okHPD.imageHist) : nullptr );
          // Prepare HPD info string
          const auto cN = m_RichSys->copyNumber(okHPD.id);
          std::ostringstream mess;
          mess << "HPD " << cN << " " << okHPD.id;
          cameraTool()->Append("TEXT",mess.str());
          for ( const auto& m : okHPD.messages ) { cameraTool()->Append("TEXT",m); }
          // Get Cleaned image
          std::unique_ptr<const TH2D> cleanedH 
            ( m_fitter.getFitHistogram( lZh.get() ? *lZh.get() : *okHPD.imageHist,
                                        m_params, okHPD.nEvents ) );
          // Append images and fits
          sendToCamera( lZh.get() ? lZh.get() : okHPD.imageHist, true );
          sendToCamera( okHPD.fitResult );
          sendToCamera( cleanedH.get(), true );
          sendToCamera( okHPD.fitResult );
        }
      }
      std::ostringstream mess;
      mess << nOKHPDsToSend << " Example OK " << type
           << " HPD calibration(s) | Run " << m_runNumber;
      cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,mess.str());
    }

    // Check how many calibrations based fully on cached info we have
    // submitted in a row
    if ( UNLIKELY( m_cachedCalibCount > m_maxCachedCalibs ) )
    {
      std::ostringstream mess;
      mess << "Many (" <<  m_cachedCalibCount
           << ") fully cached calibrations in a row.";
      warning() << mess.str() << endmsg;
      const auto l = okToPublishCalibs() ? ICameraTool::WARNING : ICameraTool::INFO;
      cameraTool()->SendAndClearTS( l, m_Name, mess.str() );
    }

    // Finally send the updated calibrations to DIM
    if ( sendDIMupdate )
    {
      // Publish to dim
      _ri_debug << "Publish to DIM... " << endmsg;
      if ( m_pPublishSvc ) { m_pPublishSvc->updateAll(); }
      //std::ostringstream mess;
      //mess << type << " XML conditions published for Run " << m_runNumber;
      //cameraTool()->SendAndClearTS(messLevel,m_Name,mess.str());
      // Add a line to the summary text file.
      const auto ok = writeCalibVersionsToDimSummaryFile();
      if ( !ok )
      {
        cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,
                                     "Problem appending to DIM summary file");
      }
    }
    // else
    // {
    //   std::ostringstream mess;
    //   mess << type << " XML conditions NOT published for Run " << m_runNumber;
    //   if ( okToPublishCalibs() ) { error() << mess.str() << endmsg; }
    //   const auto l = okToPublishCalibs() ? ICameraTool::ERROR : ICameraTool::INFO;
    //   cameraTool()->SendAndClearTS( l, m_Name, mess.str() );
    // }

    // remove run info to histogram titles
    addRunInfo( m_imageShiftSummaryHist,    false );
    addRunInfo( m_imageShiftErrSummaryHist, false );
    addRunInfo( m_hpdOccSummaryHist,        false );
    addRunInfo( m_imageRadiusSummaryHist,   false );

  } // in physics

  //firstTime = false;          // unset the first time flag
  lastState = sendUpdates;      // update the cached state
  resetCalibCounters();         // reset various counters for the next run

}

//=============================================================================

StatusCode OnlineSummary::execute()
{
  // get the ODIN object
  const auto * odin = getIfExists<LHCb::ODIN>(LHCb::ODINLocation::Default);
  if ( UNLIKELY( !odin ) )
  {
    m_runNumber = 0;
    const std::string mess = "ODIN missing !";
    cameraTool()->SendAndClearTS(ICameraTool::ERROR,m_Name,mess);
    error() << mess << endmsg;
    return StatusCode::SUCCESS;
  }

  // Current run number
  const auto RunNumber = odin->runNumber();

  // Monitor for new runs
  static unsigned long int lastRunNumberCamera = 0;
  if ( UNLIKELY( RunNumber != lastRunNumberCamera ) )
  {
    lastRunNumberCamera = RunNumber;
    std::ostringstream m;
    m << "=============> New Run " << RunNumber << " <=============";
    info() << m.str() << endmsg;
    cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,m.str());

  }

  // Check to see if a calibration should be run due to a new run being detected
  if ( UNLIKELY( m_runNumber != 0 && RunNumber != m_runNumber ) )
  {
    std::ostringstream mess;
    mess << "Unexpected run change to Run " << RunNumber << " detected";
    cameraTool()->SendAndClearTS(ICameraTool::WARNING,m_Name,mess.str());
    warning() << mess.str() << endmsg;
    runCalibration();
    resetHistograms();
  }

  // update cached run number
  m_runNumber = RunNumber;

  // count events
  ++m_nEvts;

  // First event printout
  if ( UNLIKELY( m_nEvts == 1 ) )
  {
    cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"First event seen");
    // set last time to now.
    m_timeLastCalib = time(nullptr);
  }

  // run periodic calibration ?
  if ( UNLIKELY( m_nEventsSinceCalib >= m_minEventsForCalib &&
                 m_minTimeBetweenCalibs > 0 ) )
  {
    // current time
    const time_t currentTime = time(nullptr);
    // time since last calibration
    const time_t deltaT = (currentTime - m_timeLastCalib);
    if ( UNLIKELY( deltaT >= m_minTimeBetweenCalibs ) )
    {
      // Print message on the number of events seen and the rate
      struct tm * timeinfo = localtime ( &deltaT );
      std::ostringstream messageString;
      messageString << "Seen " << m_nEventsSinceCalib << " events in past ";
      if ( timeinfo->tm_hour-1 > 0 ) { messageString << timeinfo->tm_hour-1 << " hours "; }
      if ( timeinfo->tm_min    > 0 ) { messageString << timeinfo->tm_min << " mins "; }
      if ( timeinfo->tm_sec    > 0 ) { messageString << timeinfo->tm_sec << " secs "; }
      messageString << "( " << (double)(m_nEventsSinceCalib) / (double)(deltaT)
                    << " Evt/s )";
      cameraTool()->SendAndClearTS( ICameraTool::INFO, m_Name, messageString.str() );
      _ri_debug << messageString.str() << endmsg;

      // run periodic calibration, no histogram reset...
      runCalibration("Periodic");
    }
  }

  // count events since last calibration was made
  ++m_nEventsSinceCalib;

  // count events this run
  ++m_nEventsThisRun;

  // Fill the HPD occupancy plots
  fillHistograms();

  // return
  return StatusCode::SUCCESS;
}

//=============================================================================

StatusCode OnlineSummary::finalize()
{
  // run a final calibration
  runCalibration();
  // reset the histograms, just in case
  resetHistograms();
  // send a final camera message with run statistics
  for ( const auto& r : m_processedRuns )
  {
    std::ostringstream mess;
    mess << "Run " << r.first << " calibration used " << r.second << " events";
    cameraTool()->Append("TEXT",mess.str());
  }
  cameraTool()->SendAndClearTS(ICameraTool::INFO,m_Name,"Finalized");
  // empty the run map
  m_processedRuns.clear();
  // clean up local histogram storage
  m_hists.clear();
  // clear up dim map
  m_dimDataMap.clear();
  // make sure canvas is deleted
  deleteCanvas();
  // clean up state listeners
  //finaliseDIMStates();
  // write to summary file
  if ( okToPublishCalibs() ) { writeToDimSummaryFile( m_Name + " Finalized" ); }
  // return
  return SummaryBase::finalize();
}

//=============================================================================

void OnlineSummary::fillHistograms()
{

  // Standard loop over Rich Smart IDs
  for ( const auto& UKL1 : m_SmartIDDecoder->allRichSmartIDs() )
  {
    for ( const auto& Ingress : UKL1.second )
    {
      for ( const auto& HPD : Ingress.second.hpdData() )
      {

        // Skip inhibited HPDs
        if ( UNLIKELY( HPD.second.header().inhibit() ) )
        {
          std::ostringstream mess;
          mess << HPD.second.header() << " Inhibited";
          Warning( mess.str() ).ignore();
          continue;
        }

        // HPD identifier
        const auto& smartID = HPD.second.hpdID();

        // skip bad HPD IDs
        if ( UNLIKELY( !smartID.isValid() ) )
        {
          std::ostringstream mess;
          mess << "Invalid SmartID " << smartID;
          Warning( mess.str() ).ignore();
          continue;
        }

        // skip inactive RICHes
        if ( UNLIKELY( !richIsActive(smartID) ) )
        {
          std::ostringstream mess;
          mess << smartID.rich() << " is Inactive";
          Warning( mess.str() ).ignore();
          continue; 
        }

        // hits for this HPD
        const auto & hits = HPD.second.smartIDs();

        // Count total number of hits seen
        m_nHitsThisRun += hits.size();

        // load the histograms
        auto & hists = getHists( smartID );

        // Fill occupancy histogram
        if ( hists.first ) { hists.first->Fill( double(hits.size()) ); }

        // Fill the image histogram. Note this is copied from 
        // SummaryBase::execute() locally here for CPU efficiency
        if ( hists.second ) 
        {
          for ( const auto& h : hits ) 
          { hists.second->Fill(h.pixelCol(),h.pixelRow()); }
        }

      }
    }
  }

}

//=============================================================================

bool OnlineSummary::writeHpdXml( HPDOccData& hpdOccs,
                                 LHCb::RichSmartID::Vector& inactHPDs,
                                 const std::string& subDet )
{
  // Get version
  const auto version = getVersion( subDet );
  bool OK = version.first;
  {
    const auto upS = std::to_string(m_runNumber) + " v" + std::to_string(version.second);
    if      ( subDet == m_Rich1TaskName ) { m_Rich1PubString = upS; }
    else if ( subDet == m_Rich2TaskName ) { m_Rich2PubString = upS; }
  }

  // sort first
  std::sort( hpdOccs.begin(), hpdOccs.end() );
  std::sort( inactHPDs.begin(), inactHPDs.end() );

  const boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
  const boost::filesystem::path file( "v" + std::to_string(version.second) + ".xml" );
  const boost::filesystem::path full_path = dir/file ;

  _ri_debug << "Writing occupancy to " << full_path << endmsg ;

  // make sure directory exists
  createDir(dir);

  std::ofstream logging( full_path.string().c_str(), std::ios::app ) ;  // always in append mode
  if ( logging.is_open() )
  {

    // HPD occupancies
    {
      std::string occS = "";
      for ( const auto& occ : hpdOccs )
      {
        if ( occS != "" ) occS += " " ;
        occS += std::to_string(as_int(occ.first)) + "/" + to_string_with_precision(occ.second) ;
      }
      logging << "<condition classID=\"5\" name=\"AverageHPDOccupancies\">\n";
      logging << "<paramVector name=\"Occupancies\" type=\"std::string\" comment=\"Average HPD occupancy\">"
              << occS << "</paramVector>\n";
      logging << "</condition>\n";
      logging << "\n" ;
      std::ostringstream mess;
      mess << "Wrote " << hpdOccs.size() << " HPD occupancies for " << subDet
           << " to " << full_path;
      cameraTool()->Append("TEXT",mess.str());
    }

    // Inactive HPDs
    {
      const bool updateCache = !inactHPDs.empty();
      std::string inacPDsS = "";
      for ( const auto& hpd : inactHPDs )
      {
        if ( inacPDsS != "" ) inacPDsS += " " ;
        const Rich::DAQ::HPDIdentifier id(hpd);
        inacPDsS += std::to_string( id.number() );
      }
      if ( inacPDsS.empty() )
      {
        inacPDsS = m_inactiveHPDCache[subDet];
        std::ostringstream mess;
        mess << "Using Inactive HPD cache for " << subDet << " '" << inacPDsS << "'";
        cameraTool()->Append("TEXT",mess.str());
      }
      if ( updateCache )
      {
        m_inactiveHPDCache[subDet] = inacPDsS;
        std::ostringstream mess;
        mess << "Updating Inactive HPD cache for " << subDet << " to '" << inacPDsS << "'";
        cameraTool()->Append("TEXT",mess.str());
      }
      logging << "<condition name=\"InactiveHPDList\">\n";
      logging << "<paramVector name=\"InactiveHPDListInSmartIDs\" type=\"int\">" << inacPDsS
              << "</paramVector>\n";
      logging << "</condition>\n";
      logging << "\n" ;
      std::ostringstream mess;
      mess << "Inactive HPDs for " << subDet << " '" << inacPDsS << "'";
      cameraTool()->Append("TEXT",mess.str());
    }

    // finally close the file
    logging.close();

  }
  else
  {
    std::ostringstream mess;
    mess << "Failed to open file " << full_path;
    error() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());
    OK = false;
  }

  info() << m_runNumber << ":" << full_path.string() << endmsg;

  return OK;
}

//=============================================================================

bool OnlineSummary::xmlWriter( const std::string& condition,
                               const LHCb::RichSmartID& smartID,
                               const unsigned long long version )
{
  bool OK = true;

  const std::string& subDet =
    ( smartID.rich() == Rich::DetectorType::Rich1 ? m_Rich1TaskName : m_Rich2TaskName );

  const boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
  const boost::filesystem::path file( "v" + std::to_string(version) + ".xml" );
  const boost::filesystem::path full_path = dir/file ;

  _ri_debug << "Writing image shift to " << full_path << endmsg ;

  // make sure directory exists
  createDir(dir);

  std::ofstream logging( full_path.string().c_str(), std::ios::app ) ; // always in append mode
  if ( logging.is_open() )
  {
    logging << condition << "\n";
    logging << "\n" ;
    logging.close();
  }
  else
  {
    OK = false;
    // camera message
    std::ostringstream mess;
    mess << "Failed to open " << full_path;
    error() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());
  }

  return OK;
}

//=============================================================================

bool OnlineSummary::setVersion( const std::string& subDet )
{
  bool OK = true;

  const boost::filesystem::path dir( m_xmlFilePath + "/" + subDet ) ;
  const boost::filesystem::path filename( m_xmlVersionLog );
  const boost::filesystem::path full_path = dir/filename ;

  _ri_debug << "Version file: " << full_path.string() << endmsg;

  unsigned int           run(0);
  unsigned long long version(0);

  if ( !boost::filesystem::exists(full_path) )  // the first time
  {
    try
    {
      version = 0 ;
      createDir(dir);
      std::ofstream logging( full_path.string().c_str() );
      if ( logging.is_open() )
      {
        logging << m_runNumber << " " << version << "\n";
        logging.close();
      }
      else
      {
        OK = false;
        std::ostringstream mess;
        mess << "Failed to create file " << full_path;
        error() << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
      }
    }
    catch ( const boost::filesystem::filesystem_error& expt )
    {
      OK = false;
      std::ostringstream mess;
      mess << "Failed to create file " << full_path << " " << expt.what();
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
    }
  }
  else
  {
    std::fstream file( full_path.string().c_str(), std::ios::in | std::ios::out );
    if ( file.is_open() )
    {
      // read the last version and run
      std::istringstream lastLine( getLastLine(file) );
      lastLine >> run >> version ;
      // increase version number
      ++version;
      // write out the new version
      file << m_runNumber << " " << version << "\n" ;
      // close
      file.close();
    }
    else
    {
      OK = false;
      std::ostringstream mess;
      mess << "Failed to open file " << full_path;
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
    }
  }

  //setPerms( full_path );

  _ri_debug << "Setting version: " << version
            << " for run: " << m_runNumber
            << endmsg ;

  // update version cache
  m_vCache[subDet] = std::make_pair(OK,version);

  // return
  return OK;
}

//=============================================================================

std::string OnlineSummary::getLastLine(std::fstream& in) const
{
  std::fstream::pos_type pos = in.tellg();

  std::fstream::pos_type lPos;
  while ( in >> std::ws && ignoreline(in,lPos) ) { pos = lPos; }

  in.clear();
  in.seekg(pos);

  std::string line;
  std::getline(in, line);
  return line;
}

//=============================================================================

bool OnlineSummary::writeCachedConditions()
{
  bool OK = true;

  // First write out the HPD conditions
  {
    std::ofstream file( cacheFile().c_str() );
    if ( file.is_open() )
    {
      cameraTool()->Append("TEXT","HPD Conditions Cache Location = "+cacheFile());
      // Write out cached conditions
      for ( const auto& cond : m_hpdCondCache )
      {
        file << cond.first.key()   << std::endl; // HPD ID
        file << cond.second.first  << std::endl; // Occupancy
        file << cond.second.second << std::endl; // image XML
      }
      std::ostringstream mess;
      mess << "Successfully wrote " << m_hpdCondCache.size() << " cached HPD Conditions";
      cameraTool()->Append("TEXT",mess.str());
      info() << mess.str() << endmsg;
      file.close();
    }
    else
    {
      std::ostringstream mess;
      mess << "Problem writing cache file " << cacheFile();
      cameraTool()->Append("TEXT",mess.str().c_str());
      OK = false;
    }
  }

  // then write the run stats summary map
  {
    std::ofstream file( runStatsFile().c_str() );
    if ( file.is_open() )
    {
      cameraTool()->Append("TEXT","Run Stats Cache Location = "+runStatsFile());
      const unsigned int maxRunsToWrite = 10; // should eventually make a JO..
      unsigned int nRun(0);
      for ( auto iRun = m_processedRuns.rbegin();
            iRun != m_processedRuns.rend() && ++nRun <= maxRunsToWrite; ++iRun )
      {
        file << iRun->first  << std::endl;
        file << iRun->second << std::endl;
      }
      std::ostringstream mess;
      mess << "Successfully wrote " << nRun << " Run Statistics";
      cameraTool()->Append("TEXT",mess.str());
      info() << mess.str() << endmsg;
      file.close();
    }
    else
    {
      std::ostringstream mess;
      mess << "Problem writing cache file " << runStatsFile();
      cameraTool()->Append("TEXT",mess.str().c_str());
      OK = false;
    }
  }

  // Now the inactive HPD lists
  {
    std::ofstream file( inactiveHPDFile().c_str() );
    if ( file.is_open() )
    {
      cameraTool()->Append("TEXT","Inactive HPDs Cache Location = "+inactiveHPDFile());
      for ( const auto& i : m_inactiveHPDCache )
      {
        file << i.first  << std::endl;
        file << i.second << std::endl;
      }
      std::ostringstream mess;
      mess << "Successfully wrote " << m_inactiveHPDCache.size() << " cached Inactive HPD lists";
      cameraTool()->Append("TEXT",mess.str());
      info() << mess.str() << endmsg;
      file.close();
    }
    else
    {
      std::ostringstream mess;
      mess << "Problem writing cache file " << inactiveHPDFile();
      cameraTool()->Append("TEXT",mess.str().c_str());
      OK = false;
    }

  }

  return OK;
}

//=============================================================================

void OnlineSummary::readCachedConditions()
{

  bool OK = true;

  // First the HPD conditions
  try
  {
    std::ifstream file( cacheFile().c_str() );
    if ( file.is_open() )
    {
      cameraTool()->Append("TEXT","HPD Cache Location = "+cacheFile());
      std::string idS,cond,occS;
      while ( std::getline(file,idS)  &&
              std::getline(file,occS) &&
              std::getline(file,cond)  )
      {
        try
        {
          const LHCb::RichSmartID id( boost::lexical_cast<LHCb::RichSmartID::KeyType>(idS) );
          if ( id.isValid() )
          {
            const auto occ = boost::lexical_cast<double>(occS);
            m_hpdCondCache[id] = std::make_pair(occ,cond);
            std::ostringstream mess;
            mess << id << " Occ=" << occ << " XML=" << cond;
            _ri_verbo << mess.str() << endmsg;
            cameraTool()->Append("TEXT",mess.str());
          }
        }
        catch ( const std::exception & ex )
        {
          std::ostringstream mess;
          mess << "Problem decoding condition " << ex.what();
          cameraTool()->Append("TEXT",mess.str());
        }
      }
      file.close();
      std::ostringstream mess;
      mess << "Successfully read " << m_hpdCondCache.size() << " cached HPD Conditions";
      cameraTool()->Append("TEXT",mess.str());
      info() << mess.str() << endmsg;
    }
    else
    {
      std::ostringstream mess;
      mess << "Failed to open cache file " << cacheFile();
      cameraTool()->Append("TEXT",mess.str());
      warning() << mess.str() << endmsg;
      OK = false;
    }
  }
  catch ( const std::exception & ex )
  {
    std::ostringstream mess;
    mess << "Problem reading cache file " << ex.what();
    cameraTool()->Append("TEXT",mess.str());
    m_hpdCondCache.clear();
    OK = false;
  }

  // Then the run stats.
  try
  {
    std::ifstream file( runStatsFile().c_str() );
    if ( file.is_open() )
    {
      std::string runS, statsS;
      cameraTool()->Append("TEXT","Run Stats. Cache Location = "+runStatsFile());
      while ( std::getline(file,runS)   &&
              std::getline(file,statsS)  )
      {
        try
        {
          const auto run   = boost::lexical_cast<unsigned int>(runS);
          const auto stats = boost::lexical_cast<unsigned long long>(statsS);
          m_processedRuns[run] = stats;
          std::ostringstream mess;
          mess << "Run=" << run << " Events=" << stats;
          _ri_verbo << mess.str() << endmsg;
          cameraTool()->Append("TEXT",mess.str());
        }
        catch ( const std::exception & ex )
        {
          std::ostringstream mess;
          mess << "Problem decoding run statistics " << ex.what();
          cameraTool()->Append("TEXT",mess.str());
        }
      }
      file.close();
      std::ostringstream mess;
      mess << "Successfully read " << m_processedRuns.size() << " cached Run Statistics";
      cameraTool()->Append("TEXT",mess.str());
      info() << mess.str() << endmsg;
    }
    else
    {
      std::ostringstream mess;
      mess << "Failed to open cache file " << runStatsFile();
      cameraTool()->Append("TEXT",mess.str());
      warning() << mess.str() << endmsg;
      OK = false;
    }
  }
  catch ( const std::exception & ex )
  {
    std::ostringstream mess;
    mess << "Problem reading cache file " << ex.what();
    cameraTool()->Append("TEXT",mess.str());
    m_processedRuns.clear();
    OK = false;
  }

  // Now the inactive HPD lists.
  try
  {
    std::ifstream file( inactiveHPDFile().c_str() );
    if ( file.is_open() )
    {
      std::string det, inactivehpds;
      cameraTool()->Append("TEXT","Inactive HPDs Cache Location = "+inactiveHPDFile());
      while ( std::getline(file,det) && std::getline(file,inactivehpds) )
      {
        m_inactiveHPDCache[det] = inactivehpds;
        std::ostringstream mess;
        mess << "Det=" << det << " '" << inactivehpds << "'";
        _ri_verbo << mess.str() << endmsg;
        cameraTool()->Append("TEXT",mess.str());
      }
      file.close();
      std::ostringstream mess;
      mess << "Successfully read " << m_inactiveHPDCache.size() << " cached Inactive HPD lists";
      cameraTool()->Append("TEXT",mess.str());
      info() << mess.str() << endmsg;
    }
    else
    {
      std::ostringstream mess;
      mess << "Failed to open cache file " << inactiveHPDFile();
      cameraTool()->Append("TEXT",mess.str());
      warning() << mess.str() << endmsg;
      OK = false;
    }
  }
  catch ( const std::exception & ex )
  {
    std::ostringstream mess;
    mess << "Problem reading cache file " << ex.what();
    cameraTool()->Append("TEXT",mess.str());
    m_inactiveHPDCache.clear();
    OK = false;
  }

  // Finally send the camera message
  const auto level = ( OK ? ICameraTool::INFO : ICameraTool::ERROR );
  const auto mess = ( OK ?
                      "SUCCESSFULLY read cached data" :
                      "ERROR reading cached data" );
  cameraTool()->SendAndClearTS(level,m_Name,mess);

}

//=============================================================================

void OnlineSummary::printCanvas( const std::string& tag ) const
{

  const std::string imageType = "pdf";

  // Opening file ?
  if ( "[" == tag )
  {
    try
    {
      // If same run number, delete previous file if it exists.
      if ( !m_pdfFile.empty() && m_runNumberPdfFile == m_runNumber )
      { removeFile(m_pdfFile); }

      // Directory to save summaries to
      const boost::filesystem::path dir( summaryPath() );

      // File name
      const boost::filesystem::path filename( "Run-" +
                                              std::to_string(m_runNumber) +
                                              "." + imageType );
      const boost::filesystem::path full_path = dir/filename ;

      // If PDF exists, remove before remaking
      removeFile( full_path );

      // cache image file name
      m_pdfFile = full_path.string();
      // Run number for this file
      m_runNumberPdfFile = m_runNumber;

      // Make a new canvas
      deleteCanvas();

      // Open new PDF
      canvas()->Print( ( m_pdfFile + tag ).c_str(), imageType.c_str() );

      // set watermark string
      m_watermark = watermark();

    }
    catch ( const boost::filesystem::filesystem_error& expt )
    {
      std::ostringstream mess;
      mess << "Failed to create file " << m_pdfFile << " " << expt.what();
      error() << mess.str() << endmsg;
      cameraTool()->Append("TEXT",mess.str());
      deleteCanvas();
      m_pdfFile = "";
      m_runNumberPdfFile = 0;
    }

  }
  // Closing the file ?
  else if ( "]" == tag )
  {

    // Close the file
    canvas()->Print( ( m_pdfFile + tag ).c_str(), imageType.c_str() );

    // Set group write permissions
    //setPerms( m_pdfFile );

    // delete canvas
    deleteCanvas();

    // send a message
    std::ostringstream mess;
    mess << "Created  " << m_pdfFile;
    info() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());

  }
  else
  {

    // Add time/date watermark
    canvas()->cd();
    TText text;
    text.SetNDC();
    text.SetTextSize(0.01);
    text.SetTextColor(13);
    text.DrawText( 0.01, 0.01, m_watermark.c_str() );

    // Just print
    canvas()->Print( m_pdfFile.c_str(), imageType.c_str() );

  }

}

//=============================================================================

void OnlineSummary::draw( const HPDFitResult& hpd ) const
{
  using namespace std;
  const auto & res = hpd.fitResult;
  draw( hpd.imageHist, "zcol" );
  if ( res.OK() )
  {
    // Draw the fitted circle
    TEllipse elp( (float)res.col(),
                  (float)res.row(),
                  (float)res.radInPix(),
                  (float)res.radInPix() );
    elp.SetFillStyle(0);
    elp.SetLineWidth(2);
    elp.Draw();
    // Add the fitted parameters to the image
    TText text;
    text.SetNDC();
    text.SetTextSize(0.02);
    text.DrawText( 0.375, 0.525,
                   ("x = "+to_string(res.x())+" +- "+to_string(res.xErr())+" mm").c_str() );
    text.DrawText( 0.375, 0.500,
                   ("y = "+to_string(res.y())+" +- "+to_string(res.yErr())+" mm").c_str() );
    text.DrawText( 0.375, 0.475,
                   ("r = "+to_string(res.radInMM())+" +- "+to_string(res.radErrInMM())+" mm").c_str() );
    printCanvas();
  }
  else
  {
    printCanvas();
  }
}

//=============================================================================

void OnlineSummary::fixFilePermissions() const
{
  using namespace boost::filesystem;
  try
  {
    const path dir( m_xmlFilePath + "/RichCalibSummaries" );
    createDir(dir);
    recursive_directory_iterator end_itr;
    for ( recursive_directory_iterator i(dir); i != end_itr; ++i )
    { setPerms( i->path() ); }
  }
  catch ( const filesystem_error& expt )
  {
    std::ostringstream mess;
    mess << "Failed fixing permissions " << expt.what();
    warning() << mess.str() << endmsg;
    cameraTool()->Append("TEXT",mess.str());
  }
}

//=============================================================================

bool OnlineSummary::writeToDimSummaryFile( const std::string & mess ) const
{
  bool OK = true;
  // Open the summary file in append mode
  std::ofstream file( dimSummaryFile().c_str(), std::ios_base::app );
  if ( file.is_open() )
  {
    file << watermark() << " | " << mess << std::endl;
    file.close();
    //setPerms( dimSummaryFile() );
  }
  else
  {
    std::ostringstream m;
    m << "Problem writing DIM summary file " << dimSummaryFile();
    warning() << m.str() << endmsg;
    cameraTool()->Append("TEXT",m.str());
    OK = false;
  }
  return OK;
}

//=============================================================================

bool OnlineSummary::writeCalibVersionsToDimSummaryFile() const
{
  std::ostringstream mess;
  mess << "Run " << m_runNumber << " |"
       << " " << m_Rich1TaskName << " v" << getVersion(m_Rich1TaskName).second
       << " " << m_Rich2TaskName << " v" << getVersion(m_Rich2TaskName).second;
  return writeToDimSummaryFile( mess.str() );
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( OnlineSummary )

//=============================================================================
